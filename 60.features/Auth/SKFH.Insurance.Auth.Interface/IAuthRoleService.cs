﻿using SKFH.Insurance.IAuth.Models;
using System.Threading.Tasks;
using TPI.NetCore.WebAPI.Models;

namespace SKFH.Insurance.IAuth
{
    /// <summary>
    /// 角色功能權限設定 Interface
    /// </summary>
    public interface IAuthRoleService
    {
        /// <summary>
        /// 角色功能權限設定-修改頁-確認修改API
        /// </summary>
        /// <param name="req">角色功能權限設定 Req</param>
        /// <returns>True</returns>
        Task<bool> AuthRoleConfirmEdit(RolePermissionReq req);

        /// <summary>
        /// 角色功能權限設定-查詢頁-刪除API
        /// </summary>
        /// <param name="req">角色功能權限設定 Req</param>
        /// <returns>True</returns>
        Task<bool> AuthRoleDelete(RoleReq req);

        /// <summary>
        /// 依帳號取得授權的 menu 與 button
        /// </summary>
        /// <param name="req">使用者維護 Req</param>
        /// <returns>Menu Resp</returns>
        Task<MenuResp> GetMenuButtons(AuthReq req);

        /// <summary>
        /// 角色功能權限設定-修改頁-查詢修改API
        /// </summary>
        /// <param name="req">角色功能權限設定 Req</param>
        /// <returns>角色功能權限設定 Resp</returns>
        RoleMenuResp AuthRoleEdit(RoleReq req);
    }
}
