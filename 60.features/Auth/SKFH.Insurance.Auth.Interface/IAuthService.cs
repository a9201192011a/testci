﻿using SKFH.Insurance.IAuth.Models;
using System.Threading.Tasks;
using TPI.NetCore.WebAPI.Models;

namespace SKFH.Insurance.IAuth
{
    /// <summary>
    /// AuthService Interface
    /// </summary>
    public interface IAuthService
    {
        /// <summary>
        /// 使用者維護-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">使用者維護 Req</param>
        /// <returns>使用者維護 Resp</returns>
        DataTableQueryResp<UserSearchResp> UserSearch(UserSearchReq req);

        /// <summary>
        /// 使用者維護-查詢頁-停用 API
        /// </summary>
        /// <param name="req">使用者維護-查詢頁-啟用/停用API Req</param>
        /// <returns>True</returns>
        Task<bool> UserDisabled(AuthReq req);

        /// <summary>
        /// 使用者維護-查詢頁-刪除 API
        /// </summary>
        /// <param name="req">使用者維護-查詢頁-刪除API Req</param>
        /// <returns>True</returns>
        Task<bool> UserDelete(AuthReq req);

        /// <summary>
        /// 使用者維護-新增頁-新增 API
        /// </summary>
        /// <param name="req">使用者維護-查詢頁-新增API Req</param>
        /// <returns>True</returns>
        Task<bool> UserInsert(InsertReq req);

        /// <summary>
        /// 使用者維護-修改頁-查詢修改 API
        /// </summary>
        /// <param name="req"> 使用者維護-修改頁-查詢修改API Req</param>
        /// <returns>使用者維護-修改頁-查詢修改API Resp</returns>
        EditResp UserEdit(AuthReq req);

        /// <summary>
        /// 使用者維護-修改頁-確認修改 API
        /// </summary>
        /// <param name="req">使用者維護-修改頁-確認修改 API Req</param>
        /// <returns>True</returns>
        Task<bool> UserConfirmEdit(InsertReq req);

        /// <summary>
        /// 角色維護-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">角色維護-查詢頁-查詢資料 API Req</param>
        /// <returns>角色維護-查詢頁-查詢資料 API Resp</returns>
        DataTableQueryResp<RoleSearchResp> RoleSearch(RoleSearchReq req);

        /// <summary>
        /// 角色維護-查詢頁-停用 API
        /// </summary>
        /// <param name="req">角色維護 Req</param>
        /// <returns>True</returns>
        Task<bool> RoleDisabled(RoleDisabledReq req);

        /// <summary>
        /// 使用者維護-寄發Email API
        /// </summary>
        /// <param name="req">使用者維護-寄發Email Req</param>
        /// <returns>True</returns>
        Task<bool> SendEmail(SendEmailReq req);

        /// <summary>
        /// 使用者維護-使用者重設密碼頁-查詢資料 API
        /// </summary>
        /// <param name="req">使用者維護-使用者重設密碼頁-查詢資料 API Req</param>
        /// <returns>使用者維護-使用者重設密碼頁-查詢資料 API Resp</returns>
        DataTableQueryResp<UserResetDwpSearchResp> UsersResetDwpSearch(UserResetDwpSearchReq req);

        /// <summary>
        /// 使用者維護-使用者重設密碼頁-密碼重置 API
        /// </summary>
        /// <param name="req">使用者維護-使用者重設密碼頁-密碼重置API Req</param>
        /// <returns>True</returns>
        Task<bool> UserResetDwpMultiEdit(UserResetDwdEditReq req);

        /// <summary>
        /// 角色維護-查詢頁-刪除 API
        /// </summary>
        /// <param name="req">角色維護-查詢頁-刪除 API Req</param>
        /// <returns>True</returns>
        Task<bool> RoleDelete(RoleReq req);

        /// <summary>
        /// 角色維護-修改頁-查詢修改 API
        /// </summary>
        /// <param name="req">角色功能權限設定-修改頁-查詢修改 API Req</param>
        /// <returns>角色維護-修改頁-查詢修改 API Resp</returns>
        Task<RoleEditResp> RoleEdit(RoleReq req);

        /// <summary>
        /// 角色維護-新增頁-新增 API
        /// </summary>
        /// <param name="req">角色維護-新增頁-新增 API Req</param>
        /// <returns>True</returns>
        Task<bool> RoleInsert(RoleInsertReq req);

        /// <summary>
        /// 角色維護-修改頁-確認修改 API
        /// </summary>
        /// <param name="req">角色維護-修改頁-確認修改 API Req</param>
        /// <returns>True</returns>
        Task<bool> RoleConfirmEdit(RoleInsertReq req);

    }
}
