﻿using System.Threading.Tasks;
using SKFH.Insurance.Auth.Entity.Models;
using SKFH.Insurance.IAuth.Models;

namespace SKFH.Insurance.IAuth
{
    /// <summary>
    /// 使用者登入登出 Service Interface
    /// </summary>
    public interface IUserLoginLogoutService
    {
        /// <summary>
        /// 使用者帳號密碼登入
        /// </summary>
        /// <param name="req">Login Req</param>
        /// <returns>Login Resp</returns>
        Task<LoginResp> LoginAsync(LoginReq req);

        /// <summary>
        /// 使用者登入登出-使用者重設密碼頁-確認修改 API
        /// </summary>
        /// <param name="req">使用者登入登出-使用者登入頁-重設密碼 Req</param>
        /// <returns>True</returns>
        Task<bool> ResetDwpEdit(ResetDwpEditReq req);

        /// <summary>
        /// 使用者登入登出-使用者登出 API
        /// </summary>
        /// <param name="req">Logout Req</param>
        /// <param name="logoutRequest">SSO LogoutRequest</param>
        /// <returns>True</returns>
        Task<bool>Logout(LogoutReq req,string logoutRequest = null);

        /// <summary>
        /// 依使用者帳號 , 取得使用者資料
        /// </summary>
        /// <param name="account">帳號</param>
        /// <returns>使用者資料</returns>
        Task<SysUser> GetSysUserAsync(string account);

        /// <summary>
        /// 更新該帳號的使用者資料
        /// </summary>
        /// <param name="user">使用者資料</param>
        /// <returns>無回傳值</returns>
        Task UpdateSysUserAsync(SysUser user);

        /// <summary>
        /// 更新 Token 到期時間
        /// </summary>
        /// <param name="token">登入授權</param>
        /// <returns>無回傳值</returns>
        Task UpdateAccessTokenExpiresOnAsync(AccessToken token);

        /// <summary>
        /// 新增 使用者資料
        /// </summary>
        /// <param name="user">使用者資料</param>
        /// <returns>無回傳值</returns>

        Task InsertSysUser(SysUser user);

        /// <summary>
        /// 取得密碼加密結果(帳號+密碼)
        /// </summary>
        /// <param name="account">帳號</param>
        /// <param name="dwp">密碼</param>
        /// <returns>加密結果</returns>
        string GetEncryptedDwp(string account, string dwp);

        /// <summary>
        /// 登入密碼驗證
        /// </summary>
        /// <param name="dwp">密碼</param>
        /// <returns>驗證結果</returns>
        bool ValidDwpCheck(string dwp);

        /// <summary>
        /// 產出 Cas Ticket
        /// </summary>
        /// <param name="account">帳號</param>
        /// <param name="ticket">Server Ticket</param>
        /// <returns>Ticket</returns>
        string GenCasTicket(string account, string ticket);

        /// <summary>
        /// 抓取 request 的 ticket 進行驗證
        /// </summary>
        /// <param name="encryptTicket">SSO Ticket</param>
        /// <returns>Cas 資訊</returns>
        CasInfo ValidCasTicket(string encryptTicket);

        /// <summary>
        /// 前端 SSO 登入
        /// </summary>
        /// <param name="casInfo">Cas 資訊</param>
        /// <param name="ticket">SSO Ticket</param>
        /// <returns>Login Resp</returns>
        Task<LoginResp> SSOLoginAsyn(CasInfo casInfo, string ticket = null);

        /// <summary>
        /// 取得登入資訊
        /// </summary>
        /// <param name="user">使用者資料</param>
        /// <param name="result">Login Resp</param>
        /// <param name="ssoLogin">User Prop Types</param>
        /// <param name="casSt">SSO 登入 ST 資訊</param>
        /// <returns>Login Resp</returns>
        Task<LoginResp> GetLoginInfo(SysUser user, LoginResp result,string ssoLogin,string casSt);
    }
}
