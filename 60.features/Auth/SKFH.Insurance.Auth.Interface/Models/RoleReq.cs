﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 角色功能權限設定-修改頁-查詢修改 API Req
    /// 角色功能權限設定-查詢頁-刪除 API Req
    /// 角色維護-修改頁-查詢修改 API Req
    /// 角色維護-查詢頁-刪除 API Req
    /// </summary>
    public class RoleReq
    {
        /// <summary>
        /// 角色代號
        /// </summary>
        [BindRequired]
        public string RoleId { get; set; }
    }
}