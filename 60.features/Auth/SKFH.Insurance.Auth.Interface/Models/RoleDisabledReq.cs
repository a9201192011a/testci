﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 角色維護 Req
    /// </summary>
    public class RoleDisabledReq
    {
        /// <summary>
        /// 角色代號
        /// </summary>
        [BindRequired]
        public string RoleId { get; set; }

        /// <summary>
        /// 角色狀態：0 停用 1 啟用
        /// </summary>
        [BindRequired]
        public string RoleStatus { get; set; }
    }
}