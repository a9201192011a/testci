﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using TPI.NetCore.WebAPI.Models;

namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 角色功能權限設定-查詢頁-查詢資料 API Req
    /// </summary>
    public class AuthRoleSearchReq : DataTableQueryReq
    {
        /// <summary>
        /// 角色代號
        /// </summary>
        [BindRequired]
        public string RoleId { get; set; }

        /// <summary>
        /// 角色名稱
        /// </summary>
        public string Name { get; set; }
    }
}
