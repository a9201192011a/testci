﻿namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// Logout Req
    /// </summary>
    public class LogoutReq
    {
        /// <summary>
        /// 使用者帳號
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// Token
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// SSO 登入 ST 資訊
        /// </summary>
        public string CasSt { get; set; }
    }
}