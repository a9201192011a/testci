﻿
namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 使用者維護-查詢頁-啟用/停用API Req
    /// 使用者維護-查詢頁-刪除API Req
    /// 取得使用者權限-Menu與Button-API Req
    /// 使用者維護-修改頁-查詢修改API Req
    /// </summary>
    public class AuthReq
    {
        /// <summary>
        /// 使用者帳號
        /// </summary>
        public string Account { get; set; }
    }
}