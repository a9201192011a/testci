﻿using System.ComponentModel.DataAnnotations;

namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// Login Req
    /// </summary>
    public class LoginReq
    {
        /// <summary>
        /// 使用者帳號
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 使用者密碼
        /// </summary>

        public string Dwp { get; set; }
    }
}