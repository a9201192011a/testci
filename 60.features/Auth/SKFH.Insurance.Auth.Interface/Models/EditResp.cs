﻿namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 使用者維護-修改頁-查詢修改API Resp
    /// </summary>
    public class EditResp
    {
        /// <summary>
        /// 使用者帳號
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 使用者姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 使用者信箱
        /// </summary>
        public string Email { get; set; }
    }
}