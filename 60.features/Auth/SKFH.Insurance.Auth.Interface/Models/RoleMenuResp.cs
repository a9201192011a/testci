﻿using System;
using System.Collections.Generic;

namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 角色功能權限資訊
    /// </summary>
    public class RoleMenuResp
    {
        /// <summary>
        /// 標題
        /// </summary>
        public IEnumerable<RoleTitleInfo> Title { get; set; }

        /// <summary>
        /// 主 Menu
        /// </summary>
        public IEnumerable<RoleModuleInfo> Menus { get; set; }

    }

    /// <summary>
    /// 標題資料
    /// </summary>
    public class RoleTitleInfo
    {
        /// <summary>
        /// 標題代號
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 標題名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 標題敘述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 標題順序
        /// </summary>
        public int? Sort { get; set; }
    }

    /// <summary>
    /// 主 Menu (模組功能)
    /// </summary>
    public class RoleModuleInfo
    {
        /// <summary>
        /// 功能代號
        /// </summary>
        public string ModuleId { get; set; }

        /// <summary>
        /// 功能名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 功能位址
        /// </summary>
        public string ModuleUrl { get; set; }

        /// <summary>
        /// 擴充
        /// </summary>
        public bool Expanded { get; set; }

        /// <summary>
        /// 圖示
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 功能化
        /// </summary>
        public bool IsModule { get; set; }

        /// <summary>
        /// 按鈕
        /// </summary>
        public Array Buttons { get; set; }

        /// <summary>
        /// 次頁面
        /// </summary>
        public IEnumerable<RoleFunctionInfo> Children { get; set; }

    }


    /// <summary>
    /// 次 Menu (頁面功能)
    /// </summary>
    public class RoleFunctionInfo
    {
        /// <summary>
        /// 次功能名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 次功能位址
        /// </summary>
        public string FuncUrl { get; set; }

        /// <summary>
        /// 次功能代碼
        /// </summary>
        public string FuncId { get; set; }

        /// <summary>
        /// 支持
        /// </summary>
        public bool IsSupport { get; set; }

        /// <summary>
        /// 功能化
        /// </summary>
        public bool IsModule { get; set; }

        /// <summary>
        /// 按鈕資訊
        /// </summary>
        public IEnumerable<RoleButtonInfo> Buttons { get; set; }
    }

    /// <summary>
    /// 頁面功能按鈕資訊
    /// </summary>
    public class RoleButtonInfo
    {
        /// <summary>
        /// 按鈕位址
        /// </summary>
        public string ButtonUrl { get; set; }

        /// <summary>
        /// 按鈕編碼
        /// </summary>
        public string ButtonnCode { get; set; }

        /// <summary>
        /// 確認
        /// </summary>
        public bool Check { get; set; }

        /// <summary>
        /// 執行
        /// </summary>
        public bool Enable { get; set; }
    }
}
