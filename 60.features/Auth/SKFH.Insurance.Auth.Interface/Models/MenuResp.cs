﻿using System.Collections.Generic;

namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// Menu Resp
    /// </summary>
    public class MenuResp
    {
        /// <summary>
        /// 主 Menu
        /// </summary>
        public IEnumerable<ModuleInfo> Menus { get; set; }

    }

    /// <summary>
    /// 主 Menu (模組功能)
    /// </summary>
    public class ModuleInfo
    {
        /// <summary>
        /// 功能代號
        /// </summary>
        public string ModuleId { get; set; }

        /// <summary>
        /// 功能名字
        /// </summary>
        public string ModuleName { get; set; }

        /// <summary>
        /// 功能位址
        /// </summary>
        public string ModuleUrl { get; set; }

        /// <summary>
        /// 擴充
        /// </summary>
        public bool Expanded { get; set; }

        /// <summary>
        /// 圖示
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 次 Menu
        /// </summary>
        public IEnumerable<FunctionInfo> Functions { get; set; }

    }


    /// <summary>
    /// 次 Menu (頁面功能)
    /// </summary>
    public class FunctionInfo
    {
        /// <summary>
        /// 次功能名字
        /// </summary>
        public string FuncName { get; set; }

        /// <summary>
        /// 次功能位址
        /// </summary>
        public string FuncUrl { get; set; }

        /// <summary>
        /// 次功能代碼
        /// </summary>
        public string FuncId { get; set; }

        /// <summary>
        /// 支持
        /// </summary>
        public bool IsSupport { get; set; }

        /// <summary>
        /// 頁面功能按鈕
        /// </summary>
        public IEnumerable<ButtonInfo> Buttons { get; set; }
    }

    /// <summary>
    /// 頁面功能按鈕資訊
    /// </summary>
    public class ButtonInfo
    {
        /// <summary>
        /// 按鈕位址
        /// </summary>
        public string ButtonUrl { get; set; }

        /// <summary>
        /// 按鈕編碼
        /// </summary>
        public string ButtonnCode { get; set; }

        /// <summary>
        /// 執行
        /// </summary>
        public bool Enable { get; set; }
    }
}
