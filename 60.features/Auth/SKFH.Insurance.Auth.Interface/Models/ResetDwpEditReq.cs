﻿namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 使用者登入登出-使用者登入頁-重設密碼 Req
    /// </summary>
    public class ResetDwpEditReq
    {
        /// <summary>
        /// 使用者帳號
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 輸入新密碼
        /// </summary>
        public string NewDwp { get; set; }

        /// <summary>
        /// 再次輸入新密碼
        /// </summary>
        public string ConfirmNewDwp { get; set; }

    }
}