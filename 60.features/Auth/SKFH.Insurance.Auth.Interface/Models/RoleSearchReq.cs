﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using TPI.NetCore.WebAPI.Models;

namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 角色維護-查詢頁-查詢資料 API Req
    /// </summary>
    public class RoleSearchReq : DataTableQueryReq
    {
        /// <summary>
        /// 角色代號
        /// </summary>
        [BindRequired]
        public string RoleId { get; set; }

        /// <summary>
        /// 角色名稱
        /// </summary>
        public string Name { get; set; }
    }
}
