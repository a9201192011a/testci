﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;

namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 角色維護-新增頁-新增 API Req
    /// 角色維護-修改頁-確認修改 API Req
    /// </summary>
    public class RoleInsertReq
    {

        /// <summary>
        /// 角色代號
        /// </summary>
        [BindRequired]
        public string RoleId { get; set; }

        /// <summary>
        /// 角色名稱
        /// </summary>
        [BindRequired]
        public string RoleName { get; set; }

        /// <summary>
        /// 角色狀態
        /// </summary>
        [BindRequired]
        public string RoleStatus { get; set; }

        /// <summary>
        /// 角色維護人員陣列
        /// </summary>
        [BindRequired]
        public List<InsertRoles> Roles { get; set; }
    }

    /// <summary>
    /// 角色維護人員
    /// </summary>
    public class InsertRoles
    {
        /// <summary>
        /// 角色維護人員
        /// </summary>
        [BindRequired]
        public string Account { get; set; }
    }
}