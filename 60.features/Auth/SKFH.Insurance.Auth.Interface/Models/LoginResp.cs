﻿namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// Login Resp
    /// </summary>
    public class LoginResp
    {
        /// <summary>
        /// 使用者帳號
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 使用者姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 使用者上次登入時間
        /// </summary>
        public string PreviousLoginOn { get; set; }

        /// <summary>
        /// Token (保代後台 API , Heahder Bear Token 使用)
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// 是否需要進入重設密碼頁重設密碼
        /// </summary>
        public bool NeedResetDwp { get; set; }
    }
}