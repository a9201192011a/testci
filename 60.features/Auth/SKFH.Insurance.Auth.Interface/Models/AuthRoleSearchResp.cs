﻿namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 角色功能權限設定-查詢頁-查詢資料 API Resp
    /// </summary>
    public class AuthRoleSearchResp
    {
        /// <summary>
        /// 角色代號
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// 角色名稱
        /// </summary>
        public string Name { get; set; }
    }
}
