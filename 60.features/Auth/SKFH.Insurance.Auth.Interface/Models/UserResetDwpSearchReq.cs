﻿using TPI.NetCore.WebAPI.Models;

namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 使用者維護-使用者重設密碼頁-查詢資料 API Req
    /// </summary>
    public class UserResetDwpSearchReq : DataTableQueryReq
    {
        /// <summary>
        /// 使用者帳號
        /// </summary>
        public string Account { get; set; }
        /// <summary>
        /// 使用者姓名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 使用者 Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 系統最後變更密碼日期-起日
        /// </summary>
        public string SendSysDwpStartDate { get; set; }

        /// <summary>
        /// 系統最後變更密碼日期-迄日
        /// </summary>
        public string SendSysDwpEndDate { get; set; }
    }
}
