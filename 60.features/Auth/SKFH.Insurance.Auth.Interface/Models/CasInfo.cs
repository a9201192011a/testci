﻿namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// Cas 資訊
    /// </summary>
    public class CasInfo
    {
        /// <summary>
        /// 帳號
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// SSO 登入 ST 資訊
        /// </summary>
        public string CasSt { get; set; }
    }
}