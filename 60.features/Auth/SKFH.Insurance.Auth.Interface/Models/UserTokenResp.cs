namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    ///  使用者 token 回應
    /// </summary>
    public class UserTokenResp
    {
        /// <summary>
        /// JWT access token
        /// </summary>
        public string Token { get; set; }
    }
}