﻿namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 角色維護-查詢頁-查詢資料 API Resp
    /// </summary>
    public class RoleSearchResp
    {
        /// <summary>
        /// 角色代號
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// 角色名稱
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 狀態：啟用、停用
        /// </summary>
        public string RoleStatusStr { get; set; }

        /// <summary>
        /// 狀態代號
        /// </summary>
        public string RoleStatus { get; set; }

    }
}
