namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// SSO Req
    /// </summary>
    public class SSOReq
    {
        /// <summary>
        /// SSO Ticket
        /// </summary>
        public string Ticket { get; set; }
    }
}