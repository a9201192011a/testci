﻿namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 使用者維護-查詢頁-查詢資料 API Resp
    /// </summary>
    public class UserSearchResp
    {
        /// <summary>
        /// 使用者帳號
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 使用者姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 使用者 Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 使用者狀態(SysParam)：1 啟用 0 停用 2 鎖定 3 更改密碼
        /// </summary>
        public string AccountStatusStr { get; set; }
    }
}
