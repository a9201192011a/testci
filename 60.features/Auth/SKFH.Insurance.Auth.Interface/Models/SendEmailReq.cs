﻿namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 使用者維護-寄發Email Req
    /// </summary>
    public class SendEmailReq
    {
        /// <summary>
        /// 使用者帳號
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 郵件套件設定
        /// </summary>
        public bool IsMailKit { get; set; }
    }
}