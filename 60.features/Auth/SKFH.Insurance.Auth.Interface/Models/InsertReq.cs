﻿namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 使用者維護-查詢頁-新增API Req
    /// 使用者維護-修改頁-確認修改API req
    /// </summary>
    public class InsertReq
    {
        /// <summary>
        /// 使用者帳號
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 使用者姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 使用者 Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 使用者密碼
        /// </summary>
        public string Dwp { get; set; }

    }
}