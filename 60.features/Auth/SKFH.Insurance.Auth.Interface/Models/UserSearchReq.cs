﻿using TPI.NetCore.WebAPI.Models;

namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 使用者維護-查詢頁-查詢資料 API Req
    /// </summary>
    public class UserSearchReq : DataTableQueryReq
    {
        /// <summary>
        /// 使用者帳號
        /// </summary>
        public string? Account { get; set; }

        /// <summary>
        /// 使用者姓名
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// 使用者 Email
        /// </summary>
        public string? Email { get; set; }
    }
}
