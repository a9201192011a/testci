﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;

namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 角色功能權限設定 Req
    /// </summary>
    public class RolePermissionReq
    {
        /// <summary>
        /// 角色代號
        /// </summary>
        [BindRequired]
        public string RoleId { get; set; }

        /// <summary>
        /// 主 Menu (頁面功能) 清單
        /// </summary>
        public IEnumerable<RolePermission> Menus { get; set; }
    }

    /// <summary>
    /// 主 Menu (頁面功能)
    /// </summary>
    public class RolePermission
    {
        /// <summary>
        /// 功能代碼
        /// </summary>
        public string FuncId { get; set; }

        /// <summary>
        /// 頁面功能按鈕資訊清單
        /// </summary>
        public IEnumerable<RoleButtonInfo> Buttons { get; set; }
    }

}
