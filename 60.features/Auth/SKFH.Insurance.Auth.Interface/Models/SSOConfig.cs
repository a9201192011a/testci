namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// SSO 設定
    /// </summary>
    public class SSOConfig
    {
        /// <summary>
        /// SSO URL
        /// </summary>
        public string SSOurl { get; set; }

        /// <summary>
        /// APP URL
        /// </summary>
        public string APPurl { get; set; }

        /// <summary>
        /// 保代前端 URL
        /// </summary>
        public string FrontEndUrl { get; set; }
    }
}