﻿namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 使用者維護-使用者重設密碼頁-密碼重置API Req
    /// </summary>
    public class UserResetDwdEditReq
    {
        /// <summary>
        /// 重設密碼
        /// </summary>
        public string ResetDwp { get; set; }

        /// <summary>
        /// 使用者帳號 CheckBox Array
        /// </summary>
        public string[] Accounts { get; set; }
    }
}