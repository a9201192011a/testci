﻿using System.Collections.Generic;

namespace SKFH.Insurance.IAuth.Models
{
    /// <summary>
    /// 角色維護-修改頁-查詢修改 API Resp
    /// </summary>
    public class RoleEditResp
    {
        /// <summary>
        /// 角色代號
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// 角色名稱
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        /// 角色狀態
        /// </summary>
        public string RoleStatus { get; set; }

        /// <summary>
        /// 角色內使用者
        /// </summary>
        public List<Roles> Roles { get; set; }
    }

    /// <summary>
    /// 使用者資訊
    /// </summary>
    public class Roles
    {
        /// <summary>
        /// 角色帳號
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 角色名稱
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 角色信箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 角色狀態
        /// </summary>
        public string AccountStatusStr { get; set; }

    }
}