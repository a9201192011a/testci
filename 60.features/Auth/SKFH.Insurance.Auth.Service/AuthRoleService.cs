﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TPI.NetCore;
using TPI.NetCore.Extensions;
using TPI.NetCore.WebAPI.Models;
using System.Collections.Generic;
using TPI.NetCore.Models;
using TPI.NetCore.WebContext;
using SKFH.Insurance.IAuth;
using coreLibs.DateAccess;
using Autofac;
using SKFH.Insurance.Company.Entity.Repository;
using SKFH.Insurance.IAuth.Models;
using SKFH.Insurance.Auth.Entity.Models;
using InsureBrick.Modules.Common.Interface;
using SKFH.Insurance.Common.Entity.Repository;

namespace InsureBrick.Modules.Auth.Service
{
    /// <summary>
    /// 角色功能權限設定
    /// </summary>
    public class AuthRoleService : IAuthRoleService
    {
        #region Initialization

        private readonly IUnitOfWork unitOfWork;
        private readonly ICommonService commonService;
        private readonly ISysUserRepository sysUserRepo;
        private readonly ISysRoleRepository sysRoleRepo;
        private readonly ISysRoleListRepository sysRoleListRepo;
        private readonly ISysRolePermissionRepository sysRolePermissionRepo;
        private readonly ISysFunctionRepository sysFunctionRepo;
        private readonly ISysFnbuttonMapRepository sysFnbuttonMap;
        private readonly IAccessTokenRepository accessTokenRepo;

        public AuthRoleService(
            IComponentContext componentContext,
            ICommonService commonService,
            ISysUserRepository sysUserRepo,
            ISysRoleRepository sysRoleRepo,
            ISysRoleListRepository sysRoleListRepo,
            ISysRolePermissionRepository sysRolePermissionRepo,
            ISysFunctionRepository sysFunctionRepo,
            ISysFnbuttonMapRepository sysFnbuttonMap,
            IAccessTokenRepository accessTokenRepo
        )
        {
            this.unitOfWork = componentContext.ResolveNamed<IUnitOfWork>("Auth");
            this.commonService = commonService;
            this.sysUserRepo = sysUserRepo;
            this.sysRoleRepo = sysRoleRepo;
            this.sysRoleListRepo = sysRoleListRepo;
            this.sysRolePermissionRepo = sysRolePermissionRepo;
            this.sysFunctionRepo = sysFunctionRepo;
            this.sysFnbuttonMap = sysFnbuttonMap;
            this.accessTokenRepo = accessTokenRepo;
        }

        #endregion Initialization

        /// <summary>
        /// 依帳號取得授權的 menu 與 button
        /// </summary>
        /// <param name="req">使用者維護 Req</param>
        /// <returns>Menu Resp</returns>
        public async Task<MenuResp> GetMenuButtons(AuthReq req)
        {
            //查詢帳號(啟用)
            var sysUser = this.sysUserRepo.Queryable()
                .Where(o => o.Account == req.Account)
                .FirstOrDefault();

            if (sysUser == null) throw new BusinessException($"無此帳號。");

            if (WebContext.SSOLogin == UserPropTypes.SSOLogin) // SSO 登入
            {
                // 使用者狀態非啟用、更改密碼 ,顯示提示訊息
                if (sysUser.AccountStatus != AccountStatus.Enable && sysUser.AccountStatus != AccountStatus.ResetDwp)
                    throw new BusinessException($"帳號狀態為鎖定或停用。");
            }
            else // 保代登入
            {
                // 使用者狀態非啟用 ,顯示提示訊息
                if (sysUser.AccountStatus != AccountStatus.Enable)
                    throw new BusinessException($"無此帳號或帳號未啟用。");
            }


            //查詢人員角色代號 , 多個角色
            var userRoles = this.sysRoleListRepo.Queryable().Where(o => o.Account == req.Account).Select(o => o.RoleId);

            //查詢角色維護(啟用),多個角色
            var roles = this.sysRoleRepo.Queryable().Where(o => userRoles.Contains(o.RoleId) && o.RoleStatus == "1").Select(o => o.RoleId);

            if (!roles.Any())
            {
                throw new BusinessException($"此帳號無綁定任何角色。");
            }

            //查詢多個角色擁有的功能頁按鈕權限
            var permissions = this.sysRolePermissionRepo.Queryable().Where(o => roles.Contains(o.RoleId))
                .Select(o => new { o.FunctionCode, o.ButtonCode })
                .Distinct();

            //主 Menu (啟用 , 保代-F)
            var mainMenus = this.sysFunctionRepo.Queryable()
                .Where(o => o.Fnstatus == true &&
                o.Fntype == FNType.F &&
                string.IsNullOrEmpty(o.Fnpid) &&
                o.FunctionCode != FunctionCodeType.F1V00) // 非登入登出頁
                .OrderBy(o => o.SortNum)
                .ToList();

            Dictionary<string, List<string>> functionNameList = new Dictionary<string, List<string>>();
            var menus = new List<ModuleInfo>();
            foreach (var main in mainMenus)
            {
                var mainMenu = new ModuleInfo();
                mainMenu.ModuleId = main.FunctionCode;
                mainMenu.ModuleName = main.Name;
                mainMenu.ModuleUrl = main.Fnlink;
                mainMenu.Expanded = false;
                mainMenu.Icon = "fas fa-list";

                //次 Menu (啟用 , 保代-F)
                var subMenus = this.sysFunctionRepo.Queryable()
                    .Where(o => o.Fnstatus == true &&
                    o.Fntype == FNType.F &&
                    !string.IsNullOrEmpty(o.Fnpid) && //非父層
                    o.Fnpid == main.FunctionCode && // 其 父層 FunctionCode
                    o.FunctionCode != FunctionCodeType.F1V01) // 非登入登出頁
                    .OrderBy(o => o.FunctionCode)
                    .ToList();

                var functions = new List<FunctionInfo>();
                foreach (var sub in subMenus)
                {
                    var subMenu = new FunctionInfo();
                    subMenu.FuncName = sub.Name;
                    subMenu.FuncUrl = sub.Fnlink;
                    subMenu.FuncId = sub.FunctionCode;
                    subMenu.IsSupport = true;

                    //角色功能按鈕
                    var permissionButtonCodes = permissions.Where(o => o.FunctionCode == sub.FunctionCode).Select(o => o.ButtonCode);

                    //頁面功能按鈕
                    var fucButtons = this.sysFnbuttonMap.Queryable().Where(o => o.FunctionCode == sub.FunctionCode).ToList();
                    var buttons = new List<ButtonInfo>();
                    var buttonNameList = new List<string>();
                    foreach (var fbuton in fucButtons)
                    {
                        var button = new ButtonInfo();
                        button.ButtonUrl = fbuton.Url;
                        button.ButtonnCode = fbuton.ButtonCode;
                        button.Enable = permissionButtonCodes.Contains(fbuton.ButtonCode);

                        buttons.Add(button);

                        if (button.Enable)
                            buttonNameList.Add(fbuton.ButtonCode);
                    }

                    subMenu.Buttons = buttons;
                    if (subMenu.Buttons.Any(o => o.Enable))  // 有 button 才加入至功能
                    {
                        functions.Add(subMenu);
                        functionNameList.Add(sub.FunctionCode, buttonNameList);
                    }
                }

                mainMenu.Functions = functions;
                if (mainMenu.Functions.Any())  // 有 fucntion 才加入至功能
                {
                    menus.Add(mainMenu);
                }

            }

            if (!menus.Any())
            {
                throw new BusinessException($"此帳號所屬角色未設定功能，請至角色功能權限設定設定功能。");
            }

            var result = new MenuResp();
            result.Menus = menus;


            if (req.Account == WebContext.Account)
            {
                string tokenGid = WebContext.TokenGid;
                var entity = accessTokenRepo.Queryable().FirstOrDefault(x => x.Account == req.Account && x.TokenGid.ToString() == tokenGid);
                if (entity != null)
                {
                    entity.RoleButton = functionNameList.MinifyJson();
                    this.accessTokenRepo.Update(entity);
                    await this.unitOfWork.SaveChangesAsync();
                }
            }
            return result;
        }


        /// <summary>
        /// 角色功能權限設定-修改頁-查詢修改API
        /// </summary>
        /// <param name="req">角色功能權限設定 Req</param>
        /// <returns>角色功能權限設定 Resp</returns>
        public RoleMenuResp AuthRoleEdit(RoleReq req)
        {
            //查詢角色(啟用/停用)
            var role = this.sysRoleRepo.Queryable().Where(o => o.RoleId == req.RoleId).FirstOrDefault();

            if (role == null) throw new BusinessException($"無此角色。");

            //取得按鈕參數資料
            var butttonTitles = this.commonService.SearchSysParamByGroupId(GroupIdType.ButtonCode).OrderBy(o => o.Sort)
                .ToList()
                .Select(o => new RoleTitleInfo
                {
                    Id = o.ItemId,
                    Name = o.ItemValue,
                    Description = o.ItemValue,
                    Sort = o.Sort
                });

            //查詢角色擁有的功能頁按鈕權限
            var permissions = this.sysRolePermissionRepo.Queryable().Where(o => o.RoleId == role.RoleId)
                .Select(o => new { o.FunctionCode, o.ButtonCode })
                .Distinct();

            //主 Menu (啟用 , 保代-F)
            var mainMenus = this.sysFunctionRepo.Queryable()
                .Where(o => o.Fnstatus == true &&
                o.Fntype == FNType.F &&
                string.IsNullOrEmpty(o.Fnpid) &&
                o.FunctionCode != FunctionCodeType.F1V00)
                .OrderBy(o => o.SortNum)
                .ToList();

            var menus = new List<RoleModuleInfo>();
            foreach (var main in mainMenus)
            {
                var mainMenu = new RoleModuleInfo();
                mainMenu.ModuleId = main.FunctionCode;
                mainMenu.Name = main.Name;
                mainMenu.ModuleUrl = main.Fnlink;
                mainMenu.Expanded = false; //固定值
                mainMenu.Icon = "fas fa-list";
                mainMenu.Buttons = new object[] { }; //空陣列
                mainMenu.IsModule = true; //固定值

                //次 Menu (啟用 , 保代-F)
                var subMenus = this.sysFunctionRepo.Queryable()
                    .Where(o => o.Fnstatus == true &&
                    o.Fntype == FNType.F &&
                    !string.IsNullOrEmpty(o.Fnpid) && //非父層
                    o.Fnpid == main.FunctionCode && // 其 父層 FunctionCode
                    o.FunctionCode != FunctionCodeType.F1V01) // 非登入登出頁
                    .OrderBy(o => o.FunctionCode)
                    .ToList();

                var functions = new List<RoleFunctionInfo>();
                foreach (var sub in subMenus)
                {
                    var subMenu = new RoleFunctionInfo();
                    subMenu.Name = sub.Name;
                    subMenu.FuncUrl = sub.Fnlink;
                    subMenu.FuncId = sub.FunctionCode;
                    subMenu.IsSupport = true; //固定值
                    subMenu.IsModule = false;//固定值

                    //角色功能按鈕
                    var permissionButtonCodes = permissions.Where(o => o.FunctionCode == sub.FunctionCode).Select(o => o.ButtonCode);

                    //頁面功能按鈕
                    var fucButtonCodes = this.sysFnbuttonMap.Queryable()
                        .Where(o => o.FunctionCode == sub.FunctionCode)
                        .Select(o => o.ButtonCode)
                        .ToList();

                    var buttons = new List<RoleButtonInfo>();
                    foreach (var fbuton in butttonTitles)
                    {

                        var button = new RoleButtonInfo();

                        button.ButtonUrl = string.Empty; //空值
                        button.ButtonnCode = fbuton.Id;
                        button.Enable = fucButtonCodes.Contains(fbuton.Id); // 能否勾選
                        button.Check = permissionButtonCodes.Contains(fbuton.Id); // 勾選
                        buttons.Add(button);
                    }

                    subMenu.Buttons = buttons;
                    functions.Add(subMenu);
                }

                mainMenu.Children = functions;
                menus.Add(mainMenu);
            }

            var result = new RoleMenuResp();
            result.Title = butttonTitles;
            result.Menus = menus;

            return result;
        }

        /// <summary>
        /// 角色功能權限設定-修改頁-確認修改API
        /// </summary>
        /// <param name="req">角色功能權限設定 Req</param>
        /// <returns>True</returns>
        public async Task<bool> AuthRoleConfirmEdit(RolePermissionReq req)
        {
            //查詢角色
            var role = this.sysRoleRepo.Queryable().Where(o => o.RoleId == req.RoleId).FirstOrDefault();
            if (role == null) throw new BusinessException($"無此角色。");


            //刪除舊資料
            var deleteItems = this.sysRolePermissionRepo.Queryable()
                .Where(o => o.RoleId == req.RoleId)
                .ToList();
            foreach (var del in deleteItems)
            {
                this.sysRolePermissionRepo.Delete(del);
            }


            //新增新資料
            foreach (var func in req.Menus)
            {
                //功能頁 (啟用 , 保代-F)
                var sysfunction = this.sysFunctionRepo.Queryable()
                    .Where(o =>
                    o.Fnstatus == true &&
                    o.Fntype == FNType.F &&
                    !string.IsNullOrEmpty(o.Fnpid) &&//非父層
                    o.FunctionCode == func.FuncId)
                    .FirstOrDefault();

                // 功能頁面有的按鈕
                var fucButtons = this.sysFnbuttonMap.Queryable()
                    .Where(o => o.FunctionCode == func.FuncId)
                    .Select(o => o.ButtonCode)
                    .ToList();

                foreach (var btn in func.Buttons)
                {
                    if (btn.Enable && //功能有此按鈕
                        btn.Check && //有勾選
                        sysfunction != null && //功能能為啟用
                        fucButtons.Contains(btn.ButtonnCode))//且為功能頁具有的按鈕
                    {
                        var pm = new SysRolePermission();
                        pm.RoleId = req.RoleId;
                        pm.FunctionCode = func.FuncId;
                        pm.ButtonCode = btn.ButtonnCode;
                        pm.CreateBy = WebContext.Account;
                        pm.CreateOn = DateTime.Now;
                        pm.UpdateBy = WebContext.Account;
                        pm.UpdateOn = DateTime.Now;

                        this.sysRolePermissionRepo.Insert(pm);
                    }
                }
            }

            await this.unitOfWork.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// 角色功能權限設定-查詢頁-刪除API
        /// </summary>
        /// <param name="req">角色功能權限設定 Req</param>
        /// <returns>True</returns>
        public async Task<bool> AuthRoleDelete(RoleReq req)
        {
            //查詢角色
            var role = this.sysRoleRepo.Queryable().Where(o => o.RoleId == req.RoleId).FirstOrDefault();
            if (role == null) throw new BusinessException($"無此角色。");


            //刪除舊資料
            var deleteItems = this.sysRolePermissionRepo.Queryable()
                .Where(o => o.RoleId == req.RoleId)
                .ToList();
            foreach (var del in deleteItems)
            {
                this.sysRolePermissionRepo.Delete(del);
            }

            await this.unitOfWork.SaveChangesAsync();
            return true;
        }
    }
}
