﻿using TPI.NetCore;
using TPI.NetCore.Extensions;
using TPI.NetCore.WebAPI.Models;
using TPI.NetCore.Models;
using TPI.NetCore.WebContext;
using Microsoft.Extensions.Configuration;
using SKFH.Insurance.IAuth;
using SKFH.Insurance.Company.Entity.Repository;
using SKFH.Insurance.IAuth.Models;
using coreLibs.DateAccess;
using SKFH.Insurance.Auth.Entity.Models;
using SKFH.Insurance.Common.Entity.Repository;
using InsureBrick.Modules.Common.Interface;
using Autofac;

namespace InsureBrick.Modules.Auth.Service
{
    /// <summary>
    /// AuthService
    /// </summary>
    public class AuthService : IAuthService
    {
        #region Initialization

        private readonly IUnitOfWork unitOfWork;
        private readonly IUserLoginLogoutService userLoginLogoutService;
        private readonly ISysUserRepository sysUserRepo;
        private readonly ISysRoleRepository sysRoleRepo;
        private readonly ISysRoleListRepository sysRoleListRepo;
        private readonly ISysRolePermissionRepository sysRolePermissionRepo;
        private readonly ISysUserDwphistoryRepository sysUserDwphistoryRepo;
        private readonly ISysParamRepository sysParamRepo;
        private readonly IConfiguration configuration;
        private readonly ICommonService commonService;
        //private readonly IValidationService validationService;
        //private readonly SendMailHelper sendMailHelper;
        //private readonly IEventLogService eventLog;

        public AuthService(
            ICommonService commonService,
            IComponentContext componentContext,
            //IValidationService validationService,
            IUserLoginLogoutService userLoginLogoutService,
            ISysUserRepository sysUserRepo,
            ISysRoleRepository sysRoleRepo,
            ISysRoleListRepository sysRoleListRepo,
            ISysRolePermissionRepository sysRolePermissionRepo,
            ISysUserDwphistoryRepository sysUserDwphistoryRepo,
            IConfiguration configuration,
            ISysParamRepository sysParamRepo
        //SendMailHelper sendMailHelper,
        //IEventLogService eventLog
            )
        {
            this.unitOfWork = componentContext.ResolveNamed<IUnitOfWork>("Auth");
            this.commonService = commonService;
            //this.validationService = validationService;
            this.userLoginLogoutService = userLoginLogoutService;
            this.sysUserRepo = sysUserRepo;
            this.sysRoleRepo = sysRoleRepo;
            this.sysRoleListRepo = sysRoleListRepo;
            this.sysRolePermissionRepo = sysRolePermissionRepo;
            this.sysUserDwphistoryRepo = sysUserDwphistoryRepo;
            this.configuration = configuration;
            this.sysParamRepo = sysParamRepo;
            //this.sendMailHelper = sendMailHelper;
            //this.eventLog = eventLog;
        }

        #endregion Initialization

        /// <summary>
        /// 使用者維護-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">使用者維護 Req</param>
        /// <returns>使用者維護 Resp</returns>
        public DataTableQueryResp<UserSearchResp> UserSearch(UserSearchReq req)
        {
            try
            {
                //TODO: 判斷 @後面必須是LineBank網域, 如果不是的話, 跳出? 繼續?

                // 依使用者帳號, 姓名, email, 查詢 使用者資料
                var sysuserList = this.sysUserRepo.Queryable()
                    .QueryItem(o => o.Account.Contains(req.Account), req.Account)
                    .QueryItem(o => o.Name.Contains(req.Name), req.Name)
                    .QueryItem(o => o.Email.Contains(req.Email), req.Email);

                // 只有登入使用者帳號為” INS_ADM_PRD”的使用者，才可查出” INS_ADM_PRD”的帳號
                if (WebContext.Account != AcoountType.INS_ADM_PRD)
                {
                    sysuserList = sysuserList.Where(o => o.Account != AcoountType.INS_ADM_PRD);
                }

                // 登入者狀態
                var accountStatus = this.commonService.SearchSysParamByGroupId(GroupIdType.AccountStatus);


                // 系統參數檔 查詢對應 使用者狀態
                var joinTable = from sl in sysuserList.ToList()
                                join sp in accountStatus on sl.AccountStatus equals sp.ItemId into spl
                                from sp in spl.DefaultIfEmpty()
                                select new UserSearchResp
                                {
                                    Account = sl.Account,
                                    Name = sl.Name,
                                    Email = sl.Email,
                                    AccountStatusStr = sp.ItemValue
                                };

                // 資料庫總筆數
                var totalCount = sysuserList.Count();

                // 組合資料
                var sysSearchRes = joinTable.AsQueryable()
                    .OrderBySortInfo(req.order, req.columns)
                    .Pagination(req.length, req.start);

                return new DataTableQueryResp<UserSearchResp>(sysSearchRes, totalCount);
            }
            catch (Exception ex)
            {
                //eventLog.Debug("AuthService", "UserSearch", "UserSearch", ex);
                throw;
            }
        }

        /// <summary>
        /// 使用者維護-查詢頁-停用 API
        /// </summary>
        /// <param name="req">使用者維護-查詢頁-啟用/停用API Req</param>
        /// <returns>True</returns>
        public async Task<bool> UserDisabled(AuthReq req)
        {
            var user = this.sysUserRepo.Queryable()
            .Where(o => o.Account == req.Account).FirstOrDefault();

            if (user.AccountStatus == AccountStatus.Disable)
            {
                throw new BusinessException($"使用者帳號已停用。");
            }

            user.AccountStatus = AccountStatus.Disable;//停用
            user.ModifyStatusOn = DateTime.Now; //帳號最後被停用時間
            user.UpdateBy = WebContext.Account;
            user.UpdateOn = DateTime.Now;

            this.sysUserRepo.Update(user);
            await this.unitOfWork.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// 使用者維護-寄發Email API
        /// </summary>
        /// <param name="req">使用者維護-寄發Email Req</param>
        /// <returns>True</returns>
        public async Task<bool> SendEmail(SendEmailReq req)
        {
            try
            {
                //var user = this.sysUserRepo.Queryable()
                //.Where(o => o.Account == req.Account).FirstOrDefault();

                //var mail = new MailBM()
                //{
                //    To = new List<string> { user.Email },
                //    Subject = "Email發送測試",
                //};

                //if (req.IsMailKit)
                //{
                //    var mailConfig = this.configuration.GetSection("Smtp").Get<MailConfig>();

                //    mail.Content = "您好：MailKit.Net.Smtp 發送 Email 成功";
                //    MailSender.Send(mail, mailConfig);
                //}
                //else
                //{
                //    mail.Content = "您好：System.Net.Mail 發送 Email 成功";
                //    this.sendMailHelper.SendMail(mail.To, mail.Subject, mail.Content);
                //}
            }
            catch
            {
                throw new BusinessException("Email發送測試失敗。");
            }

            return true;
        }

        /// <summary>
        /// 使用者維護-查詢頁-刪除 API
        /// </summary>
        /// <param name="req">使用者維護-查詢頁-刪除API Req</param>
        /// <returns>True</returns>
        public async Task<bool> UserDelete(AuthReq req)
        {
            var user = this.sysUserRepo.Queryable()
            .Where(o => o.Account == req.Account).FirstOrDefault();

            if (user == null)
            {
                throw new BusinessException($"此帳號不存在。");
            }

            //檢查 -角色維護-角色人員對應設定
            var roleIdList = this.sysRoleListRepo.Queryable().Where(o => o.Account == user.Account).Select(o => o.RoleId);
            if (roleIdList.Any())
            {
                throw new BusinessException($"此帳號無法刪除，請先至角色維護功能針對角色代號 {string.Join('、', roleIdList)} 刪除此帳號");
            }


            //1.刪除 使用者資料表
            this.sysUserRepo.Delete(user);

            //2.刪除 使用者密碼變更歷程 
            var histories = this.sysUserDwphistoryRepo.Queryable().Where(o => o.Account == user.Account);
            foreach (var his in histories)
            {
                this.sysUserDwphistoryRepo.Delete(his);
            }

            await this.unitOfWork.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// 使用者維護-新增頁-新增 API
        /// </summary>
        /// <param name="req">使用者維護-查詢頁-新增API Req</param>
        /// <returns>True</returns>
        public async Task<bool> UserInsert(InsertReq req)
        {
            var account = this.sysUserRepo.Queryable()
                .Where(o => o.Account == req.Account).FirstOrDefault();
            if (account != null) throw new BusinessException($"使用者帳號已存在。");

            this.userLoginLogoutService.ValidDwpCheck(req.Dwp);

            var user = new SysUser()
            {
                Account = req.Account,
                Name = req.Name,
                Email = req.Email,
                AccountStatus = AccountStatus.ResetDwp, //使用者狀態為3:更改密碼
                EncryptedDwp = this.userLoginLogoutService.GetEncryptedDwp(req.Account, req.Dwp), //新密碼
                SendSysDwpon = DateTime.Now, //系統最後變更密碼時間
                ErrorCount = 0,
                CreateBy = WebContext.Account,
                CreateOn = DateTime.Now,
                UpdateBy = WebContext.Account,
                UpdateOn = DateTime.Now
            };

            this.sysUserRepo.Insert(user);

            // 新增 「使用者密碼變更歷程」
            this.InsertSysUserDwphistory(user);

            await this.unitOfWork.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// 使用者維護-修改頁-查詢修改 API
        /// </summary>
        /// <param name="req"> 使用者維護-修改頁-查詢修改API Req</param>
        /// <returns>使用者維護-修改頁-查詢修改API Resp</returns>
        public EditResp UserEdit(AuthReq req)
        {
            var user = this.sysUserRepo.Queryable()
           .Where(o => o.Account == req.Account).FirstOrDefault();

            var result = new EditResp()
            {
                Account = user.Account,
                Name = user.Name,
                Email = user.Email
            };

            return result;
        }

        /// <summary>
        /// 使用者維護-修改頁-確認修改 API
        /// </summary>
        /// <param name="req">使用者維護-修改頁-確認修改 API Req</param>
        /// <returns>True</returns>
        public async Task<bool> UserConfirmEdit(InsertReq req)
        {
            var user = this.sysUserRepo.Queryable()
           .Where(o => o.Account == req.Account).FirstOrDefault();

            user.Name = req.Name;
            user.Email = req.Email;
            user.UpdateBy = WebContext.Account;
            user.UpdateOn = DateTime.Now;
            this.sysUserRepo.Update(user);

            await this.unitOfWork.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// 使用者維護-使用者重設密碼頁-查詢資料 API
        /// </summary>
        /// <param name="req">使用者維護-使用者重設密碼頁-查詢資料 API Req</param>
        /// <returns>使用者維護-使用者重設密碼頁-查詢資料 API Resp</returns>
        public DataTableQueryResp<UserResetDwpSearchResp> UsersResetDwpSearch(UserResetDwpSearchReq req)
        {
            try
            {
                //this.validationService.DateCheck(req.SendSysDwpStartDate, req.SendSysDwpEndDate, "系統最後變更密碼日期", true);

                // 依使用者帳號, 姓名, email, 查詢 使用者資料
                var sysuserList = this.sysUserRepo.Queryable()
                    .QueryItem(o => o.Account.Contains(req.Account), req.Account)
                    .QueryItem(o => o.Name.Contains(req.Name), req.Name)
                    .QueryItem(o => o.Email.Contains(req.Email), req.Email)
                    .QueryItem(o => o.SendSysDwpon >= Convert.ToDateTime(req.SendSysDwpStartDate), req.SendSysDwpStartDate)
                    .QueryItem(o => o.SendSysDwpon <= (Convert.ToDateTime(req.SendSysDwpEndDate).AddDays(1).AddSeconds(-1)), req.SendSysDwpEndDate);

                // 只有登入使用者帳號為” INS_ADM_PRD”的使用者，才可查出” INS_ADM_PRD”的帳號
                if (WebContext.Account != AcoountType.INS_ADM_PRD)
                {
                    sysuserList = sysuserList.Where(o => o.Account != AcoountType.INS_ADM_PRD);
                }

                // 資料庫總筆數
                var totalCount = sysuserList.Count();

                // 組合資料
                var sysSearchRes = sysuserList.ToList()
                    .Select(o =>
                    {
                        return new UserResetDwpSearchResp
                        {
                            Account = o.Account,
                            Name = o.Name,
                            Email = o.Email,
                            AccountStatusStr = this.commonService.GetSysParamValue(GroupIdType.AccountStatus, o.AccountStatus),
                            SendSysDwpon = o.SendSysDwpon?.ToString("yyyy/MM/dd HH:mm:ss")
                        };
                    })
                    .AsQueryable()
                    .OrderBySortInfo(req.order, req.columns)
                    .Pagination(req.length, req.start);

                return new DataTableQueryResp<UserResetDwpSearchResp>(sysSearchRes, totalCount);
            }
            catch (Exception ex)
            {
                //eventLog.Debug("AuthService", "UsersResetDwpSearch", "UsersResetDwpSearch", ex);
                throw;
            }
        }

        /// <summary>
        /// 使用者維護-使用者重設密碼頁-密碼重置 API
        /// </summary>
        /// <param name="req">使用者維護-使用者重設密碼頁-密碼重置API Req</param>
        /// <returns>True</returns>
        public async Task<bool> UserResetDwpMultiEdit(UserResetDwdEditReq req)
        {
            if (string.IsNullOrEmpty(req.ResetDwp)) throw new BusinessException($"重設密碼未輸入");

            if (!req.Accounts.Any()) throw new BusinessException($"請勾選帳號");

            this.userLoginLogoutService.ValidDwpCheck(req.ResetDwp);

            //需重設密碼人員
            var sysUsers = this.sysUserRepo.Queryable().Where(o => req.Accounts.Contains(o.Account));


            foreach (var user in sysUsers)
            {
                // 更新 使用者資料表 
                user.EncryptedDwp = this.userLoginLogoutService.GetEncryptedDwp(user.Account, req.ResetDwp); //新密碼
                user.ErrorCount = 0;
                user.SendSysDwpon = DateTime.Now; //系統最後變更密碼時間
                user.UpdateBy = WebContext.Account;
                user.UpdateOn = DateTime.Now;

                user.AccountStatus = AccountStatus.ResetDwp; // 帳號不為” INS_ADM_PRD”，更新狀態為：3更改密碼
                if (user.Account == AcoountType.INS_ADM_PRD)
                {
                    user.AccountStatus = AccountStatus.Enable; // 帳號為” INS_ADM_PRD”，更新狀態為：1啟用
                }

                this.sysUserRepo.Update(user);

                // 新增 「使用者密碼變更歷程」
                this.InsertSysUserDwphistory(user);
            }

            await this.unitOfWork.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// 角色維護-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">角色維護-查詢頁-查詢資料 API Req</param>
        /// <returns>角色維護-查詢頁-查詢資料 API Resp</returns>
        public DataTableQueryResp<RoleSearchResp> RoleSearch(RoleSearchReq req)
        {
            try
            {
                var sysRoleList = this.sysRoleRepo.Queryable()
                .QueryItem(o => o.RoleId.Contains(req.RoleId), req.RoleId)
                .QueryItem(o => o.Name.Contains(req.Name), req.Name);

                // 若登入之角色之使用者帳號為INS_ADM_PRD，才可查到角色代號：L99的資料
                if (WebContext.Account != AcoountType.INS_ADM_PRD)
                {
                    sysRoleList = sysRoleList.Where(o => o.RoleId != RoleIdType.L99);
                }

                // 角色狀態
                var accountStatus = this.commonService.SearchSysParamByGroupId(GroupIdType.RoleStatus);

                // 系統參數檔 查詢對應 角色狀態
                var joinTable = from sl in sysRoleList.ToList()
                                join sp in accountStatus on sl.RoleStatus equals sp.ItemId into spl
                                from sp in spl.DefaultIfEmpty()
                                select new RoleSearchResp
                                {
                                    RoleId = sl.RoleId,
                                    Name = sl.Name,
                                    RoleStatusStr = sp.ItemValue,
                                    RoleStatus = sl.RoleStatus
                                };

                // 資料庫總筆數
                var totalCount = sysRoleList.Count();

                // 組合資料
                var roleSearchRes = joinTable.AsQueryable()
                    .OrderBySortInfo(req.order, req.columns)
                    .Pagination(req.length, req.start);

                return new DataTableQueryResp<RoleSearchResp>(roleSearchRes, totalCount);
            }
            catch (Exception ex)
            {
                //eventLog.Debug("AuthService", "RoleSearch", "RoleSearch", ex);
                throw;
            }
        }

        /// <summary>
        /// 角色維護-查詢頁-停用 API
        /// </summary>
        /// <param name="req">角色維護 Req</param>
        /// <returns>True</returns>
        public async Task<bool> RoleDisabled(RoleDisabledReq req)
        {
            var role = this.sysRoleRepo.Queryable()
            .Where(o => o.RoleId == req.RoleId).FirstOrDefault();

            role.RoleStatus = req.RoleStatus;
            role.UpdateBy = WebContext.Account;
            role.UpdateOn = DateTime.Now;

            await this.unitOfWork.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// 角色維護-查詢頁-刪除 API
        /// </summary>
        /// <param name="req">角色維護-查詢頁-刪除 API Req</param>
        /// <returns>True</returns>
        public async Task<bool> RoleDelete(RoleReq req)
        {
            var roleUser = this.sysRoleListRepo.Queryable()
            .Where(o => o.RoleId == req.RoleId).ToList();

            if (roleUser.Any()) throw new BusinessException("角色內還有使用者，不可刪除。");

            var role = this.sysRoleRepo.Queryable()
            .Where(o => o.RoleId == req.RoleId).FirstOrDefault();

            this.sysRoleRepo.Delete(role);


            //刪除角色功能權限設定
            var deleteItems = this.sysRolePermissionRepo.Queryable()
                .Where(o => o.RoleId == req.RoleId)
                .ToList();
            foreach (var del in deleteItems)
            {
                this.sysRolePermissionRepo.Delete(del);
            }

            await this.unitOfWork.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// 角色維護-修改頁-查詢修改 API
        /// </summary>
        /// <param name="req">角色功能權限設定-修改頁-查詢修改 API Req</param>
        /// <returns>角色維護-修改頁-查詢修改 API Resp</returns>
        public async Task<RoleEditResp> RoleEdit(RoleReq req)
        {
            var role = this.sysRoleRepo.Queryable()
           .Where(o => o.RoleId == req.RoleId).FirstOrDefault();

            var roleUsers = this.sysRoleListRepo.Queryable()
            .Where(o => o.RoleId == req.RoleId).ToList();

            var result = new RoleEditResp();

            result.RoleId = role.RoleId;
            result.RoleName = role.Name;
            result.RoleStatus = role.RoleStatus;

            var users = new List<Roles>();
            foreach (var u in roleUsers)
            {
                var sys = this.sysUserRepo.Queryable()
                .Where(o => o.Account == u.Account).FirstOrDefault();

                var user = new Roles()
                {
                    Account = sys.Account,
                    Name = sys.Name,
                    Email = sys.Email,
                    AccountStatusStr = this.commonService.GetSysParamValue("AccountStatus", sys.AccountStatus)
                };

                users.Add(user);
            }

            result.Roles = users;

            return result;
        }

        /// <summary>
        /// 角色維護-新增頁-新增 API
        /// </summary>
        /// <param name="req">角色維護-新增頁-新增 API Req</param>
        /// <returns>True</returns>
        public async Task<bool> RoleInsert(RoleInsertReq req)
        {
            //判斷角色代碼是否重覆
            var roleID = this.sysRoleRepo.Queryable()
            .Where(o => o.RoleId == req.RoleId).FirstOrDefault();

            if (roleID != null) throw new BusinessException($"角色代碼重覆，請重新輸入。");

            //新增進角色維護
            var sysRole = new SysRole()
            {
                RoleId = req.RoleId,
                Name = req.RoleName,
                RoleStatus = req.RoleStatus,
                CreateBy = WebContext.Account,
                CreateOn = DateTime.Now
            };
            this.sysRoleRepo.Insert(sysRole);

            //新增進角色維護人員列表
            foreach (var role in req.Roles)
            {
                //防呆 user 是否存在 看要不要防呆
                var user = this.sysUserRepo.Queryable()
                .Where(o => o.Account == role.Account).FirstOrDefault()?.Account;

                if (string.IsNullOrEmpty(user)) throw new BusinessException($"角色維護人員不存在，請重新選擇。");

                var roleList = new SysRoleList()
                {
                    RoleId = req.RoleId,
                    Account = role.Account,
                    CreateBy = WebContext.Account,
                    CreateOn = DateTime.Now
                };

                this.sysRoleListRepo.Insert(roleList);
            }

            await this.unitOfWork.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// 角色維護-修改頁-確認修改 API
        /// </summary>
        /// <param name="req">角色維護-修改頁-確認修改 API Req</param>
        /// <returns>True</returns>
        public async Task<bool> RoleConfirmEdit(RoleInsertReq req)
        {
            var sysRole = this.sysRoleRepo.Queryable()
            .Where(o => o.RoleId == req.RoleId).FirstOrDefault();

            sysRole.Name = req.RoleName;
            sysRole.UpdateBy = WebContext.Account;
            sysRole.UpdateOn = DateTime.Now;
            this.sysRoleRepo.Update(sysRole);

            var users = this.sysRoleListRepo.Queryable()
            .Where(o => o.RoleId == req.RoleId).ToList();

            //把所有角色刪除
            foreach (var user in users)
            {
                this.sysRoleListRepo.Delete(user);
            }

            //新增進角色維護人員列表
            foreach (var role in req.Roles)
            {
                //防呆 user 是否存在 
                var user = this.sysUserRepo.Queryable()
                .Where(o => o.Account == role.Account).FirstOrDefault()?.Account;

                if (string.IsNullOrEmpty(user)) throw new BusinessException($"角色維護人員不存在，請重新選擇。");

                var roleList = new SysRoleList()
                {
                    RoleId = req.RoleId,
                    Account = role.Account,
                    CreateBy = WebContext.Account,
                    CreateOn = DateTime.Now
                };

                this.sysRoleListRepo.Insert(roleList);
            }

            await this.unitOfWork.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// 新增 「使用者密碼變更歷程」
        /// </summary>
        /// <param name="user">使用者資料</param>
        private void InsertSysUserDwphistory(SysUser user)
        {
            // 使用者密碼變更歷程 , 已滿10筆, 移除最早的記錄
            var histories = this.sysUserDwphistoryRepo.Queryable().Where(o => o.Account == user.Account);
            if (histories.Count() >= 10)
            {
                var pasthHistory = histories.OrderBy(o => o.SeqNo).FirstOrDefault();
                this.sysUserDwphistoryRepo.Delete(pasthHistory);
            }

            // 使用者密碼變更歷程
            var history = new SysUserDwphistory()
            {
                Account = user.Account,
                ResetDwpOn = DateTime.Now,
                PreviousDwp = user.EncryptedDwp, //記錄目前新密碼至歷程
                CreateBy = WebContext.Account,
                CreateOn = DateTime.Now
            };
            this.sysUserDwphistoryRepo.Insert(history);
        }
    }
}
