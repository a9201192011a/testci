﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TPI.NetCore;
using TPI.NetCore.Extensions;
using TPI.NetCore.Models;
using Microsoft.EntityFrameworkCore;
using TPI.Plugins.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using TPI.NetCore.WebContext;
using Microsoft.Extensions.Configuration;
using System.Text.RegularExpressions;
using TPI.NetCore.Helper;
using TPI.NetCore.WebAPI;
using SKFH.Insurance.Company.Entity.Repository;
using coreLibs.DateAccess;
using SKFH.Insurance.IAuth.Models;
using SKFH.Insurance.Auth.Entity.Models;
using SKFH.Insurance.IAuth;
using InsureBrick.Modules.Common.Interface;
using Autofac;
using SKFH.Insurance.Common.Entity.Repository;

namespace InsureBrick.Modules.Auth.Service
{
    /// <summary>
    /// 使用者登入登出 Service
    /// </summary>
    public class UserLoginLogoutService : IUserLoginLogoutService
    {
        #region Initialization

        private readonly string errorMsg = $"使用者帳號或密碼錯誤，請洽問管中心。";
        private readonly IConfiguration configuration;
        private readonly IHttpContextAccessor accessor;

        private readonly IUnitOfWork unitOfWork;
        //private readonly ICommonService commonService;
        private readonly ISysUserRepository sysUserRepo;
        private readonly ISysUserDwphistoryRepository sysUserDwphistoryRepo;
        private readonly ISysRoleRepository sysRoleRepo;
        private readonly ISysRoleListRepository sysRoleListRepo;
        private readonly ISysRolePermissionRepository sysRolePermissionRepo;
        private readonly ISysFunctionRepository sysFunctionRepo;
        private readonly ISysFnbuttonMapRepository sysFnbuttonMap;
        private readonly IAccessTokenRepository accessTokenRepo;
        private readonly ICommonService commonService;
        //private readonly ITrackableRepository<SysMsg> sysMsgRepo;
        //private readonly ITrackableRepository<SysMsgList> sysMsgListRepo;
        //private readonly IAuditLogService auditLog;

        public UserLoginLogoutService(
            IComponentContext componentContext,
            IConfiguration configuration,
            IHttpContextAccessor accessor,
            ISysUserRepository sysUserRepo,
            ISysUserDwphistoryRepository sysUserDwphistoryRepo,
            IAccessTokenRepository accessTokenRepo,
            ISysRoleListRepository sysRoleListRepo,
            ISysRolePermissionRepository sysRolePermissionRepo,
            ICommonService commonService
            //IAuditLogService auditLog,
            //ISysMsgRepository sysMsgRepo,
            //ISysMsgListRepository sysMsgListRepo,

            )
        {
            this.configuration = configuration;
            this.accessor = accessor;
            this.unitOfWork = componentContext.ResolveNamed<IUnitOfWork>("Auth");
            this.sysUserRepo = sysUserRepo;
            this.sysUserDwphistoryRepo = sysUserDwphistoryRepo;
            this.accessTokenRepo = accessTokenRepo;
            this.sysRoleListRepo = sysRoleListRepo;
            this.sysRolePermissionRepo = sysRolePermissionRepo;
            //this.auditLog = auditLog;
            this.commonService = commonService;
            //this.sysMsgRepo = sysMsgRepo;
            //this.sysMsgListRepo = sysMsgListRepo;
        }
        #endregion Initialization

        /// <summary>
        /// 使用者帳號密碼登入
        /// </summary>
        /// <param name="req">Login Req</param>
        /// <returns>Login Resp</returns>
        public async Task<LoginResp> LoginAsync(LoginReq req)
        {
            if (string.IsNullOrEmpty(req.Account)) throw new BusinessException($"帳號未輸入");
            if (string.IsNullOrEmpty(req.Dwp)) throw new BusinessException($"密碼未輸入");

            var user = await this.GetSysUserAsync(req.Account);

            this.ValidSysUser(user);
            //await this.ValidDwp(user, req.Dwp);

            var needResetDwp = this.CheckNeedResetDwp(user);

            var result = new LoginResp();
            result.NeedResetDwp = needResetDwp;

            string roleIdsStr = GetUserRole(user.Account);

            // 需重設密碼
            if (result.NeedResetDwp)
            {
                // 只有 result.NeedResetDwp 有值
                //this.auditLog.AuditLog(null, FunctionCode.F1V01, OperationType.Login, $"{user.Account} 登入", null, user.Account, roleIdsStr);
                return result;
            }

            result = await GetLoginInfo(user, result);

            //this.auditLog.AuditLog(null, FunctionCode.F1V01, OperationType.Login, $"{user.Account} 登入", null, user.Account, roleIdsStr);
            return result;
        }

        /// <summary>
        /// 使用者登入登出-使用者重設密碼頁-確認修改 API
        /// </summary>
        /// <param name="req">使用者登入登出-使用者登入頁-重設密碼 Req</param>
        /// <returns>True</returns>
        public async Task<bool> ResetDwpEdit(ResetDwpEditReq req)
        {
            #region 驗證
            //檢查1-密碼輸入不一致
            if (req.NewDwp != req.ConfirmNewDwp)
            {
                throw new BusinessException($"密碼輸入不一致，請重新輸入。");
            }

            //檢查2-符合固定密碼之安全設計
            this.ValidDwpCheck(req.NewDwp);

            //檢查3-輸入的新密碼與原密碼相同
            var user = await this.GetSysUserAsync(req.Account);
            if (user.EncryptedDwp == this.GetEncryptedDwp(req.Account, req.NewDwp))
            {
                throw new BusinessException($"不可輸入原密碼，請重新輸入。");
            }

            //檢查4-輸入的新密碼，曾經被設定過，且設定時間未超過X天。
            var dayRangeStr = this.commonService.GetSysParamValue(GroupIdType.LoginCheck, "C2");  //超過特定時間未變更密碼(單位:天)

            if (!int.TryParse(dayRangeStr, out var dayRange))
            {
                throw new BusinessException($"系統參數-超過特定時間未變更密碼(單位:天) 設定錯誤");
            }

            var encryptedDwp = this.GetEncryptedDwp(req.Account, req.NewDwp);
            var histories = this.sysUserDwphistoryRepo.Queryable().Where(o => o.Account == user.Account);

            var userHistories = histories.Where(o => o.PreviousDwp == encryptedDwp);
            foreach (var his in userHistories)
            {
                //『使用者密碼變更歷程』+未變更密碼天數上限X天
                var checkDate = his.ResetDwpOn.Value.AddDays(dayRange);

                // 系統日<=『使用者密碼變更歷程』+未變更密碼天數上限X天
                if (DateTime.Now <= checkDate)
                {
                    throw new BusinessException($"此密碼變更未超過{dayRange}天，無法使用過，請重新輸入");
                }
            }

            //檢查5 - 輸入的新密碼在近X次被設定過
            var recentCountStr = this.commonService.GetSysParamValue(GroupIdType.LoginCheck, "C3");  //使用者密碼不可與前X次相同

            if (!int.TryParse(recentCountStr, out var recentCount))
            {
                throw new BusinessException($"系統參數-使用者密碼不可與前X次相同 設定錯誤");
            }

            var recentHistories = histories.OrderByDescending(o => o.SeqNo).Take(recentCount); // 降冪 ,最近{幾次}調整的密碼
            if (recentHistories.Any(o => o.PreviousDwp == encryptedDwp))
            {
                throw new BusinessException($"此密碼變更未超過{recentCount}次，無法使用過，請重新輸入");
            }

            #endregion


            // 使用者密碼變更歷程 , 已滿10筆, 移除最早的記錄
            if (histories.Count() >= 10)
            {
                var pasthHistory = histories.OrderBy(o => o.SeqNo).FirstOrDefault();
                this.sysUserDwphistoryRepo.Delete(pasthHistory);
            }

            // 使用者密碼變更歷程
            var history = new SysUserDwphistory()
            {
                Account = user.Account,
                ResetDwpOn = DateTime.Now,
                PreviousDwp = encryptedDwp, //記錄目前新密碼至歷程
                CreateBy = req.Account,
                CreateOn = DateTime.Now
            };
            this.sysUserDwphistoryRepo.Insert(history);


            user.AccountStatus = AccountStatus.Enable; //啟用
            user.ErrorCount = 0;
            user.ResetDwpOn = DateTime.Now; //使用者最後變更密碼時間
            user.EncryptedDwp = encryptedDwp;
            user.UpdateBy = user.Account;
            user.UpdateOn = DateTime.Now;

            this.sysUserRepo.Update(user);

            await this.unitOfWork.SaveChangesAsync();

            //string roleIdsStr = GetUserRole(user.Account);

            //this.auditLog.AuditLog(null, FunctionCode.F1V01, OperationType.ResetDwp, $"{user.Account} 重設密碼", null, user.Account, roleIdsStr);

            return true;
        }

        /// <summary>
        /// 使用者登入登出-使用者登出 API
        /// </summary>
        /// <param name="req">Logout Req</param>
        /// <param name="logoutRequest">SSO LogoutRequest</param>
        /// <returns>True</returns>

        public async Task<bool> Logout(LogoutReq req, string logoutRequest = null)
        {
            if (!string.IsNullOrEmpty(req.CasSt)) // SSO登入
            {
                var query = this.accessTokenRepo.Queryable().Where(x => x.CasSt == req.CasSt); //查詢 CasSt

                if (!query.Any()) // SSO 人員已在其他系統登出。
                {
                    return true;
                }

                var account = query.FirstOrDefault()?.Account;

                var user = await this.GetSysUserAsync(account);
                user.LastLogoutOn = DateTime.Now;
                user.UpdateBy = user.Account;
                user.UpdateOn = DateTime.Now;
                this.sysUserRepo.Update(user);

                if (query.Any())
                {
                    query.ForEach(x =>
                    {
                        this.accessTokenRepo.Delete(x);
                        MemoryCacheHelper.RemoveCacheData(x.TokenGid.ToString());
                    });
                }
                await this.unitOfWork.SaveChangesAsync();

                //string roleIdsStr = GetUserRole(account);

                //// SSO 登出 記錄登出人員至 AuditLog
                //this.auditLog.AuditLog(null, FunctionCode.F1V01, OperationType.Logout, $"SSO登出 logoutRequest:{logoutRequest} , ST:{req.CasSt}", null, account, roleIdsStr);
                //this.auditLog.AuditLog(null, FunctionCode.F1V01, OperationType.Logout, $"{account} SSO登出", null, account, roleIdsStr);
            }
            else // 保代登入
            {
                var user = await this.GetSysUserAsync(req.Account);
                user.LastLogoutOn = DateTime.Now;
                user.UpdateBy = user.Account;
                user.UpdateOn = DateTime.Now;
                this.sysUserRepo.Update(user);

                var query = this.accessTokenRepo.Queryable().Where(x => x.Account == req.Account && x.JwtToken == req.Token);

                if (query.Any())
                {
                    query.ForEach(x =>
                    {
                        this.accessTokenRepo.Delete(x);
                        MemoryCacheHelper.RemoveCacheData(x.TokenGid.ToString());
                    });
                }
                await this.unitOfWork.SaveChangesAsync();
            }
            return true;
        }

        /// <summary>
        /// 產生 Access Token ( 加密處理)
        /// </summary>
        /// <param name="user">使用者資料</param>
        /// <param name="roleIds">權限列表</param>
        /// <param name="ssoLogin">User Prop Types</param>
        /// <param name="casSt">SSO 登入 ST 資訊</param>
        /// <returns>登入授權</returns>
        private AccessToken CreateAccessToken(SysUser user, IEnumerable<string> roleIds, string ssoLogin, string casSt)
        {
            var tokenGid = Guid.NewGuid();

            try
            {
                var jwtResult = JwtProvider.CreateJwt(new List<Claim>() {
                new Claim(UserPropTypes.TokenGid, tokenGid.ToString()),
                new Claim(UserPropTypes.Account, user.Account),
                new Claim(UserPropTypes.Name, user.Name),
                new Claim(UserPropTypes.RoleIds, string.Join('、',roleIds)),
                new Claim(UserPropTypes.SSOLogin, ssoLogin),
                new Claim(UserPropTypes.CasSt, casSt),
            });

                var now = DateTime.Now;

                var accessToken = new AccessToken()
                {
                    TokenGid = tokenGid,
                    Account = user.Account,
                    JwtToken = jwtResult,
                    IssuedOn = now,
                    ExpiresOn = DateTime.Now.AddMinutes(this.configuration.GetValue<double>("Jwt:Expires")),
                    ClientIp = this.accessor.HttpContext.Request.FetchClientIp(),
                    CasSt = !string.IsNullOrEmpty(casSt) ? casSt : null
                };

                return accessToken;
            }
            catch (Exception ex) { return new AccessToken(); }

        }

        /// <summary>
        /// 檢核使用者輸入的「帳號」是否存在於『使用者資料表』
        /// </summary>
        /// <param name="user">使用者資料</param>
        private void ValidSysUser(SysUser user)
        {
            if (user == null) throw new BusinessException($"使用者帳號或密碼錯誤，請重新輸入。");

            #region 驗證使用者狀態
            switch (user.AccountStatus)
            {
                case AccountStatus.Disable: //0 停用
                    {
                        throw new BusinessException(this.errorMsg);
                    }
                case AccountStatus.Enable: //1 啟用
                    {
                        // 無
                        break;
                    }
                case AccountStatus.Locking: //2 鎖定
                    {
                        throw new BusinessException(this.errorMsg);
                    }
                case AccountStatus.ResetDwp: //3 更改密碼
                    {
                        //var dayRangeStr = this.commonService.GetSysParamValue(GroupIdType.LoginCheck, "C1"); //超過特定時間未變更系統初始密碼(單位:天)

                        //if (!int.TryParse(dayRangeStr, out var dayRange))
                        //{
                        //    throw new BusinessException($"系統參數-超過特定時間未變更系統初始密碼(單位:天) 設定錯誤");
                        //}

                        //if (!user.SendSysDwpon.HasValue)
                        //{
                        //    throw new BusinessException($"此帳號無系統最後變更密碼時間");
                        //}

                        ////「系統最後發送密碼時間」+系統參數未變更天數上限
                        //var checkDate = user.SendSysDwpon.Value.AddDays(dayRange);

                        ////「系統日」大於「系統最後發送密碼時間」+系統參數未變更天數上限
                        //if (DateTime.Now > checkDate)
                        //{
                        //    throw new BusinessException(this.errorMsg);
                        //}
                        break;
                    }
                default: //非正確狀態
                    {
                        throw new BusinessException(this.errorMsg);
                    }
            }
            #endregion

        }

        /// <summary>
        /// 檢查密碼
        /// </summary>
        /// <param name="user">使用者資料</param>
        /// <param name="dwp">密碼</param>
        private async Task ValidDwp(SysUser user, string dwp)
        {
            // 對「輸入密碼」進行加密比對，加密方式：SHA256
            var encryptedDWP = this.GetEncryptedDwp(user.Account, dwp); //SHA256.Encrypt(dwp);
            if (user.EncryptedDwp != encryptedDWP) // 密碼錯誤
            {
                //var failedCountStr = this.commonService.GetSysParamValue(GroupIdType.LoginCheck, "C4"); //使用者密碼輸入錯誤次數上限

                //if (!int.TryParse(failedCountStr, out var failedCount))
                //{
                //    throw new BusinessException($"系統參數-使用者密碼輸入錯誤次數上限 設定錯誤");
                //}

                //if (user.ErrorCount >= failedCount) //已錯誤X次以上
                //{
                //    user.ErrorCount = user.ErrorCount.HasValue ? (user.ErrorCount.Value + 1) : 1;
                //    user.AccountStatus = AccountStatus.Locking; //鎖定
                //    user.AccountLockOn = DateTime.Now;
                //    user.UpdateBy = user.Account;
                //    user.UpdateOn = DateTime.Now;
                //    await this.UpdateSysUserAsync(user);
                //}
                //else if (user.ErrorCount < failedCount)//錯誤小於X次
                //{
                //    user.ErrorCount = user.ErrorCount.HasValue ? (user.ErrorCount.Value + 1) : 1;
                //    user.UpdateBy = user.Account;
                //    user.UpdateOn = DateTime.Now;
                //    await this.UpdateSysUserAsync(user);
                //}
                //else
                //{
                //    //無
                //}

                // 判斷登入使用者帳號是否為” INS_ADM_PRD”，若是” INS_ADM_PRD”，要寄發Mail通知相關人員。發送Email主旨來源取自系統訊息維護
                //if (user.Account == AcoountType.INS_ADM_PRD)
                //{
                //    this.sendMailBySysMsgFn(SysMsgFunction.A03);
                //}

                //已錯誤X次以上
                if (user.AccountStatus == AccountStatus.Locking)
                {
                    throw new BusinessException(this.errorMsg);
                }
                else //錯誤小於X次
                {
                    throw new BusinessException("使用者帳號或密碼錯誤，請重新輸入。");
                }

            }
            else  // 密碼正確
            {
                user.ErrorCount = 0;
                user.PreviousLoginOn = user.LastLoginOn; //記錄使用者上一次登入時間
                user.LastLoginOn = DateTime.Now; //更新登入時間
                user.UpdateBy = user.Account;
                user.UpdateOn = DateTime.Now;
                await this.UpdateSysUserAsync(user);
            }
        }

        /// <summary>
        /// 是否需要重設密碼
        /// </summary>
        /// <param name="user">使用者資料</param>
        /// <returns>需重設</returns>
        private bool CheckNeedResetDwp(SysUser user)
        {
            //「使用者狀態」= 3更改密碼，並於系統發送密碼 X日內登入：將畫面導入到《F1Y0403使用者重設密碼頁》，強制使用者變更密碼
            if (user.AccountStatus == AccountStatus.ResetDwp)
            {
                return true;
            }
            else if (user.AccountStatus == AccountStatus.Enable) // 「使用者狀態」= 1啟用
            {
                //1.登入使用者帳號為” INS_ADM_PRD”，不需要檢核是否需要重設密碼
                //if (user.Account == AcoountType.INS_ADM_PRD)
                //{
                //    this.sendMailBySysMsgFn(SysMsgFunction.A02);
                //    return false;
                //}

                //2. 登入使用者帳號不為” INS_ADM_PRD” , 判斷是否超過系統設定參數時間未變
                //var dayRangeStr = this.commonService.GetSysParamValue(GroupIdType.LoginCheck, "C2"); //超過特定時間未變更密碼(單位:天)

                //if (!int.TryParse(dayRangeStr, out var dayRange))
                //{
                //    throw new BusinessException($"系統參數-超過特定時間未變更密碼(單位:天) 設定錯誤");
                //}

                //if (!user.ResetDwpOn.HasValue)
                //{
                //    throw new BusinessException($"此帳號無使用者最後變更密碼時間");
                //}

                ////「使用者最後變更密碼時間」+未變更天數上限天數超過參數時間未變更密碼
                //var checkDate = user.ResetDwpOn.Value.AddDays(dayRange);

                ////「系統日」大於「使用者最後變更密碼時間」+未變更天數上限天數超過參數時間未變更密碼
                //if (DateTime.Now > checkDate)
                //{
                //    return true;
                //}
            }
            else
            {
                // 無
            }

            // 「使用者狀態」= 1啟用 , 未超過參數時間： 將畫面導入到《F1Y0404保代系統登入成功頁》
            return false;
        }

        /// <summary>
        /// 依使用者帳號 , 取得使用者資料
        /// </summary>
        /// <param name="account">帳號</param>
        /// <returns>使用者資料</returns
        public async Task<SysUser> GetSysUserAsync(string account)
        {
            var sysUser = await this.sysUserRepo.Queryable().AsNoTracking().FirstOrDefaultAsync(x => x.Account == account);
            if (sysUser == null) { return null; }

            return sysUser;
        }

        /// <summary>
        /// 更新該帳號的使用者資料
        /// </summary>
        /// <param name="user">使用者資料</param>
        /// <returns>無回傳值</returns>
        public async Task UpdateSysUserAsync(SysUser user)
        {
            this.sysUserRepo.Update(user);
            await this.unitOfWork.SaveChangesAsync();
        }

        /// <summary>
        /// 更新 Token 到期時間
        /// </summary>
        /// <param name="token">登入授權</param>
        /// <returns>無回傳值</returns>
        public async Task UpdateAccessTokenExpiresOnAsync(AccessToken token)
        {
            token.ExpiresOn = DateTime.Now.AddMinutes(this.configuration.GetValue<double>("Jwt:Expires"));
            this.accessTokenRepo.Update(token);
            await this.unitOfWork.SaveChangesAsync();
        }

        /// <summary>
        /// 新增 使用者資料
        /// </summary>
        /// <param name="user">使用者資料</param>
        /// <returns>無回傳值</returns>
        public async Task InsertSysUser(SysUser user)
        {
            user.EncryptedDwp = this.GetEncryptedDwp(user.Account, user.EncryptedDwp); //SHA256.Encrypt(user.EncryptedDwp);
            user.AccountStatus = AccountStatus.Enable;
            user.CreateBy = WebContext.Account;
            user.CreateOn = DateTime.Now;
            this.sysUserRepo.Insert(user);
            await this.unitOfWork.SaveChangesAsync();
        }

        /// <summary>
        /// 登入密碼驗證
        /// </summary>
        /// <param name="dwp">密碼</param>
        /// <returns>驗證結果</returns>
        public bool ValidDwpCheck(string dwp)
        {
            //檢查1
            // 長度：不應少於六位
            // 採英數字混合使用，包含大小寫英文字母與符號
            Regex regex1 = new Regex(@"^.*(?=.{6,})(?=.*\d)(?=.*[a-zA-Z])(?=.*[\W|_]).*$");
            if (!regex1.IsMatch(dwp)) //不符格式
            {
                throw new BusinessException($"密碼輸入錯誤，請輸入符合安全設計之密碼規則。");
            }

            //檢查3
            // 連續英文字或連號數字 => ex. AB 或 12 
            bool isPass = true;
            Regex regex3 = new Regex("([0-9])+|([a-zA-Z])+");//切出所有連續英文或連續數字的子字串，連續長度=1~n
            var matches01 = regex3.Matches(dwp);

            foreach (Match item01 in matches01)
            {
                if (item01.Value.Length >= 2)//連續英文或連續數字的子字串長度大於2，才要判斷是否為相同字元或者為連續字元
                {
                    int index = 0;//陣列指標
                    char chrbuf = '\0';//前一個字元暫存
                    int intvar0 = 0;//變化量

                    foreach (char c in item01.Value)
                    {
                        if (index > 0)
                        {
                            intvar0 = Math.Abs(Convert.ToInt32(chrbuf) - Convert.ToInt32(c));//計算遞增方向
                        }
                        chrbuf = c;
                        index++;

                        if (intvar0 == 1)//找到 2個連續的字元
                        {
                            isPass = false;
                            break;
                        }
                    }
                }

                if (!isPass)
                {
                    break;
                }
            }


            if (!isPass) //不通過
            {
                throw new BusinessException($"密碼輸入錯誤，請輸入符合安全設計之密碼規則。");
            }

            return true;
        }

        /// <summary>
        /// 取得密碼加密結果(帳號+密碼)
        /// </summary>
        /// <param name="account">帳號</param>
        /// <param name="dwp">密碼</param>
        /// <returns>加密結果</returns>
        public string GetEncryptedDwp(string account, string dwp)
        {
            // 統一大寫
            account = account.ToUpper();

            // 使用者帳號 + 密碼 , 一同加密
            return SHA256.Encrypt(account + dwp);
        }

        /// <summary>
        /// 依系統訊息發送Email
        /// </summary>
        /// <param name="sysMsgFn">訊息類別</param>
        //private void sendMailBySysMsgFn(string sysMsgFn)
        //{
        //    // 查詢 訊息類別
        //    var sysParam = this.commonService.SearchSysParamByGroupId(GroupIdType.SysMsgFunction)
        //        .Where(o => o.ItemId == sysMsgFn).FirstOrDefault();
        //    var sysMsg = this.sysMsgRepo.Queryable()
        //        .Where(o => o.SysMsgFunction == sysParam.ItemId)
        //        .FirstOrDefault();

        //    // 查詢 Email
        //    var emailList = this.sysMsgListRepo.Queryable()
        //        .Where(o => o.SysMsgFunction == sysMsg.SysMsgFunction)
        //        .Select(o => o.Email)
        //        .ToList();

        //    if (!emailList.Any())
        //    {
        //        throw new BusinessException($"請至系統訊息發送對象檔設置 Email");
        //    }

        //    try
        //    {
        //        var mail = new MailBM()
        //        {
        //            To = emailList,
        //            Subject = sysMsg.EmailSubject,
        //            Content = sysMsg.EmailContent
        //        };
        //        var mailConfig = this.configuration.GetSection("Smtp").Get<MailConfig>();
        //        MailSender.Send(mail, mailConfig);
        //    }
        //    catch 
        //    {
        //    }
        //}

        /// <summary>
        /// 產出 Cas Ticket
        /// </summary>
        /// <param name="account">帳號</param>
        /// <param name="ticket">Server Ticket</param>
        /// <returns>Ticket</returns>
        public string GenCasTicket(string account, string ticket)
        {
            // 目前先設定 1 hr 內有效。整合後，再改為 1 分鐘，並設定至參數。
            var plainTicket = $"{account}@{ticket}@{DateTime.Now.AddMinutes(60).Ticks}";

            var encryptTicket = AES256.EncryptString(plainTicket);
            return encryptTicket;
        }

        /// <summary>
        /// 抓取 request 的 ticket 進行驗證
        /// </summary>
        /// <param name="encryptTicket">SSO Ticket</param>
        /// <returns>Cas 資訊</returns>
        public CasInfo ValidCasTicket(string encryptTicket)
        {
            var plainTicket = AES256.DecryptString(encryptTicket);
            var ticketAry = plainTicket.Split('@');

            if (ticketAry.Length == 3 && long.TryParse(ticketAry[2], out var timeTicks))
            {
                if (DateTime.Now < new DateTime(timeTicks))
                    return new CasInfo { Account = ticketAry[0], CasSt = ticketAry[1] };
            }

            return null;
        }

        /// <summary>
        /// 前端 SSO 登入
        /// </summary>
        /// <param name="casInfo">Cas 資訊</param>
        /// <param name="ticket">SSO Ticket</param>
        /// <returns>Login Resp</returns>
        public async Task<LoginResp> SSOLoginAsyn(CasInfo casInfo, string ticket = null)
        {
            var user = await this.GetSysUserAsync(casInfo.Account);

            if (user == null) throw new BusinessException(ReturnCodes.Failure_InsureBrick_INSF1Y9997.ToDisplayName(), $"查無人員資料");

            //1.使用者上次登入時間
            //2.更新人員 / 更新時間
            //3.使用者最後登入時間
            user.PreviousLoginOn = user.LastLoginOn; //記錄使用者上一次登入時間
            user.LastLoginOn = DateTime.Now; //更新登入時間
            user.UpdateBy = user.Account;
            user.UpdateOn = DateTime.Now;
            await this.UpdateSysUserAsync(user);

            var result = new LoginResp();
            result = await this.GetLoginInfo(user, result, UserPropTypes.SSOLogin, casInfo.CasSt);

            //string roleIdsStr = GetUserRole(casInfo.Account);

            //// SSO 登入 記錄登出人員至 AuditLog
            //this.auditLog.AuditLog(null, FunctionCode.F1V01, OperationType.Login, $"SSO登入 ticket:{ticket}", null, casInfo.Account, roleIdsStr);
            //this.auditLog.AuditLog(null, FunctionCode.F1V01, OperationType.Login, $"{casInfo.Account} SSO登入", null, casInfo.Account, roleIdsStr);

            return result;
        }

        /// <summary>
        /// 取得登入資訊
        /// </summary>
        /// <param name="user">使用者資料</param>
        /// <param name="result">Login Resp</param>
        /// <param name="ssoLogin">User Prop Types</param>
        /// <param name="casSt">SSO 登入 ST 資訊</param>
        /// <returns>Login Resp</returns>
        public async Task<LoginResp> GetLoginInfo(SysUser user, LoginResp result, string ssoLogin = "", string casSt = "")
        {
            var roleIds = this.sysRoleListRepo.Queryable()
                .Where(o => o.Account == user.Account)
                .Select(o => o.RoleId).Distinct();
            if (!roleIds.Any())
            {
                throw new BusinessException($"此帳號無綁定任何角色。");
            }
            var permissions = this.sysRolePermissionRepo.Queryable()
                .Where(o => roleIds.Contains(o.RoleId));

            if (!permissions.Any())
            {
                throw new BusinessException($"此帳號所屬角色未設定功能，請至角色功能權限設定設定功能。");
            }

            var accessToken = this.CreateAccessToken(user, roleIds, ssoLogin, casSt);
            this.accessTokenRepo.Queryable()
                .Where(x => x.Account == accessToken.Account)
                .ForEach(token =>
                {
                    this.accessTokenRepo.Delete(token);
                    MemoryCacheHelper.RemoveCacheData(token.TokenGid.ToString());
                });

            this.accessTokenRepo.Insert(accessToken);

            await this.unitOfWork.SaveChangesAsync();

            result.Account = user.Account;
            result.Name = user.Name;
            result.PreviousLoginOn = user.PreviousLoginOn?.ToString("yyyy/MM/dd HH:mm:ss");
            result.Token = accessToken.JwtToken;

            return result;
        }

        /// <summary>
        /// 取得使用者所有設定權限(回傳 RuleId ，多筆以 "、" 分隔)
        /// </summary>
        /// <param name="account">帳號</param>
        /// <returns>權限清單</returns>
        private string GetUserRole(string account)
        {
            var roleIds = this.sysRoleListRepo.Queryable()
                .Where(o => o.Account == account)
                .Select(o => o.RoleId).Distinct();

            var roleIdsStr = string.Join('、', roleIds);

            return roleIdsStr;
        }
    }
}
