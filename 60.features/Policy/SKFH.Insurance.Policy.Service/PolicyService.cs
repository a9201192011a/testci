﻿using InsureBrick.Modules.Policy.Interface;
using AutoMapper;
using TPI.NetCore.WebAPI.Models;
using TPI.NetCore.Extensions;
using InsureBrick.Modules.Policy.Interface.Models;
using TPI.NetCore.WebAPI;
using TPI.NetCore;
using InsureBrick.Modules.Common.Interface;
using TPI.NetCore.Models;
using TPI.NetCore.Exceptions;
using TPI.NetCore.WebAPI.Interfaces;
using Microsoft.Data.SqlClient;
using InsureBrick.Domain.Helper;
using Dapper;
using System.Text.RegularExpressions;
using TPI.NetCore.DapperCmd;
using TPI.NetCore.Helper;
using System.Globalization;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using SKFH.Insurance.entity.Repositories;
using SKFH.Insurance.Policy.Entity.Models;
using SKFH.Insurance.Product.Entity.Models;
using SKFH.Insurance.Company.Entity.Repository;
using SKFH.Insurance.Member.Entity.Models;
using SKFH.Insurance.Product.Entity.Repository;

namespace InsureBrick.Modules.Policy.Service
{
    /// <summary>
    /// PolicyService
    /// </summary>
    public class PolicyService : IPolicyService
    {
        #region Initialization

        private readonly IMapper mapper;
        private readonly ICommonService commonService;
        private readonly IChannelAPICommonService channelAPICommonService;
        private readonly IPolicyMasterRepository policyMasterRepo;
        private readonly IPolicyCarInfoRepository policyCarInfoRepo;
        private readonly IPolicyPersonInfoRepository policyPersonInfoRepo;
        private readonly IPolicyDetailRepository policyDetailRepo;
        private readonly ITempPolicyMasterRepository tempPolicyMasterRepo;
        private readonly ITempPolicyDetailRepository tempPolicyDetailRepo;
        private readonly ITempPolicyPersonInfoRepository tempPolicyPersonInfoRepo;
        private readonly IPolicyClockRepository policyClockRepo;
        private readonly IConfiguration configuration;
        private readonly IPolicySearchViewRepository policySearchViewRepo;
        private readonly IProductMasterRepository productMasterRepo;
        private readonly ICampaignMasterRepository campaignMasterRepo;
        private readonly ICampaignFeatureRepository campaignFeatureRepo;
        private readonly ICampaignPlanAssuredMappingRepository campaignPlanAssuredMappingRepo;
        private readonly IPlanAssuredRepository planAssuredRepo;
        private readonly IPlanMasterRepository planMasterRepo;
        private readonly ICampaignPlanMappingRepository campaignPlanMappingRepo;
        private readonly ITempPolicyCarInfoRepository tempPolicyCarInfoRepo;
        private readonly ICompanyProfileRepository companyProfileRepo;
        private readonly IMemberProfileRepository memberProfileRepo;
        private readonly IMemberProfileLogRepository memberProfileLogRepo;
        private readonly IAcceptRecordRepository acceptRecordRepo;
        private readonly IKycrecordRepository kycRecordRepo;
        private readonly IKycrecordV1Repository kycRecordV1Repo;
        private readonly ITempKycrecordRepository tempKycrecordRepo;
        private readonly ITempKycrecordV1Repository tempKycrecordV1Repo;
        private readonly IMemberAmlRepository memberAmlRepo;

        //private readonly IEAIAdapter EAIAdapter;
        //private readonly IFEBAdapter FEBAdapter;
        //private readonly IPolicySignLogRepository policySignLogRepo;


        //private readonly IINSUnitOfWork insUnitOfWork;
        //private readonly IEventLogService eventLog;

        //private readonly IUMSAdapter UMSAdapter;
        //private readonly IProductService productService;
        //private readonly IValidationService validationService;
        //private readonly IDataLogKycrecordV1Repository dataLogKycRecordV1Repo;
        //private readonly IDataLogKycrecordRepository dataLogKycrecordRepo;

        //private readonly ITempVehicleBrandRepository tempVehicleBrandRepo;
        //private readonly ITempCarTypeListRepository tempCarTypeListRepo;
        //private readonly IPolicySignRecordRepository policySignRecordRepo;
        //private readonly ITcRecordRepository tcRecordRepo;
        //private readonly IPolicySignMasterRepository policySignMasterRepo;
        //private readonly ICustomerInfoRepository customerInfoRepo;
        //private readonly ICarTypeListRepository carTypeListRepo;
        //private readonly IDataLogPolicyMasterRepository dataLogPolicyMasterRepo;
        //private readonly IDataLogPolicyDetailRepository dataLogPolicyDetailRepo;
        //private readonly ICustomerProfileRepository customerProfileRepo;
        //private readonly ISysParamRepository sysParamRepo;
        //private readonly IOutBoundListRepository outBoundListRepo;
        //private readonly IOutBoundListDetailRepository outBoundListDetailRepo;
        //private readonly ILogLbmiddleWareRepository logLbmiddleWareRepo;
        //private readonly IUmssendListGetOrderStatusRepository umssendListGetOrderStatusRepo;
        //private readonly IOutBoundCompanySettingRepository outBoundCompanySettingRepo;
        //private readonly IOutBoundCustomerSettingRepository outBoundCustomerSettingRepo;
        //private readonly IOutBoundSamplingRecordRepository outBoundSamplingRecordRepo;
        //private readonly IDataLogPolicyPersonInfoRepository dataLogPolicyPersonInfoRepo;
        //private readonly IDataLogPolicyCarInfoRepository dataLogPolicyCarInfoRepo;
        //private readonly IInsMsgCodeRepository insMsgCodeRepo;
        //private readonly IUmssendListRenewalRepository umssendListRenewalRepo;
        //private readonly IUmssendListAmlRepository umssendListAmlRepo;
        //private readonly ITempTrialcalMasterV1Repository tempTrialcalMasterV1Repo;
        //private readonly ITempTrialcalDetailV1Repository tempTrialcalDetailV1Repo;
        //private readonly ITempInsuranceDateRepository tempInsuranceDateRepo;
        //private readonly IOtprecordRepository otprecordRepo;
        //private readonly string rntmEnvDsCd;

        //private Dictionary<TelInterviewType, stringRepository _telInterviewType = new Dictionary<TelInterviewType, stringRepository();
        public DateTime GuidDate { get; private set; } = DateTime.Now;
        private int serialNo = 0;
        private readonly string startTime;

        /// <summaryRepository
        /// DateTime.TryParseExact formats參數
        /// </summaryRepository
        private readonly string[] DateTimeFormats = new string[] { "yyyyMMdd", "yyyyMMddHHmmss" };

        /// <summaryRepository
        /// Dapper CommandTimeout 60秒
        /// </summaryRepository
        private readonly int commandTimeout = 60;

        // 紀錄商品主檔
        private Dictionary<string, ProductMaster> productMasterDic = new Dictionary<string, ProductMaster>();

        public PolicyService(
            IMapper mapper,
            ICommonService commonService,
            IChannelAPICommonService channelAPICommonService,
            IPolicyMasterRepository policyMasterRepo,
            IPolicyPersonInfoRepository policyPersonInfoRepo,
            IPolicyCarInfoRepository policyCarInfoRepo,
            IPolicyDetailRepository policyDetailRepo,
            ITempPolicyMasterRepository tempPolicyMasterRepo,
            ITempPolicyDetailRepository tempPolicyDetailRepo,
            ITempPolicyPersonInfoRepository tempPolicyPersonInfoRepo,
            IConfiguration configuration,
            IPolicyClockRepository policyClockRepo,
            IPolicySearchViewRepository policySearchViewRepo,
            IProductMasterRepository productMasterRepo,
            ICampaignMasterRepository campaignMasterRepo,
            ICampaignFeatureRepository campaignFeatureRepo,
            IPlanMasterRepository planMasterRepo,
            IPlanAssuredRepository planAssuredRepo,
            ITempPolicyCarInfoRepository tempPolicyCarInfoRepo,
            ICompanyProfileRepository companyProfileRepo,
            IMemberProfileRepository memberProfileRepo,
            IMemberProfileLogRepository memberProfileLogRepo,
            ICampaignPlanMappingRepository campaignPlanMappingRepo,
            ICampaignPlanAssuredMappingRepository campaignPlanAssuredMappingRepo,
            IAcceptRecordRepository acceptRecordRepo,
            IKycrecordRepository kycRecordRepo,
            IKycrecordV1Repository kycRecordV1Repo,
            ITempKycrecordRepository tempKycrecordRepo,
            ITempKycrecordV1Repository tempKycrecordV1Repo,
            IMemberAmlRepository memberAmlRepo

            //IEAIAdapter EAIAdapter,
            //IFEBAdapter FEBAdapter
            )
        //,

        //IINSUnitOfWork insUnitOfWork,
        //IValidationService validationService,
        //IEventLogService eventLog,
        //IUMSAdapter UMSAdapter,
        //IProductService productService,
        //IDataLogKycrecordV1Repository dataLogKycRecordV1Repo,
        //IDataLogKycrecordRepository dataLogKycrecordRepo,
        //ISysParamRepository sysParamRepo,
        //ITempVehicleBrandRepository tempVehicleBrandRepo,
        //ITempCarTypeListRepository tempCarTypeListRepo,
        //IPolicySignRecordRepository policySignRecordRepo,
        //ITcRecordRepository tcRecordRepo,
        //IPolicySignMasterRepository policySignMasterRepo,
        //ICustomerInfoRepository customerInfoRepo,
        //ICarTypeListRepository carTypeListRepo,
        //IOutBoundListRepository outBoundListRepo,
        //IOutBoundListDetailRepository outBoundListDetailRepo,
        //ICustomerProfileRepository customerProfileRepo,
        //ILogLbmiddleWareRepository logLbmiddleWareRepo,
        //IUmssendListGetOrderStatusRepository umssendListGetOrderStatusRepo,
        //IUmssendListRenewalRepository umssendListRenewalRepo,
        //IUmssendListAmlRepository umssendListAmlRepo,
        //IOutBoundCompanySettingRepository outBoundCompanySettingRepo,
        //IOutBoundCustomerSettingRepository outBoundCustomerSettingRepo,
        //IOutBoundSamplingRecordRepository outBoundSamplingRecordRepo,
        //IDataLogPolicyMasterRepository dataLogPolicyMasterRepo,
        //IDataLogPolicyDetailRepository dataLogPolicyDetailRepo,
        //IDataLogPolicyPersonInfoRepository dataLogPolicyPersonInfoRepo,
        //IDataLogPolicyCarInfoRepository dataLogPolicyCarInfoRepo,
        //IInsMsgCodeRepository insMsgCodeRepo,
        //ITempTrialcalMasterV1Repository tempTrialcalMasterV1Repo,
        //ITempTrialcalDetailV1Repository tempTrialcalDetailV1Repo,
        //ITempInsuranceDateRepository tempInsuranceDateRepo,
        //IOtprecordRepository otprecordRepo,
        //IPolicySignLogRepository policySignLogRepo,
        {
            this.mapper = mapper;
            this.commonService = commonService;
            this.policyMasterRepo = policyMasterRepo;
            this.policyPersonInfoRepo = policyPersonInfoRepo;
            this.policyCarInfoRepo = policyCarInfoRepo;
            this.policyDetailRepo = policyDetailRepo;
            this.tempPolicyMasterRepo = tempPolicyMasterRepo;
            this.tempPolicyDetailRepo = tempPolicyDetailRepo;
            this.tempPolicyPersonInfoRepo = tempPolicyPersonInfoRepo;
            this.channelAPICommonService = channelAPICommonService;
            this.configuration = configuration;
            this.policyClockRepo = policyClockRepo;
            this.policySearchViewRepo = policySearchViewRepo;
            this.productMasterRepo = productMasterRepo;
            this.campaignMasterRepo = campaignMasterRepo;
            this.campaignFeatureRepo = campaignFeatureRepo;
            this.planMasterRepo = planMasterRepo;
            this.planAssuredRepo = planAssuredRepo;
            this.campaignPlanMappingRepo = campaignPlanMappingRepo;
            this.campaignPlanAssuredMappingRepo = campaignPlanAssuredMappingRepo;
            this.tempPolicyCarInfoRepo = tempPolicyCarInfoRepo;
            this.companyProfileRepo = companyProfileRepo;
            this.memberProfileRepo = memberProfileRepo;
            this.memberProfileLogRepo = memberProfileLogRepo;
            this.acceptRecordRepo = acceptRecordRepo;
            this.kycRecordRepo = kycRecordRepo;
            this.kycRecordV1Repo = kycRecordV1Repo;
            this.tempKycrecordRepo = tempKycrecordRepo;
            this.tempKycrecordV1Repo = tempKycrecordV1Repo;
            this.memberAmlRepo = memberAmlRepo;


            //this.insUnitOfWork = insUnitOfWork;
            //this.eventLog = eventLog;
            //this.validationService = validationService;
            //this.dataLogKycRecordV1Repo = dataLogKycRecordV1Repo;
            //this.dataLogKycrecordRepo = dataLogKycrecordRepo;
            //this.EAIAdapter = EAIAdapter;
            //this.FEBAdapter = FEBAdapter;
            //this.tempVehicleBrandRepo = tempVehicleBrandRepo;
            //this.tempCarTypeListRepo = tempCarTypeListRepo;
            //this.tcRecordRepo = tcRecordRepo;
            //this.policySignRecordRepo = policySignRecordRepo;
            //this.UMSAdapter = UMSAdapter;
            //this.carTypeListRepo = carTypeListRepo;
            //this.customerInfoRepo = customerInfoRepo;
            //this.policySignMasterRepo = policySignMasterRepo;
            //this.dataLogPolicyMasterRepo = dataLogPolicyMasterRepo;
            //this.dataLogPolicyDetailRepo = dataLogPolicyDetailRepo;
            //this.sysParamRepo = sysParamRepo;
            //this.customerProfileRepo = customerProfileRepo;
            //this.outBoundListRepo = outBoundListRepo;
            //this.outBoundListDetailRepo = outBoundListDetailRepo;
            //this.logLbmiddleWareRepo = logLbmiddleWareRepo;
            //this.umssendListGetOrderStatusRepo = umssendListGetOrderStatusRepo;
            //this.outBoundCompanySettingRepo = outBoundCompanySettingRepo;
            //this.outBoundCustomerSettingRepo = outBoundCustomerSettingRepo;
            //this.outBoundSamplingRecordRepo = outBoundSamplingRecordRepo;
            //this.productService = productService;
            //this.dataLogPolicyPersonInfoRepo = dataLogPolicyPersonInfoRepo;
            //this.dataLogPolicyCarInfoRepo = dataLogPolicyCarInfoRepo;
            //this.insMsgCodeRepo = insMsgCodeRepo;
            //this.umssendListRenewalRepo = umssendListRenewalRepo;
            //this.umssendListAmlRepo = umssendListAmlRepo;
            //this.tempTrialcalMasterV1Repo = tempTrialcalMasterV1Repo;
            //this.tempTrialcalDetailV1Repo = tempTrialcalDetailV1Repo;
            //this.tempInsuranceDateRepo = tempInsuranceDateRepo;
            //this.otprecordRepo = otprecordRepo;
            //this.policySignLogRepo = policySignLogRepo;
            //startTime = Process.GetCurrentProcess().StartTime.ToString("mmss");
            //eventLog.Debug($"Process: {Process.GetCurrentProcess().Id} ,Guid StartTime {startTime}", "InitGuid", startTime);
            //this.rntmEnvDsCd = this.configuration.GetValue<string>("RuntimeEnvironmentDistinctionCode");
        }

        #endregion Initialization

        /// <summary>
        /// 保單資料查詢-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料查詢 Req</param>
        /// <returns>保單資料查詢 Resp</returns>
        public DataTableQueryResp<PolicySearchResp> PolicySearch(PolicySearchReq req)
        {
            try
            {
                this.commonService.DateCheck(req.PolicyPeriodStartDate, req.PolicyPeriodEndDate, "生效日期", true);
                this.commonService.DateCheck(req.ExpiredStartDate, req.ExpiredEndDate, "到期日期", true);

                var query = this.policySearchViewRepo.Queryable();

                #region req 條件篩選
                if (!string.IsNullOrEmpty(req.PolicyNo))
                {
                    query = query.Where(x => x.PolicyNo == req.PolicyNo);
                }

                if (!string.IsNullOrEmpty(req.PlanCategory))
                {
                    query = query.Where(x => x.PlanCategory == req.PlanCategory);
                }

                if (!string.IsNullOrEmpty(req.CaseSource))
                {
                    query = query.Where(x => x.CaseSource == req.CaseSource);
                }

                if (!string.IsNullOrEmpty(req.PolicyStatus))
                {
                    query = query.Where(x => x.PolicyStatus == req.PolicyStatus);
                }

                if (!string.IsNullOrEmpty(req.ApplicantId))
                {
                    query = query.Where(x => x.ApplicantId == req.ApplicantId);
                }

                if (!string.IsNullOrEmpty(req.InsuredId))
                {
                    query = query.Where(x => x.InsuredId == req.InsuredId);
                }

                if (!string.IsNullOrEmpty(req.ApplicantName))
                {
                    query = query.Where(x => x.ApplicantName == req.ApplicantName);
                }

                if (!string.IsNullOrEmpty(req.InsuredName))
                {
                    query = query.Where(x => x.InsuredName == req.InsuredName);
                }

                if (!string.IsNullOrEmpty(req.PolicyPeriodStartDate))
                {
                    query = query.Where(x => Convert.ToDateTime(x.PolicyPeriodDate).Date >= Convert.ToDateTime(req.PolicyPeriodStartDate).Date);
                }

                if (!string.IsNullOrEmpty(req.PolicyPeriodEndDate))
                {
                    DateTime EndDate = Convert.ToDateTime(req.PolicyPeriodEndDate).Date;
                    if (EndDate.Date < DateTime.MaxValue.Date)
                    {
                        EndDate.AddDays(1);
                    }
                    query = query.Where(x => Convert.ToDateTime(x.PolicyPeriodDate).Date < EndDate);
                }

                if (!string.IsNullOrEmpty(req.ExpiredStartDate))
                {
                    query = query.Where(x => Convert.ToDateTime(x.ExpiredDate).Date >= Convert.ToDateTime(req.ExpiredStartDate).Date);

                }

                if (!string.IsNullOrEmpty(req.ExpiredEndDate))
                {
                    DateTime EndDate = Convert.ToDateTime(req.ExpiredEndDate).Date;
                    if (EndDate.Date < DateTime.MaxValue.Date)
                    {
                        EndDate.AddDays(1);
                    }
                    query = query.Where(x => Convert.ToDateTime(x.ExpiredDate).Date < EndDate);
                }
                #endregion

                var totalCount = query.Count();

                var policySearchRsp = query.Select(x => new PolicySearchResp()
                {
                    PolicyNo = x.PolicyNo,
                    PlanCategoryStr = x.PlanCategoryStr,
                    ApplicantName = x.ApplicantName,
                    InsuredName = x.InsuredName,
                    PolicyStatusStr = x.PolicyStatusStr,
                    ProductCampaignName = x.ProductCampaignName,
                    CaseSourceStr = x.CaseSourceStr,
                    PolicyPeriodDate = x.PolicyPeriodDate,
                    ExpiredDate = x.ExpiredDate
                }).OrderBySortInfo(req.order, req.columns)
                .Pagination(req.length, req.start).ToList();

                return new DataTableQueryResp<PolicySearchResp>(policySearchRsp, totalCount);
            }
            catch (Exception ex)
            {
                //eventLog.Debug("PolicyService", "PolicySearch", "PolicySearch", ex);
                throw;
            }
        }

        /// <summary>
        /// 保單資料查詢-明細頁-要/被保人資料-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>要被保人資料 Resp</returns>
        public PolicyApplicantInfoResp PolicyApplicantInfo(PolicyReq req)
        {
            //要保人資料
            var applicant = this.policyPersonInfoRepo.Queryable()
            .FirstOrDefault(o => o.PolicyNo == req.PolicyNo && o.PersonType == PersonType.Applicant);

            //被保人資料
            var insured = this.policyPersonInfoRepo.Queryable()
            .FirstOrDefault(o => o.PolicyNo == req.PolicyNo && o.PersonType == PersonType.Insured);

            if (applicant == null)
            {
                throw new BusinessException("要/被保人資料,投保相關人員檔查無要保人資料");
            }

            if (insured == null)
            {
                throw new BusinessException("要/被保人資料,投保相關人員檔查無被保人資料");
            }

            //組成 resp
            var applicantInfo = GetPolicyApplicantInfo(applicant);

            var insuredInfo = GetPolicyApplicantInfo(insured);

            var result = new PolicyApplicantInfoResp()
            {
                Applicant = applicantInfo,
                Insured = insuredInfo
            };

            return result;
        }

        /// <summary>
        /// 保單資料查詢-明細頁-保單/繳費資訊-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>繳費資訊 Resp</returns>
        public PolicyPaymentInfoResp PolicyPaymentInfo(PolicyReq req)
        {
            //保單主檔
            var policyMaster = this.policyMasterRepo.Queryable()
                .FirstOrDefault(o => o.PolicyNo == req.PolicyNo); //保單號碼

            if (policyMaster == null)
            {
                throw new BusinessException("保單/繳費資訊,保單主檔查無資料");
            }

            //付款人資料
            var payer = this.policyPersonInfoRepo.Queryable()
            .FirstOrDefault(o => o.PolicyNo == policyMaster.PolicyNo && o.PersonType == PersonType.Payer);

            //保單車籍資料
            var policyCarInfo = this.policyCarInfoRepo.Queryable()
            .FirstOrDefault(o => o.PolicyNo == policyMaster.PolicyNo);

            //保單KYC資料
            var kycIds = this.commonService.GetKycIds(KycType.PremiumFrom);
            var premiumFromAnswerValue = this.kycRecordV1Repo.Queryable()
                .Where(o => kycIds.Contains(o.KycId)).FirstOrDefault()?.AnswerValue;

            //專案主檔
            var productMaster = this.productMasterRepo.Queryable()
                .FirstOrDefault(o => o.ProductId == policyMaster.ProductId);

            if (productMaster == null)
            {
                throw new BusinessException("保單/繳費資訊,專案主檔查無資料");
            }

            var result = new PolicyPaymentInfoResp();

            result.PolicyInfo = new PolicyInfo();
            result.PolicyInfo.CaseSourceStr = this.commonService.GetSysParamValue(GroupIdType.CaseSource, policyMaster.CaseSource);
            result.PolicyInfo.AcceptNo = policyMaster.AcceptNo;
            result.PolicyInfo.AcceptDate = policyMaster.AcceptDate.ToString("yyyy/MM/dd  HH:mm:ss");
            result.PolicyInfo.PolicyStatusStr = this.commonService.GetSysParamValue(GroupIdType.PolicyStatus, policyMaster.PolicyStatus);
            result.PolicyInfo.PaymentStatusStr = this.commonService.GetSysParamValue(GroupIdType.PaymentStatus, policyMaster.PaymentStatus);
            result.PolicyInfo.PolicySendTypeStr = this.commonService.GetSysParamValue(GroupIdType.PolicySendType, policyMaster.PolicySendType);
            result.PolicyInfo.PolicyConfStr = (policyMaster.PolicyConf.HasValue && policyMaster.PolicyConf.Value) ? YesNoType.YesStr : YesNoType.NoStr;
            result.PolicyInfo.PremiumMethodStr = this.commonService.GetSysParamValue(GroupIdType.PremiumMethod, policyMaster.PremiumMethod);
            result.PolicyInfo.ModalPermiumWithDisc = policyMaster.ModalPermiumWithDisc;
            result.PolicyInfo.PremiumFromStr = this.commonService.GetSysParamValue(GroupIdType.PremiumFrom, premiumFromAnswerValue);
            result.PolicyInfo.EffectiveDate = policyMaster.PolicyPeriodS?.ToString("yyyy/MM/dd HH:mm:ss");
            result.PolicyInfo.ExpiredDate = policyMaster.PolicyPeriodE?.ToString("yyyy/MM/dd HH:mm:ss");
            if (this.CheckIsVehicle(this.commonService.ConvertPlanTypeToInsType(productMaster.PlanType))) //商品險別為汽車險、機車險
            {
                result.PolicyInfo.CompulsoryNo = policyCarInfo?.CompulsoryNo;
            }
            else //商品險別非汽車險、機車險
            {
                result.PolicyInfo.CompulsoryNo = null;
            }

            result.PayMentInfo = new PayMentInfo();
            result.PayMentInfo.PaidTypeStr = this.commonService.GetSysParamValue(GroupIdType.PaidType, policyMaster.PaidType);
            result.PayMentInfo.PayerName = payer?.PersonName;
            result.PayMentInfo.IdNo = payer?.Idno;
            result.PayMentInfo.PersonRelationOfApplicantStr = this.commonService.GetSysParamValue(GroupIdType.PersonRelation, payer?.PersonRelationOfApplicant);
            result.PayMentInfo.AccountNo = this.GetMaskAccountNo(policyMaster.PaidType, policyMaster.AccountNo);
            return result;
        }

        /// <summary>
        /// 保單資料查詢-明細頁-投保內容-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>保單資料查詢 Resp</returns>
        public PolicyContentInfoResp PolicyContentInfo(PolicyReq req)
        {
            //保單主檔
            var policyMaster = this.policyMasterRepo.Queryable()
                .FirstOrDefault(o => o.PolicyNo == req.PolicyNo); //保單號碼

            if (policyMaster == null)
            {
                throw new BusinessException("投保內容,保單主檔查無資料");
            }

            //保單車籍資料
            var policyCarInfo = this.policyCarInfoRepo.Queryable()
                .FirstOrDefault(o => o.PolicyNo == policyMaster.PolicyNo);

            //專案主檔
            var productMaster = this.productMasterRepo.Queryable()
                .FirstOrDefault(o => o.ProductId == policyMaster.ProductId);

            if (productMaster == null)
            {
                throw new BusinessException("投保內容,專案主檔查無資料");
            }

            //方案主檔
            var campaignMaster = this.campaignMasterRepo.Queryable()
                .FirstOrDefault(o => o.ProductId == policyMaster.ProductId && o.ProjectCode == policyMaster.ProjectCode);

            var result = new PolicyContentInfoResp();

            var isVehicle = this.CheckIsVehicle(this.commonService.ConvertPlanTypeToInsType(productMaster.PlanType));
            if (isVehicle) //商品險別 = 汽車險、機車險
            {
                if (policyCarInfo == null)
                {
                    throw new BusinessException("投保內容,保單車籍資料查無資料");
                }

                result.VehicleInfo = new VehicleInfo();
                result.VehicleInfo.LicensePlateNo = policyCarInfo.LicensePlateNo;
                result.VehicleInfo.BrandStr = policyCarInfo.NbrandAbbr.Concat('/', policyCarInfo.Nbrand).Concat('-', policyCarInfo.CarType); // 廠牌名稱/廠牌車型中文名稱-車型編號
                result.VehicleInfo.VehicleTypeStr = policyCarInfo.VehicleType.Concat('-', this.commonService.GetSysParamValue(GroupIdType.VehicleType, policyCarInfo.VehicleType));
                result.VehicleInfo.ManufacturedDate = policyCarInfo.ManufacturedDate.Insert(4, "/"); // yyyy/MM
                result.VehicleInfo.RegistIssueDate = policyCarInfo.RegistIssueDate.ToTaiwanDateTimeStr();
                result.VehicleInfo.Displacement = policyCarInfo.Displacement;
                result.VehicleInfo.DisplacementUnit = this.commonService.GetSysParamValue(GroupIdType.DisplacementUnit, policyCarInfo.DisplacementUnit);
                result.VehicleInfo.EngineId = policyCarInfo.EngineId;
                result.VehicleInfo.CarBodyID = policyCarInfo.CarBodyId;
            }
            else
            {
                // 無邏輯
            }

            result.PolicyDetailInfo = new PolicyDetailInfo();
            result.PolicyDetailInfo.ProductName = productMaster.ProductId.Concat(' ', productMaster.Name);
            result.PolicyDetailInfo.CampaignName = campaignMaster?.Name;
            result.PolicyDetailInfo.IsCompulsory = policyMaster.PlanCategory == PlanCategory.CALI;
            if (isVehicle) //商品險別 = 汽車險、機車險
            {
                if (result.PolicyDetailInfo.IsCompulsory) //商品類型=強制險
                {
                    result.PolicyDetailInfo.CompulsoryPeriod = new PolicyDatePeriod
                    {
                        PolicyPeriodS = policyMaster.PolicyPeriodS?.ToString("yyyy/MM/dd HH:mm"),
                        PolicyPeriodE = policyMaster.PolicyPeriodE?.ToString("yyyy/MM/dd HH:mm")
                    };
                }
                else //商品類型≠強制險
                {
                    result.PolicyDetailInfo.ArbitraryPeriod = new PolicyDatePeriod
                    {
                        PolicyPeriodS = policyMaster.PolicyPeriodS?.ToString("yyyy/MM/dd HH:mm"),
                        PolicyPeriodE = policyMaster.PolicyPeriodE?.ToString("yyyy/MM/dd HH:mm")
                    };
                }
            }
            else
            {
                // 無邏輯
            }

            //保單明細檔 (多筆)
            var policyDetails = this.policyDetailRepo.Queryable()
                .Where(o =>
                o.PolicyNo == policyMaster.PolicyNo && //依 保單主檔.保單號碼 篩選
                o.ProductId == policyMaster.ProductId && //依 保單主檔.專案代碼 篩選
                o.ProjectCode == policyMaster.ProjectCode //依 保單主檔.方案代碼 篩選
                );

            result.PolicyDetailInfo.PolicyDetails = policyDetails
                .ToList()
                .Select(o =>
                {
                    //商品主檔 (單筆)
                    var planMaster = this.planMasterRepo.Queryable()
                        .Where(x =>
                        x.CompanyCode == productMaster.CompanyCode //依 專案主檔.保險公司代碼 篩選
                        && x.PlanCode == o.PlanCode
                        && x.PlanVer == o.PlanVer
                        && x.PaymentTerm == o.PaymentTerm
                        && x.PlanType == policyMaster.PlanType //商品險別
                        && x.BenefitTerm == o.BenefitTerm
                        ).FirstOrDefault();

                    if (planMaster == null)
                    {
                        throw new BusinessException("投保內容,保單明細檔商品查無商品主檔資料");
                    }

                    return new PolicyPlanData
                    {
                        PlanName = planMaster.PlanCode.Concat(' ', planMaster.PlanShortName),
                        CoveredItem = o.CoveredItem,
                        ItemAmount = o.Assured,
                        Unit = string.Empty,
                        ModalPlanPremium = o.ModalPlanPremium,
                        DiscPlanPremium = o.DiscPlanPremium,
                        ModalItemPermiumWithDisc = o.ModalItemPermiumWithDisc
                    };
                })
                .ToList();

            result.PolicyDetailInfo.TotalModalPlanPremium = policyDetails
                .Where(o => o.ModalPlanPremium.HasValue)
                .Sum(o => o.ModalPlanPremium.Value);

            result.PolicyDetailInfo.TotalDiscPlanPremium = policyDetails
                .Where(o => o.DiscPlanPremium.HasValue)
                .Sum(o => o.DiscPlanPremium.Value);

            result.PolicyDetailInfo.TotalModalItemPermiumWithDisc = policyDetails
                .Where(o => o.ModalItemPermiumWithDisc.HasValue)
                .Sum(o => o.ModalItemPermiumWithDisc.Value);

            return result;
        }

        /// <summary>
        /// 保單資料查詢-明細頁-受益人資料-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>受益人資訊 Resp</returns>
        public PolicyBeneficiaryInfoResp PolicyBeneficiaryInfo(PolicyReq req)
        {
            var policyMaster = this.policyMasterRepo.Queryable()
            .FirstOrDefault(o => o.PolicyNo == req.PolicyNo);

            if (policyMaster == null)
            {
                throw new BusinessException("受益人資料,保單主檔查無資料");
            }

            //判斷身故受益人分配方式
            var beneficiaryPayType = policyMaster.BeneficiaryPayType;

            var beneficiaryInfo = new List<Beneficiary>();

            if (beneficiaryPayType == BeneficiaryPayType.LegalHeir) //法定繼承人
            {
                var Info = new Beneficiary()
                {
                    BeneficiarySeq = null,
                    BeneficiaryPercent = null,
                    Name = null,
                    IdNo = null,
                    PersonRelationOfApplicantStr = null,
                    Mobile = null,
                    AddrStr = null
                };

                beneficiaryInfo.Add(Info);
            }
            else //均分、比例、順位
            {
                //找出要保人的地址拿來跟受益人對照
                var policyPersonInfo = this.policyPersonInfoRepo.Queryable().FirstOrDefault(o => o.PolicyNo == req.PolicyNo && o.PersonType == PersonType.Applicant);
                var applicantAddr = PolicyPersonInfoAddr(policyPersonInfo);

                //受益人資料(list)
                var beneficiary = this.policyPersonInfoRepo.Queryable().Where(o => o.PolicyNo == req.PolicyNo && o.PersonType == PersonType.Beneficiary).ToList();

                //順位用 BeneficiarySeq 升冪排序
                if (beneficiaryPayType == BeneficiaryPayType.Rank)
                {
                    beneficiary.OrderBy(item => int.TryParse(item.BeneficiarySeq, out int index) ? index : int.MaxValue);
                }
                //比例用 BeneficiaryPercent 降冪排序
                else if (beneficiaryPayType == BeneficiaryPayType.Ratio)
                {
                    beneficiary.OrderByDescending(item => int.TryParse(item.BeneficiaryPercent, out int index) ? index : int.MinValue);
                }

                foreach (var b in beneficiary)
                {
                    string beneficiaryAddr = PolicyPersonInfoAddr(b);
                    var Info = new Beneficiary()
                    {
                        BeneficiarySeq = beneficiaryPayType != BeneficiaryPayType.Rank ? null : b.BeneficiarySeq,
                        BeneficiaryPercent = beneficiaryPayType != BeneficiaryPayType.Ratio ? null : b.BeneficiaryPercent,
                        Name = b.PersonName,
                        IdNo = b.Idno,
                        PersonRelationOfApplicantStr = this.commonService.GetSysParamValue(GroupIdType.PersonRelation, b.PersonRelationOfApplicant),
                        Mobile = b.Mobile,
                        AddrStr = String.Compare(applicantAddr, beneficiaryAddr, CultureInfo.CurrentCulture, CompareOptions.IgnoreCase | CompareOptions.IgnoreSymbols) == 0 ? "同要保人" : beneficiaryAddr
                    };

                    beneficiaryInfo.Add(Info);
                };
            }

            var result = new PolicyBeneficiaryInfoResp()
            {
                BeneficiaryPayTypeStr = this.commonService.GetSysParamValue(GroupIdType.BeneficiaryPayType, beneficiaryPayType),
                Beneficiary = beneficiaryInfo
            };

            return result;
        }

        /// <summary>
        /// 新契約資料查詢-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">新契約資料查詢 Req</param>
        /// <returns>新契約資料查詢 Resp</returns>
        public DataTableQueryResp<ContractSearchResp> ContractSearch(ContractSearchReq req)
        {
            try
            {
                this.commonService.DateCheck(req.AcceptStartDate, req.AcceptEndDate, "進件日期", true);
                this.commonService.DateCheck(req.PolicyPeriodStartDate, req.PolicyPeriodEndDate, "生效日期", true);

                DateTime? acceptStartDate = default(DateTime?);
                DateTime? acceptEndDate = default(DateTime?);
                DateTime? policyPeriodStartDate = default(DateTime?);
                DateTime? policyPeriodEndDate = default(DateTime?);

                if (!string.IsNullOrEmpty(req.AcceptStartDate))
                {
                    acceptStartDate = Convert.ToDateTime(req.AcceptStartDate).Date;
                }

                if (!string.IsNullOrEmpty(req.AcceptEndDate))
                {
                    DateTime EndDate = Convert.ToDateTime(req.AcceptEndDate).Date;
                    if (EndDate.Date < DateTime.MaxValue.Date)
                    {
                        EndDate.AddDays(1);
                    }
                    acceptEndDate = EndDate;
                }

                if (!string.IsNullOrEmpty(req.PolicyPeriodStartDate))
                {
                    policyPeriodStartDate = Convert.ToDateTime(req.PolicyPeriodStartDate).Date;
                }

                if (!string.IsNullOrEmpty(req.PolicyPeriodEndDate))
                {
                    DateTime EndDate = Convert.ToDateTime(req.PolicyPeriodEndDate).Date;
                    if (EndDate.Date < DateTime.MaxValue.Date)
                    {
                        EndDate.AddDays(1);
                    }
                    policyPeriodEndDate = EndDate;
                }

                string countCmd = string.Empty;
                string sqlCmd = this.GetContractSearchCmd(req, out countCmd);
                var param = new
                {
                    AcceptNo = $"%{req.AcceptNo}%",
                    CaseSource = req.CaseSource,
                    AcceptStatus = req.AcceptStatus,
                    ApplicantId = $"%{req.ApplicantId}%",
                    InsuredId = $"%{req.InsuredId}%",
                    ApplicantName = $"%{req.ApplicantName}%",
                    InsuredName = $"%{req.InsuredName}%",
                    AcceptStartDate = acceptStartDate,
                    AcceptEndDate = acceptEndDate,
                    PolicyPeriodStartDate = policyPeriodStartDate,
                    PolicyPeriodEndDate = policyPeriodEndDate,
                    PendingStatus = req.PendingStatus,
                    ApplicantParam = PersonType.Applicant,
                    InsuredParam = PersonType.Insured,
                    PlanCategoryGroupId = GroupIdType.PlanCategory,
                    AcceptStatusGroupId = GroupIdType.AcceptStatus,
                    CaseSourceGroupId = GroupIdType.CaseSource,
                    PolicySignStatus = PolicySignStatus.Unsigned
                };

                using (var conn = new SqlConnection(ConfigHelper.GetINSCon()))
                {
                    conn.Open();
                    var total = conn.Query<int>(countCmd, param).Single();
                    var contractInfoRes = conn.Query<ContractSearchResp>(sqlCmd, param);
                    return new DataTableQueryResp<ContractSearchResp>(contractInfoRes, total);
                }
            }
            catch (Exception ex)
            {
                //eventLog.Debug("PolicyService", "ContractSearch", "ContractSearch", ex);
                throw;
            }
        }

        /// <summary>
        /// 取得 新契約資料查詢-查詢頁-查詢資料(ContractSearch) Sql Command
        /// </summary>
        /// <param name="req"></param>
        /// <param name="countCmd"></param>
        /// <returns></returns>
        private string GetContractSearchCmd(ContractSearchReq req, out string countCmd)
        {
            var sqlCmd = string.Empty;
            string where = string.Empty;

            // 排序、分頁條件
            var order = req.GetSortCmd(typeof(ContractSearchResp));

            if (!string.IsNullOrEmpty(req.AcceptNo))
            {
                where += "AND pm.AcceptNo LIKE @AcceptNo ";
            }

            if (!string.IsNullOrEmpty(req.CaseSource))
            {
                where += "AND pm.CaseSource = @CaseSource ";
            }

            if (!string.IsNullOrEmpty(req.AcceptStatus))
            {
                where += "AND pm.AcceptStatus = @AcceptStatus ";
            }

            if (!string.IsNullOrEmpty(req.ApplicantId))
            {
                where += "AND pm.ApplicantId LIKE @ApplicantId ";
            }

            if (!string.IsNullOrEmpty(req.InsuredId))
            {
                where += "AND pm.InsuredId LIKE @InsuredId ";
            }

            if (!string.IsNullOrEmpty(req.ApplicantName))
            {
                where += "AND pm.ApplicantName LIKE @ApplicantName ";
            }

            if (!string.IsNullOrEmpty(req.InsuredName))
            {
                where += "AND pm.InsuredName LIKE @InsuredName ";
            }

            if (!string.IsNullOrEmpty(req.AcceptStartDate))
            {
                where += "AND pm.AcceptDate >= @AcceptStartDate ";
            }

            if (!string.IsNullOrEmpty(req.AcceptEndDate))
            {
                where += "AND pm.AcceptDate < @AcceptEndDate ";
            }

            if (!string.IsNullOrEmpty(req.PolicyPeriodStartDate))
            {
                where += "AND pm.PolicyPeriodS >= @PolicyPeriodStartDate ";
            }

            if (!string.IsNullOrEmpty(req.PolicyPeriodEndDate))
            {
                where += "AND pm.PolicyPeriodS < @PolicyPeriodEndDate ";
            }

            if (!string.IsNullOrEmpty(req.PendingStatus))
            {
                where += "AND pm.PendingStatus = @PendingStatus ";
            }
            var sourceName = "UnionResult";
            var sourceCmd = $@"
;WITH {sourceName} AS
(
	SELECT
		pm.AcceptNo
		,ap.IDNo AS ApplicantId
		,ins.IDNo AS InsuredId
		,ap.PersonName AS ApplicantName
		,ins.PersonName AS InsuredName
		,pm.PlanCategory
		,pm.AcceptStatus
		,pm.ProductId
		,pm.ProjectCode
		,pm.CaseSource
		,pm.AcceptDate
		,pm.PolicyPeriodS
		,pm.PendingStatus
	FROM PolicyMaster pm
	OUTER APPLY (SELECT TOP 1 * FROM PolicyPersonInfo i WHERE i.PersonType = @ApplicantParam AND i.AcceptNo = pm.AcceptNo AND i.PlanCategory = pm.PlanCategory) ap --投保相關人員檔(要保人)
	OUTER APPLY (SELECT TOP 1 * FROM PolicyPersonInfo ins WHERE ins.PersonType = @InsuredParam AND ins.AcceptNo = pm.AcceptNo AND ins.PlanCategory = pm.PlanCategory) ins --投保相關人員檔(被保人)
	WHERE pm.PolicyNo IS NULL OR pm.PolicyNo = ''
	UNION
	SELECT 
		tpm.AcceptNo
		,ap.IDNo AS ApplicantId
		,ins.IDNo AS InsuredId
		,ap.PersonName AS ApplicantName
		,ins.PersonName AS InsuredName
		,tpm.PlanCategory
		,tpm.AcceptStatus
		,tpm.ProductId
		,tpm.ProjectCode
		,tpm.CaseSource
		,tpm.AcceptDate
		,tpm.PolicyPeriodS
		,tpm.PendingStatus
	FROM Temp_PolicyMaster tpm
	JOIN PolicySignMaster psm
	ON tpm.AcceptNo = psm.AcceptNo
	OUTER APPLY (SELECT TOP 1 * FROM Temp_PolicyPersonInfo i WHERE i.PersonType = @ApplicantParam AND i.AcceptNo = tpm.AcceptNo) ap --投保相關人員暫存檔(要保人)
	OUTER APPLY (SELECT TOP 1 * FROM Temp_PolicyPersonInfo ins WHERE ins.PersonType = @InsuredParam AND ins.AcceptNo = tpm.AcceptNo) ins --投保相關人員暫存檔(被保人)
	WHERE tpm.PolicySignStatus >= @PolicySignStatus
)";

            sqlCmd = $@"
{sourceCmd}
SELECT 
	pm.AcceptNo
	,pm.PlanCategory
	,(SELECT ItemValue FROM SysParam WHERE GroupId = @PlanCategoryGroupId AND ItemId = pm.PlanCategory) AS PlanCategoryStr
	,pm.ApplicantName
	,pm.InsuredName
	,(SELECT ItemValue FROM SysParam WHERE GroupId = @AcceptStatusGroupId AND ItemId = pm.AcceptStatus) AS AcceptStatusStr
	,prm.ProductId + ' ' + prm.Name + ' / '+ (SELECT Name FROM CampaignMaster WHERE ProductId = pm.ProductId AND ProjectCode = pm.ProjectCode) AS ProductCampaignName
	,(SELECT ItemValue FROM SysParam WHERE GroupId = @CaseSourceGroupId AND ItemId = pm.CaseSource) AS CaseSourceStr
	,CONVERT(VARCHAR, pm.AcceptDate, 111) AS AcceptDate
	,CONVERT(VARCHAR, pm.PolicyPeriodS, 111) AS PolicyPeriodDate
FROM {sourceName} AS pm
LEFT JOIN ProductMaster AS prm ON prm.ProductId = pm.ProductId
WHERE 1 = 1
{where}
{order} ";

            countCmd = $@"
{sourceCmd}
SELECT 
    COUNT(*)
From {sourceName} AS pm
WHERE 1 = 1
{where} ";

            return sqlCmd;
        }

        /// <summary>
        /// policyPersonInfo 轉換成 PolicyApplicantInfo
        /// </summary>
        /// <param name="policyPersonInfo">投保相關人員檔</param>
        /// <returns></returns>
        private PolicyApplicantInfo GetPolicyApplicantInfo(PolicyPersonInfo policyPersonInfo)
        {
            return new PolicyApplicantInfo()
            {
                Name = policyPersonInfo.PersonName,
                IdNo = policyPersonInfo.Idno,
                HintIdNo = policyPersonInfo.Idno, //前端查詢用
                Nationality = policyPersonInfo.Nationality,
                BirthdayStr = policyPersonInfo.Birthday.HasValue ? policyPersonInfo.Birthday?.ToString("yyyy/MM/dd") : string.Empty,
                GenderStr = this.commonService.GetSysParamValue(GroupIdType.Gender, policyPersonInfo.Gender),
                Email = policyPersonInfo.Email,
                MaritalStatusStr = this.commonService.GetSysParamValue(GroupIdType.MaritalStatus, policyPersonInfo.MaritalStatus),
                Phone1 = policyPersonInfo.Phone1,
                Phone2 = policyPersonInfo.Phone2,
                Mobile = policyPersonInfo.Mobile,
                AddrStr = PolicyPersonInfoAddr(policyPersonInfo),
                HouseholdAddrStr = PolicyPersonInfoAddr(policyPersonInfo, true),
            };
        }

        /// <summary>
        /// 新契約資料查詢-明細頁-要/被保人資料-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>要被保人資料 Resp</returns>
        public PolicyApplicantInfoResp ContractApplicantInfo(ContractReq req)
        {
            var isOfficial = IsOfficialPolicyInfo(req.AcceptNo, req.PlanCategory);
            PolicyPersonInfo applicant = null; //要保人資料
            PolicyPersonInfo insured = null; //被保人資料

            if (isOfficial)
            {
                applicant = this.policyPersonInfoRepo.Queryable()
                    .FirstOrDefault(o => o.AcceptNo == req.AcceptNo && o.PlanCategory == req.PlanCategory && o.PersonType == PersonType.Applicant);

                insured = this.policyPersonInfoRepo.Queryable()
                    .FirstOrDefault(o => o.AcceptNo == req.AcceptNo && o.PlanCategory == req.PlanCategory && o.PersonType == PersonType.Insured);
            }
            else
            {
                var tempApplicant = this.tempPolicyPersonInfoRepo.Queryable()
                    .FirstOrDefault(o => o.AcceptNo == req.AcceptNo && string.IsNullOrEmpty(o.PolicyNo) && o.PersonType == PersonType.Applicant);

                var tempInsured = this.tempPolicyPersonInfoRepo.Queryable()
                    .FirstOrDefault(o => o.AcceptNo == req.AcceptNo && string.IsNullOrEmpty(o.PolicyNo) && o.PersonType == PersonType.Insured);

                applicant = this.mapper.Map<PolicyPersonInfo>(tempApplicant);
                insured = this.mapper.Map<PolicyPersonInfo>(tempInsured);
            }

            if (applicant == null)
            {
                throw new BusinessException("要/被保人資料,投保相關人員檔查無要保人資料");
            }

            if (insured == null)
            {
                throw new BusinessException("要/被保人資料,投保相關人員檔查無被保人資料");
            }

            //組成 resp
            var applicantInfo = GetPolicyApplicantInfo(applicant);

            var insuredInfo = GetPolicyApplicantInfo(insured);

            var result = new PolicyApplicantInfoResp()
            {
                Applicant = applicantInfo,
                Insured = insuredInfo
            };

            return result;
        }

        /// <summary>
        /// 新契約資料查詢-明細頁-保單/繳費資訊-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>繳費資訊 Resp</returns>
        public PolicyPaymentInfoResp ContractPaymentInfo(ContractReq req)
        {
            var isOfficial = IsOfficialPolicyInfo(req.AcceptNo, req.PlanCategory);
            PolicyMaster policyMaster = null;
            if (isOfficial)
            {
                policyMaster = this.policyMasterRepo.Queryable()
                .FirstOrDefault(o => o.AcceptNo == req.AcceptNo && o.PlanCategory == req.PlanCategory); //進件編號
            }
            else
            {
                var tempPolicyMaster = this.tempPolicyMasterRepo.Queryable()
                    .FirstOrDefault(o => o.AcceptNo == req.AcceptNo && o.PlanCategory == req.PlanCategory); //進件編號
                policyMaster = this.mapper.Map<PolicyMaster>(tempPolicyMaster);
            }
            if (policyMaster == null)
            {
                var str = isOfficial ? string.Empty : "暫存";
                throw new BusinessException($"保單/繳費資訊,保單{str}主檔查無資料");
            }
            PolicyPersonInfo payer = null;
            PolicyCarInfo policyCarInfo = null;
            string premiumFromAnswerValue = string.Empty;
            var kycIds = this.commonService.GetKycIds(KycType.PremiumFrom);
            if (isOfficial)
            {
                //付款人資料
                payer = this.policyPersonInfoRepo.Queryable()
                    .FirstOrDefault(o => o.AcceptNo == req.AcceptNo && o.PlanCategory == req.PlanCategory && o.PersonType == PersonType.Payer);

                //保單車籍資料
                policyCarInfo = this.policyCarInfoRepo.Queryable()
                .FirstOrDefault(o => o.AcceptNo == policyMaster.AcceptNo && o.PlanCategory == req.PlanCategory);

            }
            else
            {
                //付款人資料
                var tempPayer = this.tempPolicyPersonInfoRepo.Queryable()
                    .FirstOrDefault(o => o.AcceptNo == req.AcceptNo && o.PersonType == PersonType.Payer);

                //保單車籍資料
                var tempPolicyCarInfo = this.tempPolicyCarInfoRepo.Queryable()
                .FirstOrDefault(o => o.AcceptNo == policyMaster.AcceptNo);

                payer = this.mapper.Map<PolicyPersonInfo>(tempPayer);
                policyCarInfo = this.mapper.Map<PolicyCarInfo>(tempPolicyCarInfo);
            }

            //專案主檔
            var productMaster = this.productMasterRepo.Queryable()
                .FirstOrDefault(o => o.ProductId == policyMaster.ProductId);

            if (productMaster == null)
            {
                throw new BusinessException("保單/繳費資訊,專案主檔查無資料");
            }

            var result = new PolicyPaymentInfoResp();

            result.PolicyInfo = new PolicyInfo();
            result.PolicyInfo.CaseSourceStr = this.commonService.GetSysParamValue(GroupIdType.CaseSource, policyMaster.CaseSource);
            result.PolicyInfo.AcceptNo = policyMaster.AcceptNo;
            result.PolicyInfo.AcceptDate = policyMaster.AcceptDate.ToString("yyyy/MM/dd  HH:mm:ss");
            result.PolicyInfo.AcceptStatusStr = this.commonService.GetSysParamValue(GroupIdType.AcceptStatus, policyMaster.AcceptStatus);
            result.PolicyInfo.PaymentStatusStr = this.commonService.GetSysParamValue(GroupIdType.PaymentStatus, policyMaster.PaymentStatus);
            result.PolicyInfo.PolicySendTypeStr = this.commonService.GetSysParamValue(GroupIdType.PolicySendType, policyMaster.PolicySendType);
            result.PolicyInfo.PolicyConfStr = (policyMaster.PolicyConf.HasValue && policyMaster.PolicyConf.Value) ? YesNoType.YesStr : YesNoType.NoStr;
            result.PolicyInfo.PremiumMethodStr = this.commonService.GetSysParamValue(GroupIdType.PremiumMethod, policyMaster.PremiumMethod);
            result.PolicyInfo.ModalPermiumWithDisc = policyMaster.ModalPermiumWithDisc;
            result.PolicyInfo.PremiumFromStr = this.commonService.GetSysParamValue(GroupIdType.PremiumFrom, premiumFromAnswerValue);

            if (this.CheckIsVehicle(this.commonService.ConvertPlanTypeToInsType(productMaster.PlanType))) //商品險別為汽車險、機車險
            {
                result.PolicyInfo.EffectiveDate = policyMaster.PolicyPeriodS?.ToString("yyyy/MM/dd HH:mm:ss");
                result.PolicyInfo.ExpiredDate = policyMaster.PolicyPeriodE?.ToString("yyyy/MM/dd HH:mm:ss");
                result.PolicyInfo.CompulsoryNo = policyCarInfo?.CompulsoryNo;
            }
            else //商品險別非汽車險、機車險
            {
                result.PolicyInfo.EffectiveDate = policyMaster.PolicyPeriodS?.ToString("yyyy/MM/dd HH:mm:ss");
                result.PolicyInfo.ExpiredDate = policyMaster.PolicyPeriodE?.ToString("yyyy/MM/dd HH:mm:ss");
                result.PolicyInfo.CompulsoryNo = null;
            }

            result.PayMentInfo = new PayMentInfo();
            result.PayMentInfo.PaidTypeStr = this.commonService.GetSysParamValue(GroupIdType.PaidType, policyMaster.PaidType);
            result.PayMentInfo.PayerName = payer?.PersonName;
            result.PayMentInfo.IdNo = payer?.Idno;
            result.PayMentInfo.PersonRelationOfApplicantStr = this.commonService.GetSysParamValue(GroupIdType.PersonRelation, payer?.PersonRelationOfApplicant);
            result.PayMentInfo.AccountNo = this.GetMaskAccountNo(policyMaster.PaidType, policyMaster.AccountNo);
            return result;
        }

        /// <summary>
        /// 新契約資料查詢-明細頁-投保內容-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>保單資料查詢 Resp</returns>
        public PolicyContentInfoResp ContractContentInfo(ContractReq req)
        {
            var isOfficial = IsOfficialPolicyInfo(req.AcceptNo, req.PlanCategory);
            var officeCialStatus = isOfficial ? string.Empty : "暫存";
            PolicyMaster policyMaster = null;
            PolicyCarInfo policyCarInfo = null;
            List<PolicyDetail> policyDetailList = null;
            if (isOfficial)
            {
                //保單主檔
                policyMaster = this.policyMasterRepo.Queryable()
                    .FirstOrDefault(o => o.AcceptNo == req.AcceptNo && o.PlanCategory == req.PlanCategory); //進件編號、商品類型
                                                                                                            //保單車籍資料
                policyCarInfo = this.policyCarInfoRepo.Queryable()
                    .FirstOrDefault(o => o.AcceptNo == req.AcceptNo && o.PlanCategory == req.PlanCategory);
                //保單明細
                policyDetailList = this.policyDetailRepo.Queryable()
                    .Where(o => o.AcceptNo == req.AcceptNo && o.PlanCategory == req.PlanCategory).ToList();
            }
            else
            {
                var tempPolicyMaster = this.tempPolicyMasterRepo.Queryable()
                    .FirstOrDefault(o => o.AcceptNo == req.AcceptNo && o.PlanCategory == req.PlanCategory); //進件編號、商品類型
                policyMaster = this.mapper.Map<PolicyMaster>(tempPolicyMaster);
                var tempPolicyCarInfo = this.tempPolicyCarInfoRepo.Queryable()
                    .FirstOrDefault(o => o.AcceptNo == req.AcceptNo);
                policyCarInfo = this.mapper.Map<PolicyCarInfo>(tempPolicyCarInfo);
                var tempPolicyDetailList = this.tempPolicyDetailRepo.Queryable()
                    .Where(o => o.AcceptNo == req.AcceptNo && o.PlanCategory == req.PlanCategory).ToList();
                policyDetailList = this.mapper.Map<List<TempPolicyDetail>, List<PolicyDetail>>(tempPolicyDetailList);
            }

            if (policyMaster == null)
            {
                throw new BusinessException($"投保內容,保單主檔{officeCialStatus}查無資料");
            }

            //專案主檔
            var productMaster = this.productMasterRepo.Queryable()
                .FirstOrDefault(o => o.ProductId == policyMaster.ProductId);

            if (productMaster == null)
            {
                throw new BusinessException("投保內容,專案主檔查無資料");
            }

            //方案主檔
            var campaignMaster = this.campaignMasterRepo.Queryable()
                .FirstOrDefault(o => o.ProductId == policyMaster.ProductId && o.ProjectCode == policyMaster.ProjectCode);

            var result = new PolicyContentInfoResp();

            var isVehicle = this.CheckIsVehicle(this.commonService.ConvertPlanTypeToInsType(productMaster.PlanType));

            if (isVehicle) //商品險別 = 汽車險、機車險
            {
                if (policyCarInfo == null)
                {
                    throw new BusinessException($"投保內容,保單車籍{officeCialStatus}資料查無資料");
                }

                result.VehicleInfo = new VehicleInfo();
                result.VehicleInfo.LicensePlateNo = policyCarInfo.LicensePlateNo;
                result.VehicleInfo.BrandStr = policyCarInfo.NbrandAbbr.Concat('/', policyCarInfo.Nbrand).Concat('-', policyCarInfo.CarType); // 廠牌名稱/廠牌車型中文名稱-車型編號
                result.VehicleInfo.VehicleTypeStr = policyCarInfo.VehicleType.Concat('-', this.commonService.GetSysParamValue(GroupIdType.VehicleType, policyCarInfo.VehicleType));
                result.VehicleInfo.ManufacturedDate = policyCarInfo.ManufacturedDate.Insert(4, "/"); // yyyy/MM
                result.VehicleInfo.RegistIssueDate = policyCarInfo.RegistIssueDate.ToTaiwanDateTimeStr();
                result.VehicleInfo.Displacement = policyCarInfo.Displacement;
                result.VehicleInfo.DisplacementUnit = this.commonService.GetSysParamValue(GroupIdType.DisplacementUnit, policyCarInfo.DisplacementUnit);
                result.VehicleInfo.EngineId = policyCarInfo.EngineId;
                result.VehicleInfo.CarBodyID = policyCarInfo.CarBodyId;
            }
            else
            {
                // 無邏輯
            }

            result.PolicyDetailInfo = new PolicyDetailInfo();
            result.PolicyDetailInfo.ProductName = productMaster.ProductId.Concat(' ', productMaster.Name);
            result.PolicyDetailInfo.CampaignName = campaignMaster?.Name;

            if (isVehicle) //商品險別 = 汽車險、機車險
            {
                if (policyMaster.PlanCategory == PlanCategory.CALI) //商品類型=強制險
                {
                    result.PolicyDetailInfo.CompulsoryPeriod = new PolicyDatePeriod();
                    result.PolicyDetailInfo.CompulsoryPeriod.PolicyPeriodS = policyMaster.PolicyPeriodS?.ToString("yyyy/MM/dd HH:mm");
                    result.PolicyDetailInfo.CompulsoryPeriod.PolicyPeriodE = policyMaster.PolicyPeriodE?.ToString("yyyy/MM/dd HH:mm");
                }
                else //商品類型≠強制險
                {
                    result.PolicyDetailInfo.ArbitraryPeriod = new PolicyDatePeriod();
                    result.PolicyDetailInfo.ArbitraryPeriod.PolicyPeriodS = policyMaster.PolicyPeriodS?.ToString("yyyy/MM/dd HH:mm");
                    result.PolicyDetailInfo.ArbitraryPeriod.PolicyPeriodE = policyMaster.PolicyPeriodE?.ToString("yyyy/MM/dd HH:mm");
                }
            }
            else
            {
                // 無邏輯
            }

            result.PolicyDetailInfo.PolicyDetails = policyDetailList
                .Select(o =>
                {
                    //商品主檔 (單筆)
                    var planMaster = this.planMasterRepo.Queryable()
                        .Where(x =>
                        x.CompanyCode == productMaster.CompanyCode //依 專案主檔.保險公司代碼 篩選
                        && x.PlanCode == o.PlanCode
                        && x.PlanVer == o.PlanVer
                        && x.PaymentTerm == o.PaymentTerm
                        && x.BenefitTerm == o.BenefitTerm
                        && x.PlanType == policyMaster.PlanType //商品險別
                        ).FirstOrDefault();

                    if (planMaster == null)
                    {
                        throw new BusinessException($"投保內容,保單明細{officeCialStatus}檔商品查無商品主檔資料");
                    }

                    return new PolicyPlanData
                    {
                        PlanName = planMaster.PlanCode.Concat(' ', planMaster.PlanShortName),
                        CoveredItem = o.CoveredItem,
                        ItemAmount = o.Assured,
                        Unit = string.Empty,
                        ModalPlanPremium = o.ModalPlanPremium,
                        DiscPlanPremium = o.DiscPlanPremium,
                        ModalItemPermiumWithDisc = o.ModalItemPermiumWithDisc
                    };
                })
                .ToList();

            result.PolicyDetailInfo.TotalModalPlanPremium = policyDetailList
                .Where(o => o.ModalPlanPremium.HasValue)
                .Sum(o => o.ModalPlanPremium.Value);

            result.PolicyDetailInfo.TotalDiscPlanPremium = policyDetailList
                .Where(o => o.DiscPlanPremium.HasValue)
                .Sum(o => o.DiscPlanPremium.Value);

            result.PolicyDetailInfo.TotalModalItemPermiumWithDisc = policyDetailList
                .Where(o => o.ModalItemPermiumWithDisc.HasValue)
                .Sum(o => o.ModalItemPermiumWithDisc.Value);

            return result;
        }

        /// <summary>
        /// 新契約資料查詢-明細頁-受益人資料-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>受益人資訊 Resp</returns>
        public PolicyBeneficiaryInfoResp ContractBeneficiaryInfo(ContractReq req)
        {
            var isOfficial = IsOfficialPolicyInfo(req.AcceptNo, req.PlanCategory);
            var officialStatus = isOfficial ? string.Empty : "暫存";
            PolicyMaster policyMaster = null;
            PolicyPersonInfo policyPersonInfo = null;
            List<PolicyPersonInfo> beneficiaries = new List<PolicyPersonInfo>(); //受益人資料(list)
            if (isOfficial)
            {
                policyMaster = this.policyMasterRepo.Queryable()
                    .FirstOrDefault(o => o.AcceptNo == req.AcceptNo && o.PlanCategory == req.PlanCategory);
                //找出要保人的地址拿來跟受益人對照
                policyPersonInfo = this.policyPersonInfoRepo.Queryable().Where(o => o.AcceptNo == req.AcceptNo && o.PersonType == PersonType.Applicant).FirstOrDefault();

                //受益人資料(list)
                beneficiaries = this.policyPersonInfoRepo.Queryable()
                .Where(o => o.AcceptNo == req.AcceptNo && o.PlanCategory == req.PlanCategory && o.PersonType == PersonType.Beneficiary).ToList();
            }
            else
            {
                var tempPolicyMaster = this.tempPolicyMasterRepo.Queryable()
                    .FirstOrDefault(o => o.AcceptNo == req.AcceptNo && o.PlanCategory == req.PlanCategory);

                var tempPersonInfo = this.tempPolicyPersonInfoRepo.Queryable().
                    FirstOrDefault(o => o.AcceptNo == req.AcceptNo && o.PersonType == PersonType.Applicant);
                policyMaster = this.mapper.Map<PolicyMaster>(tempPolicyMaster);
                policyPersonInfo = this.mapper.Map<PolicyPersonInfo>(tempPersonInfo);
                var tempBeneficiaries = this.tempPolicyPersonInfoRepo.Queryable().Where(o => o.AcceptNo == req.AcceptNo).ToList();
                beneficiaries = this.mapper.Map<List<TempPolicyPersonInfo>, List<PolicyPersonInfo>>(tempBeneficiaries);
            }

            if (policyMaster == null)
            {
                throw new BusinessException($"受益人資料,保單{officialStatus}主檔查無資料");
            }

            //判斷身故受益人分配方式
            var beneficiaryPayType = policyMaster.BeneficiaryPayType;

            var beneficiaryInfo = new List<Beneficiary>();

            if (beneficiaryPayType == BeneficiaryPayType.LegalHeir) //法定繼承人
            {
                var Info = new Beneficiary()
                {
                    BeneficiarySeq = null,
                    BeneficiaryPercent = null,
                    Name = null,
                    IdNo = null,
                    PersonRelationOfApplicantStr = null,
                    Mobile = null,
                    AddrStr = null
                };

                beneficiaryInfo.Add(Info);
            }
            else //均分、比例、順位
            {
                //找出要保人的地址拿來跟受益人對照
                var applicantAddr = PolicyPersonInfoAddr(policyPersonInfo);

                //順位用 BeneficiarySeq 升冪排序
                if (beneficiaryPayType == BeneficiaryPayType.Rank)
                {
                    beneficiaries = beneficiaries.OrderBy(item => int.TryParse(item.BeneficiarySeq, out int index) ? index : int.MaxValue).ToList();
                }
                //比例用 BeneficiaryPercent 降冪排序
                else if (beneficiaryPayType == BeneficiaryPayType.Ratio)
                {
                    beneficiaries = beneficiaries.OrderByDescending(item => int.TryParse(item.BeneficiaryPercent, out int index) ? index : int.MinValue).ToList();
                }

                foreach (var b in beneficiaries)
                {
                    string beneficiaryAddr = PolicyPersonInfoAddr(b);
                    var Info = new Beneficiary()
                    {
                        BeneficiarySeq = beneficiaryPayType != BeneficiaryPayType.Rank ? null : b.BeneficiarySeq,
                        BeneficiaryPercent = beneficiaryPayType != BeneficiaryPayType.Ratio ? null : b.BeneficiaryPercent,
                        Name = b.PersonName,
                        IdNo = b.Idno,
                        PersonRelationOfApplicantStr = this.commonService.GetSysParamValue(GroupIdType.PersonRelation, b.PersonRelationOfApplicant),
                        Mobile = b.Mobile,
                        AddrStr = String.Compare(applicantAddr, beneficiaryAddr, CultureInfo.CurrentCulture, CompareOptions.IgnoreCase | CompareOptions.IgnoreSymbols) == 0 ? "同要保人" : beneficiaryAddr
                    };

                    beneficiaryInfo.Add(Info);
                };
            }

            var result = new PolicyBeneficiaryInfoResp()
            {
                BeneficiaryPayTypeStr = this.commonService.GetSysParamValue(GroupIdType.BeneficiaryPayType, beneficiaryPayType),
                Beneficiary = beneficiaryInfo
            };

            return result;
        }

        /// <summary>
        /// 新契約資料查詢-明細頁-核保進度-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>核保進度訊息 Resp</returns>
        public ContractScheduleInfoResp ContractScheduleInfo(ContractReq req)
        {
            var isOfficial = IsOfficialPolicyInfo(req.AcceptNo, req.PlanCategory);
            var officialStatus = isOfficial ? string.Empty : "暫存";
            PolicyMaster policy = null;
            if (isOfficial)
            {
                //保單資料
                policy = this.policyMasterRepo.Queryable()
                .FirstOrDefault(o => o.AcceptNo == req.AcceptNo && o.PlanCategory == req.PlanCategory);
            }
            else
            {
                var tempPolicy = this.tempPolicyMasterRepo.Queryable()
                    .FirstOrDefault(o => o.AcceptNo == req.AcceptNo && o.PlanCategory == req.PlanCategory);
                policy = this.mapper.Map<PolicyMaster>(tempPolicy);
            }

            if (policy == null)
            {
                throw new BusinessException($"核保進度,保單{officialStatus}主檔查無資料");
            }

            var result = new ContractScheduleInfoResp()
            {
                PendingStatusStr = this.commonService.GetSysParamValue(GroupIdType.PendingStatus, policy.PendingStatus),
                ApplyDate = policy.ApplyDate.ToString("yyyy/MM/dd HH:mm:ss"),
                AcceptDate = policy.AcceptDate.ToString("yyyy/MM/dd HH:mm:ss"),
                PolicySignDate = policy.PolicySignDate?.ToString("yyyy/MM/dd HH:mm:ss"),
                SendInsComDate = policy.SendInsComDate?.ToString("yyyy/MM/dd HH:mm:ss"),
                InsComReceiveDate = policy.InsComReceiveDate?.ToString("yyyy/MM/dd HH:mm:ss"),
                InsComUWDate = policy.InsComUwdate?.ToString("yyyy/MM/dd HH:mm:ss"),
                InsComUWPassDate = policy.InsComUwpassDate?.ToString("yyyy/MM/dd HH:mm:ss"),
                TransactionDateStr = policy.TransactionDate.HasValue ? policy.TransactionDate.Value.ToString("yyyy/MM/dd") : string.Empty,
                PolicySendDateStr = policy.PolicySendDate.HasValue ? policy.PolicySendDate.Value.ToString("yyyy/MM/dd") : string.Empty,
            };

            return result;
        }

        /// <summary>
        /// 保險未成功件查詢-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">保險未成功件查詢 Req</param>
        /// <returns>保險未成功件查詢 Resp</returns>
        public DataTableQueryResp<TempPolicySearchResp> TempPolicySearch(TempPolicySearchReq req)
        {
            try
            {
                this.commonService.DateCheck(req.AcceptStartDate, req.AcceptEndDate, "進件日期", true);
                DateTime? acceptStartDate = default(DateTime?);
                DateTime? acceptEndDate = default(DateTime?);

                if (!string.IsNullOrEmpty(req.AcceptStartDate))
                {
                    DateTime startDate = Convert.ToDateTime(req.AcceptStartDate).Date;
                    acceptStartDate = startDate;
                }
                if (!string.IsNullOrEmpty(req.AcceptEndDate))
                {
                    DateTime EndDate = Convert.ToDateTime(req.AcceptEndDate).Date;
                    if (EndDate.Date < DateTime.MaxValue.Date)
                    {
                        EndDate.AddDays(1);
                    }
                    acceptEndDate = EndDate;
                }
                string countCmd = string.Empty;
                string sqlCmd = this.GetTempPolicySearchCmd(req, out countCmd);
                var param = new
                {
                    AcceptNo = $"%{req.AcceptNo}%",
                    CaseSource = req.CaseSource,
                    ApplicantId = $"%{req.ApplicantId}%",
                    InsuredId = $"%{req.InsuredId}%",
                    ApplicantName = $"%{req.ApplicantName}%",
                    InsuredName = $"%{req.InsuredName}%",
                    AcceptStartDate = acceptStartDate,
                    AcceptEndDate = acceptEndDate
                };

                using (var conn = new SqlConnection(ConfigHelper.GetINSCon()))
                {
                    conn.Open();
                    var total = conn.Query<int>(countCmd, param).Single();
                    var tempPolicyInfoRes = conn.Query<TempPolicySearchResp>(sqlCmd, param);
                    return new DataTableQueryResp<TempPolicySearchResp>(tempPolicyInfoRes, total);
                }
            }
            catch (Exception ex)
            {
                //eventLog.Debug("PolicyService", "TempPolicySearch", "TempPolicySearch", ex);
                throw;
            }
        }

        /// <summary>
        /// 取得 保險未成功件查詢-查詢頁-查詢資料(TempPolicySearch) Sql Command
        /// </summary>
        /// <param name="req"></param>
        /// <param name="countCmd"></param>
        /// <returns></returns>
        private string GetTempPolicySearchCmd(TempPolicySearchReq req, out string countCmd)
        {
            var sqlCmd = string.Empty;
            string where = string.Empty;

            // 排序、分頁條件
            var order = req.GetSortCmd(typeof(TempPolicySearchResp));

            if (!string.IsNullOrEmpty(req.AcceptNo))
            {
                where += "and pm.AcceptNo like @AcceptNo ";
            }

            if (!string.IsNullOrEmpty(req.CaseSource))
            {
                where += "and pm.CaseSource = @CaseSource ";
            }

            if (!string.IsNullOrEmpty(req.ApplicantId))
            {
                where += "and ap.IDNo like @ApplicantId ";
            }

            if (!string.IsNullOrEmpty(req.InsuredId))
            {
                where += "and ins.IDNo like @InsuredId ";
            }

            if (!string.IsNullOrEmpty(req.ApplicantName))
            {
                where += "and ap.PersonName like @ApplicantName ";
            }

            if (!string.IsNullOrEmpty(req.InsuredName))
            {
                where += "and ins.PersonName like @InsuredName ";
            }

            if (!string.IsNullOrEmpty(req.AcceptStartDate))
            {
                where += "and pm.AcceptDate >= @AcceptStartDate ";
            }

            if (!string.IsNullOrEmpty(req.AcceptEndDate))
            {
                where += "and pm.AcceptDate < @AcceptEndDate ";
            }

            sqlCmd = $@"
With
Applicants As --要保人
(
Select * from Temp_PolicyPersonInfo
Where PersonType='Applicant'
),
Insureds As --被保人
(
Select * from Temp_PolicyPersonInfo
Where PersonType='Insured'
),
SignLog As --案件查詢審核歷程
(
	Select * From
		(
		Select *,
		ROW_NUMBER() OVER(PARTITION BY p.AcceptNo ORDER BY p.LogDate DESC) RowNumber --以 AcceptNo 分組,並以 LogDate 降冪排序
		from PolicySignLog as p
		)　AS PolicySignLogRowNumber
	Where RowNumber = 1 --篩選第一筆資料
),
TPM AS
(
	SELECT AcceptNo, CaseSource, AcceptDate, ProductId, ProjectCode FROM Temp_PolicyMaster
	Group By AcceptNo, CaseSource, AcceptDate, ProductId, ProjectCode
)
Select
pm.AcceptNo
,ap.PersonName AS ApplicantName
,ins.PersonName AS InsuredName
,prm.ProductId+' '+prm.Name+' / '+(Select Name from CampaignMaster Where ProductId=pm.ProductId and ProjectCode = pm.ProjectCode) AS ProductCampaignName
,(Select ItemValue from SysParam Where GroupId='CaseSource' and ItemId = pm.CaseSource) AS CaseSourceStr
,Convert(varchar,pm.AcceptDate,111) AS AcceptDate
,psm.Msg AS SignInfo
From TPM as pm
Left join Applicants as ap on ap.AcceptNo = pm.AcceptNo
Left join Insureds as ins on ins.AcceptNo = pm.AcceptNo
Left join ProductMaster as prm on prm.ProductId = pm.ProductId
Left join SignLog as psm on psm.AcceptNo = pm.AcceptNo
where 1=1
{where}
{order} ";

            countCmd = $@"
With
Applicants As --要保人
(
Select * from Temp_PolicyPersonInfo
Where PersonType='Applicant'
),
Insureds As --被保人
(
Select * from Temp_PolicyPersonInfo
Where PersonType='Insured'
),
SignLog As --案件查詢審核歷程
(
	Select * From
		(
		Select *,
		ROW_NUMBER() OVER(PARTITION BY p.AcceptNo ORDER BY p.LogDate DESC) RowNumber --以 AcceptNo 分組,並以 LogDate 降冪排序
		from PolicySignLog as p
		)　AS PolicySignLogRowNumber
	Where RowNumber = 1 --篩選第一筆資料
),
TPM AS
(
	SELECT AcceptNo, CaseSource, AcceptDate, ProductId, ProjectCode FROM Temp_PolicyMaster
	Group By AcceptNo, CaseSource, AcceptDate, ProductId, ProjectCode
)

SELECT COUNT(*)
From TPM as pm
Left join Applicants as ap on ap.AcceptNo = pm.AcceptNo
Left join Insureds as ins on ins.AcceptNo = pm.AcceptNo
Left join ProductMaster as prm on prm.ProductId = pm.ProductId
Left join SignLog as psm on psm.AcceptNo = pm.AcceptNo
where 1=1
{where} ";

            return sqlCmd;
        }

        /// <summary>
        /// 查詢保單清單
        /// </summary>
        /// <param name="req">查詢保單清單 Req</param>
        /// <returns>保單查詢 Resp</returns>
        public async Task<StandardModel<GetListResp>> GetList(StandardModel<GetListReq> req)
        {
            // Log
            ////this.eventLog.Trace("GetList Start", req.Header.ItalIfId, req.Header.ItalIfId);

            // Log
            ////this.eventLog.Trace("Step1 GetList_CheckGetListReq_Start", req.Header.ItalIfId, req.Header.ItalIfId);

            // 資料檢核
            this.CheckGetListReq(req.Data, req.Header.CustId, req.Header.ItalIfId);

            // Log
            ////this.eventLog.Trace("Step1 GetList_CheckGetListReq_End", req.Header.ItalIfId, req.Header.ItalIfId);

            // Log
            ////this.eventLog.Trace("Step2 GetList_QueryPolicyMaster_Start", req.Header.ItalIfId, req.Header.ItalIfId);

            var custId = req.Header.CustId;

            //依照 Yumin 要求前端改版前先註解 this.validationService.CheckChannelMemberStatus(custId, req.Header.ItalIfId);

            // 資料處理
            // 以客戶唯一識別碼 + 險種類型+保單狀態，查找 PolicyMaster ，取得保單清單資料
            var policyMasters = this.policyMasterRepo.Queryable()
                .Where(p =>
                p.CustId == custId &&
                p.ExtendStatus != PolicyExtendStatus.Extended);

            //if (policyMasters == null || policyMasters.Count() == 0)
            ////this.eventLog.Log("LBEI004009", $"GetList Error: LBEI004009, CustId:{custId} ExtendStatus != 03 data not exist PolicyMaster", req.Header.ItalIfId, req.Header.ItalIfId);

            bool havePolicy = policyMasters.Where(p => p.PolicyStatus == PolicyStatusType.Active || p.PolicyStatus == PolicyStatusType.Expired).Any();

            // 如果 request 傳入 03，表示查詢 PolicyStatus = 01 及 03 的資料。
            if (req.Data.InsStatus == InsStatusType.All)
            {
                policyMasters = policyMasters
                    .Where(p => p.PolicyStatus == PolicyStatusType.Active || p.PolicyStatus == PolicyStatusType.Expired);

                //if (policyMasters == null || policyMasters.Count() == 0)
                ////this.eventLog.Log("LBEI004009", $"GetList Error: LBEI004009, CustId:{custId} PolicyStatus is 01 or 03 data not exist PolicyMaster", req.Header.ItalIfId, req.Header.ItalIfId);
            }
            else
            {
                var policyStatus = PolicyStatusType.Active;
                if (req.Data.InsStatus == InsStatusType.Expired)
                {
                    policyStatus = PolicyStatusType.Expired;
                }

                policyMasters = policyMasters.Where(p => p.PolicyStatus == policyStatus);

                //if (policyMasters == null || policyMasters.Count() == 0)
                ////this.eventLog.Log("LBEI004009", $"GetList Error: LBEI004009, CustId:{custId} PolicyStatus is {policyStatus} data not exist PolicyMaster", req.Header.ItalIfId, req.Header.ItalIfId);
            }

            // 判斷查詢險種 01-汽車險 02-機車險 03-全部
            if (req.Data.InsType != InsType.Vehicle)
            {
                var planType = ProductType.Car_2;
                if (InsType.Moto == req.Data.InsType)
                {
                    planType = ProductType.Moto;
                }

                policyMasters = policyMasters.Where(p => p.PlanType == planType);

                //if (policyMasters == null || policyMasters.Count() == 0)
                ////this.eventLog.Log("LBEI004009", $"GetList Error: LBEI004009, CustId:{custId} PlanType is {planType} data not exist PolicyMaster", req.Header.ItalIfId, req.Header.ItalIfId);
            }

            var getListResp = new GetListResp() { CarList = new List<CarListInfo>() };

            if (policyMasters == null || policyMasters.Count() == 0)
            {
                throw new ChannelAPIException(
                    ReturnCodes.Failure_InsChannel_LBEI004009, StandardResCodeType.NormalProcessing, getListResp);
            }

            // Log
            ////this.eventLog.Trace("Step2 GetList QueryPolicyMaster End", req.Header.ItalIfId, req.Header.ItalIfId);

            // Log
            ////this.eventLog.Trace("Step3 GetList GetCarListInfo Start", req.Header.ItalIfId, req.Header.ItalIfId);

            // 用保險公司代碼對應保險公司名稱用
            Dictionary<string, string> companyNameDic = new Dictionary<string, string>();
            Dictionary<string, string> projectNameDic = new Dictionary<string, string>();
            foreach (var policyMaster in policyMasters)
            {
                getListResp.CarList.Add(this.GetCarListInfo(policyMaster, ref companyNameDic, ref projectNameDic));
            }

            // Log
            ////this.eventLog.Trace("Step3 GetList GetCarListInfo End", req.Header.ItalIfId, req.Header.ItalIfId);

            // 依保單生效日最新到最舊排序
            getListResp.CarList = getListResp.CarList.OrderByDescending(o => o.SDate).ToList();

            // Log
            ////this.eventLog.Trace("GetList End", req.Header.ItalIfId, req.Header.ItalIfId);

            MemberProfile memberProfile = this.memberProfileRepo.Queryable().FirstOrDefault(m => m.CustId == custId);

            var memberProfileLog = new MemberProfileLog();
            var date = DateTime.Now;
            if (memberProfile != null && havePolicy)
            {
                string addYear = this.commonService.GetSysParamValue("BatchParam", "MemberPolicy");
                int year = 0;

                //if (string.IsNullOrEmpty(addYear))
                //    this.eventLog.Log("INS0000001", "GetList Update ExpirationDate Error: INS0000001, SysParam MemberPolicy is empty", req.Header.ItalIfId, req.Header.ItalIfId);
                //else if (!int.TryParse(addYear, out year))
                //    this.eventLog.Log("INS0000002", $"GetList Update ExpirationDate Error: INS0000002, SysParam MemberPolicy: {addYear}, format is not Integer", req.Header.ItalIfId, req.Header.ItalIfId);

                memberProfile.ExpirationDate = date.AddYears(year);
                memberProfile.SystemUseDay = date;
                memberProfile.UpdateOn = date;
                memberProfileLog.CustId = custId;
                memberProfileLog.LogDate = date;
                memberProfileLog.Msg = $"會員過期日: {memberProfile.ExpirationDate.Value.ToString("yyyy-MM-dd")}、保代系統使用日: 今日";
                memberProfileLog.ModifyEvent = "更新會員過期日、更新保代系統使用日";
                memberProfileLog.ModifyBy = ModifyBy.GetListAPI;

                using (var conn = new SqlConnection(ConfigHelper.GetINSCon()))
                {
                    conn.Open();
                    using (var trans = conn.BeginTransaction())
                    {
                        try
                        {
                            await conn.ExecuteAsync(MemberProfileCmd.Update, memberProfile, trans, commandTimeout: this.commandTimeout);

                            await conn.ExecuteAsync(MemberProfileLogCmd.Insert, memberProfileLog, trans, commandTimeout: this.commandTimeout);

                            await trans.CommitAsync();
                        }
                        catch (Exception ex)
                        {
                            ////this.eventLog.Log("INS0000004", "GetList Update ExpirationDate & SystemUseDay Commit Error", req.Header.ItalIfId, req.Header.ItalIfId, ex);
                            await trans.RollbackAsync();

                        }
                    }
                }
            }

            var messageHeader = this.commonService.GetChannelSuccessMessageHeader();

            return this.channelAPICommonService.GetResp<GetListResp>(
                req.Header, messageHeader, StandardResCodeType.NormalProcessing, getListResp);
        }

        /// <summary>
        /// 檢核傳入的查詢資料
        /// </summary>
        /// <param name="req">查詢保單清單 Req</param>
        /// <param name="custId">Cust Id</param>
        /// <param name="italIfId">ItalIf Id</param>
        /// <exception cref="ChannelAPIException"></exception>
        private void CheckGetListReq(GetListReq req, string custId, string italIfId)
        {
            // 傳入的客戶唯一識別碼為空
            if (string.IsNullOrEmpty(custId))
            {
                // Log
                //this.eventLog.Log("LBEI001001", "GetList CheckGetListReq Error: LBEI001001, Request CustId is empty", italIfId, italIfId);

                throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "custId");
            }

            // 險種類型為空
            if (string.IsNullOrEmpty(req.InsType))
            {
                // Log
                //this.eventLog.Log("LBEI001001", "GetList CheckGetListReq Error: LBEI001001, Request InsType is empty", italIfId, italIfId);

                throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "insType");
            }

            // 險種類型不為 01 汽車或 02 機車或 03 車險
            if (req.InsType != InsType.Car && req.InsType != InsType.Moto && req.InsType != InsType.Vehicle)
            {
                // Log
                //this.eventLog.Log("LBEI001002", "GetList CheckGetListReq Error: LBEI001002, Request InsType not 01,02,03", italIfId, italIfId);

                throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001002, "insType");
            }

            // 保單狀態為空
            if (string.IsNullOrEmpty(req.InsStatus))
            {
                // Log
                //this.eventLog.Log("LBEI001001", "GetList CheckGetListReq Error: LBEI001002, Request InsStatus is empty", italIfId, italIfId);

                throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "insStatus");
            }

            // 保單狀態值不為 01 生效,02 過期,03 全部任一種
            if (req.InsStatus != InsStatusType.Active &&
                req.InsStatus != InsStatusType.Expired &&
                req.InsStatus != InsStatusType.All)
            {
                // Log
                //this.eventLog.Log("LBEI001002", "GetList CheckGetListReq Error: LBEI001002, Request InsStatus not 01,02,03", italIfId, italIfId);

                throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001002, "insStatus");
            }
        }

        /// <summary>
        /// 取得保單清單查詢結果
        /// </summary>
        private CarListInfo GetCarListInfo(
            PolicyMaster pm,
            ref Dictionary<string, string> companyNameDic,
            ref Dictionary<string, string> projectNameDic)
        {
            var res = new CarListInfo();

            // 取得商品主檔
            var productMaster = this.GetProductMaster(pm.ProductId);
            var companyCode = pm.AcceptNo.Substring(0, 3);
            if (productMaster != null)
            {
                companyCode = productMaster.CompanyCode;
            }

            // 取得保險公司資料
            var companyName = string.Empty;
            if (companyNameDic.ContainsKey(companyCode))
            {
                companyName = companyNameDic[companyCode];
            }
            else
            {
                var companyProfile = this.companyProfileRepo.Queryable()
                    .FirstOrDefault(c => c.CompanyCode == companyCode);
                if (companyProfile != null)
                {
                    companyNameDic.Add(companyCode, companyProfile.CompanyName);
                    companyName = companyNameDic[companyCode];
                }
            }

            // 取得保險公司名稱
            res.InsuranceCompany = companyName;

            // 取得方案名稱
            var projectName = string.Empty;
            if (projectNameDic.ContainsKey($"{pm.ProductId}{pm.ProjectCode}"))
            {
                projectName = projectNameDic[$"{pm.ProductId}{pm.ProjectCode}"];
            }
            else
            {
                var campaignMaster = this.campaignMasterRepo.Queryable()
                    .FirstOrDefault(c => c.ProductId == pm.ProductId && c.ProjectCode == pm.ProjectCode);
                if (campaignMaster != null)
                {
                    projectNameDic.Add($"{pm.ProductId}{pm.ProjectCode}", campaignMaster.Name);
                    projectName = projectNameDic[$"{pm.ProductId}{pm.ProjectCode}"];
                }
            }

            res.ProjectName = projectName;
            res.PolicyNo = pm.PolicyNo;
            res.AcceptNo = pm.AcceptNo;
            res.ProjectCode = pm.ProjectCode;
            res.SDate = pm.PolicyPeriodS.HasValue
                ? pm.PolicyPeriodS.Value.ToString("yyyyMMdd")
                : string.Empty;
            res.EDate = pm.PolicyPeriodE.HasValue
                ? pm.PolicyPeriodE.Value.ToString("yyyyMMdd")
                : string.Empty;

            // 保單車籍資料檔
            var policyCarInfo = this.policyCarInfoRepo.Queryable()
                .FirstOrDefault(p => p.AcceptNo == pm.AcceptNo && p.PolicyNo == pm.PolicyNo);

            res.TagID = policyCarInfo?.LicensePlateNo;
            res.InsStatus = pm.PolicyStatus == PolicyStatusType.Expired
                ? InsStatusType.Expired
                : pm.PolicyStatus == PolicyStatusType.Active
                    ? InsStatusType.Active
                    : "";

            // pdcd = productId =
            // LB 2 碼
            // 保險公司代碼 3 碼 , ex 產險 P + 公會碼 2 碼
            // 商品種類 1 碼 , ex 汽車險 V ，機車險 M
            // 險種 1 碼 , ex C 強制險 , T 任意險 or 強 任
            // 流水號 2 碼
            // 共 9 碼
            res.InsuranceType = !string.IsNullOrEmpty(pm.ProductId) && pm.ProductId.Length > 6
                ? pm.ProductId[6].ToString()
                : string.Empty;
            // this.GetInsuranceType(pm.AcceptNo, pm.ProjectCode, companyCode);

            res.IsExtendYn = pm.ExtendStatus == PolicyExtendStatus.Renewable
                ? YesNoType.Yes
                : YesNoType.No;

            var insType = string.Empty;
            switch (pm.PlanType)
            {
                case ProductType.Car:
                case ProductType.Car_2:
                    insType = InsType.Car;
                    break;

                case ProductType.Moto:
                    insType = InsType.Moto;
                    break;

                default:
                    insType = string.Empty;
                    break;
            }

            res.InsType = insType;

            return res;
        }

        /// <summary>
        /// 取得商品主檔
        /// </summary>
        private ProductMaster GetProductMaster(string productId)
        {
            if (this.productMasterDic.ContainsKey(productId)) return this.productMasterDic[productId];

            this.productMasterDic[productId] = this.productMasterRepo.Queryable()
                .FirstOrDefault(p => p.ProductId == productId);

            return this.productMasterDic[productId];
        }

        /// <summary>
        /// 帳號/卡號 顯示格式
        /// </summary>
        /// <returns></returns>
        private string GetMaskAccountNo(string paidType, string accountNo)
        {
            Match match;
            switch (paidType)
            {
                case PaidType.CreditCard: //付款方式為簽帳卡，顯示格式：NNNN - NN * *-****-NNNN(N為數字、*為固定隱碼)
                    match = Regex.Match(MaskHelper.MaskCreditCard(accountNo), @"^(\d{4})([\d|\*]{4})(\*{4})(\d{4})$");
                    return string.Format(
                        "{0}-{1}-{2}-{3}", match.Groups[1], match.Groups[2], match.Groups[3], match.Groups[4]);

                case PaidType.Account:
                    match = Regex.Match(MaskHelper.MaskBankAccount(accountNo), @"^(\d{3})([\d|\*]{12,14})$");
                    return string.Format(
                        "{0}-{1}", match.Groups[1], match.Groups[2]);
            }

            return string.Empty;
        }

        /// <summary>
        /// 查詢保單內容
        /// </summary>
        /// <param name="req">查詢保單內容 Req</param>
        /// <returns>查詢保單內容 Resp</returns>
        public StandardModel<GetDetailResp> GetDetail(StandardModel<GetDetailReq> req)
        {
            // Log
            //this.eventLog.Trace("GetDetail Start", req.Header.ItalIfId, req.Header.ItalIfId);

            // Log
            //this.eventLog.Trace("Step1 GetDetail CheckResHeader Start", req.Header.ItalIfId, req.Header.ItalIfId);

            // 資料檢核
            this.CheckGetDetailReq(req.Data, req.Header.ItalIfId);

            // Log
            //this.eventLog.Trace("Step1 GetDetail CheckResHeader End", req.Header.ItalIfId, req.Header.ItalIfId);

            // Log
            //this.eventLog.Trace("Step2 GetDetail GetAcceptRecord Start", req.Header.ItalIfId, req.Header.ItalIfId);

            // 取得受理編號狀態檔資料
            var acceptRecord = this.acceptRecordRepo.Queryable()
                .FirstOrDefault(a => a.AcceptNo == req.Data.AcceptNo);

            if (acceptRecord == null)
            {
                // Log
                //this.eventLog.Log("LBEI004001", $"GetDetail Error: LBEI004001, AcceptNo: {req.Data.AcceptNo} not exist AcceptRecord", req.Header.ItalIfId, req.Header.ItalIfId);

                throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);
            }

            // Log
            //this.eventLog.Trace("Step2 GetDetail GetAcceptRecord End", req.Header.ItalIfId, req.Header.ItalIfId);

            // Log
            //this.eventLog.Trace("Step3 GetDetail CheckIsSearchReal Start", req.Header.ItalIfId, req.Header.ItalIfId);

            // 判斷查詢為主檔或暫存檔
            // AcceptRecord.CheckOrderNo 這個欄位判斷~ 1(true) 在主檔 0(false) 在 temp
            var isSearchReal = acceptRecord.CheckOrderNo.HasValue && acceptRecord.CheckOrderNo.Value;

            // Log
            //this.eventLog.Trace($"Step3 GetDetail CheckIsSearchReal End", req.Header.ItalIfId, req.Header.ItalIfId);

            //this.eventLog.Debug($"GetDetail CheckIsSearchReal isSearchReal: {isSearchReal}", req.Header.ItalIfId, req.Header.ItalIfId);

            // Log
            //this.eventLog.Trace("Step4 GetDetail QueryPolicyMaster Start", req.Header.ItalIfId, req.Header.ItalIfId);

            // 取得保單主檔
            var policyMaster = this.QueryPolicyMaster(req.Data, isSearchReal, req.Header.CustId, req.Header.ItalIfId);

            // Log
            //this.eventLog.Trace("Step4 GetDetail QueryPolicyMaster End", req.Header.ItalIfId, req.Header.ItalIfId);

            // 查詢失敗，交易結果回傳為空
            if (policyMaster == null)
                throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);

            // Log
            //this.eventLog.Trace("Step5 GetDetail GetDetailRes Start", req.Header.ItalIfId, req.Header.ItalIfId);

            // 取得回傳結果
            var res = this.GetDetailRes(req.Data, policyMaster, isSearchReal, req.Header.ItalIfId);

            // Log
            //this.eventLog.Trace("Step5 GetDetail GetDetailRes End", req.Header.ItalIfId, req.Header.ItalIfId);

            if (res == null)
            {
                // Log
                //this.eventLog.Log("LBEI004001", "GetDetail GetDetailRes Error: LBEI004001", req.Header.ItalIfId, req.Header.ItalIfId);

                throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);
            }

            // Log
            //this.eventLog.Trace("GetDetail End", req.Header.ItalIfId, req.Header.ItalIfId);

            var messageHeader = this.commonService.GetChannelSuccessMessageHeader();

            return this.channelAPICommonService.GetResp<GetDetailResp>(
                req.Header, messageHeader, StandardResCodeType.NormalProcessing, res);
        }

        /// <summary>
        /// 檢核傳入的查詢資料
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        private void CheckGetDetailReq(GetDetailReq req, string italIfId)
        {
            // 若傳入的 受理編號 & 保單號 & 牌照號碼 均為空，則限制 受理編號 為必填
            if (string.IsNullOrEmpty(req.AcceptNo) &&
                string.IsNullOrEmpty(req.PolicyNo) &&
                string.IsNullOrEmpty(req.TagID))
            {
                // Log
                //this.eventLog.Log("LBEI001001", "GetDetail CheckGetDetailReq Error: LBEI001001, Request AcceptNo and PolicyNo and TagID is empty", italIfId, italIfId);

                throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "acceptNo, policyNo, tagID");
            }
        }

        /// <summary>
        /// 依參數查詢保單主檔
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        private PolicyMaster QueryPolicyMaster(GetDetailReq req, bool isSearchReal, string custId, string italIfId)
        {
            // 查詢暫保單存檔
            if (!isSearchReal) return this.QueryTempPolicyMaster(req, custId, italIfId);

            PolicyMaster result = null;

            // 全部都有輸入(受理編號/保單號/牌照號碼) , 查詢順序為 保單號 + 牌照號碼 + 受理編號
            if (!string.IsNullOrEmpty(req.AcceptNo) &&
                !string.IsNullOrEmpty(req.PolicyNo) &&
                !string.IsNullOrEmpty(req.TagID))
            {
                result = this.policyMasterRepo.Queryable()
                    .Join(
                    this.policyCarInfoRepo.Queryable(),
                    pm => new { pm.AcceptNo, pm.PolicyNo },
                    pc => new { pc.AcceptNo, pc.PolicyNo },
                    (pm, pc) => new { pm, pc })
                    .FirstOrDefault(o =>
                        o.pm.AcceptNo == req.AcceptNo &&
                        o.pm.PolicyNo == req.PolicyNo &&
                        o.pc.LicensePlateNo == req.TagID &&
                        o.pm.CustId == custId)
                    ?.pm;

                if (result == null)
                    //this.eventLog.Log("LBEI004001", $"GetDetail QueryPolicyMaster Error: LBEI004001, AcceptNo: {req.AcceptNo}, PolicyNo: {req.PolicyNo}, TagID (LicensePlateNo): {req.TagID}, CustId: {custId} data not exist PolicyMaster Join PolicyCarInfo", italIfId, italIfId);
                    return result;
            }

            // 當任一未輸入(保單號/牌照號碼)
            // 受理編號 + 保單號
            if (!string.IsNullOrEmpty(req.PolicyNo))
            {
                result = this.policyMasterRepo.Queryable()
                    .FirstOrDefault(p => p.AcceptNo == req.AcceptNo && p.PolicyNo == req.PolicyNo && p.CustId == custId);

                if (result == null)
                    //this.eventLog.Log("LBEI004001", $"GetDetail QueryPolicyMaster Error: LBEI004001, AcceptNo: {req.AcceptNo}, PolicyNo: {req.PolicyNo}, CustId: {custId} data not exist PolicyMaster", italIfId, italIfId);

                    return result;
            }

            // 受理編號 + 牌照號碼
            if (!string.IsNullOrEmpty(req.TagID))
            {
                result = this.policyMasterRepo.Queryable()
                    .Join(
                    this.policyCarInfoRepo.Queryable(),
                    pm => pm.AcceptNo,
                    pc => pc.AcceptNo,
                    (pm, pc) => new { pm, pc })
                    .FirstOrDefault(o =>
                        o.pm.AcceptNo == req.AcceptNo &&
                        o.pc.LicensePlateNo == req.TagID &&
                        o.pm.CustId == custId)
                    ?.pm;

                if (result == null)
                    //this.eventLog.Log("LBEI004001", $"GetDetail QueryPolicyMaster Error: LBEI004001, AcceptNo: {req.AcceptNo}, TagID (LicensePlateNo): {req.TagID}, CustId: {custId} data not exist PolicyMaster Join PolicyCarInfo", italIfId, italIfId);

                    return result;
            }

            // 僅傳入受理編號
            result = this.policyMasterRepo.Queryable()
                .FirstOrDefault(p => p.AcceptNo == req.AcceptNo && p.CustId == custId);

            //if (result == null)
            //this.eventLog.Log("LBEI004001", $"GetDetail QueryPolicyMaster Error: LBEI004001, AcceptNo: {req.AcceptNo}, CustId: {custId} data not exist PolicyMaster", italIfId, italIfId);

            return result;
        }

        /// <summary>
        /// 依參數查詢 Temp 保單主檔
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        private PolicyMaster QueryTempPolicyMaster(GetDetailReq req, string custId, string italIfId)
        {
            TempPolicyMaster res = null;

            // 全部都有輸入(受理編號/保單號/牌照號碼) , 查詢順序為 保單號 + 牌照號碼 + 受理編號
            if (!string.IsNullOrEmpty(req.AcceptNo) &&
                !string.IsNullOrEmpty(req.PolicyNo) &&
                !string.IsNullOrEmpty(req.TagID))
            {
                res = this.tempPolicyMasterRepo.Queryable()
                    .Join(
                    this.tempPolicyCarInfoRepo.Queryable(),
                    pm => new { pm.AcceptNo, pm.PolicyNo },
                    pc => new { pc.AcceptNo, pc.PolicyNo },
                    (pm, pc) => new { pm, pc })
                    .FirstOrDefault(o =>
                        o.pm.AcceptNo == req.AcceptNo &&
                        o.pm.PolicyNo == req.PolicyNo &&
                        o.pc.LicensePlateNo == req.TagID &&
                        o.pm.CustId == custId)
                    ?.pm;

                if (res == null)
                    //this.eventLog.Log("LBEI004001", $"GetDetail QueryTempPolicyMaster Error: LBEI004001, AcceptNo: {req.AcceptNo}, PolicyNo: {req.PolicyNo}, TagID (LicensePlateNo): {req.TagID}, CustId: {custId} data not exist TempPolicyMaster Join TempPolicyCarInfo", italIfId, italIfId);

                    return this.mapper.Map<PolicyMaster>(res);
            }

            // 當任一未輸入(保單號/牌照號碼)
            // 受理編號 + 保單號
            if (!string.IsNullOrEmpty(req.PolicyNo))
            {
                res = this.tempPolicyMasterRepo.Queryable()
                    .FirstOrDefault(p => p.AcceptNo == req.AcceptNo && p.PolicyNo == req.PolicyNo && p.CustId == custId);

                if (res == null)
                    //this.eventLog.Log("LBEI004001", $"GetDetail QueryTempPolicyMaster Error: LBEI004001, AcceptNo: {req.AcceptNo}, PolicyNo: {req.PolicyNo}, CustId: {custId} data not exist TempPolicyMaster", italIfId, italIfId);

                    return this.mapper.Map<PolicyMaster>(res);
            }

            // 受理編號 + 牌照號碼
            if (!string.IsNullOrEmpty(req.TagID))
            {
                res = this.tempPolicyMasterRepo.Queryable()
                    .Join(
                    this.tempPolicyCarInfoRepo.Queryable(),
                    pm => pm.AcceptNo,
                    pc => pc.AcceptNo,
                    (pm, pc) => new { pm, pc })
                    .FirstOrDefault(o =>
                        o.pm.AcceptNo == req.AcceptNo &&
                        o.pc.LicensePlateNo == req.TagID &&
                        o.pm.CustId == custId)
                    ?.pm;

                if (res == null)
                    //this.eventLog.Log("LBEI004001", $"GetDetail QueryTempPolicyMaster Error: LBEI004001, AcceptNo: {req.AcceptNo}, TagID (LicensePlateNo): {req.TagID}, CustId: {custId} data not exist TempPolicyMaster Join TempPolicyCarInfo", italIfId, italIfId);

                    return this.mapper.Map<PolicyMaster>(res);
            }

            // 僅傳入受理編號
            res = this.tempPolicyMasterRepo.Queryable()
                .FirstOrDefault(p => p.AcceptNo == req.AcceptNo && p.CustId == custId);

            //if (res == null)
            //this.eventLog.Log("LBEI004001", $"GetDetail QueryTempPolicyMaster Error: LBEI004001, AcceptNo: {req.AcceptNo}, CustId: {custId} data not exist TempPolicyMaster", italIfId, italIfId);

            return this.mapper.Map<PolicyMaster>(res);
        }

        /// <summary>
        /// 取得回傳結果
        /// </summary>
        /// <param name="req"></param>
        /// <param name="policyMaster"></param>
        /// <returns></returns>
        private GetDetailResp GetDetailRes(GetDetailReq req, PolicyMaster policyMaster, bool isSearchReal, string italIfId)
        {
            // 取得暫存資料
            if (!isSearchReal)
                return this.GetTempDetailRes(req, policyMaster);

            var res = new GetDetailResp();

            #region 資料來源

            // 取得會員主檔資料
            var memberProfile = this.memberProfileRepo.Queryable()
                .FirstOrDefault(m => m.CustId == policyMaster.CustId);

            if (memberProfile == null) return null;

            // 取得專案主檔
            var productMaster = this.GetProductMaster(policyMaster.ProductId);

            // 方案主檔
            var campaignMaster = this.campaignMasterRepo.Queryable()
                .FirstOrDefault(c => c.ProductId == policyMaster.ProductId && c.ProjectCode == policyMaster.ProjectCode);

            // 取得保險公司代碼
            var companyCode = string.Empty;
            if (productMaster != null)
            {
                companyCode = productMaster.CompanyCode;
            }

            // 取得保險公司資料
            var companyProfile = this.companyProfileRepo.Queryable()
                .FirstOrDefault(c => c.CompanyCode == companyCode);

            // 取得保單明細檔
            var policyDetails = this.policyDetailRepo.Queryable()
                .Where(p =>
                    p.AcceptNo == policyMaster.AcceptNo &&
                    p.ProjectCode == policyMaster.ProjectCode &&
                    p.ProductId == policyMaster.ProductId);

            // 取得商品主檔
            var planMasters = this.planMasterRepo.Queryable().Join(policyDetails,
                pm => new { pm.PlanCode, pm.PaymentTerm },
                pd => new { pd.PlanCode, pd.PaymentTerm },
                (pm, pd) => new { pm, pd })
                .Where(o =>
                o.pm.PlanVer == o.pd.PlanVer &&
                o.pm.CompanyCode == companyCode &&
                o.pm.PlanType == policyMaster.PlanType
                )
                .Select(o => o.pm);

            // 取得投保相關人員檔
            var policyPersonInfo = this.policyPersonInfoRepo.Queryable()
                .FirstOrDefault(p => p.AcceptNo == policyMaster.AcceptNo && p.PersonType == PersonType.Applicant);

            // 取得保單 KYC 資料
            var kycRecord = this.kycRecordRepo.Queryable()
                .FirstOrDefault(k => k.AcceptNo == policyMaster.AcceptNo);

            // 取得保單車籍資料表
            var policyCarInfo = this.policyCarInfoRepo.Queryable()
                .FirstOrDefault(p => p.AcceptNo == policyMaster.AcceptNo);

            // 取得會員AML EDD歷程資料
            var memberAml = this.memberAmlRepo.Queryable()
                .OrderByDescending(m => m.CreateOn)
                .FirstOrDefault(m => m.AcceptNo == policyMaster.AcceptNo);

            #endregion

            #region 方案相關

            // 方案內容(商品物件)
            var projectItemInfos = new List<ProjectItemInfo>();
            ProjectItemInfo projectItemInfo;
            foreach (PlanMaster planMaster in planMasters)
            {
                var policyDetail = policyDetails
                    .FirstOrDefault(p =>
                    p.PlanCode == planMaster.PlanCode &&
                    p.PlanVer == planMaster.PlanVer &&
                    p.PaymentTerm == planMaster.PaymentTerm
                    );

                projectItemInfo = new ProjectItemInfo();
                projectItemInfo.PlanCode = planMaster.PlanCode;
                projectItemInfo.Item = planMaster.PlanShortName;
                projectItemInfo.ContentPremium = policyDetail?.ModalItemPermiumWithDisc;
                projectItemInfo.GuaranteeItems = this.GetGuaranteeItemInfo(policyDetail?.CoveredItem);

                projectItemInfos.Add(projectItemInfo);
            }

            // 強制險方案資料(專案物件)
            var projectInfo = new ProjectInfo();
            if (productMaster != null)
            {
                res.PdCd = productMaster.ProductId;
                projectInfo.InsuranceId = productMaster.CompanyCode;
                res.InsType = productMaster.InsType;
            }

            // 方案主檔
            if (campaignMaster != null)
            {
                projectInfo.ProjectCode = campaignMaster.ProjectCode;
                projectInfo.ProjectName = campaignMaster.Name;
            }

            if (companyProfile != null)
            {
                projectInfo.InsuranceCompany = companyProfile.CompanyShortName;
                projectInfo.InsuranceTel = $"{companyProfile.CompanyPhoneAreaCode}-{companyProfile.CompanyPhoneLastCode}";
            }

            projectInfo.CompulsoryPremium = policyMaster.ModalPermiumWithDisc;
            projectInfo.CompulsorySDate = policyMaster.PolicyPeriodS.HasValue
                ? policyMaster.PolicyPeriodS.Value.ToString("yyyyMMdd")
                : string.Empty;
            projectInfo.CompulsoryEDate = policyMaster.PolicyPeriodE.HasValue
                ? policyMaster.PolicyPeriodE.Value.ToString("yyyyMMdd")
                : string.Empty;
            projectInfo.ProjectItems = projectItemInfos;

            res.CustId = policyMaster.CustId;
            res.PolicyNo = policyMaster.PolicyNo;
            res.ProjectList = projectInfo;

            #endregion

            #region 車籍相關

            if (policyCarInfo != null)
            {
                res.TagID = policyCarInfo.LicensePlateNo;
                res.VehicleType = policyCarInfo.VehicleType;
                res.IssueDate = policyCarInfo.RegistIssueDate.HasValue
                    ? Convert.ToDateTime(policyCarInfo.RegistIssueDate.Value)
                        .ToTaiwanDateTimeStr()
                        .Replace("/", "")
                        .PadLeft(7, '0')
                    : string.Empty;
                res.ManufacturedDate = policyCarInfo.ManufacturedDate;
                res.Displacement = policyCarInfo.Displacement;
                res.TransportUnit = policyCarInfo.TransportUnit;
                res.EngineId = policyCarInfo.EngineId;

                res.VehicleBrand = new VehicleBrandInfo()
                {
                    BrandId = policyCarInfo.Ibrand != null && policyCarInfo.Ibrand.Length > 3
                    ? policyCarInfo.Ibrand.Substring(0, 2)
                    : policyCarInfo.Ibrand,
                    BrandName = policyCarInfo.NbrandAbbr
                };

                res.CarTypeList = new List<CarTypeInfo>()
                {
                    new CarTypeInfo()
                    {
                        DisplayName = policyCarInfo.Nbrand,
                        CarType = policyCarInfo.CarType
                    }
                };
            }

            res.InsStatus = policyMaster.PolicyStatus == PolicyStatusType.Expired
                ? InsStatusType.Expired
                : policyMaster.PolicyStatus == PolicyStatusType.Active
                    ? InsStatusType.Active
                    : "";

            #endregion

            #region 要被保人及KYC相關

            if (policyPersonInfo != null)
            {
                res.SendZipCode = policyPersonInfo.Zipcode;
                res.SendCity = policyPersonInfo.SendCity;
                res.SendCountry = policyPersonInfo.SendCountry;
                res.SendAddr = policyPersonInfo.Addr;
                res.InsuredSex = policyPersonInfo.Gender;
                res.InsuredId = policyPersonInfo.Idno;
                res.InsuredName = policyPersonInfo.PersonName;
                res.InsuredBirthday = policyPersonInfo.Birthday.HasValue
                    ? policyPersonInfo.Birthday.Value.ToString("yyyyMMdd")
                    : string.Empty;
                res.InsuredNationality = policyPersonInfo.Nationality;
                res.InsuredMobile = policyPersonInfo.Mobile;
                res.InsuredEmail = policyPersonInfo.Email;
                res.JobClFrstStgeCd = policyPersonInfo.JobClFrstStgeCd;
                res.JobClScndStgeCd = policyPersonInfo.JobClScndStgeCd;
                res.JobDtlTpCd = policyPersonInfo.JobDtlTpCd;
            }

            if (kycRecord != null)
            {
                res.InformItem01 = kycRecord.Guardianship.HasValue
                    ? kycRecord.Guardianship.Value
                        ? YesNoType.Yes
                        : YesNoType.No
                    : string.Empty;
                res.InformItem02 = kycRecord.LivingAbroad.HasValue
                    ? kycRecord.LivingAbroad.Value
                        ? YesNoType.Yes
                        : YesNoType.No
                    : string.Empty;
                res.InformItem03 = kycRecord.PoliticalPerson;
                res.InformItem04 = kycRecord.SensitiveOccupation.HasValue
                    ? kycRecord.SensitiveOccupation.Value
                        ? YesNoType.Yes
                        : YesNoType.No
                    : string.Empty;
                res.InformItem05 = kycRecord.PremiumFrom;
                res.InformItem06 = kycRecord.TerminationWithin3M.HasValue
                    ? kycRecord.TerminationWithin3M.Value
                        ? YesNoType.Yes
                        : YesNoType.No
                    : string.Empty;
            }

            res.PolicyDelivery = policyMaster.PolicySendType;
            res.PayBy = policyMaster.PaidType;
            res.OldRiskLevel = memberProfile.AmlriskLevel;
            if (memberAml != null)
            {
                res.NewRiskLevel = memberAml.NewRiskLevel;
                res.AlertId = memberAml.AlertId;
            }

            if (!string.IsNullOrEmpty(policyMaster.AccountNo))
            {
                res.CardNo = policyMaster.AccountNo.IndexOf("-") != -1
                    ? policyMaster.AccountNo.Replace("-", "")
                    : policyMaster.AccountNo;
            }

            res.IsExtendYn = res.IsExtendYn = policyMaster.ExtendStatus == PolicyExtendStatus.Renewable
                ? YesNoType.Yes
                : YesNoType.No;

            // 系統日 ≧「保單主檔 」的保單迄日，回傳 BeforeDueStatus 為 0，否則一律回傳 null
            // t1 早於 t2，回傳值：-1
            res.BeforeDueStatus = policyMaster.PolicyPeriodE.HasValue &&
                DateTime.Compare(DateTime.Now, policyMaster.PolicyPeriodE.Value) != -1
                ? "0"
                : null;

            #endregion

            return res;
        }

        /// <summary>
        /// 取得回傳暫存結果
        /// </summary>
        /// <param name="req"></param>
        /// <param name="policyMaster"></param>
        /// <returns></returns>
        private GetDetailResp GetTempDetailRes(GetDetailReq req, PolicyMaster policyMaster)
        {
            var res = new GetDetailResp();

            #region 資料來源

            // 取得會員主檔資料
            var memberProfile = this.memberProfileRepo.Queryable()
                .FirstOrDefault(m => m.CustId == policyMaster.CustId);

            if (memberProfile == null) return null;

            // 取得專案主檔
            var productMaster = this.GetProductMaster(policyMaster.ProductId);

            // 方案主檔
            var campaignMaster = this.campaignMasterRepo.Queryable()
                .FirstOrDefault(c => c.ProductId == policyMaster.ProductId && c.ProjectCode == policyMaster.ProjectCode);

            // 取得保險公司代碼
            var companyCode = string.Empty;
            if (productMaster != null)
            {
                companyCode = productMaster.CompanyCode;
            }

            // 取得保險公司資料
            var companyProfile = this.companyProfileRepo.Queryable()
                .FirstOrDefault(c => c.CompanyCode == companyCode);

            // 取得保單明細檔
            var policyDetails = this.tempPolicyDetailRepo.Queryable()
                .Where(p =>
                    p.AcceptNo == policyMaster.AcceptNo &&
                    p.ProjectCode == policyMaster.ProjectCode &&
                    p.ProductId == policyMaster.ProductId);

            // 取得商品主檔
            var planMasters = this.planMasterRepo.Queryable().Join(policyDetails,
                pm => new { pm.PlanCode, pm.PaymentTerm },
                pd => new { pd.PlanCode, pd.PaymentTerm },
                (pm, pd) => new { pm, pd })
                .Where(o =>
                o.pm.PlanVer == o.pd.PlanVer &&
                o.pm.CompanyCode == companyCode &&
                o.pm.PlanType == policyMaster.PlanType
                )
                .Select(o => o.pm);

            // 取得投保相關人員檔
            var policyPersonInfo = this.tempPolicyPersonInfoRepo.Queryable()
                .FirstOrDefault(p => p.AcceptNo == policyMaster.AcceptNo && p.PersonType == PersonType.Applicant);

            // 取得保單 KYC 資料
            var kycRecord = this.tempKycrecordRepo.Queryable()
                .FirstOrDefault(k => k.AcceptNo == policyMaster.AcceptNo);

            // 取得保單車籍資料表
            var policyCarInfo = this.tempPolicyCarInfoRepo.Queryable()
                .FirstOrDefault(p => p.AcceptNo == policyMaster.AcceptNo);

            // 取得會員AML EDD歷程資料
            var memberAml = this.memberAmlRepo.Queryable()
                .OrderByDescending(m => m.CreateOn)
                .FirstOrDefault(m => m.AcceptNo == policyMaster.AcceptNo);

            #endregion

            #region 方案相關

            // 方案內容(商品物件)
            var projectItemInfos = new List<ProjectItemInfo>();
            ProjectItemInfo projectItemInfo;
            foreach (PlanMaster planMaster in planMasters)
            {
                var policyDetail = policyDetails
                    .FirstOrDefault(p =>
                    p.PlanCode == planMaster.PlanCode &&
                    p.PlanVer == planMaster.PlanVer &&
                    p.PaymentTerm == planMaster.PaymentTerm);

                projectItemInfo = new ProjectItemInfo();
                projectItemInfo.PlanCode = planMaster.PlanCode;
                projectItemInfo.Item = planMaster.PlanShortName;
                projectItemInfo.ContentPremium = policyDetail?.ModalItemPermiumWithDisc;
                projectItemInfo.GuaranteeItems = this.GetGuaranteeItemInfo(policyDetail?.CoveredItem);

                projectItemInfos.Add(projectItemInfo);
            }

            // 強制險方案資料(專案物件)
            var projectInfo = new ProjectInfo();
            if (productMaster != null)
            {
                res.PdCd = productMaster.ProductId;
                projectInfo.InsuranceId = productMaster.CompanyCode;
                res.InsType = productMaster.InsType;
            }

            // 方案主檔
            if (campaignMaster != null)
            {
                projectInfo.ProjectCode = campaignMaster.ProjectCode;
                projectInfo.ProjectName = campaignMaster.Name;
            }

            if (companyProfile != null)
            {
                projectInfo.InsuranceCompany = companyProfile.CompanyShortName;
                projectInfo.InsuranceTel = $"{companyProfile.CompanyPhoneAreaCode}-{companyProfile.CompanyPhoneLastCode}";
            }

            projectInfo.CompulsoryPremium = policyMaster.ModalPermiumWithDisc;
            projectInfo.CompulsorySDate = policyMaster.PolicyPeriodS.HasValue
                ? policyMaster.PolicyPeriodS.Value.ToString("yyyyMMdd")
                : string.Empty;
            projectInfo.CompulsoryEDate = policyMaster.PolicyPeriodE.HasValue
                ? policyMaster.PolicyPeriodE.Value.ToString("yyyyMMdd")
                : string.Empty;
            projectInfo.ProjectItems = projectItemInfos;

            res.CustId = policyMaster.CustId;
            res.PolicyNo = policyMaster.PolicyNo;
            res.ProjectList = projectInfo;

            #endregion

            #region 車籍相關

            // 取得保單車籍資料_廠牌物件暫存檔資料
            if (policyCarInfo != null)
            {
                res.TagID = policyCarInfo.LicensePlateNo;
                res.VehicleType = policyCarInfo.VehicleType;
                res.IssueDate = policyCarInfo.RegistIssueDate.HasValue
                    ? Convert.ToDateTime(policyCarInfo.RegistIssueDate.Value)
                        .ToTaiwanDateTimeStr()
                        .Replace("/", "")
                        .PadLeft(7, '0')
                    : string.Empty;
                res.ManufacturedDate = policyCarInfo.ManufacturedDate;
                res.Displacement = policyCarInfo.Displacement;
                res.TransportUnit = policyCarInfo.TransportUnit;
                res.EngineId = policyCarInfo.EngineId;
                res.VehicleBrand = new VehicleBrandInfo()
                {
                    BrandId = policyCarInfo.Ibrand,
                    BrandName = policyCarInfo.NbrandAbbr
                };

                //var getAcceptNoStatus = SaveStatus.GetAcceptNo;

                // 非取得進件編號階段
                //TODO PolicyMaster 已無 SaveStatus 欄位，20230217 Remark，待修正
                /*
                if (policyMaster.SaveStatus != getAcceptNoStatus)
                {
                    res.CarTypeList = new List<CarTypeInfo>()
                    {
                        new CarTypeInfo
                        {
                            DisplayName = policyCarInfo.Nbrand,
                            CarType = policyCarInfo.CarType
                        }
                    };
                }
                else
                {
                    var vehicleBrand = this.tempVehicleBrandRepo.Queryable()
                            .OrderByDescending(t => t.CreateOn)
                            .FirstOrDefault(v =>
                            v.PdCd == policyMaster.PdCd &&
                            v.CustId == policyMaster.CustId &&
                            v.LicensePlateNo == policyCarInfo.LicensePlateNo
                            );

                    if (vehicleBrand != null)
                    {
                        // 取得保單車籍資料_車型物件暫存檔資料
                        var carTypeList = this.tempCarTypeListRepo.Queryable()
                            .Where(c =>
                            c.PdCd == policyMaster.PdCd &&
                            c.CustId == policyMaster.CustId &&
                            c.LicensePlateNo == vehicleBrand.LicensePlateNo &&
                            c.CreateOn == vehicleBrand.CreateOn
                            );

                        res.CarTypeList = carTypeList.Select(c => new CarTypeInfo()
                        {
                            DisplayName = c.Nbrand,
                            CarType = c.CarType
                        }).ToList();
                    }
                }
                */
            }

            res.InsStatus = policyMaster.PolicyStatus == PolicyStatusType.Expired
                ? InsStatusType.Expired
                : policyMaster.PolicyStatus == PolicyStatusType.Active
                    ? InsStatusType.Active
                    : "";

            #endregion

            #region 要被保人及KYC相關

            if (policyPersonInfo != null)
            {
                res.SendZipCode = policyPersonInfo.Zipcode;
                res.SendCity = policyPersonInfo.SendCity;
                res.SendCountry = policyPersonInfo.SendCountry;
                res.SendAddr = policyPersonInfo.Addr;
                res.InsuredSex = policyPersonInfo.Gender;
                res.InsuredId = policyPersonInfo.Idno;
                res.InsuredName = policyPersonInfo.PersonName;
                res.InsuredBirthday = policyPersonInfo.Birthday.HasValue
                    ? policyPersonInfo.Birthday.Value.ToString("yyyyMMdd")
                    : string.Empty;
                res.InsuredNationality = policyPersonInfo.Nationality;
                res.InsuredMobile = policyPersonInfo.Mobile;
                res.InsuredEmail = policyPersonInfo.Email;
                res.JobClFrstStgeCd = policyPersonInfo.JobClFrstStgeCd;
                res.JobClScndStgeCd = policyPersonInfo.JobClScndStgeCd;
                res.JobDtlTpCd = policyPersonInfo.JobDtlTpCd;
            }

            if (kycRecord != null)
            {
                res.InformItem01 = kycRecord.Guardianship.HasValue
                    ? kycRecord.Guardianship.Value
                        ? YesNoType.Yes
                        : YesNoType.No
                    : string.Empty;

                res.InformItem02 = kycRecord.LivingAbroad.HasValue
                    ? kycRecord.LivingAbroad.Value
                        ? YesNoType.Yes
                        : YesNoType.No
                    : string.Empty;

                res.InformItem03 = kycRecord.PoliticalPerson;

                res.InformItem04 = kycRecord.SensitiveOccupation.HasValue
                    ? kycRecord.SensitiveOccupation.Value
                        ? YesNoType.Yes
                        : YesNoType.No
                    : string.Empty;

                res.InformItem05 = kycRecord.PremiumFrom;

                res.InformItem06 = kycRecord.TerminationWithin3M.HasValue
                    ? kycRecord.TerminationWithin3M.Value
                        ? YesNoType.Yes
                        : YesNoType.No
                    : string.Empty;
            }

            res.PolicyDelivery = policyMaster.PolicySendType;
            res.PayBy = policyMaster.PaidType;
            res.OldRiskLevel = memberProfile.AmlriskLevel;
            if (memberAml != null)
            {
                res.NewRiskLevel = memberAml.NewRiskLevel;
                res.AlertId = memberAml.AlertId;
            }

            if (!string.IsNullOrEmpty(policyMaster.AccountNo))
            {
                res.CardNo = policyMaster.AccountNo.IndexOf("-") != -1
                    ? policyMaster.AccountNo.Replace("-", "")
                    : policyMaster.AccountNo;
            }

            res.IsExtendYn = policyMaster.ExtendStatus == PolicyExtendStatus.Renewable
                ? YesNoType.Yes
                : YesNoType.No;

            // 系統日 ≧「保單主檔 」的保單迄日，回傳 BeforeDueStatus 為 0，否則一律回傳 null
            // t1 早於 t2，回傳值：-1
            res.BeforeDueStatus = policyMaster.PolicyPeriodE.HasValue &&
                DateTime.Compare(DateTime.Now, policyMaster.PolicyPeriodE.Value) != -1
                ? "0"
                : null;

            #endregion

            return res;
        }

        /// <summary>
        /// 取得方案保障項目物件
        /// </summary>
        /// <returns></returns>
        private List<GuaranteeItemInfo> GetGuaranteeItemInfo(string str)
        {
            var res = new List<GuaranteeItemInfo>();

            if (string.IsNullOrEmpty(str)) return res;

            // str 預期內容:傷害醫療保障_20萬/身故失能保障_200萬
            // strSlashList 存放分割 / 後的資料，預期內容 [傷害醫療保障_20萬,身故失能保障_200萬]
            var strSlashList = new List<string>();
            if (str.IndexOf("/") != -1)
            {
                var strSplitSlash = str.Split("/");

                foreach (var item in strSplitSlash)
                {
                    strSlashList.Add(item);
                }
            }
            else
            {
                strSlashList.Add(str);
            }

            GuaranteeItemInfo guaranteeItemInfo;
            // 分割字串內容的 _ ,預期內容 [傷害醫療保障,20萬]
            foreach (var item in strSlashList)
            {
                if (item.IndexOf("_") == -1) continue;

                guaranteeItemInfo = new GuaranteeItemInfo();

                var splitStr = item.Split("_");
                guaranteeItemInfo.Content = splitStr[0];
                guaranteeItemInfo.Value = splitStr[1];

                res.Add(guaranteeItemInfo);
            }

            return res;
        }

        /// <summary>
        /// 檢查商品險別=機車險或汽車險
        /// </summary>
        /// <param name="insType"></param>
        /// <returns></returns>
        private bool CheckIsVehicle(string insType)
        {
            return this.commonService.SearchSysParamByGroupId(GroupIdType.InsType).Select(o => o.ItemId).Any(o => o == insType);
        }


        ///// <summary>
        ///// 檢核保費試算取得 CIF 欄位值
        ///// </summary>
        //private void CheckCIFRes(QueryCIFResp CIFRes, string italIfId)
        //{
        //    // CIF 的 natlId 為空
        //    if (string.IsNullOrEmpty(CIFRes.NatlId))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001005", "CheckCIFRes Error: LBEI001005, Response NatlId is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001005, "natlId");
        //    }

        //    // 檢核取得的 ID 、 性別正確性
        //    this.validationService.CheckCIFNatlId(CIFRes.NatlId, CIFRes.GndrDsCd, italIfId);

        //    // CIF 的 custNm 為空
        //    if (string.IsNullOrEmpty(CIFRes.CustNm))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001005", "CheckCIFRes Error: LBEI001005, Response CustNm is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001005, "custNm");
        //    }

        //    // CIF 的 brthDt 為空
        //    if (string.IsNullOrEmpty(CIFRes.BrthDt))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001005", "CheckCIFRes Error: LBEI001005, Response BrthDt is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001005, "brthDt");
        //    }
        //}

        ///// <summary>
        ///// 檢核傳入的查詢資料
        ///// </summary>
        ///// <returns></returns>
        //private void CheckPremiumTrialReq(PremiumTrialReq req, string custId, string italIfId)
        //{
        //    // 傳入的客戶唯一識別碼為空
        //    if (string.IsNullOrEmpty(custId))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001001", "PremiumTrial CheckPremiumTrialReq Error: LBEI001001, Request CustId is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "custId");
        //    }

        //    // 傳入的 pdCd 為空
        //    if (string.IsNullOrEmpty(req.PdCd))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001001", "PremiumTrial CheckPremiumTrialReq Error: LBEI001001, Request PdCd is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "pdCd");
        //    }

        //    // 傳入的 pdCd 長度太短，無法解析商品種類
        //    if (req.PdCd.Length < 6)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001002", "PremiumTrial CheckPremiumTrialReq Error: LBEI001002, Request PdCd Length < 6", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001002, "pdCd");
        //    }

        //    // 傳入的牌照號碼前車牌為空
        //    if (string.IsNullOrEmpty(req.TagIdFirst))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001001", "PremiumTrial CheckPremiumTrialReq Error: LBEI001001, Request TagIdFirst is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "tagIdFirst");
        //    }

        //    // 傳入的牌照號碼後車牌為空
        //    if (string.IsNullOrEmpty(req.TagIdLast))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001001", "PremiumTrial CheckPremiumTrialReq Error: LBEI001001, Request TagIdLast is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "tagIdLast");
        //    }

        //    // 傳入的車種代號/車輛種類為空直接結束
        //    if (string.IsNullOrEmpty(req.VehicleTypeCd)) return;

        //    var vehicleTypes = this.commonService.SearchSysParamByGroupId(GroupIdType.VehicleType)
        //        .Select(s => s.ItemId);

        //    // 傳入車種代號/車輛種類不是
        //    // (01重型機車/02輕型機車/03自用小客車/04自用小貨車/22自用小客貨車/32 大型重型機車)
        //    // 其中一個
        //    if (!vehicleTypes.Contains(req.VehicleTypeCd))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001002", $"PremiumTrial CheckPremiumTrialReq Error: LBEI001002, VehicleTypeCd: {req.VehicleTypeCd} not exist in SysParam VehicleType Group", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001002, "vehicleTypeCd");
        //    }
        //}

        ///// <summary>
        ///// 檢核專案是否已過期 & 專案是否還在銷售中
        ///// </summary>
        //private void CheckProductSaleState(string productId, string italIfId)
        //{
        //    // 依 公司代碼、專案代碼 查詢 專案主檔
        //    var productMaster = this.GetProductMaster(productId);

        //    // 查詢不到-無效專案代碼
        //    if (productMaster == null)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI004004", $"CheckProductSaleState Error: LBEI004004, ProductId: {productId} not exist ProductMaster", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004004);
        //    }

        //    var isSailing = this.commonService
        //        .QuerySaleState(SaleStateType.Sailing, productMaster.ProductStartOn, productMaster.ProductEndOn);

        //    // 非銷售中專案
        //    if (!isSailing)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI004005", $"CheckProductSaleState Error: LBEI004005, ProductId: {productId} not sailing", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004005);
        //    }
        //}


        ///// <summary>
        ///// 檢核保險公司回傳的方案代碼、商品代碼與資料庫是否匹配
        ///// </summary>
        //private void CheckProjectList(
        //    string pdcd, string companyCode, List<FEBProjectInfo> projectList, string insType, string italIfId)
        //{
        //    // 取得方案商品對應檔資料
        //    var campaignPlanMappings = this.campaignPlanMappingRepo.Queryable()
        //        .Where(c => c.CompanyCode == companyCode && c.ProductId == pdcd)
        //        .ToList();

        //    var planType = pdcd[5].ToString();

        //    // 取得保險公司所有險種銷售中且最大版本號
        //    var planMaxVersions = this.commonService.GetPlanMaxVersions()
        //        .Where(p =>
        //        p.CompanyCode == companyCode &&
        //        p.PlanType == planType &&
        //        this.commonService.QuerySaleState(SaleStateType.Sailing, p.PlanStartOn, p.PlanEndOn));

        //    CampaignPlanMapping campaignPlanMapping;
        //    PlanMasterDTO planMaster;
        //    bool isExistProject;
        //    foreach (FEBProjectInfo project in projectList)
        //    {
        //        foreach (var projectItem in project.ProjectItems)
        //        {
        //            isExistProject = campaignPlanMappings.Any(c => c.ProjectCode == project.ProjectCode);

        //            // 方案不存在
        //            if (!isExistProject)
        //            {
        //                // Log
        //                //this.eventLog.Log("LBEI004006", $"PremiumTrial CheckProjectList Error: LBEI004006, CompanyCode: {companyCode}, ProductId: {pdcd}, ProjectCode: {project.ProjectCode} not exist in CampaignPlanMapping", italIfId, italIfId);

        //                throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004006, project.ProjectName);
        //            }

        //            campaignPlanMapping = campaignPlanMappings
        //                .FirstOrDefault(c => c.ProjectCode == project.ProjectCode && c.PlanCode == projectItem.ItemCode);

        //            // 方案中的險種不存在
        //            if (campaignPlanMapping == null)
        //            {
        //                // Log
        //                //this.eventLog.Log("LBEI004007", $"PremiumTrial CheckProjectList Error: LBEI004007, CompanyCode: {companyCode}, ProductId: {pdcd}, ProjectCode: {project.ProjectCode},PlanCode: {projectItem.ItemCode} not exist in CampaignPlanMapping", italIfId, italIfId);

        //                throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004007, project.ProjectName, projectItem.ItemCode);
        //            }

        //            // 篩選最大版本號的商品資料
        //            planMaster = planMaxVersions
        //                .FirstOrDefault(p =>
        //                    p.PlanCode == projectItem.ItemCode &&
        //                    p.PaymentTerm == campaignPlanMapping.PaymentTerm &&
        //                    p.PlanType == planType);

        //            // 方案中的險種繳費年期對應不上
        //            if (planMaster == null)
        //            {
        //                // Log
        //                //this.eventLog.Log("LBEI004007", $"PremiumTrial CheckProjectList Error: LBEI004007, PlanCode: {projectItem.ItemCode}, PaymentTerm: {campaignPlanMapping.PaymentTerm}, PlanType: {planType} not exist in PlanMaster", italIfId, italIfId);

        //                throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004007, project.ProjectName, projectItem.ItemCode);
        //            }
        //        }
        //    }
        //}

        ///// <summary>
        ///// 取得專案保費試算結果
        ///// </summary>
        ///// <param name="projectList"></param>
        ///// <returns></returns>
        //private List<ProjectInfo> GetProjectList(List<FEBProjectInfo> projectList, string companyCode)
        //{
        //    var res = new List<ProjectInfo>();

        //    var companyProfile = this.companyProfileRepo.Queryable()
        //        .FirstOrDefault(c => c.CompanyCode == companyCode);

        //    ProjectInfo item;
        //    foreach (var project in projectList)
        //    {
        //        item = new ProjectInfo();
        //        item.InsuranceId = companyProfile != null
        //            ? companyProfile.CompanyCode
        //            : string.Empty;
        //        item.InsuranceCompany = companyProfile != null
        //            ? companyProfile.CompanyShortName
        //            : string.Empty;
        //        item.InsuranceTel = companyProfile != null
        //            ? $"{companyProfile.CompanyPhoneAreaCode}-{companyProfile.CompanyPhoneLastCode}"
        //            : string.Empty;
        //        item.ProjectCode = project.ProjectCode;
        //        item.ProjectName = project.ProjectName;
        //        item.CompulsoryPremium = project.CompulsoryPremium;
        //        item.CompulsorySDate = project.BegDateSgn;
        //        item.CompulsoryEDate = project.EndDateSgn;
        //        item.ProjectItems = this.GetProjectItemInfos(project.ProjectItems);

        //        res.Add(item);
        //    }

        //    return res;
        //}

        ///// <summary>
        ///// 取得專案保費試算方案內容
        ///// </summary>
        ///// <param name="projectItems"></param>
        ///// <returns></returns>
        //private List<ProjectItemInfo> GetProjectItemInfos(List<ProjectItem> projectItems)
        //{
        //    var res = new List<ProjectItemInfo>();
        //    ProjectItemInfo projectItemInfo;
        //    foreach (var item in projectItems)
        //    {
        //        projectItemInfo = new ProjectItemInfo();

        //        projectItemInfo.PlanCode = item.ItemCode;
        //        projectItemInfo.Item = item.ItemName;
        //        projectItemInfo.ContentPremium = item.Premium;

        //        projectItemInfo.GuaranteeItems = this.GetGuaranteeItemInfo(item.Content);

        //        res.Add(projectItemInfo);
        //    }

        //    return res;
        //}

        ///// <summary>
        ///// 儲存保費試算內容
        ///// </summary>
        ///// <returns></returns>
        //private async Task SavePremiumTrial(
        //    string custId, PremiumTrialReq req, QueryGetCarInfoResp queryCarInfoRes,
        //    List<FEBProjectInfo> projectList, QueryCIFResp CIFRes, string vehicleType,
        //    string italIfId)
        //{
        //    var tagId = $"{req.TagIdFirst}-{req.TagIdLast}";
        //    var now = DateTime.Now;
        //    List<TempCarTypeList> tempCarTypeList = new List<TempCarTypeList>();

        //    TempVehicleBrand tempVehicleBrand = new TempVehicleBrand();
        //    tempVehicleBrand.PdCd = req.PdCd;
        //    tempVehicleBrand.CustId = custId;
        //    tempVehicleBrand.LicensePlateNo = tagId;
        //    tempVehicleBrand.VehicleType = vehicleType;
        //    tempVehicleBrand.CreateBy = custId;
        //    tempVehicleBrand.CreateOn = now;
        //    tempVehicleBrand.UpdateBy = custId;
        //    tempVehicleBrand.UpdateOn = now;

        //    //if (queryCarInfoRes != null)
        //    //{
        //    //    tempVehicleBrand.Ibrand = queryCarInfoRes.BrandID;
        //    //    tempVehicleBrand.NbrandAbbr = queryCarInfoRes.BrandName;
        //    //    tempVehicleBrand.RegistrationIssueDate = queryCarInfoRes.RegistrationIssueDate;
        //    //    tempVehicleBrand.VehicleType = queryCarInfoRes.VehicleType;
        //    //    tempVehicleBrand.ManufacturedDate = queryCarInfoRes.ManufacturedDate;
        //    //    tempVehicleBrand.Displacement = queryCarInfoRes.Displacement?.ToString("0.##");
        //    //    if (queryCarInfoRes.CarTypes != null && queryCarInfoRes.CarTypes.Count > 0)
        //    //    {
        //    //        tempVehicleBrand.TransportUnit = queryCarInfoRes.CarTypes.FirstOrDefault()?.TransportUnit?.ToString("0");
        //    //    }

        //    //    TempCarTypeList tempCarType;
        //    //    foreach (var carType in queryCarInfoRes.CarTypes)
        //    //    {
        //    //        tempCarType = new TempCarTypeList();
        //    //        tempCarType.PdCd = req.PdCd;
        //    //        tempCarType.CustId = custId;
        //    //        tempCarType.LicensePlateNo = tagId;
        //    //        tempCarType.CarType = carType.CarType;
        //    //        tempCarType.Nbrand = carType.DisplayName;
        //    //        tempCarType.CreateBy = custId;
        //    //        tempCarType.CreateOn = now;
        //    //        tempCarType.UpdateBy = custId;
        //    //        tempCarType.UpdateOn = now;
        //    //        tempCarTypeList.Add(tempCarType);
        //    //    }
        //    //}

        //    List<TempTrialCalMaster> tempTrialCalMasters = new List<TempTrialCalMaster>();
        //    List<TempTrialCalDetail> tempTrialCalDetails = new List<TempTrialCalDetail>();
        //    TempTrialCalMaster tempTrialCalMaster;
        //    TempTrialCalDetail tempTrialCalDetail;
        //    foreach (var project in projectList)
        //    {
        //        tempTrialCalMaster = new TempTrialCalMaster();
        //        tempTrialCalMaster.PdCd = req.PdCd;
        //        tempTrialCalMaster.CustId = custId;
        //        tempTrialCalMaster.ProjectCode = project.ProjectCode;

        //        tempTrialCalMaster.BegDateSgn = DateTime.ParseExact(project.BegDateSgn, "yyyyMMdd", null, System.Globalization.DateTimeStyles.AllowWhiteSpaces);

        //        tempTrialCalMaster.EndDateSgn = DateTime.ParseExact(project.EndDateSgn, "yyyyMMdd", null, System.Globalization.DateTimeStyles.AllowWhiteSpaces);

        //        tempTrialCalMaster.ProjectName = project.ProjectName;
        //        tempTrialCalMaster.CompulsoryPremium = project.CompulsoryPremium;
        //        tempTrialCalMaster.CreateBy = custId;
        //        tempTrialCalMaster.CreateOn = now;
        //        tempTrialCalMaster.UpdateBy = custId;
        //        tempTrialCalMaster.UpdateOn = now;
        //        tempTrialCalMasters.Add(tempTrialCalMaster);

        //        foreach (var item in project.ProjectItems)
        //        {
        //            tempTrialCalDetail = new TempTrialCalDetail();
        //            tempTrialCalDetail.PdCd = req.PdCd;
        //            tempTrialCalDetail.CustId = custId;
        //            tempTrialCalDetail.ProjectCode = project.ProjectCode;
        //            tempTrialCalDetail.ItemCode = item.ItemCode;
        //            tempTrialCalDetail.ItemName = item.ItemName;
        //            tempTrialCalDetail.ItemDescription = item.ItemDescription;
        //            tempTrialCalDetail.Content = item.Content;
        //            tempTrialCalDetail.Premium = item.Premium;
        //            tempTrialCalDetail.CreateBy = custId;
        //            tempTrialCalDetail.CreateOn = now;
        //            tempTrialCalDetail.UpdateBy = custId;
        //            tempTrialCalDetail.UpdateOn = now;
        //            tempTrialCalDetails.Add(tempTrialCalDetail);
        //        }
        //    }

        //    // 紀錄當前執行指令的 TableName
        //    var tableName = string.Empty;
        //    using (var conn = new SqlConnection(ConfigHelper.GetINSCon()))
        //    {
        //        conn.Open();
        //        using (var trans = conn.BeginTransaction())
        //        {
        //            try
        //            {
        //                if (tempVehicleBrand != null)
        //                {
        //                    tableName = nameof(TempVehicleBrand);
        //                    await conn.ExecuteAsync(Temp_VehicleBrandCmd.Insert, tempVehicleBrand, trans, commandTimeout: this.commandTimeout);
        //                }

        //                if (tempCarTypeList.Any())
        //                {
        //                    tableName = nameof(TempCarTypeList);
        //                    await conn.ExecuteAsync(Temp_CarTypeListCmd.Insert, tempCarTypeList, trans, commandTimeout: this.commandTimeout);
        //                }

        //                if (tempTrialCalMasters.Any())
        //                {
        //                    tableName = nameof(TempTrialCalMaster);
        //                    await conn.ExecuteAsync(Temp_TrialCalMasterCmd.Insert, tempTrialCalMasters, trans, commandTimeout: this.commandTimeout);
        //                }

        //                if (tempTrialCalDetails.Any())
        //                {
        //                    tableName = nameof(TempTrialCalDetail);
        //                    await conn.ExecuteAsync(Temp_TrialCalDetailCmd.Insert, tempTrialCalDetails, trans, commandTimeout: this.commandTimeout);
        //                }

        //                if (CIFRes != null)
        //                {
        //                    tableName = nameof(CustomerInfo);
        //                    await conn.ExecuteAsync(CustomerInfoCmd.InsertOrUpdate, CIFRes, trans, commandTimeout: this.commandTimeout);
        //                }

        //                await trans.CommitAsync();
        //            }
        //            catch (Exception ex)
        //            {
        //                if (tableName == nameof(CustomerInfo))
        //                    //this.eventLog.Log("INS0000004", $"PremiumTrial SavePremiumTrial Error: INS0000004, Table: CustomerInfo, CIFRes: {JsonConvert.SerializeObject(CIFRes)}", italIfId, italIfId, ex);
        //                else
        //                            //this.eventLog.Log("INS0000004", $"PremiumTrial SavePremiumTrial Error: INS0000004, Table: {tableName}", italIfId, italIfId, ex);

        //                            await trans.RollbackAsync();

        //                throw new DapperException(tableName, ex);
        //            }
        //        }
        //    }
        //}


        ///// <summary>
        ///// 呼叫 AML FetchService API
        ///// </summary>
        //private async Task<StandardModel<QueryAMLFetchServiceResp>> QueryAMLFetchService(
        //    StandardHeader standardHeader, string interfaceId)
        //{
        //    // 發查 EAI 系統 AML_FetchService API
        //    var amlFetchServiceRes =
        //        await this.EAIAdapter.QueryAMLFetchService(standardHeader, interfaceId);

        //    // 若 FetchService 回傳的交易結果不是成功，回傳:RspsRsltCd != "0"
        //    if (amlFetchServiceRes == null ||
        //        (amlFetchServiceRes.Header != null &&
        //        amlFetchServiceRes.Header.RspsRsltCd != StandardResCodeType.NormalProcessing))
        //    {
        //        var msgJson = string.Empty;
        //        StandardHeader header = standardHeader;
        //        if (amlFetchServiceRes != null)
        //        {
        //            msgJson = JsonConvert.SerializeObject(amlFetchServiceRes.Message);
        //            header = amlFetchServiceRes.Header;
        //            // Log
        //            //this.eventLog.Log("LBEI005024", $"CreateOrder 01 QueryAMLFetchService Error: LBEI005024, Response data missing or RspsRsltCd not 0, Message: {msgJson}", interfaceId, interfaceId);
        //        }
        //        else
        //        {
        //            // Log
        //            //this.eventLog.Log("LBEI005024", $"CreateOrder 01 QueryAMLFetchService Error: LBEI005024, Response data is null", interfaceId, interfaceId);
        //        }

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI005024);
        //    }

        //    // Log
        //    //this.eventLog.Trace("CreateOrder 01 CheckAMLFetchServiceResp Start", interfaceId, interfaceId);

        //    // 檢核 AML_FetchService API 回傳欄位
        //    this.CheckAMLFetchServiceResp(amlFetchServiceRes.Data, interfaceId);

        //    // Log
        //    //this.eventLog.Trace("CreateOrder 01 CheckAMLFetchServiceResp End", interfaceId, interfaceId);

        //    return amlFetchServiceRes;
        //}

        ///// <summary>
        ///// 檢核 AML FetchService API 回傳欄位值
        ///// </summary>
        //private void CheckAMLFetchServiceResp(QueryAMLFetchServiceResp resp, string italIfId)
        //{
        //    // 回傳的 riskLevel 欄位為空值
        //    if (resp == null ||
        //        resp.FetchServiceResponse == null ||
        //        resp.FetchServiceResponse.PartyProperties == null ||
        //        string.IsNullOrEmpty(resp.FetchServiceResponse.PartyProperties.RiskLevel))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001005", "CreateOrder 01 CheckAMLFetchServiceResp Error: LBEI001005, Response RiskLevel is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001005, "riskLevel");
        //    }

        //    // 回傳的 riskLevel 欄位不為空值，且值不是 1、2、4 其中一個
        //    if (resp.FetchServiceResponse.PartyProperties.RiskLevel != AmlRiskLevelType.Low &&
        //        resp.FetchServiceResponse.PartyProperties.RiskLevel != AmlRiskLevelType.Medium &&
        //        resp.FetchServiceResponse.PartyProperties.RiskLevel != AmlRiskLevelType.High)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001006", "CreateOrder 01 CheckAMLFetchServiceResp Error: LBEI001006, Response RiskLevel value not 1,2,4", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001006, "riskLevel");
        //    }
        //}

        ///// <summary>
        ///// 檢核要 Create Open PRE 或 Close PRE
        ///// </summary>
        ///// <returns></returns>
        //private string CheckCreatePREType(FetchServiceResp fetchServiceResp, string custId)
        //{
        //    var alertFlag = string.Empty;
        //    var riskLevel = string.IsNullOrEmpty(fetchServiceResp.PartyProperties.RiskLevel)
        //        ? fetchServiceResp.PartyProperties.RiskLevel
        //        : fetchServiceResp.PartyProperties.RiskLevel.Trim();

        //    // ClosePRE 時機: riskLevel (風險等級)為 2 或 1(中、低風險) 。
        //    if (riskLevel != AmlRiskLevelType.High)
        //    {
        //        alertFlag = YesNoType.No;
        //    }

        //    if (alertFlag != string.Empty) return alertFlag;

        //    // 取得會員狀態
        //    var memberStatus = this.commonService.GetMemberStatus(custId);

        //    // Open PRE 時機 1 : 高風險，且 AML EDD 狀態為 Reject
        //    if (riskLevel == AmlRiskLevelType.High && memberStatus.AmlEDDStatus == AmlEddResultType.Reject)
        //    {
        //        alertFlag = YesNoType.Yes;
        //    }

        //    if (alertFlag != string.Empty) return alertFlag;

        //    FetchServiceAlertDetail_TupleType alertDetail = null;
        //    if (fetchServiceResp.AlertDetails != null &&
        //        fetchServiceResp.AlertDetails.AlertDetails_InnerSet != null &&
        //        fetchServiceResp.AlertDetails.AlertDetails_InnerSet.FetchService_AlertDetails_InnerSet_TupleType != null)
        //    {
        //        alertDetail = fetchServiceResp.AlertDetails.AlertDetails_InnerSet.FetchService_AlertDetails_InnerSet_TupleType
        //            .FirstOrDefault(a => a.AlertID == memberStatus.AlertId);
        //    };

        //    // Open PRE 時機 2 : 高風險，且會員資料無 Alert ID 或 無 AlertDetail 或無 CloseDate
        //    if (riskLevel == AmlRiskLevelType.High &&
        //        (string.IsNullOrEmpty(memberStatus.AlertId) || alertDetail == null || string.IsNullOrEmpty(alertDetail.ClosedDate)))
        //    {
        //        alertFlag = YesNoType.Yes;
        //    }

        //    if (alertFlag != string.Empty) return alertFlag;

        //    // Open PRE 時機 3 : 高風險，且系統日減上次 EDD Alert Close Date > 7 天
        //    if (alertDetail != null)
        //    {
        //        var subDays = DateTime.Now.Subtract(DateTime.Parse(alertDetail.ClosedDate)).Days;
        //        if (subDays > 7)
        //        {
        //            alertFlag = YesNoType.Yes;
        //        }
        //    }

        //    return alertFlag;
        //}

        ///// <summary>
        ///// 呼叫 AML AML_IMPL_AML_CDD_PRE_DistributeAlertService API
        ///// </summary>
        //private async Task<StandardModel<QueryAMLImplAmlCddRreResp>> QueryAMLIMPLCddPre(
        //    StandardHeader standardHeader, string alertFlag, string interfaceId,
        //    string riskLevel, string acceptNo)
        //{
        //    // 發查 EAI 系統 QueryAMLIMPLCddPre API
        //    var amlPRERes =
        //        await this.EAIAdapter.QueryAMLIMPLCddPre(
        //            standardHeader, alertFlag, interfaceId, riskLevel, acceptNo);

        //    // 若 AMLIMPLCddPre 回傳的交易結果不是成功，回傳:RspsRsltCd != "0"
        //    if (amlPRERes == null ||
        //        (amlPRERes.Header != null &&
        //        amlPRERes.Header.RspsRsltCd != StandardResCodeType.NormalProcessing))
        //    {
        //        var msgJson = string.Empty;
        //        StandardHeader header = standardHeader;
        //        if (amlPRERes != null)
        //        {
        //            msgJson = JsonConvert.SerializeObject(amlPRERes.Message);
        //            header = amlPRERes.Header;
        //        }

        //        // Log
        //        //this.eventLog.Log("LBEI005054", $"CreateOrder 01 QueryAMLIMPLCddPre Error: LBEI005054, Response data missing or RspsRsltCd not 0, Message: {msgJson}", interfaceId, interfaceId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI005054);
        //    }

        //    // Log
        //    //this.eventLog.Trace($"CreateOrder 01 CheckAMLIMPLCddPreResp Start", interfaceId, interfaceId);

        //    // 檢核 QueryAMLIMPLCddPre API 回傳欄位
        //    this.CheckAMLIMPLCddPreResp(amlPRERes.Data, interfaceId);

        //    // Log
        //    //this.eventLog.Trace($"CreateOrder 01 CheckAMLIMPLCddPreResp End", interfaceId, interfaceId);

        //    return amlPRERes;
        //}

        ///// <summary>
        ///// 檢核 AML AML_IMPL_AML_CDD_PRE_DistributeAlertService API 回傳欄位值
        ///// </summary>
        //private void CheckAMLIMPLCddPreResp(QueryAMLImplAmlCddRreResp resp, string italIfId)
        //{
        //    // 回傳的 alertId 欄位為空值
        //    if (resp == null ||
        //        resp.IMPL_AML_CDD_PRE_DistributeAlertServiceResponse == null ||
        //        string.IsNullOrEmpty(resp.IMPL_AML_CDD_PRE_DistributeAlertServiceResponse.AlertId))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001005", "CreateOrder 01 CheckAMLIMPLCddPreResp Error: LBEI001005, Response AlertId is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001005, "alertId");
        //    }

        //    // 回傳的 alertFlag 欄位為空值
        //    if (string.IsNullOrEmpty(resp.IMPL_AML_CDD_PRE_DistributeAlertServiceResponse.AlertFlag))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001005", "CreateOrder 01 CheckAMLIMPLCddPreResp Error: LBEI001005, Response AlertFlag is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001005, "alertFlag");
        //    }
        //}

        ///// <summary>
        ///// 儲存發查 AML 結果
        ///// </summary>
        ///// <param name="fetchServiceResp"></param>
        ///// <param name="amlCddRreresp"></param>
        ///// <param name="custId"></param>
        ///// <param name="acceptNo"></param>
        ///// <param name="alertFlag"></param>
        ///// <param name="italIfId"></param>
        ///// <returns></returns>
        //private async Task SaveAMLRes(
        //    QueryAMLFetchServiceResp fetchServiceResp, QueryAMLImplAmlCddRreResp amlCddRreresp,
        //    string custId, string acceptNo, string alertFlag, string italIfId)
        //{
        //    var memberProfile = this.memberProfileRepo.Queryable()
        //        .FirstOrDefault(m => m.CustId == custId);

        //    if (memberProfile == null)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI004002", $"CreateOrder 01 SaveAMLRes Error: LBEI004002, CustId: {custId} not exist in MemberProfile", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004002);
        //    }

        //    var now = DateTime.Now;

        //    DateTime? closeDate = null;
        //    if (fetchServiceResp != null &&
        //        fetchServiceResp.FetchServiceResponse != null &&
        //        fetchServiceResp.FetchServiceResponse.AlertDetails != null &&
        //        fetchServiceResp.FetchServiceResponse.AlertDetails.AlertDetails_InnerSet != null &&
        //        fetchServiceResp.FetchServiceResponse.AlertDetails
        //        .AlertDetails_InnerSet.FetchService_AlertDetails_InnerSet_TupleType != null)
        //    {
        //        var alertDetail = fetchServiceResp.FetchServiceResponse.AlertDetails
        //            .AlertDetails_InnerSet.FetchService_AlertDetails_InnerSet_TupleType
        //            .FirstOrDefault(a => a.AlertID == memberProfile.AlertId);

        //        if (alertDetail != null && !string.IsNullOrEmpty(alertDetail.ClosedDate))
        //        {
        //            closeDate = DateTime.Parse(alertDetail.ClosedDate);
        //        }
        //    };

        //    DateTime? lastReviewDate = null;
        //    var newRiskLevel = string.Empty;

        //    if (fetchServiceResp != null &&
        //        fetchServiceResp.FetchServiceResponse != null &&
        //        fetchServiceResp.FetchServiceResponse.PartyProperties != null)
        //    {
        //        if (!string.IsNullOrEmpty(fetchServiceResp.FetchServiceResponse.PartyProperties.LastReviewDate))
        //        {
        //            lastReviewDate =
        //                DateTime.Parse(fetchServiceResp.FetchServiceResponse.PartyProperties.LastReviewDate);
        //        }

        //        newRiskLevel = string.IsNullOrEmpty(fetchServiceResp.FetchServiceResponse.PartyProperties.RiskLevel)
        //            ? fetchServiceResp.FetchServiceResponse.PartyProperties.RiskLevel
        //            : fetchServiceResp.FetchServiceResponse.PartyProperties.RiskLevel.Trim();
        //    }

        //    var newAlertId = memberProfile.AlertId;
        //    if (amlCddRreresp != null &&
        //        amlCddRreresp.IMPL_AML_CDD_PRE_DistributeAlertServiceResponse != null)
        //    {
        //        newAlertId = amlCddRreresp.IMPL_AML_CDD_PRE_DistributeAlertServiceResponse.AlertId;
        //    }

        //    // 更新資料會員資料
        //    memberProfile.AmlriskLevel = newRiskLevel;

        //    // alertFlag = Y 或 N 時
        //    if (!string.IsNullOrEmpty(alertFlag))
        //    {
        //        memberProfile.AlertId = newAlertId;
        //        memberProfile.AmllastReviewDate = lastReviewDate;
        //    }

        //    string amleddresult = memberProfile.Amleddresult;
        //    if (alertFlag == YesNoType.No)
        //    {
        //        amleddresult = AmlEddResultType.NA;
        //        memberProfile.Amleddresult = AmlEddResultType.NA;
        //        memberProfile.AmlclosedDate = now;
        //    }

        //    if (alertFlag == YesNoType.Yes)
        //    {
        //        amleddresult = string.Empty;
        //        memberProfile.Amleddresult = string.Empty;
        //        memberProfile.AmlclosedDate = null;
        //    }

        //    memberProfile.UpdateBy = custId;
        //    memberProfile.UpdateOn = now;

        //    var memberAml = new MemberAml();
        //    memberAml.CustId = custId;
        //    memberAml.AcceptNo = acceptNo;
        //    memberAml.Amleddresult = string.IsNullOrEmpty(amleddresult)
        //        ? amleddresult
        //        : amleddresult.Trim();
        //    // memberAml.Amleddstatus = amleddstatus;
        //    memberAml.AmllastReviewDate = lastReviewDate;
        //    memberAml.AmlclosedDate = closeDate;
        //    memberAml.CreateBy = custId;
        //    memberAml.CreateOn = now;
        //    memberAml.UpdateBy = custId;
        //    memberAml.UpdateOn = now;
        //    memberAml.NewRiskLevel = newRiskLevel;
        //    memberAml.AlertId = newAlertId;

        //    // 紀錄當前執行指令的 TableName
        //    var tableName = string.Empty;
        //    using (var conn = new SqlConnection(ConfigHelper.GetINSCon()))
        //    {
        //        conn.Open();
        //        using (var trans = conn.BeginTransaction())
        //        {
        //            try
        //            {
        //                tableName = nameof(MemberAml);
        //                await conn.ExecuteAsync(
        //                    MemberAMLCmd.Insert, memberAml, trans, commandTimeout: this.commandTimeout);

        //                tableName = nameof(MemberProfile);
        //                await conn.ExecuteAsync(
        //                    MemberProfileCmd.Update, memberProfile, trans, commandTimeout: this.commandTimeout);

        //                await trans.CommitAsync();
        //            }
        //            catch (Exception ex)
        //            {
        //                //this.eventLog.Log("INS0000004", "CreateOrder 01 SaveAMLRes Commit Error", italIfId, italIfId, ex);
        //                await trans.RollbackAsync();


        //                throw new DapperException(tableName, ex);
        //            }
        //        }
        //    }
        //}

        ///// <summary>
        ///// CreateOrder KYC 階段，發送 AOA 方法
        ///// </summary>
        //private async Task KYCStepSendAOA(
        //    StandardHeader header, string acceptNo, string informItemPremium)
        //{
        //    var italIfId = header.ItalIfId;

        //    // Log
        //    //this.eventLog.Trace("CreateOrder 01 KYCStepSendAOA Start", italIfId, italIfId);

        //    // 取得投保相關人員檔_暫存檔要保人資料
        //    var tempPolicyPersonInfo = this.tempPolicyPersonInfoRepo.Queryable()
        //        .FirstOrDefault(t => t.AcceptNo == acceptNo && t.PersonType == PersonType.Applicant);
        //    if (tempPolicyPersonInfo == null)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI004001", $"CreateOrder 01 KYCStepSendAOA Error: LBEI004001, AcceptNo: {acceptNo} and PersonType: {PersonType.Applicant} not exist in TempPolicyPersonInfo", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);
        //    }

        //    StandardModel<SendUMSResp> UMSRes = null;
        //    try
        //    {
        //        var req =
        //            this.GetSendAOAReq(header.CustId, informItemPremium, tempPolicyPersonInfo.PersonName);

        //        // 發送 AML EDD 補件 AOA
        //        UMSRes = await this.UMSAdapter.SendEDDNotify(header, req, header.ItalIfId);
        //    }
        //    catch (Exception ex)
        //    {
        //        // Log
        //        //this.eventLog.Log("INS0000008", $"CreateOrder 01 KYCStepSendAOA Error: INS0000008", italIfId, italIfId, ex);

        //        throw;
        //    }
        //    finally
        //    {
        //        // Log
        //        //this.eventLog.Trace($"CreateOrder 01 SaveSendAOARes Start", italIfId, italIfId);

        //        var logHeader = UMSRes != null ? UMSRes.Header : header;

        //        // 紀錄 AOA 發送結果
        //        await this.SaveSendAOARes(
        //            this.GetSendAOAHeader(logHeader),
        //            this.GetSendAOAReq(header.CustId, informItemPremium, tempPolicyPersonInfo.PersonName),
        //            UMSRes,
        //            italIfId);

        //        // Log
        //        //this.eventLog.Trace($"CreateOrder 01 SaveSendAOARes End", italIfId, italIfId);
        //    }

        //    // 若 UMS 回傳的交易結果不是成功，回傳:ReturnCode = LBNA000001 回覆的錯誤訊息。
        //    if (UMSRes == null || (UMSRes.Message != null && UMSRes.Message.MsgId != "LBNA000001"))
        //    {
        //        var msgJson = string.Empty;
        //        StandardHeader resHeader = header;
        //        if (UMSRes != null)
        //        {
        //            msgJson = JsonConvert.SerializeObject(UMSRes.Message);
        //            resHeader = UMSRes.Header;
        //        }

        //        // Log
        //        //this.eventLog.Log("LBEI006004", $"CreateOrder 01 SendAOA Error: LBEI006004, Response data missing or MsgId not LBNA000001, Message: {msgJson}", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI006004);
        //    }

        //    // Log
        //    //this.eventLog.Trace($"CreateOrder 01 SendAOA End", italIfId, italIfId);

        //    throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI003003);
        //}

        ///// <summary>
        ///// 紀錄 AOA 發送結果
        ///// </summary>
        ///// <returns></returns>
        //private async Task SaveSendAOARes(
        //    StandardHeader header, SendUMSReq req, StandardModel<SendUMSResp> resp, string italIfId)
        //{
        //    var now = DateTime.Now;

        //    var sendStatus = UMSNoticeSendStatus.Successfully;
        //    // 若 UMS 回傳的交易結果不是成功，回傳:ReturnCode = LBNA000001 回覆的錯誤訊息。
        //    if (resp == null || (resp.Message != null && resp.Message.MsgId != "LBNA000001"))
        //    {
        //        sendStatus = UMSNoticeSendStatus.Failed;
        //    }

        //    var umssendListAml = new UmssendListAml()
        //    {
        //        ReqHeaders = JsonConvert.SerializeObject(header),
        //        ReqBody = JsonConvert.SerializeObject(req),
        //        SendStatus = sendStatus,
        //        ErrorRetry = 0,
        //        ReturnCode = resp != null && resp.Message != null
        //        ? resp.Message.MsgId
        //        : string.Empty,
        //        RespHeaders = resp != null
        //        ? JsonConvert.SerializeObject(resp.Header)
        //        : string.Empty,
        //        RespBody = resp != null
        //        ? JsonConvert.SerializeObject(resp.Data)
        //        : string.Empty,
        //        RunBy = italIfId,
        //        CreateBy = italIfId,
        //        UpdateBy = italIfId,
        //        CreateOn = now,
        //        UpdateOn = now
        //    };

        //    // 紀錄當前執行指令的 TableName
        //    var tableName = string.Empty;
        //    using (var conn = new SqlConnection(ConfigHelper.GetINSCon()))
        //    {
        //        conn.Open();
        //        using (var trans = conn.BeginTransaction())
        //        {
        //            try
        //            {
        //                tableName = nameof(UmssendListAml);
        //                await conn.InsertAsync(umssendListAml, trans, commandTimeout: this.commandTimeout);

        //                await trans.CommitAsync();
        //            }
        //            catch (Exception ex)
        //            {
        //                //this.eventLog.Log("INS0000004", "CreateOrder 01 SaveSendAOARes Commit Error", italIfId, italIfId, ex);

        //                await trans.RollbackAsync();

        //                throw new DapperException(tableName, ex);
        //            }
        //        }
        //    }
        //}

        ///// <summary>
        ///// 取得發送 AOA ReqHeader
        ///// </summary>
        //private StandardHeader GetSendAOAHeader(StandardHeader header)
        //{
        //    string guid = GenGuid();
        //    string timestamp = GuidDate.ToString("yyyyMMddHHmmssfff");

        //    var res = this.mapper.Map<StandardHeader>(header);
        //    res.Guid = guid;
        //    res.ItalGuid = guid;
        //    res.ItalIfId = InsChannelOperation.UMS_SendEDDNotify;
        //    res.IfId = InsChannelOperation.UMS_SendEDDNotify;
        //    res.RcvSrvcId = "SZCUC016003";
        //    res.RcvSysCd = ReceiveSysCodeType.TO_CBK;
        //    res.RqstRspsDsCd = RqstRspsDsCdType.Request;
        //    res.TgmTpCd = TelegramCodeType.Sync;
        //    res.FrstTnsnTmstmp = timestamp;
        //    res.TnsnTmstmp = timestamp;

        //    return res;
        //}

        ///// <summary>
        ///// 取得發送 AOA Req
        ///// </summary>
        ///// <returns></returns>
        //private SendUMSReq GetSendAOAReq(
        //    string custId, string informItemPremium, string personName)
        //{
        //    // KYC 文件名稱
        //    var kycName = this.commonService.GetSysParamValue("PremiumFrom", informItemPremium);

        //    var req = new SendUMSReq();
        //    req.NotiTpCd = "01";
        //    req.CustNotiKndCd = "07";
        //    req.UmsMsgId = "INS001";
        //    req.SbjtAreaCd = "INS00"; // CCSBZ // INS00
        //    req.CustId = custId;
        //    req.ArrId = DateTime.Now.ToString("yyyyMMddHHmmssfff");
        //    req.RsvdSndDtm = DateTime.Now.ToString("yyyyMMddHHmmss");
        //    req.TpltParmVal1 = this.ChineseNameMake(personName);
        //    req.TpltParmVal2 = DateTime.Now.AddDays(7).ToString("yyyy/MM/dd");
        //    req.TpltParmVal3 = kycName;
        //    req.TpltParmVal4 = string.Empty;
        //    req.MbleAppId = "1";

        //    // 預設帶入欄位空值
        //    req.NtcRsndChnlSeqNbr = "";
        //    req.AcctNbr = "";
        //    req.TpltParmVal5 = "";
        //    req.TpltParmVal6 = "";
        //    req.TpltParmVal7 = "";
        //    req.TpltParmVal8 = "";
        //    req.TpltParmVal9 = "";
        //    req.TpltParmVal10 = "";
        //    req.TpltParmVal11 = "";
        //    req.TpltParmVal12 = "";

        //    return req;
        //}

        ///// <summary>
        ///// 遮罩中文姓名
        ///// </summary>
        //private string ChineseNameMake(string name) => name.Length switch
        //{
        //    var len when len == 2 | len == 3 => name.MaskString(1, 1),
        //    4 => name.MaskString(1, 2),
        //    var len when len > 4 => name.MaskString(1, len - 2),
        //    _ => name
        //};

        ///// <summary>
        ///// 廠牌查詢
        ///// </summary>
        ///// <param name="req">廠牌查詢 Req</param>
        ///// <returns>廠牌查詢 Resp</returns>
        //public StandardModel<GetBrandResp> GetBrand(StandardModel<GetBrandReq> req)
        //{
        //    // Log
        //    //this.eventLog.Trace("GetBrand Start", req.Header.ItalIfId, req.Header.ItalIfId);

        //    // Log
        //    //this.eventLog.Trace("Step1 GetModel CheckGetBrandReq Start", req.Header.ItalIfId, req.Header.ItalIfId);

        //    // 檢核 VehicleType
        //    this.CheckVehicleType(req.Data.VehicleTypeCd, req.Header.ItalIfId);

        //    // Log
        //    //this.eventLog.Trace("Step1 GetModel CheckGetBrandReq End", req.Header.ItalIfId, req.Header.ItalIfId);

        //    // Log
        //    //this.eventLog.Trace("Step2 GetBrand GetCarBrandList Start", req.Header.ItalIfId, req.Header.ItalIfId);

        //    // 資料查詢
        //    var vehicleBrandItems = this.commonService.SearchSysParamByGroupId(GroupIdType.CarBrandList)
        //        .AsQueryable()
        //        .QueryItem(c => c.ItemValue.Contains(req.Data.BrandValue, StringComparison.CurrentCultureIgnoreCase), req.Data.BrandValue)
        //        .Where(c => c.ItemId.EndsWith(req.Data.VehicleTypeCd))
        //        .Select(c => new VehicleBrandItem() { BrandCode = c.ItemId.Substring(0, 2), BrandValue = c.ItemValue })
        //        .ToList();

        //    // Log
        //    //this.eventLog.Trace("Step2 GetBrand GetCarBrandList End", req.Header.ItalIfId, req.Header.ItalIfId);

        //    var res = new GetBrandResp()
        //    {
        //        VehicleBrandList = vehicleBrandItems
        //    };

        //    // 查無資料
        //    if (vehicleBrandItems == null || vehicleBrandItems.Count() == 0)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI004001", $"GetBrand GetCarBrandList Error: LBEI004001, Request ItemValue: {req.Data.BrandValue} and ItemId:{req.Data.VehicleTypeCd} not exist in SysParam CarBrandList Group", req.Header.ItalIfId, req.Header.ItalIfId);

        //        throw new ChannelAPIException(
        //            ReturnCodes.Failure_InsChannel_LBEI004001, StandardResCodeType.NormalProcessing, res);
        //    }

        //    // Log
        //    //this.eventLog.Trace("GetBrand End", req.Header.ItalIfId, req.Header.ItalIfId);

        //    var messageHeader = this.commonService.GetChannelSuccessMessageHeader();

        //    return this.channelAPICommonService.GetResp<GetBrandResp>(
        //        req.Header, messageHeader, StandardResCodeType.NormalProcessing, res);
        //}

        ///// <summary>
        ///// 廠牌查詢(任意險)
        ///// </summary>
        ///// <param name="req">廠牌查詢 Req</param>
        ///// <returns>廠牌查詢 Resp</returns>
        //public StandardModel<GetBrandResp> InquireBrandID(StandardModel<InquireBrandReq> req)
        //{
        //    var italIfId = req?.Header?.ItalIfId;
        //    // Log
        //    //this.eventLog.Trace("InquireBrandID Start", italIfId, italIfId);

        //    // Log
        //    //this.eventLog.Trace("Step1 InquireBrandID CheckVehicleType Start", italIfId, italIfId);

        //    // 檢核 VehicleType
        //    this.ValidateVehicleType(req?.Data?.VehicleType, italIfId);

        //    // Log
        //    //this.eventLog.Trace("Step1 InquireBrandID CheckVehicleType End", italIfId, italIfId);

        //    // Log
        //    //this.eventLog.Trace("Step2 InquireBrandID GetCarBrandList Start", italIfId, italIfId);

        //    // 資料查詢
        //    var vehicleBrandItems = this.commonService.SearchSysParamByGroupId(GroupIdType.CarBrandList)
        //        .AsQueryable()
        //        .QueryItem(c => c.ItemValue.Contains(req.Data.BrandValue, StringComparison.CurrentCultureIgnoreCase), req.Data.BrandValue)
        //        .Where(c => c.ItemId.EndsWith(req.Data.VehicleType))
        //        .Select(c => new VehicleBrandItem() { BrandCode = c.ItemId.Substring(0, 2), BrandValue = c.ItemValue })
        //        .ToList();

        //    // Log
        //    //this.eventLog.Trace("Step2 InquireBrandID GetCarBrandList End", italIfId, italIfId);

        //    var res = new GetBrandResp()
        //    {
        //        VehicleBrandList = vehicleBrandItems
        //    };

        //    // 查無資料
        //    if (vehicleBrandItems == null || vehicleBrandItems.Count() == 0)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI004001", $"InquireBrandID GetCarBrandList Error: LBEI004001, Request ItemValue: {req.Data.BrandValue} and ItemId:{req.Data.VehicleType} not exist in SysParam CarBrandList Group", req.Header.ItalIfId, req.Header.ItalIfId);

        //        throw new ChannelAPIException(
        //            ReturnCodes.Failure_InsChannel_LBEI004001, StandardResCodeType.NormalProcessing, res);
        //    }

        //    // Log
        //    //this.eventLog.Trace("InquireBrandID End", italIfId, italIfId);

        //    var messageHeader = this.commonService.GetChannelSuccessMessageHeader();

        //    return this.channelAPICommonService.GetResp<GetBrandResp>(
        //        req.Header, messageHeader, StandardResCodeType.NormalProcessing, res);
        //}

        ///// <summary>
        ///// 車型查詢
        ///// </summary>
        ///// <param name="req">車型查詢 Req</param>
        ///// <returns>車型查詢 Resp</returns>
        //public StandardModel<GetModelResp> GetModel(StandardModel<GetModelReq> req)
        //{
        //    // Log
        //    //this.eventLog.Trace("GetModel Start", req.Header.ItalIfId, req.Header.ItalIfId);

        //    // Log
        //    //this.eventLog.Trace("Step1 GetModel CheckGetModelReq Start", req.Header.ItalIfId, req.Header.ItalIfId);

        //    // 傳入欄位檢核
        //    this.CheckGetModelReq(req.Data, req.Header.ItalIfId);

        //    // Log
        //    //this.eventLog.Trace("Step1 GetModel CheckGetModelReq End", req.Header.ItalIfId, req.Header.ItalIfId);

        //    // Log
        //    //this.eventLog.Trace("Step2 GetModel GetCarTypeList Start", req.Header.ItalIfId, req.Header.ItalIfId);

        //    // 資料查詢
        //    var carTypeItems = this.carTypeListRepo.Queryable()
        //        .QueryItem(c => c.VehicleType == req.Data.VehicleTypeCd, req.Data.VehicleTypeCd)
        //        .QueryItem(c => c.Ibrand.StartsWith(req.Data.BrandCode), req.Data.BrandCode)
        //        .QueryItem(c => c.Nbrand.Replace(" ", "").Contains(req.Data.CategoryValue.Replace(" ", "")), req.Data.CategoryValue)
        //        .Select(c => new CarTypeItem() { CategoryCode = c.Ibrand, CategoryValue = c.Nbrand })
        //        .ToList();

        //    // Log
        //    //this.eventLog.Trace("Step2 GetModel GetCarTypeList End", req.Header.ItalIfId, req.Header.ItalIfId);

        //    var res = new GetModelResp()
        //    {
        //        CarTypeList = carTypeItems
        //    };

        //    // 查無資料
        //    if (carTypeItems == null || carTypeItems.Count() == 0)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI004001", $"GetModel GetCarTypeList Error: LBEI004001, Request VehicleType: {req.Data.VehicleTypeCd} and Ibrand:{req.Data.BrandCode} and Nbrand: {req.Data.CategoryValue} not exist in CarTypeList", req.Header.ItalIfId, req.Header.ItalIfId);

        //        throw new ChannelAPIException(
        //            ReturnCodes.Failure_InsChannel_LBEI004001, StandardResCodeType.NormalProcessing, res);
        //    }

        //    // Log
        //    //this.eventLog.Trace("GetModel End", req.Header.ItalIfId, req.Header.ItalIfId);

        //    var messageHeader = this.commonService.GetChannelSuccessMessageHeader();

        //    return this.channelAPICommonService.GetResp<GetModelResp>(
        //        req.Header, messageHeader, StandardResCodeType.NormalProcessing, res);
        //}

        ///// <summary>
        ///// 車型查詢
        ///// </summary>
        ///// <param name="req">車型查詢 Req</param>
        ///// <returns>車型查詢 Resp</returns>
        //public StandardModel<GetModelResp> InquireCarType(StandardModel<InquireCarTypeReq> req)
        //{
        //    var italIfId = req?.Header?.ItalIfId;
        //    // Log
        //    //this.eventLog.Trace("InquireCarType Start", italIfId, italIfId);

        //    // Log
        //    //this.eventLog.Trace("Step1 InquireCarType CheckInquireCarTypeReq Start", italIfId, italIfId);

        //    #region check req data
        //    // 傳入欄位檢核
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(req.Data.BrandCode), req?.Data?.BrandCode, italIfId);
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(req.Data.VehicleType), req?.Data?.VehicleType, italIfId);

        //    // 傳入的出廠年小於1911(如果沒傳此參數，則會是 0)
        //    if (req?.Data?.ManufacturedYear < 1911)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001002", "InquireCarType CheckInquireCarTypeReq Error: LBEI001001, Request ManufacturedYear is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001002, "manufacturedYear");
        //    }

        //    // 檢核 VehicleType
        //    this.ValidateVehicleType(req?.Data?.VehicleType, italIfId);
        //    #endregion

        //    // Log
        //    //this.eventLog.Trace("Step1 InquireCarType CheckInquireCarTypeReq End", italIfId, italIfId);

        //    // Log
        //    //this.eventLog.Trace("Step2 InquireCarType GetCarTypeList Start", italIfId, italIfId);

        //    // 資料查詢
        //    var carTypeItems = this.carTypeListRepo.Queryable()
        //        .QueryItem(c => c.VehicleType == req.Data.VehicleType, req.Data.VehicleType)
        //        .QueryItem(c => c.Ibrand.StartsWith(req.Data.BrandCode), req.Data.BrandCode)
        //        .QueryItem(c => c.Nbrand.Replace(" ", "").Contains(req.Data.CategoryValue.Replace(" ", "")), req.Data.CategoryValue)
        //        .QueryItem(c => c.ManufacturedYear == req.Data.ManufacturedYear, req.Data.ManufacturedYear.ToString())
        //        .Select(c => new CarTypeItem() { CategoryCode = c.Ibrand, CategoryValue = c.Nbrand })
        //        .ToList();

        //    // Log
        //    //this.eventLog.Trace("Step2 InquireCarType GetCarTypeList End", italIfId, italIfId);

        //    var res = new GetModelResp()
        //    {
        //        CarTypeList = carTypeItems
        //    };

        //    // 查無資料
        //    if (carTypeItems == null || carTypeItems.Count() == 0)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI004001", $"InquireCarType GetCarTypeList Error: LBEI004001, Request VehicleType: {req.Data.VehicleType} and Ibrand:{req.Data.BrandCode} and Nbrand: {req.Data.CategoryValue} not exist in CarTypeList", req.Header.ItalIfId, req.Header.ItalIfId);

        //        throw new ChannelAPIException(
        //            ReturnCodes.Failure_InsChannel_LBEI004001, StandardResCodeType.NormalProcessing, res);
        //    }

        //    // Log
        //    //this.eventLog.Trace("InquireCarType End", italIfId, italIfId);

        //    var messageHeader = this.commonService.GetChannelSuccessMessageHeader();

        //    return this.channelAPICommonService.GetResp<GetModelResp>(
        //        req.Header, messageHeader, StandardResCodeType.NormalProcessing, res);
        //}



        ///// <summary>
        ///// 方案組合(任意險)
        ///// </summary>
        ///// <param name="req">方案組合 Req</param>
        ///// <returns>方案組合 Resp</returns>
        //public StandardModel<GetProjectSetResp> F3115_InquireProjectSet(StandardModel<InquireProjectSetReq> req)
        //{
        //    GetProjectSetResp res = new GetProjectSetResp();
        //    MessageHeader messageHeader = new MessageHeader();
        //    var custId = req?.Header?.CustId;
        //    var acceptNo = req?.Data?.AcceptNo;
        //    var italIfId = req?.Header?.ItalIfId;
        //    var productCode = req?.Data?.ProductCode;
        //    try
        //    {
        //        #region check req data
        //        this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(custId), custId, italIfId);
        //        this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(acceptNo), acceptNo, italIfId);
        //        this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(productCode), productCode, italIfId);
        //        this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(req.Data.TagID), req.Data?.TagID, italIfId);
        //        // 檢查是否仍在銷售中
        //        CheckProductSaleState(productCode, italIfId);

        //        #endregion

        //        #region get and check data from db
        //        //以受理編號(AcceptNo)去保單車籍資料檔_暫存檔(Temp_PolicyCarInfo)取得車種類型(VehicleType)
        //        var vehicleType = this.tempPolicyCarInfoRepo.Queryable()
        //                            .Where(s => s.AcceptNo == acceptNo)
        //                            .Select(f => f.VehicleType)
        //                            .FirstOrDefault();
        //        this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(vehicleType), vehicleType, italIfId);

        //        //以車種類型(vehicleType)去取出可選擇的方案清單
        //        var allowProjectList = this.commonService.SearchSysParamByGroupId(GroupIdType.ApplicableVehicleList)
        //                                    .Where(w => w.ItemValue.Contains(vehicleType))
        //                                    .Select(x => x.ItemId)
        //                                    .ToList() ?? new List<string>();

        //        if (allowProjectList.Count() == 0)
        //        {
        //            // Log
        //            //this.eventLog.Log("LBEI004001", $"InquireProjectSetReq Error: LBEI004001, vehicleType: {vehicleType} not exist in {nameof(SysParam)}", italIfId, italIfId);
        //        }
        //        var productMaster = this.productMasterRepo.Queryable().Where(x => x.ProductId == productCode).FirstOrDefault();
        //        var campaignMasterList = this.campaignMasterRepo.Queryable().Where(x => x.ProductId == productCode && allowProjectList.Contains(x.ProjectCode)).ToList();
        //        var tempInsuranceDate = this.tempInsuranceDateRepo.Queryable().Where(s => s.AcceptNo == acceptNo).FirstOrDefault();
        //        var companyProfile = this.companyProfileRepo.Queryable().Where(x => x.CompanyCode == productMaster.CompanyCode).FirstOrDefault();

        //        if (campaignMasterList == null || campaignMasterList.Count() == 0)
        //        {
        //            // Log
        //            //this.eventLog.Log("LBEI004001", $"InquireProjectSetReq Error: LBEI004001, acceptNo: {req.Data.AcceptNo} Data {nameof(CampaignMaster)} join {nameof(SysParam)} not exist", italIfId, italIfId);

        //            throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);
        //        }
        //        if (tempInsuranceDate == null)
        //        {
        //            // Log
        //            //this.eventLog.Log("LBEI004001", $"InquireProjectSetReq Error: LBEI004004, acceptNo: {req.Data.AcceptNo} Data {nameof(TempInsuranceDate)}  not exist", italIfId, italIfId);

        //            throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);
        //        }
        //        if (companyProfile == null)
        //        {
        //            // Log
        //            //this.eventLog.Log("LBEI004001", $"InquireProjectSetReq Error: LBEI004001, acceptNo: {req.Data.AcceptNo} Data {nameof(CompanyProfile)}  not exist", italIfId, italIfId);

        //            throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);
        //        }
        //        #endregion

        //        #region check plan category and prepare date for res
        //        //以受理編號(AcceptNo)去強制險與任意險保期_暫存檔(Temp_InsuranceDate)查可投保類別            
        //        var insurableKind = string.Empty;

        //        if (tempInsuranceDate.IsCompulsoryInsRenewable.HasValue && tempInsuranceDate.IsCompulsoryInsRenewable.Value)
        //        {
        //            insurableKind = PlanCategory.CALI;  //強制險
        //        }
        //        if (tempInsuranceDate.IsVoluntaryInsRenewable.HasValue && tempInsuranceDate.IsVoluntaryInsRenewable.Value)
        //        {
        //            insurableKind = PlanCategory.VALI;  //任意險
        //        }
        //        if (tempInsuranceDate.IsCompulsoryInsRenewable.HasValue &&
        //            tempInsuranceDate.IsCompulsoryInsRenewable.Value &&
        //            tempInsuranceDate.IsVoluntaryInsRenewable.HasValue &&
        //            tempInsuranceDate.IsVoluntaryInsRenewable.Value)
        //        {
        //            insurableKind = string.Empty;  //強制+任意險
        //        }

        //        // 取得目前可投保的方案代碼
        //        var projectCodeList = campaignMasterList.Select(x => x.ProjectCode).ToList();
        //        // 取得可投保的方案代碼方案代碼下特色資料
        //        var campaignFeatureList = this.campaignFeatureRepo.Queryable().Where(x => x.ProductId == productCode && projectCodeList.Contains(x.ProjectCode)).ToList();
        //        // 取得可投保的方案商品資料
        //        var campaignPlanDataList = this.campaignPlanMappingRepo.Queryable()
        //                                    .Where(x => x.ProductId == productCode && projectCodeList.Contains(x.ProjectCode))
        //                                    .Join(
        //                                        this.planMasterRepo.Queryable(),
        //                                        cpm => new { cpm.CompanyCode, cpm.PlanCode, cpm.PlanVer, cpm.PlanType, cpm.PaymentTerm, cpm.BenefitTerm },
        //                                        p => new { p.CompanyCode, p.PlanCode, p.PlanVer, p.PlanType, p.PaymentTerm, p.BenefitTerm },
        //                                        (cpm, p) => new { CampaignPlanMapping = cpm, PlanMaster = p }
        //                                    ).ToList();
        //        // 該專案可投保方案資訊
        //        var campaignMasterAllDataList = campaignMasterList.OrderBy(x => x.CampaignSort)
        //                                            .Select(x => new
        //                                            {
        //                                                CampaignMaster = x,
        //                                                Features = campaignFeatureList.Where(y => y.ProjectCode == x.ProjectCode).ToList(),
        //                                                // 只抓主險
        //                                                Plans = campaignPlanDataList.Where(y => y.CampaignPlanMapping.ProjectCode == x.ProjectCode && y.CampaignPlanMapping.ParentCampaignGid == null).ToList()
        //                                            }).ToList();


        //        if (string.Equals(insurableKind, PlanCategory.CALI))
        //        {
        //            // 抓後台設定的單強方案
        //            campaignMasterAllDataList = campaignMasterAllDataList
        //                                            .Where(x => x.Plans.All(y => string.Equals(y.PlanMaster.PlanCategory, PlanCategory.CALI)))
        //                                            .ToList();
        //        }
        //        else if (string.Equals(insurableKind, PlanCategory.VALI))
        //        {
        //            // 抓後台設定強+任方案,且過濾掉強制險
        //            campaignMasterAllDataList = campaignMasterAllDataList
        //                                            .Where(x => x.Plans.Any(y => string.Equals(y.PlanMaster.PlanCategory, PlanCategory.VALI)))
        //                                            .Select(x => new
        //                                            {
        //                                                CampaignMaster = x.CampaignMaster,
        //                                                // 取得任意險方案特色
        //                                                Features = x.Features.Where(y => string.Equals(y.PlanCategory, PlanCategory.VALI)).ToList(),
        //                                                // 取得任意險商品
        //                                                Plans = x.Plans.Where(y => string.Equals(y.PlanMaster.PlanCategory, PlanCategory.VALI)).ToList()
        //                                            }).ToList();
        //        }

        //        #endregion

        //        #region get res
        //        // 整理回覆資料
        //        List<ProjectAvailableItem> projectList = new List<ProjectAvailableItem>();

        //        foreach (var item in campaignMasterAllDataList)
        //        {
        //            var unit = new ProjectAvailableItem()
        //            {
        //                StartingPrice = item.Features.Sum(x => (int.TryParse(x.ReferencePremium, out int referencePremium) ? referencePremium : 0)),
        //                OriginalPrice = Convert.ToInt32(item.Features.Sum(x => x.OriginalPremium)),
        //                ProjectCode = item.CampaignMaster.ProjectCode,
        //                ProjectName = item.CampaignMaster.Name,
        //                Tag = item.CampaignMaster.CampaignTag,
        //                Period = item.Plans.Select(x => x.CampaignPlanMapping.BenefitTerm).FirstOrDefault(),
        //                Coverages = string.Join(",", item.Features.OrderBy(x => x.PlanCategory).Select(x => x.CampaignFeatures))?.Split(',', StringSplitOptions.RemoveEmptyEntries).Select(y => new ProjectCoverage
        //                {
        //                    Coverage = y
        //                }).ToList()
        //            };
        //            projectList.Add(unit);
        //        }

        //        res = new GetProjectSetResp()
        //        {
        //            CompanyName = companyProfile.CompanyName,
        //            ProductCode = productCode,
        //            ProductName = productMaster.Name,
        //            Projects = projectList
        //        };
        //        #endregion
        //        messageHeader = this.commonService.GetChannelSuccessMessageHeader();
        //    }
        //    catch (Exception e)
        //    {
        //        this.eventLog.Error(e.Message, "F3115_InquireProjectSet");
        //        throw;
        //    }

        //    return this.channelAPICommonService.GetResp<GetProjectSetResp>(req.Header, messageHeader, StandardResCodeType.NormalProcessing, res);
        //}

        ///// <summary>
        ///// F3119_儲存KYC結果(任意險)
        ///// </summary>
        ///// <param name="req"></param>
        ///// <returns></returns>
        ///// <exception cref="ChannelAPIException"></exception>
        ///// <exception cref="DapperException"></exception>
        //public async Task<StandardModel<object>> SaveKYCRecord(StandardModel<SaveKYCRecordReq> req)
        //{
        //    var italIfId = req?.Header?.ItalIfId;
        //    var custId = req?.Header?.CustId;
        //    // req.Header可能因舊的QueryAMLFetchService / QueryAMLIMPLCddPre / KYCStepSendAOA被修改
        //    // 待舊版程式調整前，先以此方式備份req.Header
        //    var responseHeader = this.mapper.Map<StandardHeader>(req.Header);

        //    #region Step1 檢核傳入參數

        //    this.CheckSaveKYCRecordReq(req.Data, custId, italIfId);

        //    #endregion

        //    #region Step2 檢核會員資料

        //    var res = this.commonService.GetMemberStatusV2(custId);

        //    //查無資料
        //    if (res.MemberStatus != MemberStatus.Enable)
        //    {
        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004002);
        //    }
        //    //無效會員
        //    if (res.IsMemberExpired)
        //    {
        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004003);
        //    }

        //    #endregion

        //    #region Step3 AML_FetchService API、判斷產生 Open 或 Close PRE

        //    StandardHeader header;

        //    // 發查 EAI 系統 AML_FetchService API
        //    var amlFetchServiceRes = await this.QueryAMLFetchService(req.Header, italIfId);

        //    header = amlFetchServiceRes.Header;

        //    //判斷產生 Open 或 Close PRE
        //    var alertFlag = this.CheckCreatePREType(amlFetchServiceRes.Data.FetchServiceResponse, custId);

        //    // alertFlag 不為空
        //    if (alertFlag != string.Empty)
        //    {
        //        // 發查 EAI 系統 AML_IMPL_AML_CDD_PRE_DistributeAlertService API
        //        var amlIMPLCddPreRes = await this.QueryAMLIMPLCddPre(
        //            header, alertFlag, header.ItalIfId,
        //            amlFetchServiceRes.Data.FetchServiceResponse.PartyProperties.RiskLevel,
        //            req.Data.AcceptNo);

        //        header = amlIMPLCddPreRes.Header;

        //        //將 AML 呼叫結果寫入 DB
        //        await this.SaveAMLRes(amlFetchServiceRes.Data, amlIMPLCddPreRes.Data, custId,
        //            req.Data.AcceptNo, alertFlag, italIfId);


        //        #region 高風險且 OPEN PRE 發送 AOA，回傳 3003 參數代碼

        //        if (alertFlag == YesNoType.Yes)
        //        {
        //            // 取得[保費收入來源] KYC 答案值
        //            var informItemPremium = req.Data.KycInfo
        //                .Where(f => this.commonService.GetKycIds(KycType.PremiumFrom).Contains(f.Id))
        //                .Select(s => s.AnswerValue)
        //                .FirstOrDefault();

        //            await this.KYCStepSendAOA(
        //               header, req.Data.AcceptNo, informItemPremium);
        //        }

        //        #endregion
        //    }
        //    else
        //    {
        //        #region 將 AML 呼叫結果寫入 DB
        //        await this.SaveAMLRes(
        //            amlFetchServiceRes.Data, null, custId,
        //            req.Data.AcceptNo, alertFlag, italIfId);

        //        #endregion
        //    }

        //    #endregion

        //    #region Step4 寫入保單 KYC 資料檔V1_暫存檔(Temp_KYCRecordV1)            
        //    // 紀錄當前執行指令的 TableName
        //    var tableName = string.Empty;

        //    List<TempKycrecordV1> tempKycrecordV1List = new List<TempKycrecordV1>();

        //    TempKycrecordV1 tempKycrecordV1;
        //    foreach (var item in req.Data.KycInfo)
        //    {
        //        tempKycrecordV1 = new TempKycrecordV1()
        //        {
        //            AcceptNo = req.Data.AcceptNo,
        //            KycId = item.Id,
        //            KycContent = item.Content,
        //            AnswerTitle = item.AnswerTitle,
        //            AnswerValue = item.AnswerValue,
        //            CreateKycTime = DateTimeOffset.FromUnixTimeSeconds(item.CreateTime).LocalDateTime,
        //            CreateBy = req.Header.CustId,
        //            CreateOn = DateTime.Now,
        //            UpdateBy = req.Header.CustId,
        //            UpdateOn = DateTime.Now
        //        };

        //        tempKycrecordV1List.Add(tempKycrecordV1);
        //    }

        //    using (var conn = new SqlConnection(ConfigHelper.GetINSCon()))
        //    {
        //        conn.Open();
        //        using (var trans = conn.BeginTransaction())
        //        {
        //            try
        //            {
        //                tableName = nameof(TempKycrecordV1);
        //                await conn.ExecuteAsync(Temp_KYCRecordV1Cmd.InsertOrUpdate, tempKycrecordV1List, trans, commandTimeout: this.commandTimeout);

        //                await trans.CommitAsync();
        //            }
        //            catch (Exception ex)
        //            {
        //                //this.eventLog.Log("INS0000004", "SaveKYCRecord Commit Error", italIfId, italIfId, ex);

        //                await trans.RollbackAsync();

        //                throw new DapperException(tableName, ex);
        //            }
        //        }
        //    }

        //    #endregion

        //    var messageHeader = this.commonService.GetChannelSuccessMessageHeader();

        //    return this.channelAPICommonService.GetResp<object>(
        //        responseHeader, messageHeader, StandardResCodeType.NormalProcessing, null);
        //}

        ///// <summary>
        ///// 檢核儲存 KYC 結果傳入的參數(任意險)
        ///// </summary>
        ///// <param name="req"></param>
        ///// <param name="custId"></param>
        ///// <param name="italIfId"></param>
        //private void CheckSaveKYCRecordReq(SaveKYCRecordReq req, string custId, string italIfId)
        //{
        //    // 傳入的 CustId 為空
        //    if (string.IsNullOrEmpty(custId))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001001", "CheckSaveKYCRecordReq Error: LBEI001001, Request CustId is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "custId");
        //    }

        //    // 傳入的受理編號為空
        //    if (string.IsNullOrEmpty(req.AcceptNo))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001001", "CheckSaveKYCRecordReq Error: LBEI001001, Request AcceptNo is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "acceptNo");
        //    }

        //    // 傳入的 Id 為空
        //    if (req.KycInfo != null && req.KycInfo.Select(s => s.Id).Where(w => w.Trim() == "").Count() > 0)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001001", "CheckSaveKYCRecordReq Error: LBEI001001, Request KycId is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "KycId");
        //    }

        //    // 傳入的 answerValue 為空
        //    if (req.KycInfo != null && req.KycInfo.Select(s => s.AnswerValue).Where(w => w.Trim() == "").Count() > 0)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001001", "CheckSaveKYCRecordReq Error: LBEI001001, Request AnswerValue is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "answerValue");
        //    }

        //    //檢核 KYC 問項
        //    #region 取得[是否為國際組織重要政治性職務人士] KycId，傳入須為 0、1、2 其中一個
        //    var politicalPersonsIds = this.commonService.GetSysParamValue("KycType", "PoliticalPerson")
        //                        ?.Split(',', StringSplitOptions.RemoveEmptyEntries)
        //                        ?.Where(x => !string.IsNullOrWhiteSpace(x))
        //                        ?.Select(x => x.Trim())
        //                        ?.ToList() ?? new List<string>();

        //    var politicalPersonsValue = req.KycInfo.Where(x => politicalPersonsIds.Contains(x.Id)).Select(s => s.AnswerValue).FirstOrDefault();

        //    if (politicalPersonsValue != null)
        //    {
        //        var politicalPersons = this.commonService.SearchSysParamByGroupId(GroupIdType.PoliticalPerson)
        //            .Select(s => s.ItemId);

        //        // 傳入的值不為空，且輸入值不是 0、1、2 其中一個
        //        if (!politicalPersons.Contains(politicalPersonsValue.ToUpper()))
        //        {
        //            //Log
        //            //this.eventLog.Log("LBEI001002", $"CheckSaveKYCRecordReq Error: LBEI001002, Request answerValue not exist in SysParam KycType Group", italIfId, italIfId);

        //            throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001002, "answerValue 輸入值不是 0、1、2其中一個");
        //        }
        //    }
        //    #endregion

        //    #region 取得[職業類別] KYCId，傳入值須為1、2
        //    var professionTypeIds = this.commonService.GetSysParamValue("KycType", "ProfessionType")
        //                        ?.Split(',', StringSplitOptions.RemoveEmptyEntries)
        //                        ?.Where(x => !string.IsNullOrWhiteSpace(x))
        //                        ?.Select(x => x.Trim())
        //                        ?.ToList() ?? new List<string>();

        //    var professionTypeValue = req.KycInfo.Where(x => professionTypeIds.Contains(x.Id)).Select(s => s.AnswerValue).FirstOrDefault();

        //    if (professionTypeValue != null)
        //    {
        //        var professionTypes = this.commonService.SearchSysParamByGroupId(GroupIdType.ProfessionType)
        //            .Select(s => s.ItemId);

        //        // 傳入的值不為空，且輸入值不是1、2 其中一個
        //        if (!professionTypes.Contains(professionTypeValue.ToUpper()))
        //        {
        //            //Log
        //            //this.eventLog.Log("LBEI001002", $"CheckSaveKYCRecordReq Error: LBEI001002, Request answerValue not exist in SysParam KycType Group", italIfId, italIfId);

        //            throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001002, "answerValue 輸入值不是 1、2其中一個");
        //        }
        //    }
        //    #endregion

        //    #region 取得[是否為聽語障人士] KYCId，傳入值須為Y、N
        //    var hearingSpeechImpairedIds = this.commonService.GetSysParamValue("KycType", "HearingSpeechImpaired")
        //                        ?.Split(',', StringSplitOptions.RemoveEmptyEntries)
        //                        ?.Where(x => !string.IsNullOrWhiteSpace(x))
        //                        ?.Select(x => x.Trim())
        //                        ?.ToList() ?? new List<string>();

        //    var hearingSpeechImpairedValue = req.KycInfo.Where(x => hearingSpeechImpairedIds.Contains(x.Id)).Select(s => s.AnswerValue).FirstOrDefault();

        //    if (hearingSpeechImpairedValue != null)
        //    {
        //        var hearingSpeechImpairedTypes = this.commonService.SearchSysParamByGroupId(GroupIdType.HearingSpeechImpaired)
        //            .Select(s => s.ItemId);

        //        // 傳入的值不為空，且輸入值不是Y、N其中一個
        //        if (!hearingSpeechImpairedTypes.Contains(hearingSpeechImpairedValue.ToUpper()))
        //        {
        //            //Log
        //            //this.eventLog.Log("LBEI001002", $"CheckSaveKYCRecordReq Error: LBEI001002, Request answerValue not exist in SysParam KycType Group", italIfId, italIfId);

        //            throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001002, "answerValue 輸入值不是Y、N其中一個");
        //        }
        //    }
        //    #endregion

        //    #region 取得[保費收入來源] KYCId，傳入值須為1、2、3、4、5、6、7
        //    var premiumFromIds = this.commonService.GetSysParamValue("KycType", "PremiumFrom")
        //                        ?.Split(',', StringSplitOptions.RemoveEmptyEntries)
        //                        ?.Where(x => !string.IsNullOrWhiteSpace(x))
        //                        ?.Select(x => x.Trim())
        //                        ?.ToList() ?? new List<string>();

        //    var premiumFromValue = req.KycInfo.Where(x => premiumFromIds.Contains(x.Id)).Select(s => s.AnswerValue).FirstOrDefault();

        //    if (premiumFromValue != null)
        //    {
        //        var premiumFromTypes = this.commonService.SearchSysParamByGroupId(GroupIdType.PremiumFrom)
        //            .Select(s => s.ItemId);

        //        // 傳入的值不為空，且輸入值不是1、2、3、4、5、6、7 其中一個
        //        if (!premiumFromTypes.Contains(premiumFromValue.ToUpper()))
        //        {
        //            //Log
        //            //this.eventLog.Log("LBEI001002", $"CheckSaveKYCRecordReq Error: LBEI001002, Request answerValue not exist in SysParam KycType Group", italIfId, italIfId);

        //            throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001002, "answerValue 輸入值不是1、2、3、4、5、6、7其中一個");
        //        }
        //    }
        //    #endregion

        //    #region 取得[投保前三個月內是否有辦理終止契約、貸款或保險單借款之情形] KYCId，傳入值須為Y、N
        //    var terminationWithin3MIds = this.commonService.GetSysParamValue("KycType", "TerminationWithin3M")
        //                        ?.Split(',', StringSplitOptions.RemoveEmptyEntries)
        //                        ?.Where(x => !string.IsNullOrWhiteSpace(x))
        //                        ?.Select(x => x.Trim())
        //                        ?.ToList() ?? new List<string>();

        //    var terminationWithin3MValue = req.KycInfo.Where(x => terminationWithin3MIds.Contains(x.Id)).Select(s => s.AnswerValue).FirstOrDefault();

        //    if (terminationWithin3MValue != null)
        //    {
        //        var terminationWithin3MTypes = this.commonService.SearchSysParamByGroupId(GroupIdType.TerminationWithin3M)
        //            .Select(s => s.ItemId);

        //        // 傳入的值不為空，且輸入值不是Y、N其中一個
        //        if (!terminationWithin3MTypes.Contains(terminationWithin3MValue.ToUpper()))
        //        {
        //            //Log
        //            //this.eventLog.Log("LBEI001002", $"CheckSaveKYCRecordReq Error: LBEI001002, Request answerValue not exist in SysParam KycType Group", italIfId, italIfId);

        //            throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001002, "answerValue 輸入值不是Y、N其中一個");
        //        }
        //    }
        //    #endregion

        //}

        ///// <summary>
        ///// 檢核車型查詢傳入的參數
        ///// </summary>
        ///// <param name="req"></param>
        ///// <param name="italIfId"></param>
        //private void CheckGetModelReq(GetModelReq req, string italIfId)
        //{
        //    // 傳入的廠牌代碼為空
        //    if (string.IsNullOrEmpty(req.BrandCode))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001001", "GetModel CheckGetModelReq Error: LBEI001001, Request BrandCode is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "brandCode");
        //    }

        //    // 檢核 VehicleType
        //    this.CheckVehicleType(req.VehicleTypeCd, italIfId);
        //}

        ///// <summary>
        ///// 檢核車種代號
        ///// </summary>
        ///// <param name="vehicleTypeCd"></param>
        ///// <param name="italIfId"></param>
        //private void CheckVehicleType(string vehicleTypeCd, string italIfId)
        //{
        //    // 傳入的車種代號為空
        //    if (string.IsNullOrEmpty(vehicleTypeCd))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001001", "CheckVehicleType Error: LBEI001001, Request VehicleTypeCd is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "vehicleTypeCd");
        //    }

        //    var vehicleTypes = this.commonService.SearchSysParamByGroupId(GroupIdType.VehicleType)
        //        .Select(s => s.ItemId);

        //    // 傳入車種代號/車輛種類不是
        //    // (01重型機車/02輕型機車/03自用小客車/04自用小貨車/22自用小客貨車/32大型重型機車/34輕型小型機車)
        //    // 其中一個(目前資料表中沒有34，須新增)
        //    if (!vehicleTypes.Contains(vehicleTypeCd))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001002", $"CheckVehicleType Error: LBEI001002, Request VehicleTypeCd: {vehicleTypeCd} not exist in SysParam VehicleType Group", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001002, "vehicleTypeCd");
        //    }
        //}

        ///// <summary>
        ///// 檢核車種代號(任意險)
        ///// </summary>
        ///// <param name="vehicleType"></param>
        ///// <param name="italIfId"></param>
        //private void ValidateVehicleType(string vehicleType, string italIfId)
        //{
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(vehicleType), vehicleType, italIfId);

        //    var vehicleTypes = this.commonService.SearchSysParamByGroupId(GroupIdType.VehicleType)
        //        .Select(s => s.ItemId);

        //    this.validationService.CheckQueryObjectNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), "", "", nameof(SysParam), vehicleTypes, italIfId);

        //    // 傳入車種代號/車輛種類不是
        //    // (01重型機車/02輕型機車/03自用小客車/04自用小貨車/22自用小客貨車/32大型重型機車/34輕型小型機車)
        //    if (!vehicleTypes.Contains(vehicleType))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001002", $"CheckVehicleType Error: LBEI001002, Request VehicleType: {vehicleType} not exist in SysParam VehicleType Group", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001002, "vehicleType");
        //    }
        //}

        ///// <summary>
        ///// 回覆核保結果
        ///// </summary>
        ///// <param name="req">回覆核保結果 Req</param>
        ///// <returns>回覆核保結果 Resp</returns>
        //public async Task<StandardModel<ReplyOrderStatusResp>> ReplyOrderStatus(
        //    StandardModel<ReplyOrderStatusReq> req)
        //{
        //    // Log
        //    //this.eventLog.Trace("ReplyOrderStatus Start", req.Header.ItalIfId, req.Header.ItalIfId);

        //    // Log
        //    //this.eventLog.Trace("ReplyOrderStatus CheckReplyOrderStatusReq Start", req.Header.ItalIfId, req.Header.ItalIfId);

        //    // 傳入欄位檢核
        //    this.CheckReplyOrderStatusReq(req.Data, req.Header.ItalIfId);

        //    // Log
        //    //this.eventLog.Trace("ReplyOrderStatus CheckReplyOrderStatusReq End", req.Header.ItalIfId, req.Header.ItalIfId);

        //    // Log
        //    //this.eventLog.Trace("ReplyOrderStatus CheckReplyOrderStatusData Start", req.Header.ItalIfId, req.Header.ItalIfId);

        //    // 傳入的 orderNo + acceptNo 查找 保單主檔
        //    var policyMaster = this.policyMasterRepo.Queryable()
        //        .FirstOrDefault(p => p.OrderNo == req.Data.OrderNo && p.AcceptNo == req.Data.AcceptNo);
        //    if (policyMaster == null)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI004001", $"ReplyOrderStatus CheckReplyOrderStatusData Error: LBEI004001, OrderNo: {req.Data.OrderNo}, AcceptNo: {req.Data.AcceptNo} data not exist PolicyMasterRepo", req.Header.ItalIfId, req.Header.ItalIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);
        //    }

        //    // 檢核回覆核保結果與 DB 資料是否匹配
        //    this.CheckReplyOrderStatusData(req.Data, policyMaster, req.Header.ItalIfId);

        //    // Log
        //    //this.eventLog.Trace("ReplyOrderStatus CheckReplyOrderStatusData End", req.Header.ItalIfId, req.Header.ItalIfId);

        //    // Log
        //    //this.eventLog.Trace("ReplyOrderStatus UpdateReplyOrderStatusData Start", req.Header.ItalIfId, req.Header.ItalIfId);

        //    // 傳入的訂單狀態、保單號碼、訊息備註更新至保單主檔
        //    await this.UpdateReplyOrderStatusData(req.Data, policyMaster, req.Header.ItalIfId);

        //    // Log
        //    //this.eventLog.Trace("ReplyOrderStatus UpdateReplyOrderStatusData End", req.Header.ItalIfId, req.Header.ItalIfId);

        //    // Log
        //    //this.eventLog.Trace("ReplyOrderStatus End", req.Header.ItalIfId, req.Header.ItalIfId);

        //    var messageHeader = this.commonService.GetChannelSuccessMessageHeader();

        //    return this.channelAPICommonService.GetResp<ReplyOrderStatusResp>(
        //        req.Header, messageHeader, StandardResCodeType.NormalProcessing, null);
        //}

        ///// <summary>
        ///// 檢核回覆核保結果傳入的參數值
        ///// </summary>
        ///// <param name="req"></param>
        ///// <param name="italIfId"></param>
        //private void CheckReplyOrderStatusReq(ReplyOrderStatusReq req, string italIfId)
        //{
        //    // 傳入的保險公司訂單編號為空
        //    if (string.IsNullOrEmpty(req.OrderNo))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001001", "ReplyOrderStatus CheckReplyOrderStatusReq Error: LBEI001001, Request OrderNo is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "orderNo");
        //    }

        //    // 傳入的通路進件編號為空
        //    if (string.IsNullOrEmpty(req.AcceptNo))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001001", "ReplyOrderStatus CheckReplyOrderStatusReq Error: LBEI001001, Request AcceptNo is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "acceptNo");
        //    }

        //    // 傳入的保單號碼為空
        //    if (string.IsNullOrEmpty(req.PolicyNo))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001001", "ReplyOrderStatus CheckReplyOrderStatusReq Error: LBEI001001, Request PolicyNo is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "policyNo");
        //    }

        //    // 傳入的要保人 ID 為空
        //    if (string.IsNullOrEmpty(req.Id))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001001", "ReplyOrderStatus CheckReplyOrderStatusReq Error: LBEI001001, Request Id is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "id");
        //    }

        //    // 傳入的訂單狀態為空
        //    if (string.IsNullOrEmpty(req.OrderStatus))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001001", "ReplyOrderStatus CheckReplyOrderStatusReq Error: LBEI001001, Request OrderStatus is empty", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, "orderStatus");
        //    }

        //    // 取得訂單狀態資料
        //    var replyOrderStatus =
        //        this.commonService.SearchSysParamByGroupId(GroupIdType.ReplyOrderStatus)
        //        .Select(s => s.ItemId);
        //    // 傳入的 orderStatus 不為空，且輸入值不是 0、1、2
        //    if (!replyOrderStatus.Contains(req.OrderStatus))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001002", $"ReplyOrderStatus CheckReplyOrderStatusReq Error: LBEI001002, Request OrderStatus: {req.OrderStatus} not exist in SysParam ReplyOrderStatus Group", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001002, "orderStatus");
        //    }
        //}

        ///// <summary>
        ///// 檢核回覆核保結果與 DB 資料是否匹配
        ///// </summary>
        ///// <param name="req"></param>
        ///// <param name="policyMaster"></param>
        ///// <param name="italIfId"></param>
        //private void CheckReplyOrderStatusData(ReplyOrderStatusReq req, PolicyMaster policyMaster, string italIfId)
        //{
        //    // 查找投保相關人員檔
        //    var policyPersonInfo = this.policyPersonInfoRepo.Queryable()
        //        .FirstOrDefault(p => p.AcceptNo == req.AcceptNo && p.PersonType == PersonType.Applicant);
        //    if (policyPersonInfo == null)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI004001", $"ReplyOrderStatus CheckReplyOrderStatusData Error: LBEI004001, Request AcceptNo: {req.AcceptNo} and PersonType: {PersonType.Applicant} not exist in PolicyPersonInfo", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);
        //    }

        //    // 比對保單主檔要保人 ID 與傳入值 id 是否一致
        //    if (policyPersonInfo.Idno != req.Id)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI006003", $"ReplyOrderStatus CheckReplyOrderStatusData Error: LBEI006003,  Request AcceptNo: {req.AcceptNo} and PersonType: {PersonType.Applicant} and Idno: {req.Id} not exist in PolicyPersonInfo", italIfId, italIfId);

        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI006003);
        //    }
        //}

        ///// <summary>
        ///// 將傳入的訂單狀態、保單號碼、訊息備註更新至保單主檔
        ///// </summary>
        ///// <param name="req"></param>
        ///// <param name="policyMaster"></param>
        ///// <param name="italIfId"></param>
        ///// <returns></returns>
        //private async Task UpdateReplyOrderStatusData(ReplyOrderStatusReq req, PolicyMaster policyMaster, string italIfId)
        //{
        //    if (!string.IsNullOrEmpty(req.Message))
        //    {
        //        policyMaster.OrderMessage = req.Message;
        //    }

        //    // TODO 訂單狀態轉換寫入
        //    policyMaster.PolicyNo = req.PolicyNo;
        //    policyMaster.UpdateOn = DateTime.Now;

        //    // 紀錄當前執行指令的 TableName
        //    var tableName = string.Empty;
        //    using (var conn = new SqlConnection(ConfigHelper.GetINSCon()))
        //    {
        //        conn.Open();
        //        using (var trans = conn.BeginTransaction())
        //        {
        //            try
        //            {
        //                tableName = nameof(PolicyMaster);
        //                await conn.ExecuteAsync(PolicyMasterCmd.Update, policyMaster, trans, commandTimeout: this.commandTimeout);

        //                await trans.CommitAsync();
        //            }
        //            catch (Exception ex)
        //            {
        //                //this.eventLog.Log("INS0000004", "UpdateReplyOrderStatusData Commit Error", italIfId, italIfId, ex);

        //                await trans.RollbackAsync();

        //                throw new DapperException(tableName, ex);
        //            }
        //        }
        //    }
        //}

        ///// <summary>
        ///// CreateOrder 使用 GetOrderStatus 進行 DoubleCheck
        ///// </summary>
        ///// <param name="policyMasterCustId">PolicyMaster CustId</param>
        ///// <param name="policyMasterAcceptNo">PolicyMaster AcceptNo</param>
        ///// <param name="policyPersonInfoIdno">PolicyPersonInfo Idno</param>
        ///// <param name="productMasterInsType">ProductMaster InsType</param>
        ///// <param name="productMasterCompanyCode">ProductMaster CompanyCode</param>
        ///// <returns>保險公司 Get Order Status Resp</returns>
        //public async Task<StandardModel<SendGetOrderStatusResp>> CreateOrderCallGetOrderStatusDoubleCheck(string policyMasterCustId, string policyMasterAcceptNo, string policyPersonInfoIdno, string productMasterInsType, string productMasterCompanyCode)
        //{
        //    var header = GenGetOrderStatusHeader(policyMasterCustId);
        //    var data = GenGetOrderStatusData(policyMasterAcceptNo, policyPersonInfoIdno, productMasterInsType);

        //    try
        //    {
        //        var response = await FEBAdapter.GetOrderStatus(header, data, productMasterCompanyCode);

        //        return response;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}

        ///// <summary>
        ///// 執行 CallGetOrderStatus 並更新資料
        ///// </summary>
        ///// <param name="acceptNo">進件編號</param>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <returns>執行過程是否有錯</returns>
        //public async Task<bool> CallGetOrderStatus(string acceptNo, string batchId)
        //{
        //    const string functionName = "CallGetOrderStatus";
        //    DateTime time = DateTime.Now;

        //    var acceptData = this.GetOfficialAcceptData(acceptNo);
        //    var (isValid, validateErrorMsg) = this.ValidateAcceptData(acceptData);
        //    if (!isValid)
        //    {
        //        eventLog.Log("INS0000001", validateErrorMsg, functionName);
        //    }
        //    var currentPolicyMaster = acceptData.PolicyMasters.First();
        //    var policyPersonInfo = acceptData.PolicyPersonInfos.First(o => o.PersonType == PersonType.Applicant);
        //    string pendingStatus = currentPolicyMaster.PendingStatus;
        //    string orderStatus = currentPolicyMaster.OrderStatus;
        //    string paymentStatus = currentPolicyMaster.PaymentStatus;
        //    int retry = 0;
        //    bool needRetry = false;
        //    bool sendFailAOA = false;
        //    string productMasterInsType = null;
        //    string productMasterCompanyCode = null;

        //    try
        //    {
        //        var productMasterData = this.productMasterRepo.Queryable()
        //        .Where(policy => policy.ProductId == currentPolicyMaster.ProductId)
        //        .Select(policy => new { policy.PlanType, policy.CompanyCode })
        //        .First();

        //        productMasterInsType = this.commonService.GetSysParamValue(GroupIdType.InsTypeMapping, productMasterData.PlanType);
        //        productMasterCompanyCode = productMasterData.CompanyCode;

        //        if (string.IsNullOrEmpty(productMasterInsType))
        //        {
        //            eventLog.Log("INS0000001", $"ProductMaster 查無 ProductId 為 {currentPolicyMaster.ProductId} 的 InsType", functionName, currentPolicyMaster.CustId);
        //            return false;
        //        }

        //        if (string.IsNullOrEmpty(productMasterCompanyCode))
        //        {
        //            eventLog.Log("INS0000001", $"ProductMaster 查無 ProductId 為 {currentPolicyMaster.ProductId} 的 CompanyCode", functionName);
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        eventLog.Log("INS0000001", $"ProductMaster 查無 ProductId 為 {currentPolicyMaster.ProductId} 的資料", functionName, currentPolicyMaster.CustId, ex);
        //        return false;
        //    }

        //    var header = GenGetOrderStatusHeader(currentPolicyMaster.CustId);
        //    var data = GenGetOrderStatusData(currentPolicyMaster.AcceptNo, policyPersonInfo.Idno, productMasterInsType);
        //    GetOrderStatusReturnData getOrderStatusReturnData = null;
        //    List<GetOrderStatusPolicyListData> policyList = null;
        //    do
        //    {
        //        var response = await FEBAdapter.GetOrderStatus(header, data, productMasterCompanyCode);
        //        if (CheckGetOrderStatusResponseData(response))
        //        {
        //            if (response.Message.MsgId == "0000")
        //            {
        //                getOrderStatusReturnData = response.Data.ReturnData;
        //                policyList = response.Data.ReturnData.PolicyList;
        //                orderStatus = getOrderStatusReturnData.OrderStatus;
        //                paymentStatus = getOrderStatusReturnData.PaymentStatus;
        //                if (!CheckGetOrderStatusSuccessData(getOrderStatusReturnData, acceptData.PolicyMasters))
        //                {
        //                    return false;
        //                }
        //                if (getOrderStatusReturnData.OrderStatus == OrderStatus.Processing && getOrderStatusReturnData.PaymentStatus == PaymentStatus.Unpaid)
        //                {
        //                    if (retry == 3)
        //                    {
        //                        eventLog.Log("INS0000011", $"GetOrderStatus 連續三次回傳 OrderStatus {getOrderStatusReturnData.OrderStatus} 且 PaymentStatus {getOrderStatusReturnData.PaymentStatus}", functionName, currentPolicyMaster.CustId);
        //                        needRetry = false;
        //                        sendFailAOA = true;
        //                    }
        //                    else
        //                    {
        //                        needRetry = true;
        //                        continue;
        //                    }
        //                }
        //                else if (getOrderStatusReturnData.OrderStatus == OrderStatus.Success && (getOrderStatusReturnData.PaymentStatus == PaymentStatus.Unpaid || getOrderStatusReturnData.PaymentStatus == PaymentStatus.PaidFail))
        //                {
        //                    eventLog.Log("INS0000011", $"GetOrderStatus 回傳 OrderStatus {getOrderStatusReturnData.OrderStatus} PaymentStatus {getOrderStatusReturnData.PaymentStatus}", functionName, currentPolicyMaster.CustId);
        //                    sendFailAOA = true;
        //                }
        //            }
        //            else
        //            {
        //                eventLog.Log("INS0000011", $"GetOrderStatus 回傳結果非 0000，{response.Message.MsgCont}", functionName, currentPolicyMaster.CustId);
        //                sendFailAOA = true;
        //            }

        //            if (sendFailAOA)
        //            {
        //                if (CheckSendFailAOA(productMasterCompanyCode, response.Message, batchId))
        //                    await AddGetOrderStatusAOA(currentPolicyMaster, false, batchId);

        //                pendingStatus = PendingStatus.UnderwriteFail;
        //                orderStatus = null;
        //                paymentStatus = null;
        //            }
        //        }
        //        else
        //            return false;
        //    } while (needRetry && retry++ < 3);

        //    foreach (var policyMaster in acceptData.PolicyMasters)
        //    {
        //        policyMaster.OrderStatus = orderStatus;
        //        policyMaster.PendingStatus = pendingStatus;
        //        policyMaster.PaymentStatus = paymentStatus;
        //        policyMaster.UpdateBy = batchId;
        //        policyMaster.UpdateOn = time;
        //        if (policyList != null && policyList.Any())
        //        {
        //            //2023-03-28 白箱問題修正(Null Dereference) Archi
        //            var policyNo = string.Empty;
        //            if (getOrderStatusReturnData != null)
        //            {
        //                policyNo = getOrderStatusReturnData.PolicyList?.FirstOrDefault(o => o.Category == policyMaster.PlanCategory)?.PolicyNo;
        //                policyMaster.PolicyNo = policyNo;
        //            }
        //            if (currentPolicyMaster.PendingStatus == PendingStatus.Underwriting)
        //            {
        //                policyMaster.InsComUwpassDate = time;
        //                policyMaster.PendingStatus = PendingStatus.UnderwritePass;
        //                policyMaster.PolicyStatus = PolicyStatusType.Active;
        //                policyMaster.ExtendStatus = ExtendStatus.Unable;
        //            }

        //            var policyDetails = acceptData.PolicyDetails.Where(o => o.PlanCategory == policyMaster.PlanCategory).ToList();
        //            policyDetails.ForEach(policyDetail =>
        //            {
        //                policyDetail.PolicyNo = policyNo;
        //                policyDetail.UpdateBy = batchId;
        //                policyDetail.UpdateOn = time;
        //                policyDetailRepo.Update(policyDetail);
        //                DataLogPolicyDetail dataLogPolicyDetail = mapper.Map<DataLogPolicyDetail>(policyDetail);
        //                dataLogPolicyDetail.CreateBy = batchId;
        //                dataLogPolicyDetail.CreateOn = time;
        //                dataLogPolicyDetailRepo.Insert(dataLogPolicyDetail);
        //            });

        //            var policyPersonInfos = acceptData.PolicyPersonInfos.Where(o => o.PlanCategory == policyMaster.PlanCategory).ToList();
        //            policyPersonInfos.ForEach(policyPersonInfo =>
        //            {
        //                policyPersonInfo.PolicyNo = policyNo;
        //                policyPersonInfo.UpdateBy = batchId;
        //                policyPersonInfo.UpdateOn = time;
        //                policyPersonInfoRepo.Update(policyPersonInfo);
        //                DataLogPolicyPersonInfo dataLogPolicyPersonInfo = mapper.Map<DataLogPolicyPersonInfo>(policyPersonInfo);
        //                dataLogPolicyPersonInfo.CreateBy = batchId;
        //                dataLogPolicyPersonInfo.CreateOn = time;
        //                dataLogPolicyPersonInfoRepo.Insert(dataLogPolicyPersonInfo);
        //            });

        //            var policyCarInfos = acceptData.PolicyCarInfos.Where(o => o.PlanCategory == policyMaster.PlanCategory).ToList();
        //            policyCarInfos.ForEach(policyCarInfo =>
        //            {
        //                policyCarInfo.PolicyNo = policyNo;
        //                policyCarInfo.UpdateOn = time;
        //                policyCarInfo.UpdateBy = batchId;
        //                policyCarInfoRepo.Update(policyCarInfo);
        //                DataLogPolicyCarInfo dataLogPolicyCarInfo = mapper.Map<DataLogPolicyCarInfo>(policyCarInfo);
        //                dataLogPolicyCarInfo.CreateBy = batchId;
        //                dataLogPolicyCarInfo.CreateOn = time;
        //                dataLogPolicyCarInfoRepo.Insert(dataLogPolicyCarInfo);
        //            });

        //            acceptData.KycRecordV1s.ForEach(kycRecordV1 =>
        //            {
        //                kycRecordV1.PolicyNo = policyNo;
        //                kycRecordV1.UpdateBy = batchId;
        //                kycRecordV1.UpdateOn = time;
        //                kycRecordV1Repo.Update(kycRecordV1);
        //                DataLogKycrecordV1 dataLogKycrecordV1 = mapper.Map<DataLogKycrecordV1>(kycRecordV1);
        //                dataLogKycrecordV1.CreateBy = batchId;
        //                dataLogKycrecordV1.CreateOn = time;
        //                dataLogKycRecordV1Repo.Insert(dataLogKycrecordV1);
        //            });
        //        }
        //    }
        //    if (orderStatus == OrderStatus.Processed)
        //    {
        //        var customerProfile = this.customerProfileRepo.Queryable()
        //       .Where(customerProfileRepo => customerProfileRepo.CustId == currentPolicyMaster.CustId)
        //       .FirstOrDefault();

        //        bool needInsert = false;
        //        if (customerProfile == null)
        //        {
        //            customerProfile = new CustomerProfile();
        //            customerProfile.CustId = currentPolicyMaster.CustId;
        //            customerProfile.CreateOn = time;
        //            customerProfile.CreateBy = batchId;
        //            needInsert = true;
        //        }

        //        customerProfile.Idno = policyPersonInfo.Idno;
        //        customerProfile.Name = policyPersonInfo.PersonName;
        //        customerProfile.Birthday = policyPersonInfo.Birthday.Value;
        //        customerProfile.Mobile = policyPersonInfo.Mobile;
        //        customerProfile.Phone1 = policyPersonInfo.Phone1;
        //        customerProfile.Phone2 = policyPersonInfo.Phone2;
        //        customerProfile.Email = policyPersonInfo.Email;
        //        customerProfile.Zipcode = policyPersonInfo.Zipcode;
        //        customerProfile.SendCity = policyPersonInfo.SendCity;
        //        customerProfile.SendCountry = policyPersonInfo.SendCountry;
        //        customerProfile.Addr = policyPersonInfo.Addr;
        //        customerProfile.UpdateOn = time;
        //        customerProfile.UpdateBy = batchId;

        //        if (needInsert)
        //            customerProfileRepo.Insert(customerProfile);
        //        else
        //            customerProfileRepo.Update(customerProfile);
        //    }

        //    this.insUnitOfWork.BeginTransaction();
        //    try
        //    {
        //        await this.insUnitOfWork.SaveChangesAsync();
        //    }
        //    catch (Exception ex)
        //    {
        //        this.insUnitOfWork.Rollback();
        //        eventLog.Log("INS0000004", "GetOrderStatus 更新 DB 失敗", functionName, currentPolicyMaster.CustId, ex);
        //        return false;
        //    }
        //    this.insUnitOfWork.Commit();
        //    this.insUnitOfWork.ResetContextState();
        //    return true;
        //}

        ///// <summary>
        ///// 驗證 GetOrderStatus 0000 回傳資料是否足夠
        ///// </summary>
        ///// <param name="getOrderStatusReturnData"></param>
        ///// <param name="currentPolicyMasters"></param>
        ///// <returns></returns>
        //private bool CheckGetOrderStatusSuccessData(GetOrderStatusReturnData getOrderStatusReturnData, List<PolicyMaster> currentPolicyMasters)
        //{
        //    var functionName = "CheckGetOrderStatusSuccessData";
        //    var acceptNo = currentPolicyMasters.First().AcceptNo;
        //    if (string.IsNullOrEmpty(getOrderStatusReturnData.OrderStatus))
        //    {
        //        eventLog.Log("INS0000010", $"GetOrderStatus 受理編號: {acceptNo}, 呼叫 GetOrderStatus 取得的 OrderStatus is null", functionName);
        //        return false;
        //    }

        //    var policyList = getOrderStatusReturnData.PolicyList;
        //    Dictionary<string, bool> categoryCheck = currentPolicyMasters.ToDictionary(o => o.PlanCategory, o => policyList.Count(p => p.Category == o.PlanCategory) == 1);

        //    if (policyList == null
        //        || policyList.Count() != currentPolicyMasters.Count
        //        || categoryCheck.Any(o => !o.Value)
        //        || (policyList.Any(o => string.IsNullOrEmpty(o.PolicyNo)) && policyList.Any(o => !string.IsNullOrEmpty(o.PolicyNo))))
        //    {
        //        //this.eventLog.Log("INS0000013", $"CreateOrder 受理編號: {acceptNo}, 呼叫 CreateOrder 取得的 PolicyList 數量不符", functionName);
        //        return false;
        //    }
        //    return true;
        //}

        ///// <summary>
        ///// 檢查並添加電訪清單
        ///// </summary>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <returns>無回傳值</returns>
        //public async Task CheckTelInterviewList(string batchId)
        //{
        //    const string functionName = "CallGetOrderStatus";

        //    if (InitSamplingTypeStr(batchId) == false)
        //        return;

        //    int? currentProductInfoSamplingPct = null;
        //    bool needTelInterview = true;
        //    string productMasterCompanyCode = null;
        //    List<PolicyDetail> policyDetailList = null;

        //    eventLog.Trace("檢查並添加電訪清單", functionName);

        //    var acceptList = this.policyMasterRepo.Queryable()
        //        .Where(policy => policy.PendingStatus == PendingStatus.UnderwritePass && string.IsNullOrEmpty(policy.TelInterviewType) && policy.InsComUwpassDate.Value.Date <= DateTime.Now.Date).AsEnumerable()
        //        .GroupBy(o => o.AcceptNo).ToList();

        //    foreach (var acceptInfo in acceptList)
        //    {
        //        acceptInfo.ToList().ForEach(data => data.TelInterviewType = GetSamplingTypeStr(TelInterviewType.InProgress));
        //        await this.insUnitOfWork.SaveChangesAsync();
        //    }

        //    eventLog.Info($"添加電訪清單需處理的筆數為: {acceptList.Count}", functionName);

        //    if (acceptList.Count > 0)
        //    {
        //        int count = 1;
        //        try
        //        {
        //            eventLog.Builder.Init("CheckTelInterviewList");
        //            foreach (var acceptInfo in acceptList)
        //            {
        //                eventLog.Info($"處理中 {count++}/{acceptList.Count}", functionName);

        //                try
        //                {
        //                    productMasterCompanyCode = this.productMasterRepo.Queryable()
        //                    .Where(policy => policy.ProductId == acceptInfo.First().ProductId)
        //                    .Select(policy => policy.CompanyCode)
        //                    .First();
        //                }
        //                catch (Exception)
        //                {
        //                    eventLog.Builder.Append("INS0000001", $"ProductMaster 查無 ProductId 為 {acceptInfo.First().ProductId} 的 CompanyCode");
        //                    continue;
        //                }

        //                policyDetailList = policyDetailRepo.Queryable()
        //                .Where(policy => policy.AcceptNo == acceptInfo.Key &&
        //                        policy.ProductId == acceptInfo.First().ProductId &&
        //                        policy.ProjectCode == acceptInfo.First().ProjectCode)
        //                .ToList();

        //                if (policyDetailList.Count == 0)
        //                {
        //                    eventLog.Builder.Append("INS0000001", $"PolicyDetail 查無 AcceptNo 為 {acceptInfo.First().AcceptNo},ProductId 為 {acceptInfo.First().ProductId},ProjectCode 為 {acceptInfo.First().ProjectCode} 的資料");
        //                    continue;
        //                }

        //                foreach (var policyDetail in policyDetailList)
        //                {
        //                    //無特殊商品設定則會是 null
        //                    currentProductInfoSamplingPct = outBoundCompanySettingRepo.Queryable().Where(setting => setting.CompanyCode == productMasterCompanyCode && setting.PlanCode == policyDetail.PlanCode && setting.PlanType == acceptInfo.First().PlanType).FirstOrDefault()?.SamplingPct;

        //                    if (currentProductInfoSamplingPct == 0)
        //                    {
        //                        needTelInterview = false;
        //                    }
        //                    else
        //                    {
        //                        //當只要有一組特殊商品沒設定或設定比例大於0，則需要進入電訪抽樣流程
        //                        needTelInterview = true;
        //                        break;
        //                    }
        //                }

        //                if (needTelInterview == false)
        //                {
        //                    DateTime date = DateTime.Now;
        //                    foreach (var policyMaster in acceptInfo)
        //                    {
        //                        policyMaster.TelInterviewType = GetSamplingTypeStr(TelInterviewType.None);
        //                        policyMaster.UpdateBy = batchId;
        //                        policyMaster.UpdateOn = date;
        //                        policyMasterRepo.Update(policyMaster);
        //                        DataLogPolicyMaster dataLogPolicyMaster = mapper.Map<DataLogPolicyMaster>(policyMaster);
        //                        dataLogPolicyMaster.CreateBy = batchId;
        //                        dataLogPolicyMaster.CreateOn = date;
        //                        dataLogPolicyMasterRepo.Insert(dataLogPolicyMaster);
        //                    }
        //                    try
        //                    {
        //                        await this.insUnitOfWork.SaveChangesAsync();
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        eventLog.Builder.Append("INS0000004", "CheckTelInterviewList 更新電訪清單失敗", "NeedTelInterview false", ex);
        //                    }
        //                }
        //                else
        //                {
        //                    try
        //                    {
        //                        if (CalcOutBoundSampling(acceptInfo.ToList(), productMasterCompanyCode, policyDetailList, batchId))
        //                            await this.insUnitOfWork.SaveChangesAsync();
        //                        else
        //                            this.insUnitOfWork.ResetContextState();
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        eventLog.Builder.Append("INS0000004", "CheckTelInterviewList 更新電訪清單失敗", "NeedTelInterview true", ex);
        //                    }
        //                }
        //            }
        //        }
        //        catch
        //        {
        //            try
        //            {
        //                acceptList.SelectMany(o => o).Where(data => data.TelInterviewType == GetSamplingTypeStr(TelInterviewType.InProgress)).ForEach(data => data.TelInterviewType = null);
        //                await this.insUnitOfWork.SaveChangesAsync();
        //            }
        //            catch
        //            {
        //                eventLog.Info($"CheckTelInterviewList Error", functionName);
        //            }
        //        }
        //        eventLog.Builder.Finish();
        //    }
        //    this.insUnitOfWork.ResetContextState();
        //}

        ///// <summary>
        ///// 檢查及添加 GetOrderStatus AOA
        ///// </summary>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <returns>無回傳值</returns>
        ////PendingStatus 為 03 但沒發過 AOA 需添加
        //public async Task ReissueAOA(string batchId)
        //{
        //    const string functionName = "ReissueAOA";

        //    eventLog.Trace("檢查並於待發送清單添加核保成功 AOA", functionName);
        //    var acceptList = this.policyMasterRepo.Queryable()
        //        .Where(policy => policy.PendingStatus == PendingStatus.UnderwritePass && (policy.PolicyAoa == null || policy.PolicyAoa != PolicyAOA.Successfully && policy.PolicyAoa != PolicyAOA.InProgress)).AsEnumerable()
        //        .GroupBy(o => o.AcceptNo).ToList();

        //    if (acceptList.Any())
        //    {
        //        acceptList.ForEach(acceptInfo => acceptInfo.ToList().ForEach(policy => policy.PolicyAoa = PolicyAOA.InProgress));
        //        await this.insUnitOfWork.SaveChangesAsync();
        //    }

        //    eventLog.Info($"添加核保成功 AOA 需處理的筆數為: {acceptList.Count}", functionName);

        //    int count = 1;
        //    foreach (var acceptInfo in acceptList)
        //    {
        //        eventLog.Info($"處理中 {count++}/{acceptList.Count}", functionName);
        //        await AddGetOrderStatusAOA(acceptInfo.First(), true, batchId);
        //        foreach (var policy in acceptInfo)
        //        {
        //            policy.PolicyAoa = PolicyAOA.Successfully;
        //            policy.UpdateBy = batchId;
        //            policy.UpdateOn = GuidDate;
        //            this.policyMasterRepo.Update(policy);
        //            DataLogPolicyMaster dataLogPolicyMaster = mapper.Map<DataLogPolicyMaster>(policy);
        //            dataLogPolicyMaster.CreateBy = batchId;
        //            dataLogPolicyMaster.CreateOn = GuidDate;
        //            dataLogPolicyMasterRepo.Insert(dataLogPolicyMaster);
        //        }
        //        await this.insUnitOfWork.SaveChangesAsync();
        //    }
        //    this.insUnitOfWork.ResetContextState();
        //}

        ///// <summary>
        ///// 建立指定類型的 GetOrderStatus AOA
        ///// </summary>
        ///// <param name="policyMaster">保單主檔</param>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <returns>無回傳值</returns>
        //public async Task AddGetOrderStatusRejectAOA(PolicyMaster policyMaster, string batchId)
        //{
        //    await AddGetOrderStatusAOA(policyMaster, false, batchId);
        //}

        ///// <summary>
        ///// 建立指定類型的 GetOrderStatus AOA
        ///// </summary>
        ///// <param name="currentPolicyMaster"></param>
        ///// <param name="isAccept"></param>
        ///// <param name="batchId"></param>
        ///// <returns></returns>
        //private async Task AddGetOrderStatusAOA(PolicyMaster currentPolicyMaster, bool isAccept, string batchId)
        //{
        //    try
        //    {
        //        umssendListGetOrderStatusRepo.Insert(GenUMSSendListData(currentPolicyMaster, isAccept, batchId));
        //        await this.insUnitOfWork.SaveChangesAsync();
        //    }
        //    catch (Exception ex)
        //    {
        //        eventLog.Log("INS0000004", "GetOrderStatus 更新 AOA 失敗", "AddGetOrderStatusAOA", currentPolicyMaster.AcceptNo, ex);
        //    }
        //}

        ///// <summary>
        ///// Check Response Data From FEBAdapter.GetOrderStatus
        ///// </summary>
        ///// <param name="response"></param>
        ///// <returns></returns>
        //private bool CheckGetOrderStatusResponseData(StandardModel<SendGetOrderStatusResp> response)
        //{
        //    var functionName = "CheckGetOrderStatusResponseData";
        //    if (response == null)
        //    {
        //        eventLog.Log("INS0000010", "GetOrderStatus 取得結果為 null", functionName);
        //        return false;
        //    }

        //    if (response == null)
        //    {
        //        eventLog.Log("INS0000010", "GetOrderStatus 取得的 response 為空", functionName);
        //        return false;
        //    }

        //    if (response.Data == null)
        //    {
        //        eventLog.Log("INS0000010", "GetOrderStatus 取得的 Data 欄位為空", functionName);
        //        return false;
        //    }

        //    if (response.Message == null)
        //    {
        //        eventLog.Log("INS0000010", "GetOrderStatus 取得的 Message 欄位為空", functionName);
        //        return false;
        //    }

        //    if (string.IsNullOrEmpty(response.Message.MsgId))
        //    {
        //        eventLog.Log("INS0000010", "GetOrderStatus 取得的 StatusCode 欄位為空", functionName);
        //        return false;
        //    }

        //    if (response.Message.MsgId == "0000" && response.Data.ReturnData == null)
        //    {
        //        eventLog.Log("INS0000010", "GetOrderStatus StatusCode 為 0000 但取得的 ReturnData 欄位為空", functionName);
        //        return false;
        //    }

        //    return true;
        //}

        ///// <summary>
        ///// 建立 GetOrderStatus Header
        ///// </summary>
        ///// <param name="custId"></param>
        ///// <returns></returns>
        //private StandardHeader GenGetOrderStatusHeader(string custId)
        //{
        //    string guid = GenGuid();
        //    string timestamp = GuidDate.ToString("yyyyMMddHHmmssfff");

        //    return new StandardHeader()
        //    {
        //        StdTgmVer = 58,
        //        Guid = guid,
        //        GuidSeqNbr = 1,
        //        ItalGuid = guid,
        //        RcvSysCd = ReceiveSysCodeType.TO_CBK,
        //        RcvSrvcId = "",
        //        RqstRspsDsCd = RqstRspsDsCdType.Request,
        //        TgmTpCd = TelegramCodeType.Sync,
        //        IfId = string.Empty,
        //        ScrnId = "00000000000",
        //        StaffId = "INSystem",
        //        DeptId = "99999",
        //        FrstTnsnSysCd = "INS",
        //        FrstTnsnNodeNbr = "01",
        //        FrstTnsnIstcNbr = "01",
        //        FrstChnlDsCd = "INS",
        //        ItalIfId = string.Empty,
        //        FrstTnsnTmstmp = timestamp,
        //        CustId = custId,
        //        LclCd = "zh-TW",
        //        RntmEnvDsCd = this.rntmEnvDsCd,
        //        TnsnSysCd = "INS",
        //        TnsnNodeNbr = "01",
        //        TnsnIstcNbr = "01",
        //        TnsnTmstmp = timestamp
        //    };
        //}

        ///// <summary>
        ///// 取毒 FEBAdapter.GetOrderStatus Request
        ///// </summary>
        ///// <param name="acceptNo"></param>
        ///// <param name="id"></param>
        ///// <param name="insType"></param>
        ///// <returns></returns>
        //private SendGetOrderStatusReq GenGetOrderStatusData(string acceptNo, string id, string insType)
        //{
        //    return new SendGetOrderStatusReq()
        //    {
        //        AcceptNo = acceptNo,
        //        Id = id,
        //        InsType = insType
        //    };
        //}

        ///// <summary>
        ///// 檢測逾期電訪
        ///// </summary>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <returns>執行是否成功</returns>
        //public async Task<bool> CheckOverdueTelInterview(string batchId)
        //{
        //    const string functionName = "CheckOverdueTelInterview";

        //    eventLog.Trace("GetOrderStatus 檢測逾期電訪", functionName);

        //    var overduSstr = sysParamRepo.Queryable()
        //    .Where(sysParam => sysParam.GroupId == "OutBoundBatch" && sysParam.ItemId == "Overdue")
        //    .Select(sysParam => sysParam.ItemValue)
        //    .FirstOrDefault();

        //    int overdue = 0;

        //    if (string.IsNullOrEmpty(overduSstr))
        //    {
        //        eventLog.Log("INS0000001", "SysParam 查無 GroupId 為 OutBoundBatch 的資料", "CheckOverdueTelInterview");
        //        return false;
        //    }
        //    else
        //    {
        //        try
        //        {
        //            overdue = Int32.Parse(overduSstr);
        //        }
        //        catch (FormatException ex)
        //        {
        //            eventLog.Log("INS0000002", "OutBoundBatch 的 Overdue 不是數字", "CheckOverdueTelInterview", overduSstr, ex);
        //            return false;
        //        }
        //    }
        //    DateTime date = DateTime.Now;
        //    DateTime expiredDate = date.AddHours(0 - overdue);

        //    try
        //    {
        //        outBoundListRepo.Queryable()
        //        .Where(outBoundListRepo => outBoundListRepo.OutBoundOverdue == OutBoundOverdue.OnOverdue &&
        //                outBoundListRepo.OutBoundStatus == OutBoundStatus.Wait &&
        //                outBoundListRepo.SampleDate < expiredDate)
        //        .ForEach(outBoundListRepo => { outBoundListRepo.OutBoundOverdue = OutBoundOverdue.Overdue; outBoundListRepo.UpdateBy = batchId; outBoundListRepo.UpdateOn = date; });

        //        await this.insUnitOfWork.SaveChangesAsync();
        //        this.insUnitOfWork.ResetContextState();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        eventLog.Log("INS0000004", "CheckExpiredList 更新 OutBoundList 失敗", "CheckOverdueTelInterview", "overduSstr", ex);
        //        this.insUnitOfWork.ResetContextState();
        //        return false;
        //    }
        //}

        ///// <summary>
        ///// Get UmssendListGetOrderStatus Object
        ///// </summary>
        ///// <param name="policyMaster"></param>
        ///// <param name="isAccept"></param>
        ///// <param name="batchId"></param>
        ///// <returns></returns>
        //private UmssendListGetOrderStatus GenUMSSendListData(PolicyMaster policyMaster, bool isAccept, string batchId)
        //{
        //    string reqHeaders = GenUMSHeader(GenGuid(), policyMaster.CustId);
        //    string reqBody = GenUMSData(policyMaster, isAccept);

        //    UmssendListGetOrderStatus umssendListGetOrderStatus = new UmssendListGetOrderStatus()
        //    {
        //        CreateBy = batchId,
        //        UpdateBy = batchId,
        //        CreateOn = GuidDate,
        //        UpdateOn = GuidDate,
        //        ReqHeaders = reqHeaders,
        //        ReqBody = reqBody,
        //        SendStatus = UMSNoticeSendStatus.Pending,
        //        ErrorRetry = 0,
        //        AcceptNo = policyMaster.AcceptNo
        //    };

        //    return umssendListGetOrderStatus;
        //}

        ///// <summary>
        ///// 發送 GetOrderStatus AOA
        ///// </summary>
        ///// <param name="repository">UMS Type</param>
        ///// <param name="dateTime">重送日期</param>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <param name="userSpecifiedDate">是否為使用者觸發</param>
        ///// <returns>無回傳值</returns>
        //public async Task SendAOA(UMS repository, DateTime dateTime, string batchId, bool userSpecifiedDate = false)
        //{
        //    switch (repository)
        //    {
        //        case UMS.GetOrderStatus:
        //            if (userSpecifiedDate)
        //                await SendAOAList(umssendListGetOrderStatusRepo.Queryable()
        //                    .Where(sendList => sendList.SendStatus != UMSNoticeSendStatus.Successfully &&
        //                                       sendList.SendStatus != UMSNoticeSendStatus.Ignore &&
        //                                       sendList.UpdateOn.Value.Date == dateTime.Date)
        //                    .ToList(), dateTime, batchId);
        //            else
        //                await SendAOAList(umssendListGetOrderStatusRepo.Queryable()
        //                    .Where(sendList => sendList.SendStatus == UMSNoticeSendStatus.Pending &&
        //                                       sendList.CreateOn.Value.Date == dateTime.Date)
        //                    .ToList(), dateTime, batchId);
        //            break;

        //        case UMS.AML:
        //            if (userSpecifiedDate)
        //                await SendAOAList(umssendListAmlRepo.Queryable()
        //                    .Where(sendList => sendList.SendStatus != UMSNoticeSendStatus.Successfully &&
        //                                       sendList.SendStatus != UMSNoticeSendStatus.Ignore &&
        //                                       sendList.UpdateOn.Value.Date == dateTime.Date)
        //                    .ToList(), dateTime, batchId);
        //            else
        //                await SendAOAList(umssendListAmlRepo.Queryable()
        //                    .Where(sendList => sendList.SendStatus == UMSNoticeSendStatus.Pending &&
        //                                       sendList.CreateOn.Value.Date == dateTime.Date)
        //                    .ToList(), dateTime, batchId);
        //            break;

        //        case UMS.Renewal:
        //            if (userSpecifiedDate)
        //                await SendAOAList(umssendListRenewalRepo.Queryable()
        //                    .Where(sendList => sendList.SendStatus != UMSNoticeSendStatus.Successfully &&
        //                                       sendList.SendStatus != UMSNoticeSendStatus.Ignore &&
        //                                       sendList.UpdateOn.Value.Date == dateTime.Date)
        //                    .ToList(), dateTime, batchId);
        //            else
        //                await SendAOAList(umssendListRenewalRepo.Queryable()
        //                    .Where(sendList => sendList.SendStatus == UMSNoticeSendStatus.Pending &&
        //                                       sendList.CreateOn.Value.Date == dateTime.Date)
        //                    .ToList(), dateTime, batchId);
        //            break;
        //    }
        //}
        ///// <summary>
        ///// 發送 GetOrderStatus AOAList
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="sendAOAList"></param>
        ///// <param name="dateTime"></param>
        ///// <param name="batchId"></param>
        ///// <returns></returns>
        //private async Task SendAOAList<T>(List<T> sendAOAList, DateTime dateTime, string batchId)
        //{
        //    const string functionName = "SendAOAList";
        //    string tableName = "";
        //    if (sendAOAList.Count > 0)
        //    {
        //        if (sendAOAList is List<UmssendListGetOrderStatus>)
        //        {
        //            var temp = sendAOAList as List<UmssendListGetOrderStatus>;
        //            temp?.ForEach(data => data.SendStatus = UMSNoticeSendStatus.InProgress);
        //            await this.insUnitOfWork.SaveChangesAsync();
        //            tableName = "UMSSendList_getOrderStatus";
        //        }
        //        else if (sendAOAList is List<UmssendListAml>)
        //        {
        //            var temp = sendAOAList as List<UmssendListAml>;
        //            temp?.ForEach(data => data.SendStatus = UMSNoticeSendStatus.InProgress);
        //            await this.insUnitOfWork.SaveChangesAsync();
        //            tableName = "UMSSendList_AML";
        //        }
        //        else if (sendAOAList is List<UmssendListRenewal>)
        //        {
        //            var temp = sendAOAList as List<UmssendListRenewal>;
        //            temp?.ForEach(data => data.SendStatus = UMSNoticeSendStatus.InProgress);
        //            await this.insUnitOfWork.SaveChangesAsync();
        //            tableName = "UMSSendList_Renewal";
        //        }
        //    }

        //    eventLog.Info($"{tableName} {dateTime.ToString("yyyyMMdd")} 的待發送筆數為: {sendAOAList.Count}", functionName);

        //    int count = 1;
        //    eventLog.Builder.Init("SendAOAList");
        //    foreach (var snedData in sendAOAList)
        //    {
        //        eventLog.Info($"發送中 {count++}/{sendAOAList.Count}", functionName);
        //        await CallUMSAPI(snedData, batchId);
        //    }
        //    eventLog.Builder.Finish();
        //    this.insUnitOfWork.ResetContextState();
        //}

        ///// <summary>
        ///// 呼叫UMSAPI
        ///// </summary>
        ///// <param name="snedData"></param>
        ///// <param name="batchId"></param>
        ///// <returns></returns>
        //private async Task CallUMSAPI(dynamic snedData, string batchId)
        //{
        //    const string functionName = "CallUMSAPI";
        //    if (snedData == null ||
        //       (!(snedData is UmssendListGetOrderStatus) &&
        //       !(snedData is UmssendListAml) &&
        //       !(snedData is UmssendListRenewal)))
        //    {
        //        eventLog.Fatal("CallUMSAPI snedData is null", functionName);
        //        return;
        //    }

        //    SendUMSReq body = JsonConvert.DeserializeObject<SendUMSReq>(snedData.ReqBody);
        //    StandardHeader header = JsonConvert.DeserializeObject<StandardHeader>(snedData.ReqHeaders);

        //    StandardModel<SendUMSResp> UMSResp = null;

        //    try
        //    {
        //        UMSResp = await UMSAdapter.SendAOA(header, body);
        //        if (UMSResp == null)
        //        {
        //            eventLog.Builder.Append("INS0000008", "Call UMS 回傳為 Null", body.CustId);
        //            snedData.RespBody = "";
        //            snedData.RespHeaders = "";
        //            snedData.ReturnCode = "";
        //            snedData.SendStatus = UMSNoticeSendStatus.Failed;
        //        }
        //        else
        //        {
        //            if (UMSResp.Header == null)
        //                eventLog.Builder.Append("INS0000008", "Call UMS Response Header is null", body.CustId);
        //            else
        //                snedData.RespHeaders = JsonConvert.SerializeObject(UMSResp.Header).Truncate(4000, "(Truncated)");

        //            if (UMSResp.Message == null)
        //                eventLog.Builder.Append("INS0000008", "Call UMS Response Message is null", body.CustId);
        //            else
        //            {
        //                snedData.RespBody = UMSResp.Data != null ? JsonConvert.SerializeObject(UMSResp.Data).Truncate(4000, "(Truncated)") :
        //                                                          JsonConvert.SerializeObject(UMSResp.Message?.AdtlMsg).Truncate(4000, "(Truncated)");

        //                snedData.ReturnCode = UMSResp.Message?.MsgId;
        //                snedData.SendStatus = ((string)snedData.ReturnCode).Equals("LBNA000001") || ((string)snedData.ReturnCode).Equals("LBSA000004") ? UMSNoticeSendStatus.Successfully : UMSNoticeSendStatus.Failed;
        //            }

        //            if (UMSResp.Header == null || UMSResp.Message == null)
        //                snedData.SendStatus = UMSNoticeSendStatus.Failed;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        while (ex.InnerException != null)
        //        {
        //            ex = ex.InnerException;
        //        }
        //        snedData.RespBody = "";
        //        snedData.RespHeaders = "";
        //        snedData.ReturnCode = "";
        //        snedData.SendStatus = UMSNoticeSendStatus.Failed;
        //        eventLog.Builder.Append("INS0000008", "Call UMS Response 非預期資料", body.CustId, ex);
        //    }
        //    eventLog.Trace($"更新 UMS 發送結果", functionName);
        //    if (((int?)snedData.ErrorRetry).HasValue &&
        //        (string.IsNullOrEmpty(snedData.SendStatus) || ((string)snedData.SendStatus) != UMSNoticeSendStatus.Successfully))
        //        snedData.ErrorRetry++;

        //    snedData.RunBy = batchId;
        //    snedData.UpdateBy = batchId;
        //    snedData.UpdateOn = DateTime.Now;

        //    try
        //    {
        //        await this.insUnitOfWork.SaveChangesAsync();
        //        eventLog.Trace($"UMS 發送結果更新完成", functionName);
        //    }
        //    catch (Exception ex)
        //    {
        //        eventLog.Builder.Append("INS0000004", $"{snedData.GetType().Name} 更新 UMS 發送結果失敗", body.CustId, ex);
        //    }
        //}

        ///// <summary>
        ///// 產生 UMSData
        ///// </summary>
        ///// <param name="policyMaster"></param>
        ///// <param name="isAccept"></param>
        ///// <returns></returns>
        //private string GenUMSData(PolicyMaster policyMaster, bool isAccept)
        //{
        //    string umsMsgId;
        //    string tpltParmVal1;
        //    if (isAccept)
        //    {
        //        umsMsgId = "INS005";

        //        string productName = this.productMasterRepo.Queryable()
        //            .Where(repo => repo.ProductId == policyMaster.ProductId)
        //            .Select(repo => repo.Name)
        //            .FirstOrDefault();

        //        string licensePlateNo = this.policyCarInfoRepo.Queryable()
        //            .Where(repo => repo.AcceptNo == policyMaster.AcceptNo)
        //            .Select(repo => repo.LicensePlateNo)
        //            .FirstOrDefault();

        //        tpltParmVal1 = $"{productName}({licensePlateNo.MaskLicensePlateNoString()})";
        //    }
        //    else
        //    {
        //        umsMsgId = "INS004";
        //        tpltParmVal1 = "";
        //    }

        //    var data = new SendUMSReq()
        //    {
        //        UmsMsgId = umsMsgId,
        //        CustId = policyMaster.CustId,
        //        TpltParmVal1 = tpltParmVal1,
        //        TpltParmVal2 = "",
        //        TpltParmVal3 = "",
        //        TpltParmVal4 = "",
        //        TpltParmVal5 = "",
        //        TpltParmVal6 = "",
        //        TpltParmVal7 = "",
        //        TpltParmVal8 = "",
        //        TpltParmVal9 = "",
        //        TpltParmVal10 = "",
        //        TpltParmVal11 = "",
        //        TpltParmVal12 = ""
        //    };

        //    return JsonConvert.SerializeObject(data);
        //}

        ///// <summary>
        ///// 取得電訪清單
        ///// </summary>
        ///// <param name="currentPolicyMasters"></param>
        ///// <param name="companyCode"></param>
        ///// <param name="policyDetailList"></param>
        ///// <param name="batchId"></param>
        ///// <returns></returns>
        //private bool CalcOutBoundSampling(List<PolicyMaster> currentPolicyMasters, string companyCode, List<PolicyDetail> policyDetailList, string batchId)
        //{
        //    DateTime date = DateTime.Now;
        //    var policyInfo = currentPolicyMasters.First();
        //    var policyMasterList = this.policyMasterRepo.Queryable()
        //    .Where(policy => policy.CustId.Equals(policyInfo.CustId)).ToList();

        //    if (policyMasterList.Count == 0)
        //    {
        //        eventLog.Builder.Append("INS0000001", $"PolicyMaster 查無 CustId 為 {policyInfo.CustId} 的資料", "CheckOutBoundSampling");
        //        return false;
        //    }

        //    bool newCustomer = !policyMasterList.Any(policy => policy.AcceptNo != policyInfo.AcceptNo);

        //    List<TelInterviewInfo> outBoundCompanySettingSamplingPcts = (from pd in policyDetailList
        //                                                                 join setting in outBoundCompanySettingRepo.Queryable()
        //                                                                 on pd.PlanCode equals setting.PlanCode
        //                                                                 where setting.CompanyCode == companyCode && setting.PlanType == policyInfo.PlanType && setting.SamplingPct > 0
        //                                                                 orderby setting.SamplingPct descending
        //                                                                 select new TelInterviewInfo()
        //                                                                 {
        //                                                                     TelInterviewType = TelInterviewType.Special,
        //                                                                     CompanyCode = setting.CompanyCode,
        //                                                                     PlanCode = setting.PlanCode,
        //                                                                     PlanType = setting.PlanType,
        //                                                                     SamplingPct = setting.SamplingPct
        //                                                                 }).ToList();

        //    OutBoundCustomerSetting customerPctInfo = this.outBoundCustomerSettingRepo.Queryable().FirstOrDefault();
        //    if (customerPctInfo != null)
        //    {
        //        outBoundCompanySettingSamplingPcts.Add(new TelInterviewInfo()
        //        {
        //            TelInterviewType = newCustomer ? TelInterviewType.NewCustomer : TelInterviewType.OldCustomer,
        //            CompanyCode = companyCode,
        //            SamplingPct = newCustomer ? customerPctInfo.NewCustomerPct : customerPctInfo.OldCustomerPct
        //        });
        //    }
        //    outBoundCompanySettingSamplingPcts = outBoundCompanySettingSamplingPcts.OrderByDescending(o => o.SamplingPct).ToList();

        //    int bingoIndex = 0;
        //    bool bingo = false;
        //    //處理抽樣
        //    for (int index = 0; index < outBoundCompanySettingSamplingPcts.Count; index++)
        //    {
        //        var (molecular, denominator) = GetAcceptPercentage(outBoundCompanySettingSamplingPcts[index], policyInfo.InsComUwpassDate.Value);
        //        var productBingo = Sampling(outBoundCompanySettingSamplingPcts[index].SamplingPct, molecular, denominator);
        //        if (bingo == false && productBingo)
        //        {
        //            bingoIndex = index;
        //            bingo |= productBingo;
        //        }

        //        var detail = new OutBoundListDetail()
        //        {
        //            AcceptNo = policyInfo.AcceptNo,
        //            TelInterviewType = GetSamplingTypeStr(outBoundCompanySettingSamplingPcts[index].TelInterviewType),
        //            TelInterviewStatus = productBingo && !policyInfo.HearingSpeechImpaired.GetValueOrDefault(),
        //            CompanyCode = outBoundCompanySettingSamplingPcts[index].CompanyCode,
        //            PlanCode = outBoundCompanySettingSamplingPcts[index].PlanCode,
        //            PlanType = outBoundCompanySettingSamplingPcts[index].PlanType,
        //            CreateBy = batchId,
        //            CreateOn = date,
        //            UpdateBy = batchId,
        //            UpdateOn = date,
        //        };
        //        this.outBoundListDetailRepo.Insert(detail);
        //    }

        //    OutBoundList samplingRecord = new OutBoundList()
        //    {
        //        AcceptNo = policyInfo.AcceptNo,
        //        CustId = policyInfo.CustId,
        //        AcceptStatus = policyInfo.AcceptStatus,
        //        CompanyCode = companyCode,
        //        ProductId = policyInfo.ProductId,
        //        ProjectCode = policyInfo.ProjectCode,
        //        OrderNo = policyInfo.OrderNo,
        //        CaseSource = policyInfo.CaseSource,
        //        SampleDate = date,
        //        HearingSpeechImpaired = policyInfo.HearingSpeechImpaired,
        //        OutBoundStatus = bingo ? OutBoundStatus.Wait : OutBoundStatus.NotInList,
        //        OutBoundOverdue = bingo ? OutBoundOverdue.OnOverdue : null,
        //        CreateBy = batchId,
        //        CreateOn = date,
        //        UpdateBy = batchId,
        //        UpdateOn = date
        //    };

        //    outBoundListRepo.Insert(samplingRecord);

        //    #region 更新保單主檔及保單主檔歷程檔
        //    foreach (var policyMaster in currentPolicyMasters)
        //    {
        //        policyMaster.TelInterviewType = bingo
        //            ? GetSamplingTypeStr(outBoundCompanySettingSamplingPcts[bingoIndex].TelInterviewType)
        //            : GetSamplingTypeStr(TelInterviewType.NoTelInterview);
        //        policyMaster.UpdateBy = batchId;
        //        policyMaster.UpdateOn = date;
        //        policyMasterRepo.Update(policyMaster);
        //        DataLogPolicyMaster dataLogPolicyMaster = mapper.Map<DataLogPolicyMaster>(policyMaster);
        //        dataLogPolicyMaster.CreateBy = batchId;
        //        dataLogPolicyMaster.CreateOn = date;
        //        dataLogPolicyMasterRepo.Insert(dataLogPolicyMaster);
        //    }
        //    #endregion

        //    return true;
        //}

        ///// <summary>
        ///// 取得電訪種類字串
        ///// </summary>
        ///// <param name="type"></param>
        ///// <returns></returns>
        //private string GetSamplingTypeStr(TelInterviewType type)
        //{
        //    string telInterviewTypeStr = "";

        //    if (_telInterviewType.Count > 0 && _telInterviewType.TryGetValue(type, out telInterviewTypeStr))
        //        return telInterviewTypeStr;
        //    else
        //        return string.Empty;
        //}

        ///// <summary>
        ///// 計算是否被抽樣
        ///// </summary>
        ///// <param name="precentage"></param>
        ///// <param name="count"></param>
        ///// <param name="denominatorCount"></param>
        ///// <returns></returns>
        //private bool Sampling(int precentage, int count, int denominatorCount)
        //{
        //    if (precentage <= 0 || count < 0)
        //        return false;
        //    else if (count == 0)
        //        return true;
        //    else
        //        return (Convert.ToDouble(count) / Convert.ToDouble(denominatorCount)) * 100 < precentage;
        //}

        ///// <summary>
        ///// 取得目前計件進件比
        ///// </summary>
        ///// <param name="telInterviewInfo"></param>
        ///// <param name="samplingDate"></param>
        ///// <returns></returns>
        //private (int molecular, int denominator) GetAcceptPercentage(TelInterviewInfo telInterviewInfo, DateTime samplingDate)
        //{
        //    var detailQuery = (from ob in this.outBoundListRepo.Queryable()
        //                       join obd in this.outBoundListDetailRepo.Queryable()
        //                       on ob.AcceptNo equals obd.AcceptNo
        //                       where ob.SampleDate.Date == samplingDate.Date
        //                       && obd.TelInterviewType == GetSamplingTypeStr(telInterviewInfo.TelInterviewType)
        //                       select obd);

        //    if (telInterviewInfo.TelInterviewType == TelInterviewType.Special)
        //    {
        //        detailQuery = detailQuery.Where(o => o.PlanCode == telInterviewInfo.PlanCode
        //        && o.PlanType == telInterviewInfo.PlanType
        //        && o.CompanyCode == telInterviewInfo.CompanyCode);
        //    }
        //    var details = detailQuery.ToList();
        //    var molecularCount = details.Count(o => o.TelInterviewStatus);
        //    var denominatorCount = details.Count();

        //    return (molecularCount, denominatorCount);
        //}

        ///// <summary>
        ///// 取得電訪設定 電訪種類
        ///// </summary>
        ///// <param name="batchId"></param>
        ///// <returns></returns>
        //private bool InitSamplingTypeStr(string batchId)
        //{
        //    const string functionName = "InitSamplingTypeStr";

        //    eventLog.Trace("取得 SysParam 電訪設定", functionName);
        //    var telInterviewTypeList = sysParamRepo.Queryable()
        //    .Where(sysParam => sysParam.GroupId == "SamplingType")
        //    .ToList();

        //    if (telInterviewTypeList.Count == 0)
        //    {
        //        eventLog.Log("INS0000001", "CheckTelInterviewList Error, GroupId SamplingType data not exist in SysParam", functionName);
        //        return false;
        //    }

        //    string telInterviewTypeStr = "";
        //    foreach (TelInterviewType type in (TelInterviewType[])Enum.GetValues(typeof(TelInterviewType)))
        //    {
        //        var str = telInterviewTypeList.Where(param => param.ItemValue == type.ToDisplayName()).Select(param => param.ItemId).FirstOrDefault();
        //        if (string.IsNullOrEmpty(str))
        //        {
        //            eventLog.Log("INS0000001", "SysParam 查無對應的 SamplingType 參數", "CheckOutBoundSampling");
        //            return false;
        //        }
        //        if (_telInterviewType.Count > 0 && _telInterviewType.TryGetValue(type, out telInterviewTypeStr))
        //        {
        //            _telInterviewType[type] = str;
        //        }
        //        else
        //            _telInterviewType.Add(type, str);
        //    }

        //    return true;
        //}

        ///// <summary>
        ///// 判斷是否需要發送核保失敗且拒保的 AOA(多筆)
        ///// </summary>
        ///// <param name="companyCode">保險公司代碼</param>
        ///// <param name="messageHeader">Response Message Header</param>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <returns>是否需發送拒保AOA</returns>
        //public bool CheckSendFailAOA(string companyCode, IEnumerable<AdtlMsgCntModel> msgCntList, string batchId)
        //{
        //    var res = new List<bool>();
        //    foreach (var adtlMsg in msgCntList)
        //    {
        //        // 轉換保險公司對應代碼
        //        var returnCodeStr = this.commonService.InsMsgCodeTransfer(companyCode, adtlMsg.AdtlMsgId, adtlMsg.AdtlMsgCont);
        //        // 檢核回傳訊息是否為保單拒保件代碼
        //        var isReject = this.CheckIsPolicyReject(returnCodeStr);

        //        // 紀錄 Event Log
        //        //this.eventLog.Debug($"保險公司訊息代碼: {adtlMsg.AdtlMsgId}, 轉換 Ins 代碼: {returnCodeStr}, 是否發送 AOA: {isReject}", batchId);
        //        res.Add(isReject);
        //    }

        //    return res.TrueForAll(o => o);
        //}

        ///// <summary>
        ///// 判斷是否需要發送核保失敗且拒保的 AOA
        ///// </summary>
        ///// <param name="companyCode">保險公司代碼</param>
        ///// <param name="messageHeader">Response Message Header</param>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <returns>是否需發送拒保AOA</returns>
        //public bool CheckSendFailAOA(string companyCode, MessageHeader messageHeader, string batchId)
        //{
        //    // 轉換保險公司對應代碼
        //    var returnCodeStr = this.commonService.InsMsgCodeTransfer(companyCode, messageHeader.MsgId, messageHeader.MsgCont);

        //    // 檢核回傳訊息是否為保單拒保件代碼
        //    var res = this.CheckIsPolicyReject(returnCodeStr);

        //    // 紀錄 Event Log
        //    //this.eventLog.Debug($"保險公司訊息代碼: {messageHeader.MsgId}, 轉換 Ins 代碼: {returnCodeStr}, 是否發送 AOA: {res.ToString()}", batchId);

        //    return res;
        //}

        ///// <summary>
        ///// 檢核回傳訊息是否為保單拒保件代碼
        ///// </summary>
        ///// <param name="returnCodeStr"></param>
        ///// <returns></returns>
        //private bool CheckIsPolicyReject(string returnCodeStr)
        //{
        //    var insMsgCode = this.insMsgCodeRepo.Queryable()
        //        .FirstOrDefault(i => i.MsgCode == returnCodeStr);

        //    if (insMsgCode == null) return false;

        //    return insMsgCode.IsPolicyReject.HasValue
        //        ? insMsgCode.IsPolicyReject.Value
        //        : false;
        //}

        ///// <summary>
        ///// 判斷是否為可重新送件代碼
        ///// </summary>
        ///// <param name="companyCode">保險公司代碼</param>
        ///// <param name="msgCntList">Additional message ID</param>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <returns>是否為可重新送件</returns>
        //public bool CheckResend(string companyCode, IEnumerable<AdtlMsgCntModel> msgCntList, string batchId)
        //{
        //    var res = new List<bool>();
        //    foreach (var adtlMsg in msgCntList)
        //    {
        //        // 轉換保險公司對應代碼
        //        var returnCodeStr = this.commonService.InsMsgCodeTransfer(companyCode, adtlMsg.AdtlMsgId, adtlMsg.AdtlMsgCont);
        //        // 檢核回傳訊息是否為可重新送件代碼
        //        var isResend = this.CheckIsNeedResend(returnCodeStr);

        //        // 紀錄 Event Log
        //        //this.eventLog.Debug($"保險公司訊息代碼: {adtlMsg.AdtlMsgId}. 轉換 Ins 代碼: {returnCodeStr}, 是否為可重新送件代碼: {isResend}", batchId);
        //        res.Add(isResend);
        //    }

        //    return res.TrueForAll(o => o);
        //}

        ///// <summary>
        ///// 檢核回傳訊息是否為可重新送件代碼
        ///// </summary>
        //private bool CheckIsNeedResend(string returnCodeStr)
        //{
        //    var insMsgCode = this.insMsgCodeRepo.Queryable()
        //        .FirstOrDefault(i => i.MsgCode == returnCodeStr);

        //    if (insMsgCode == null) return false;

        //    return insMsgCode.IsNeedResend.HasValue
        //        ? insMsgCode.IsNeedResend.Value
        //        : false;
        //}

        ///// <summary>
        ///// 判斷是否為保險公司 Retry 代碼
        ///// </summary>
        ///// <param name="companyCode">保險公司代碼</param>
        ///// <param name="msgCntList">Additional message ID</param>
        ///// <returns>是否需 Retry</returns>
        //public bool CheckRetry(string companyCode, IEnumerable<AdtlMsgCntModel> msgCntList)
        //{
        //    var checkArray = HttpClientHelper.GetSysParamRetryCode();
        //    //有任一錯誤訊息不在 Retry 範圍內，則不 Retry
        //    return !msgCntList.Any(msg => !checkArray.Contains(msg.AdtlMsgId));
        //}

        ///// <summary>
        ///// 產出 UMS 用的 Header
        ///// </summary>
        ///// <param name="guid">UMS Guid</param>
        ///// <param name="custid">Cust Id</param>
        ///// <returns>UMS Header</returns>
        //public string GenUMSHeader(string guid, string custid)
        //{
        //    var header = new StandardHeader()
        //    {
        //        Guid = guid,
        //        ItalGuid = guid,
        //        CustId = custid
        //    };

        //    return JsonConvert.SerializeObject(header);
        //}

        ///// <summary>
        ///// 產出 UMS 用的 Guid
        ///// </summary>
        ///// <returns>UMS Guid</returns>
        //public string GenGuid()
        //{
        //    if (serialNo > 9999)
        //        serialNo = 0;

        //    GuidDate = DateTime.Now;
        //    string guid = $"{GuidDate.ToString("yyyyMMddHHmmssfff")}INS0101{startTime}{serialNo++.ToString().PadLeft(4, '0')}";
        //    return guid;
        //}

        /// <summary>
        /// 取得完整地址
        /// </summary>
        /// <param name="policyPersonInfo">投保相關人員檔</param>
        /// <param name="getHousehold">是否取得 Household</param>
        /// <returns>完整地址</returns>
        public string PolicyPersonInfoAddr(PolicyPersonInfo policyPersonInfo, bool getHousehold = false)
        {
            if (policyPersonInfo == null)
                return string.Empty;

            if (getHousehold)
                return policyPersonInfo.HouseholdZipCode + policyPersonInfo.HouseholdsendCity + policyPersonInfo.HouseholdCountry + policyPersonInfo.HouseholdAddr;
            else
                return policyPersonInfo.Zipcode + policyPersonInfo.SendCity + policyPersonInfo.SendCountry + policyPersonInfo.Addr;
        }

        /// <summary>
        /// 取得完整地址
        /// </summary>
        /// <param name="TempPolicyPersonInfo">投保相關人員暫存檔</param>
        /// <param name="getHousehold">是否取得 Household</param>
        /// <returns>完整地址</returns>
        public string PolicyPersonInfoAddr(TempPolicyPersonInfo TempPolicyPersonInfo, bool getHousehold = false)
        {
            if (TempPolicyPersonInfo == null)
                return string.Empty;

            if (getHousehold)
                return TempPolicyPersonInfo.HouseholdZipCode + TempPolicyPersonInfo.HouseholdsendCity + TempPolicyPersonInfo.HouseholdCountry + TempPolicyPersonInfo.HouseholdAddr;
            else
                return TempPolicyPersonInfo.Zipcode + TempPolicyPersonInfo.SendCity + TempPolicyPersonInfo.SendCountry + TempPolicyPersonInfo.Addr;
        }


        ///// <summary>
        ///// F3116_確認方案組合
        ///// </summary>
        ///// <param name="req"></param>
        ///// <returns></returns>
        //public async Task<StandardModel<CheckInquireProjectSetResp>> F3116_CheckInquireProjectSet(StandardModel<CheckInquireProjectSetReq> req)
        //{
        //    CheckInquireProjectSetResp res = new CheckInquireProjectSetResp();
        //    res.Plans = new List<PlanInfoList>();
        //    StandardModel<GetProjectListV2Resp> companyApiResult = new StandardModel<GetProjectListV2Resp>();
        //    var companyProjectCode = string.Empty;
        //    var custId = req?.Header?.CustId;
        //    var acceptNo = req?.Data?.AcceptNo;
        //    var italIfId = req?.Header?.ItalIfId;

        //    #region check req data

        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(custId), custId, italIfId);
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(acceptNo), acceptNo, italIfId);
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(req.Data.ProductCode), req.Data?.ProductCode, italIfId);
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(req.Data.ProjectCode), req.Data?.ProjectCode, italIfId);
        //    // 檢查是否仍在銷售中
        //    CheckProductSaleState(req.Data.ProductCode, italIfId);

        //    if (req.Data.SelectedYear == null || req.Data.SelectedYear <= 0)
        //    {
        //        //this.eventLog.Log("LBEI001001", $"PolicyService F3116_CheckInquireProjectSet Check SelectedYear Error: LBEI001001, Request SelectedYear is empty", italIfId, italIfId);
        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, $"SelectedYear");
        //    }
        //    #endregion

        //    #region get basic data
        //    // 取得專案資訊
        //    var productMasterData = this.productMasterRepo.Queryable().Where(x => x.ProductId == req.Data.ProductCode).FirstOrDefault();
        //    // 取得方案資訊
        //    var campaignMasterData = this.campaignMasterRepo.Queryable().Where(x => x.ProjectCode == req.Data.ProjectCode).FirstOrDefault();
        //    if (campaignMasterData == null)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI004001", $"PolicyService F3116_CheckInquireProjectSet  Error: LBEI004001, CustId: {custId} and AcceptNo: {acceptNo} query data from {nameof(CampaignMaster)} join  {nameof(ProductMaster)}not exist", italIfId, italIfId);
        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);
        //    }

        //    // 取得車籍資料
        //    var carInfo = this.tempPolicyCarInfoRepo.Queryable().Where(x => x.AcceptNo == acceptNo).FirstOrDefault();
        //    if (carInfo == null)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI004001", $"PolicyService F3116_CheckInquireProjectSet  Error: LBEI004001, CustId: {custId} and AcceptNo: {acceptNo} query data from {nameof(TempPolicyCarInfo)} not exist", italIfId, italIfId);
        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);
        //    }

        //    // 取得保險公司資訊
        //    var companyData = this.companyProfileRepo.Queryable().Where(x => x.CompanyCode == productMasterData.CompanyCode).FirstOrDefault();

        //    // 取得"保險公司險種類型與車種可投保商品上限"設定(節省空間與後續運算，僅取該保險公司設定值)
        //    var insuredLimitDict = this.commonService.SearchSysParamByGroupId(GroupIdType.InsuredLimit)
        //                                            ?.Where(x => x.ItemId.StartsWith(productMasterData.CompanyCode))
        //                                            ?.ToDictionary(x => x.ItemId, x => x.ItemValue)
        //                                            ?? new Dictionary<string, string>();

        //    // 取得該方案的商品資訊
        //    // 1. 抓出該方案對應的商品詳細資訊
        //    // 2. 抓出該方案商品對應可投保的保額選項詳細資訊
        //    var campaignPlanList = this.campaignPlanMappingRepo.Queryable().Where(x => x.ProductId == req.Data.ProductCode && x.ProjectCode == req.Data.ProjectCode)
        //        .Join(
        //            this.planMasterRepo.Queryable(),
        //            cp => new { cp.CompanyCode, cp.PlanCode, cp.PlanVer, cp.PlanType, cp.PaymentTerm, cp.BenefitTerm },
        //            p => new { p.CompanyCode, p.PlanCode, p.PlanVer, p.PlanType, p.PaymentTerm, p.BenefitTerm },
        //            (cp, p) => new
        //            {
        //                CampaignPlanMapping = cp,
        //                PlanMaster = p,
        //                PlanAssuredList = this.campaignPlanAssuredMappingRepo.Queryable().Where(y => y.CampaignGid == cp.CampaignGid)
        //                    .Join(
        //                        this.planAssuredRepo.Queryable(),
        //                        cpm => cpm.PlanAssuredNo,
        //                        pa => pa.SeqNo,
        //                        (cpm, pa) => new { CampaignPlanAssuredMapping = cpm, PlanAssured = pa })
        //                    .OrderBy(x => x.PlanAssured.ReferAssured)
        //                    .ThenBy(x => x.CampaignPlanAssuredMapping.SeqNo)
        //                    .Select(x => x.PlanAssured)
        //                    .ToList()
        //            }).ToList();

        //    if (campaignPlanList == null || campaignPlanList.Count == 0)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI004001", $"PolicyService F3116_CheckInquireProjectSet  Error: LBEI004001, CustId: {custId} and AcceptNo: {acceptNo} query data from {nameof(CampaignPlanMapping)} join  {nameof(ProductMaster)}not exist", italIfId, italIfId);
        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);
        //    }

        //    if (insuredLimitDict.Count > 0)
        //    {
        //        // 過濾"保險公司險種類型與車種可投保商品上限"
        //        // 因Anonymous Type's Read Only Property,故重新建立物件
        //        campaignPlanList = campaignPlanList.Select(x => new
        //        {
        //            CampaignPlanMapping = x.CampaignPlanMapping,
        //            PlanMaster = x.PlanMaster,
        //            PlanAssuredList = x.PlanAssuredList.Where(x => !insuredLimitDict.ContainsKey($"{x.CompanyCode}.{x.PlanCode}.{carInfo.VehicleType}") || insuredLimitDict[$"{x.CompanyCode}.{x.PlanCode}.{carInfo.VehicleType}"].Contains(x.ItemCode)).ToList()
        //        }).ToList();
        //    }

        //    #endregion

        //    #region 過濾僅可投保單強/單任
        //    // 取得是否可投保強制險或任意險
        //    var tempInsuranceDate = this.tempInsuranceDateRepo.Queryable().Where(s => s.AcceptNo == acceptNo).FirstOrDefault() ?? new TempInsuranceDate();

        //    // 僅可投保強制險
        //    if (tempInsuranceDate.IsCompulsoryInsRenewable.GetValueOrDefault() && !tempInsuranceDate.IsVoluntaryInsRenewable.GetValueOrDefault())
        //    {
        //        // 抓主險為強制險的商品
        //        campaignPlanList = campaignPlanList
        //                    .Where(x => x.CampaignPlanMapping.ParentCampaignGid != null || (x.CampaignPlanMapping.ParentCampaignGid == null && string.Equals(x.PlanMaster.PlanCategory, PlanCategory.CALI)))
        //                    .ToList();
        //    }
        //    // 僅可投保任意險
        //    else if (!tempInsuranceDate.IsCompulsoryInsRenewable.GetValueOrDefault() && tempInsuranceDate.IsVoluntaryInsRenewable.GetValueOrDefault())
        //    {
        //        // 抓商品類型為任意險商品
        //        campaignPlanList = campaignPlanList
        //                    .Where(x => string.Equals(x.PlanMaster.PlanCategory, PlanCategory.VALI))
        //                    .ToList();
        //    }

        //    #endregion

        //    #region 判斷是否呼叫保險公司GetProjectList Api，如需呼叫取得GetProjectList資訊，並過濾保代後台設定可投保商品與保額。

        //    if (productMasterData.RunApi)
        //    {
        //        // 取得客戶資訊
        //        var custInofo = this.customerInfoRepo.Queryable().Where(x => x.CustId == custId).FirstOrDefault();
        //        if (custInofo == null)
        //        {
        //            // Log
        //            //this.eventLog.Log("LBEI004001", $"PolicyService F3116_CheckInquireProjectSet  Error: LBEI004001, CustId: {custId} and AcceptNo: {acceptNo} query data from {nameof(CustomerInfo)} not exist", italIfId, italIfId);
        //            throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);
        //        }

        //        // 取得PlanType對應insType
        //        var insType = this.commonService.GetSysParamValue(GroupIdType.InsTypeMapping, productMasterData.PlanType);

        //        GetProjectListV2Req febReq = new GetProjectListV2Req()
        //        {
        //            InsType = insType,
        //            Term = req.Data.SelectedYear ?? 1,
        //            ID = custInofo.NatlId,
        //            Name = custInofo.CustNm,
        //            Birthday = custInofo.BrthDt,
        //            TagID = carInfo.LicensePlateNo,
        //            VehicleType = carInfo.VehicleType,
        //            ManufacturedDate = carInfo.ManufacturedDate,
        //            Displacement = carInfo.Displacement?.ToString("0.##"),
        //            CarType = carInfo.CarType,
        //            BrandID = carInfo.Ibrand,
        //            EngineID = carInfo.EngineId,
        //            CarBodyID = carInfo.CarBodyId,
        //            TransportUnit = carInfo.TransportUnit?.ToString(),
        //            RegistrationIssueDate = carInfo.RegistIssueDate?.ToTaiwanDateTimeStr()?.Replace("/", string.Empty),
        //        };

        //        companyApiResult = await this.FEBAdapter.GetProjectListV2(req.Header, febReq, italIfId, productMasterData.CompanyCode);
        //        if (companyApiResult?.Message == null || companyApiResult.Message.MsgId != ReturnCodes.Success_0000.ToDisplayName() || companyApiResult.Data == null)
        //        {
        //            var msgJson = string.Empty;
        //            StandardHeader header = req.Header;
        //            IEnumerable<AdtlMsgCntModel> msgCntList = null;
        //            if (companyApiResult != null)
        //            {
        //                msgJson = JsonConvert.SerializeObject(companyApiResult.Message);

        //                if (companyApiResult.Message != null)
        //                {
        //                    // 取得與保險公司對應的代碼
        //                    msgCntList = this.commonService.InsMultipleMsgCodeTransfer(productMasterData.CompanyCode, companyApiResult.Message.MsgId, companyApiResult.Message.AdtlMsg);
        //                }
        //            }

        //            // Log
        //            //this.eventLog.Log($"{msgCntList?.FirstOrDefault()?.AdtlMsgId ?? companyApiResult?.Message?.MsgId}",
        //            $"[{productMasterData.ProductId}]{nameof(F3116_CheckInquireProjectSet)} GetProjectListV2 Error: {companyApiResult?.Message?.MsgId}, Response data missing or MsgId not 0000, Message: {msgJson}",
        //                            italIfId,
        //                            italIfId);

        //            var adtlMsgCntModels = msgCntList?.Skip(1)?.ToList();
        //            throw new ChannelAPIException(header, msgCntList?.FirstOrDefault()?.AdtlMsgId, msgCntList?.FirstOrDefault()?.AdtlMsgCont, adtlMsgCntModels?.Count ?? 0, adtlMsgCntModels);
        //        }

        //        companyProjectCode = companyApiResult.Data.ProjectList.FirstOrDefault()?.ProjectCode;
        //        var projectItemsDIY = companyApiResult.Data.ProjectList.FirstOrDefault()?.ProjectItemsDIY?.ToList() ?? new List<ProjectItemsDIY>();

        //        // 過濾保代後台與保險公司回傳可投保商品與保額
        //        campaignPlanList = campaignPlanList
        //                            .Join(
        //                                projectItemsDIY,
        //                                cp => cp.PlanMaster.PlanCode,
        //                                p => p.ItemKey,
        //                                (cp, p) => new
        //                                {
        //                                    CampaignPlanMapping = cp.CampaignPlanMapping,
        //                                    PlanMaster = cp.PlanMaster,
        //                                    PlanAssuredList = cp.PlanAssuredList
        //                                        .Join(
        //                                            p.Options,
        //                                            pa => pa.ItemCode,
        //                                            diyOp => diyOp.OptionKey,
        //                                            (pa, diyOp) => new PlanAssured()
        //                                            {
        //                                                SeqNo = pa.SeqNo,
        //                                                CompanyCode = pa.CompanyCode,
        //                                                PlanCode = pa.PlanCode,
        //                                                PaymentTerm = pa.PaymentTerm,
        //                                                BenefitTerm = pa.BenefitTerm,
        //                                                PlanType = pa.PlanType,
        //                                                PlanVer = pa.PlanVer,
        //                                                ItemCode = pa.ItemCode,
        //                                                ItemAbbreviation = pa.ItemAbbreviation,
        //                                                Assured = diyOp.Content,
        //                                                ReferAssured = pa.ReferAssured,
        //                                                Multiples = pa.Multiples,
        //                                                CreateBy = pa.CreateBy,
        //                                                CreateOn = pa.CreateOn,
        //                                                UpdateBy = pa.UpdateBy,
        //                                                UpdateOn = pa.UpdateOn
        //                                            }).ToList()
        //                                }).ToList();
        //    }

        //    #endregion

        //    #region get resp
        //    res = new CheckInquireProjectSetResp()
        //    {
        //        InsuranceCompanyName = companyData.CompanyName,
        //        ProjectCode = campaignMasterData.ProjectCode,
        //        ProjectName = campaignMasterData.Name,
        //        CompanyProjectCode = companyProjectCode,
        //        CurrentSelectYear = campaignPlanList?.FirstOrDefault()?.PlanMaster?.BenefitTerm ?? 1,
        //        Plans = campaignPlanList.Where(x => x.CampaignPlanMapping.ParentCampaignGid == null && x.PlanAssuredList?.Count > 0).OrderBy(x => x.CampaignPlanMapping.CampaignPlanSort).Select(x => new PlanInfoList()
        //        {
        //            ID = x.PlanMaster.PlanCode,
        //            DisplayName = x.PlanMaster.PlanFullName,
        //            IsRequired = x.CampaignPlanMapping.Required,
        //            DefaultOption = x.PlanAssuredList.FirstOrDefault()?.ItemCode ?? string.Empty,
        //            CoverTitle = x.PlanMaster.Abbreviation,
        //            PlanCategory = x.PlanMaster.PlanCategory,
        //            PlanCategoryName = this.commonService.GetSysParamValue(GroupIdType.PlanCategory, x.PlanMaster.PlanCategory),
        //            ContractType = int.TryParse(x.PlanMaster.PrimaryRiderInd, out int planContractType) ? planContractType : 0,
        //            AccessoryContractSelectAll = x.CampaignPlanMapping.Selectall ?? false,
        //            Options = x.PlanAssuredList.Select(y => new PremieumOptions()
        //            {
        //                OptionKey = y.ItemCode,
        //                DisplayName = y.Assured,
        //                ReferencePrice = y.ReferAssured ?? 1
        //            }).ToList(),
        //            SubPlans = !campaignPlanList.Any(y => y.CampaignPlanMapping.ParentCampaignGid == x.CampaignPlanMapping.CampaignGid) ? null :
        //                            campaignPlanList.Where(y => y.CampaignPlanMapping.ParentCampaignGid == x.CampaignPlanMapping.CampaignGid).OrderBy(y => y.CampaignPlanMapping.CampaignPlanSort).Select(y => new SubPlanList()
        //                            {
        //                                ID = y.PlanMaster.PlanCode,
        //                                DisplayName = y.PlanMaster.PlanFullName,
        //                                IsRequired = y.CampaignPlanMapping.Required,
        //                                DefaultOption = y.PlanAssuredList.FirstOrDefault()?.ItemCode ?? string.Empty,
        //                                CoverTitle = y.PlanMaster.Abbreviation,
        //                                PlanCategory = y.PlanMaster.PlanCategory,
        //                                PlanCategoryName = this.commonService.GetSysParamValue(GroupIdType.PlanCategory, y.PlanMaster.PlanCategory),
        //                                ContractType = int.TryParse(y.PlanMaster.PrimaryRiderInd, out int subplanContractType) ? subplanContractType : 0,
        //                                Options = y.PlanAssuredList.Select(z => new SubPremieumOptions()
        //                                {
        //                                    OptionKey = z.ItemCode,
        //                                    DisplayName = z.Assured,
        //                                    ReferencePrice = z.ReferAssured ?? 1,
        //                                    Multipler = z.Multiples ?? 1
        //                                }).ToList()
        //                            }).ToList()
        //        }).ToList()
        //    };

        //    #endregion

        //    return this.channelAPICommonService.GetResp<CheckInquireProjectSetResp>(req.Header, this.commonService.GetChannelSuccessMessageHeader(), StandardResCodeType.NormalProcessing, res);
        //}

        ///// <summary>
        ///// F3117 保費試算
        ///// </summary>
        ///// <param name="req"></param>
        ///// <returns></returns>
        //public async Task<StandardModel<GetQuoteResp>> F3117_GetQuote(StandardModel<GetQuoteReq> req)
        //{
        //    GetQuoteResp res = new GetQuoteResp();
        //    List<TempTrialcalDetailV1> detail = new List<TempTrialcalDetailV1>();
        //    MessageHeader messageHeader = new MessageHeader();
        //    var custId = req.Header?.CustId;
        //    var acceptNo = req.Data?.AcceptNo;
        //    var italIfId = req.Header?.ItalIfId;
        //    var projectCode = req?.Data?.ProjectCode;

        //    #region check req data

        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(custId), custId, italIfId);
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(acceptNo), acceptNo, italIfId);
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(projectCode), projectCode, italIfId);

        //    // 取得專案代碼
        //    var productId = this.policySignMasterRepo.Queryable().Where(p => p.CustId == custId && p.AcceptNo == acceptNo).Select(x => x.ProductId).FirstOrDefault();
        //    if (string.IsNullOrWhiteSpace(productId))
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI004001", $"F3117_GetQuote Error: LBEI004001, CustId: {custId} and AcceptNo: {acceptNo} PolicySignMaster not exist", italIfId, italIfId);
        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);
        //    }

        //    // 檢查是否仍在銷售中
        //    CheckProductSaleState(productId, italIfId);

        //    // 驗證方案是否存在
        //    var isExistProject = this.campaignPlanMappingRepo.Queryable()
        //                            .Where(x => x.ProductId == productId && x.ProjectCode == req.Data.ProjectCode)
        //                            .Any();

        //    // 方案不存在
        //    if (!isExistProject)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI004006", $"{nameof(PolicyService)} F3117_GetQuote Error: LBEI004006, ProductId: {productId}, ProjectCode: {req.Data.ProjectCode} not exist in CampaignPlanMapping", italIfId, italIfId);
        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004006, string.Empty);
        //    }
        //    #endregion

        //    #region query data
        //    // 取得專案資訊
        //    var productMasterData = this.productMasterRepo.Queryable().Where(x => x.ProductId == productId).FirstOrDefault();

        //    // 取得PlanType對應insType
        //    var insType = this.commonService.GetSysParamValue(GroupIdType.InsTypeMapping, productMasterData.PlanType);

        //    // 取得年期(term)
        //    var term = this.campaignPlanMappingRepo.Queryable().Where(x => x.ProductId == productId && x.ProjectCode == projectCode).Select(x => x.BenefitTerm).FirstOrDefault();

        //    // 取得客戶資料
        //    var custInfo = this.customerInfoRepo.Queryable().Where(x => x.CustId.Equals(custId)).FirstOrDefault();

        //    // 取得車籍資料
        //    var carInfo = this.tempPolicyCarInfoRepo.Queryable().Where(x => x.AcceptNo.Equals(acceptNo)).FirstOrDefault();

        //    // 取得Sysparm設定自負額資料
        //    var deductibleSysParm = this.sysParamRepo.Queryable().Where(x => x.GroupId == GroupIdType.Deductible);

        //    // 取得Sysparm設定自負額單位資料
        //    var deductibleUnitSysParm = this.sysParamRepo.Queryable().Where(x => x.GroupId == GroupIdType.DeductibleUnit);

        //    Dictionary<string, object> ob = new Dictionary<string, object>();
        //    ob.Add(nameof(CustomerInfo), custInfo);
        //    ob.Add(nameof(TempPolicyCarInfo), carInfo);
        //    ob.Add(nameof(SysParamDTO), deductibleSysParm);

        //    foreach (var item in ob)
        //    {
        //        if (item.Value == null)
        //        {
        //            // Log
        //            //this.eventLog.Log("LBEI004001", $"CreateOrderIns Error: LBEI004001, CustId: {custId} and AcceptNo: {acceptNo} check data {item.Key} not exist", italIfId, italIfId);
        //            throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001, $"{item.Key} not exist");
        //        }
        //    }
        //    #endregion

        //    #region get getQuote req
        //    List<GetQuoteReqSelectedItems> getQuoteReqSelectedItems = new List<GetQuoteReqSelectedItems>();
        //    foreach (var item in req.Data.SelectedItems)
        //    {
        //        var sysDeductibleUnit = deductibleUnitSysParm.Where(x => string.Equals(x.ItemId, $"{productMasterData.CompanyCode}.{item.ItemKey}")).FirstOrDefault();
        //        var sysDeductible = deductibleSysParm.Where(x => string.Equals(x.ItemId, $"{productMasterData.CompanyCode}.{item.ItemKey}")).FirstOrDefault();
        //        if (sysDeductible != null)
        //        {
        //            getQuoteReqSelectedItems.Add(new GetQuoteReqSelectedItems()
        //            {
        //                ItemKey = item.ItemKey,
        //                OptionKey = item.OptionKey,
        //                DeductibleKey = sysDeductible.ItemValue,
        //                DeductibleUnit = sysDeductibleUnit?.ItemValue ?? "%"
        //            });
        //        }
        //        else
        //        {
        //            getQuoteReqSelectedItems.Add(new GetQuoteReqSelectedItems()
        //            {
        //                ItemKey = item.ItemKey,
        //                OptionKey = item.OptionKey
        //            });
        //        }
        //    }

        //    GetFEBQuoteReq febReq = new GetFEBQuoteReq()
        //    {
        //        InsType = insType,
        //        ProjectCode = req?.Data?.CompanyProjectCode,
        //        Term = term.ToString(),
        //        ID = custInfo.NatlId,
        //        Birthday = custInfo.BrthDt,
        //        TagID = carInfo.LicensePlateNo,
        //        VehicleType = carInfo.VehicleType,
        //        ManufacturedDate = carInfo.ManufacturedDate,
        //        Displacement = carInfo.Displacement?.ToString("0.##"),
        //        CarType = carInfo.CarType,
        //        BrandID = carInfo.CarType.Substring(0, 2),
        //        TransportUnit = carInfo.TransportUnit.ToString(),
        //        Tonnage = carInfo.VehicleType.Equals(VehicleType.N04) ? carInfo.Tonnage.ToString() : string.Empty,
        //        RegistrationIssueDate = carInfo.RegistIssueDate?.ToTaiwanDateTimeStr()?.Replace("/", string.Empty),
        //        Sex = SexyType.MaleCode == custInfo.GndrDsCd ? SexyType.Male : SexyType.FemaleCode == custInfo.GndrDsCd ? SexyType.Female : string.Empty,
        //        OwnerMobile = custInfo.MbleTelNbr,
        //        SendZipCode = custInfo.RltdZipCd,
        //        SendCity = custInfo.RltdBsicAddrCont.Split(" ")[0],
        //        SendCountry = custInfo.RltdBsicAddrCont.Split(" ")[1],
        //        SendAddr = custInfo.RltdDtlAddrCont,
        //        OwnerEmail = custInfo.EmalAddr,
        //        PolicySendType = PolicySendType.Electric,
        //        Nationality = custInfo.RltdAddrHrcyCd,
        //        CustomerAttr = CustomerAttr.Unprofessional,
        //        OccupationCategory = "1",//hard code
        //        SelectedItems = getQuoteReqSelectedItems
        //    };
        //    #endregion

        //    #region Call FEBAdapter then save into db
        //    var getQuoteResult = await this.FEBAdapter.GetQuote(req.Header, febReq, italIfId, productMasterData.CompanyCode);
        //    if (getQuoteResult == null || getQuoteResult.Message == null || getQuoteResult.Message.MsgId != ReturnCodes.Success_0000.ToDisplayName() || getQuoteResult.Data == null)
        //    {
        //        var msgJson = string.Empty;
        //        StandardHeader header = req.Header;
        //        IEnumerable<AdtlMsgCntModel> msgCntList = null;
        //        if (getQuoteResult != null)
        //        {
        //            msgJson = JsonConvert.SerializeObject(getQuoteResult.Message);

        //            if (getQuoteResult.Message != null)
        //            {
        //                // 取得與保險公司對應的代碼
        //                msgCntList = this.commonService.InsMultipleMsgCodeTransfer(productMasterData.CompanyCode, getQuoteResult.Message.MsgId, getQuoteResult.Message.AdtlMsg);
        //            }
        //        }

        //        // Log
        //        //this.eventLog.Log($"{msgCntList?.FirstOrDefault()?.AdtlMsgId ?? getQuoteResult?.Message?.MsgId}",
        //        $"[{productMasterData.ProductId}]{nameof(F3117_GetQuote)} GetQuote Error: {getQuoteResult?.Message?.MsgId}, Response data missing or MsgId not 0000, Message: {msgJson}",
        //                            italIfId,
        //                            italIfId);

        //        var adtlMsgCntModels = msgCntList?.Skip(1)?.ToList();
        //        throw new ChannelAPIException(header, msgCntList?.FirstOrDefault()?.AdtlMsgId, msgCntList?.FirstOrDefault()?.AdtlMsgCont, adtlMsgCntModels?.Count ?? 0, adtlMsgCntModels);
        //    }
        //    else
        //    {

        //        // 判斷是否要從getInsuranceDate帶預設強制險/任意險保單起訖日，當強與任起訖日皆為空時，則帶預設值
        //        var needDefaultDate = string.IsNullOrEmpty(getQuoteResult.Data.CompulsoryEffectiveDate) &&
        //                                string.IsNullOrEmpty(getQuoteResult.Data.CompulsoryExpirationDate) &&
        //                                string.IsNullOrEmpty(getQuoteResult.Data.VoluntaryEffectiveDate) &&
        //                                string.IsNullOrEmpty(getQuoteResult.Data.VoluntaryExpirationDate);

        //        var tempInsuranceDateData = this.tempInsuranceDateRepo.Queryable().Where(x => x.AcceptNo == req.Data.AcceptNo).FirstOrDefault();
        //        this.tempTrialcalMasterV1Repo.Insert(new TempTrialcalMasterV1()
        //        {
        //            AcceptNo = acceptNo,
        //            CustId = custId,
        //            ProductId = productId,
        //            ProjectCode = projectCode,
        //            CompulsoryPremium = int.TryParse(getQuoteResult.Data.CompulsoryPremium, out int compulsoryPremium) ? compulsoryPremium : (int?)null,
        //            VoluntaryPremiums = int.TryParse(getQuoteResult.Data.VoluntaryPremiums, out int voluntaryPremiums) ? voluntaryPremiums : (int?)null,
        //            TotalPremium = int.TryParse(getQuoteResult.Data.TotalPremium, out int totalPremium) ? totalPremium : (int?)null,
        //            BeforeDiscountTotalPremium = int.TryParse(getQuoteResult.Data.BeforeDiscountTotalPremium, out int beforeDiscountTotalPremium) ? beforeDiscountTotalPremium : (int?)null,
        //            CompulsoryEffectiveDate = needDefaultDate ? tempInsuranceDateData?.CompulsoryEffectiveDate : DateTime.TryParseExact(getQuoteResult.Data.CompulsoryEffectiveDate, DateTimeFormats, CultureInfo.CurrentCulture, DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces, out DateTime compulsoryEffectiveDate) ? compulsoryEffectiveDate : (DateTime?)null,
        //            CompulsoryExpirationDate = needDefaultDate ? tempInsuranceDateData?.CompulsoryEffectiveDate.Value.AddYears(term) : DateTime.TryParseExact(getQuoteResult.Data.CompulsoryExpirationDate, DateTimeFormats, CultureInfo.CurrentCulture, DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces, out DateTime compulsoryExpirationDate) ? compulsoryExpirationDate : (DateTime?)null,
        //            VoluntaryEffectiveDate = needDefaultDate ? tempInsuranceDateData?.VoluntaryEffectiveDate : DateTime.TryParseExact(getQuoteResult.Data.VoluntaryEffectiveDate, DateTimeFormats, CultureInfo.CurrentCulture, DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces, out DateTime voluntaryEffectiveDate) ? voluntaryEffectiveDate : (DateTime?)null,
        //            VoluntaryExpirationDate = needDefaultDate ? tempInsuranceDateData?.VoluntaryEffectiveDate.Value.AddYears(term) : DateTime.TryParseExact(getQuoteResult.Data.VoluntaryExpirationDate, DateTimeFormats, CultureInfo.CurrentCulture, DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces, out DateTime voluntaryExpirationDate) ? voluntaryExpirationDate : (DateTime?)null,
        //            CreateBy = custId,
        //            CreateOn = DateTime.Now
        //        });
        //        await this.insUnitOfWork.SaveChangesAsync();

        //        var trialcalMaster = this.tempTrialcalMasterV1Repo.Queryable().Where(x => x.AcceptNo == acceptNo).OrderByDescending(x => x.CreateOn).FirstOrDefault();
        //        detail.AddRange(getQuoteResult.Data.SelectedItems.Select(x => new TempTrialcalDetailV1()
        //        {
        //            AcceptNo = acceptNo,
        //            TrialcalMasterSeq = trialcalMaster.SeqNo,
        //            ProjectCode = projectCode,
        //            ItemKey = x.ItemKey,
        //            DisplayName = string.IsNullOrEmpty(req.Data.SelectedItems.Where(y => y.ItemKey.Equals(x.ItemKey)).FirstOrDefault()?.OptionKey) && int.TryParse(x.OptionKey, out _) ? int.Parse(x.OptionKey).ToString("#,##0") : null,
        //            OptionKey = x.OptionKey,
        //            Premium = int.TryParse(x.Premium, out int premium) ? premium : 0,
        //            DeductibleKey = x.DeductibleKey,
        //            DeductibleUnit = x.DeductibleUnit,
        //            CreateBy = custId,
        //            CreateOn = DateTime.Now
        //        }).ToList());

        //        foreach (var item in detail)
        //        {
        //            this.tempTrialcalDetailV1Repo.Insert(item);
        //        }

        //        await this.insUnitOfWork.SaveChangesAsync();

        //        List<GetQuoteRespSelectedItem> selectedItems = new List<GetQuoteRespSelectedItem>();
        //        selectedItems.AddRange(getQuoteResult.Data.SelectedItems.Select(x => new GetQuoteRespSelectedItem()
        //        {
        //            ItemKey = x.ItemKey,
        //            OptionKey = x.OptionKey,
        //            Premium = int.TryParse(x.Premium, out _) ? int.Parse(x.Premium) : 0,
        //            DisplayName = string.IsNullOrEmpty(req.Data.SelectedItems.Where(y => y.ItemKey.Equals(x.ItemKey)).FirstOrDefault().OptionKey) && int.TryParse(x.OptionKey, out _) ? int.Parse(x.OptionKey).ToString("#,##0") : string.Empty,
        //        }).ToList());

        //        res = new GetQuoteResp()
        //        {
        //            CompulsoryPremium = trialcalMaster.CompulsoryPremium?.ToString(),
        //            VoluntaryPremiums = trialcalMaster.VoluntaryPremiums?.ToString(),
        //            TotalPremium = trialcalMaster.TotalPremium?.ToString(),
        //            BeforeDiscountTotalPremium = trialcalMaster.BeforeDiscountTotalPremium?.ToString(),
        //            CompulsoryEffectiveDate = this.commonService.ParseDatetimeToTimeSpan(trialcalMaster.CompulsoryEffectiveDate),
        //            CompulsoryExpirationDate = this.commonService.ParseDatetimeToTimeSpan(trialcalMaster.CompulsoryExpirationDate),
        //            VoluntaryEffectiveDate = this.commonService.ParseDatetimeToTimeSpan(trialcalMaster.VoluntaryEffectiveDate),
        //            VoluntaryExpirationDate = this.commonService.ParseDatetimeToTimeSpan(trialcalMaster.VoluntaryExpirationDate),
        //            SelectedItems = selectedItems
        //        };
        //    }
        //    #endregion
        //    messageHeader = this.commonService.GetChannelSuccessMessageHeader();

        //    return this.channelAPICommonService.GetResp<GetQuoteResp>(req.Header, messageHeader, StandardResCodeType.NormalProcessing, res);
        //}


        ///// <summary>
        ///// F3118 儲存保單結果
        ///// </summary>
        ///// <param name="req"></param>
        ///// <returns></returns>
        //public async Task<StandardModel<SavePolicyDataResp>> F3118_SavePolicyData(StandardModel<SavePolicyDataReq> req)
        //{
        //    TempTrialcalMasterV1 trialMaster = new TempTrialcalMasterV1();
        //    List<TempTrialcalDetailV1> trialDetailList = new List<TempTrialcalDetailV1>();
        //    List<TempPolicyMaster> tempTargetMasterList = new List<TempPolicyMaster>();
        //    List<TempPolicyDetail> tempTargetDetailList = new List<TempPolicyDetail>();
        //    List<TempPolicyPersonInfo> tempTargetPersonInfoList = new List<TempPolicyPersonInfo>();
        //    SavePolicyDataResp res = new SavePolicyDataResp();
        //    MessageHeader messageHeader = new MessageHeader();
        //    bool match = true;
        //    var custId = req?.Header?.CustId;
        //    var acceptNo = req?.Data?.AcceptNo;
        //    var italIfId = req?.Header?.ItalIfId;
        //    var projectCode = req?.Data?.ProjectCode;

        //    #region check req data
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(custId), custId, italIfId);
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(acceptNo), acceptNo, italIfId);
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(req.Data.ProjectCode), projectCode, italIfId);

        //    // 取得專案代碼
        //    var productId = this.tempTrialcalMasterV1Repo.Queryable().Where(m => m.AcceptNo == acceptNo).OrderByDescending(m => m.SeqNo).Select(x => x.ProductId).FirstOrDefault();
        //    // 檢查是否仍在銷售中
        //    CheckProductSaleState(productId, italIfId);
        //    #endregion

        //    #region get resp data (取得會員狀態)
        //    // 取得會員狀態
        //    var member = this.memberProfileRepo.Queryable().Where(x => x.CustId == custId).FirstOrDefault();
        //    res.MemberStatus = member != null ? member.MemberStatus : MemberStatus.Unregistered;
        //    res.IsRegister = res.MemberStatus != MemberStatus.Enable;
        //    #endregion

        //    #region 取得最後一筆試算紀錄
        //    var tempTrialData = this.tempTrialcalMasterV1Repo.Queryable().Where(m => m.AcceptNo.Equals(acceptNo)).OrderByDescending(m => m.SeqNo).Take(1)
        //        .Join(
        //            this.tempTrialcalDetailV1Repo.Queryable().Where(d => d.AcceptNo.Equals(acceptNo)),
        //            m => m.SeqNo,
        //            d => d.TrialcalMasterSeq,
        //            (m, d) => new { m, d }
        //        ).ToList();

        //    // need to check the logic
        //    if (tempTrialData == null || string.IsNullOrEmpty(tempTrialData.Select(x => x.m.ProjectCode).FirstOrDefault()))
        //    {
        //        match = false;
        //    }

        //    foreach (var item in req.Data.SelectedItems)
        //    {
        //        var compareData = tempTrialData.Where(x => x.d.ItemKey.Equals(item.ItemKey) && x.d.OptionKey.Equals(item.OptionKey)).FirstOrDefault();
        //        if (compareData == null)
        //        {
        //            match = false;
        //        }
        //    }

        //    // 如無執行過試算，則重新取得試算結果
        //    if (!match)
        //    {
        //        StandardModel<GetQuoteReq> getQuoteObj = new StandardModel<GetQuoteReq>();

        //        getQuoteObj.Header = req.Header;
        //        getQuoteObj.Data = new GetQuoteReq()
        //        {
        //            AcceptNo = acceptNo,
        //            CompanyProjectCode = req.Data.CompanyProjectCode,
        //            ProjectCode = projectCode,
        //            SelectedItems = req.Data.SelectedItems.AsQueryable().Select(x => new GetQuoteReqSelectItem()
        //            {
        //                ItemKey = x.ItemKey,
        //                OptionKey = x.OptionKey
        //            }).ToList()
        //        };

        //        await this.F3117_GetQuote(getQuoteObj);

        //        tempTrialData = this.tempTrialcalMasterV1Repo.Queryable().Where(m => m.AcceptNo.Equals(acceptNo)).OrderByDescending(m => m.SeqNo).Take(1)
        //                    .Join(
        //                        this.tempTrialcalDetailV1Repo.Queryable().Where(d => d.AcceptNo.Equals(acceptNo)),
        //                        m => m.SeqNo,
        //                        d => d.TrialcalMasterSeq,
        //                        (m, d) => new { m, d }
        //                    ).ToList();
        //    }

        //    trialMaster = tempTrialData.AsQueryable().Select(x => x.m).FirstOrDefault();
        //    trialDetailList = tempTrialData.AsQueryable().Select(x => x.d).ToList();
        //    #endregion

        //    #region save data to db
        //    // 取得專案資訊
        //    var productMaster = this.productMasterRepo.Queryable().Where(o => o.ProductId == trialMaster.ProductId).FirstOrDefault();

        //    // 1. 抓出該方案對應的商品詳細資訊
        //    // 2. Join 試算清單，取得投保商品
        //    // 3. Join 商品保額，取得該商品保額選項詳細資訊
        //    // 4. 直接輸出TempPolicyDetail List
        //    var tempPolicyDetailList = this.campaignPlanMappingRepo.Queryable().Where(x => x.CompanyCode == productMaster.CompanyCode && x.ProductId == productMaster.ProductId && x.ProjectCode == trialMaster.ProjectCode)
        //        .Join(
        //            this.planMasterRepo.Queryable(),
        //            cp => new { cp.CompanyCode, cp.PlanCode, cp.PlanVer, cp.PlanType, cp.PaymentTerm, cp.BenefitTerm },
        //            p => new { p.CompanyCode, p.PlanCode, p.PlanVer, p.PlanType, p.PaymentTerm, p.BenefitTerm },
        //            (cp, p) => new { CampaignPlanMapping = cp, PlanMaster = p }).ToList()
        //        .Join(
        //            trialDetailList,
        //            cpp => cpp.PlanMaster.PlanCode,
        //            td => td.ItemKey,
        //            (cpp, td) => new { cpp.CampaignPlanMapping, cpp.PlanMaster, TrialDetail = td })
        //        .Join(
        //            this.planAssuredRepo.Queryable().ToList(),
        //            cpptd => new { cpptd.PlanMaster.CompanyCode, cpptd.PlanMaster.PlanCode, cpptd.PlanMaster.PlanVer, cpptd.PlanMaster.PlanType, cpptd.PlanMaster.PaymentTerm, cpptd.PlanMaster.BenefitTerm, cpptd.TrialDetail.OptionKey },
        //            pa => new { pa.CompanyCode, pa.PlanCode, pa.PlanVer, pa.PlanType, pa.PaymentTerm, pa.BenefitTerm, OptionKey = pa.ItemCode },
        //            (cpptd, pa) => new { cpptd.CampaignPlanMapping, cpptd.PlanMaster, cpptd.TrialDetail, PlanAssured = pa })
        //        .Select(x =>
        //        {
        //            var assured = !string.IsNullOrWhiteSpace(x.TrialDetail.DisplayName) ? x.TrialDetail.DisplayName : x.PlanAssured.Assured;
        //            var splitAbbreviation = x.PlanMaster.Abbreviation?.Split("/") ?? new string[0];
        //            var splitAssured = assured?.Split("/") ?? new string[0];
        //            var coveredItem = string.Join("/", Enumerable.Zip(splitAbbreviation, splitAssured, (x, y) => string.Join("_", new string[] { x, y })).ToArray());

        //            var item = new TempPolicyDetail()
        //            {
        //                AcceptNo = x.TrialDetail.AcceptNo,
        //                ProductId = trialMaster.ProductId,
        //                ProjectCode = x.TrialDetail.ProjectCode,
        //                PlanCode = x.PlanMaster.PlanCode,
        //                PlanVer = x.PlanMaster.PlanVer,
        //                PaymentTerm = x.PlanMaster.PaymentTerm,
        //                BenefitTerm = x.PlanMaster.BenefitTerm,
        //                PlanCategory = x.PlanMaster.PlanCategory,
        //                OptionKey = x.TrialDetail.OptionKey,
        //                DeductibleKey = x.TrialDetail.DeductibleKey,
        //                DeductibleUnit = x.TrialDetail.DeductibleUnit,
        //                ModalItemPermiumWithDisc = x.TrialDetail.Premium,
        //                // TODO:資料可能不完全
        //                //CoveredItem = coveredItem,
        //                Assured = assured,
        //                Abbreviation = x.PlanMaster.Abbreviation,
        //                CreateBy = custId,
        //                CreateOn = DateTime.Now,
        //                UpdateBy = custId,
        //                UpdateOn = DateTime.Now
        //            };

        //            return item;
        //        }).ToList() ?? new List<TempPolicyDetail>();

        //    var policySigMaster = this.policySignMasterRepo.Queryable().Where(x => x.AcceptNo == acceptNo).OrderByDescending(x => x.SeqNo).FirstOrDefault();
        //    if (policySigMaster == null)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI004001", $"PolicyService F3118_SavePolicyData  Error: LBEI004001, CustId: {custId} and AcceptNo: {acceptNo} query data from {nameof(policySigMaster)} not exist", italIfId, italIfId);
        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);
        //    }
        //    var acceptDate = policySigMaster.AcceptDate;

        //    // 取得tempPolicyDetailList有的商品類型
        //    var planCategoryList = tempPolicyDetailList.Select(x => x.PlanCategory).Distinct().OrderBy(x => x).ToList();

        //    foreach (var planCategoryItem in planCategoryList)
        //    {

        //        var detailList = tempPolicyDetailList.Where(x => string.Equals(x.PlanCategory, planCategoryItem)).ToList();
        //        // 取得第一筆Detail 資料
        //        var firstDetailData = detailList.FirstOrDefault();

        //        // 取得detail第一筆商品資訊
        //        var planMaster = this.planMasterRepo.Queryable()
        //                                .Where(x => x.CompanyCode == productMaster.CompanyCode &&
        //                                            x.PlanType == productMaster.PlanType &&
        //                                            x.PlanCode == firstDetailData.PlanCode &&
        //                                            x.PlanVer == firstDetailData.PlanVer &&
        //                                            x.PaymentTerm == firstDetailData.PaymentTerm &&
        //                                            x.BenefitTerm == firstDetailData.BenefitTerm)
        //                                .SingleOrDefault();

        //        tempTargetMasterList.Add(new TempPolicyMaster()
        //        {
        //            AcceptNo = trialMaster.AcceptNo,
        //            AcceptDate = acceptDate,
        //            CompanyProjectCode = req.Data.CompanyProjectCode,
        //            ProductId = trialMaster.ProductId,
        //            PdCd = trialMaster.ProductId,
        //            CustId = trialMaster.CustId,
        //            ProjectCode = trialMaster.ProjectCode,
        //            CaseSource = CaseType.Web,
        //            PolicyPeriodS = string.Equals(planCategoryItem, PlanCategory.CALI) ? trialMaster.CompulsoryEffectiveDate : trialMaster.VoluntaryEffectiveDate,
        //            PolicyPeriodE = string.Equals(planCategoryItem, PlanCategory.CALI) ? trialMaster.CompulsoryExpirationDate : trialMaster.VoluntaryExpirationDate,
        //            PlanCategory = planCategoryItem,
        //            PlanType = productMaster.PlanType,
        //            ModalPermiumWithDisc = string.Equals(planCategoryItem, PlanCategory.CALI) ? trialMaster.CompulsoryPremium : trialMaster.VoluntaryPremiums,
        //            BeneficiaryPayType = "0",
        //            BenefitTerm = planMaster?.BenefitTerm,
        //            PaymentTerm = planMaster?.PaymentTerm,
        //            Currency = planMaster?.Currency,
        //            BenefitTermType = planMaster?.BenefitTermType,
        //            PaymentTermType = planMaster?.PaymentTermType,
        //            PremiumMethod = planMaster?.PremiumMethod,
        //            PolicySendType = PolicySendType.Electric,
        //            NeedOtp = YesNoType.No,
        //            NeedConfirmationEmail = YesNoType.Yes,
        //            CreateBy = custId,
        //            CreateOn = DateTime.Now,
        //            UpdateBy = custId,
        //            UpdateOn = DateTime.Now
        //        });

        //        tempTargetDetailList.AddRange(detailList);
        //    }

        //    #region 投保相關人員檔_暫存檔
        //    // 取得客戶資料表(CIF)
        //    var customerInfo = this.customerInfoRepo.Queryable().FirstOrDefault(c => c.CustId == custId);
        //    if (customerInfo == null)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI004001", $"GetAcceptNo SaveAcceptNo Error: LBEI004001, CustId: {custId} data not exist CustomerInfo", italIfId, italIfId);
        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);
        //    }

        //    // 取得系統參數人員類別資料
        //    var personTypes = this.commonService.SearchSysParamByGroupId(GroupIdType.PersonType);
        //    foreach (var personType in personTypes)
        //    {
        //        if (personType.ItemId == PersonType.Annuitant) continue;

        //        TempPolicyPersonInfo tempPolicyPersonInfo = new TempPolicyPersonInfo();
        //        tempPolicyPersonInfo.AcceptNo = acceptNo;
        //        tempPolicyPersonInfo.PersonType = personType.ItemId;
        //        tempPolicyPersonInfo.Idno = customerInfo.NatlId;
        //        tempPolicyPersonInfo.PersonName = customerInfo.CustNm;
        //        tempPolicyPersonInfo.Gender = SexyType.MaleCode == customerInfo.GndrDsCd
        //            ? SexyType.Male
        //            : SexyType.FemaleCode == customerInfo.GndrDsCd
        //                ? SexyType.Female
        //                : string.Empty;
        //        if (!string.IsNullOrEmpty(customerInfo.BrthDt))
        //        {
        //            tempPolicyPersonInfo.Birthday = DateTime.ParseExact(customerInfo.BrthDt, "yyyyMMdd", null, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
        //        }

        //        tempPolicyPersonInfo.Zipcode = customerInfo.RltdZipCd;
        //        tempPolicyPersonInfo.SendCity = customerInfo.RltdBsicAddrCont?.Split(" ")?.FirstOrDefault();
        //        tempPolicyPersonInfo.SendCountry = customerInfo.RltdBsicAddrCont?.Split(" ")?.LastOrDefault();
        //        tempPolicyPersonInfo.Addr = customerInfo.RltdDtlAddrCont;
        //        tempPolicyPersonInfo.Nationality = customerInfo.RltdAddrHrcyCd;
        //        tempPolicyPersonInfo.Mobile = customerInfo.MbleTelNbr;
        //        tempPolicyPersonInfo.Email = customerInfo.EmalAddr;
        //        tempPolicyPersonInfo.JobClFrstStgeCd = customerInfo.JobClFrstStgeCd;
        //        tempPolicyPersonInfo.JobClScndStgeCd = customerInfo.JobClScndStgeCd;
        //        tempPolicyPersonInfo.JobDtlTpCd = customerInfo.JobDtlTpCd;
        //        tempPolicyPersonInfo.PersonRelationOfApplicant = PersonRelationType.Self;
        //        tempPolicyPersonInfo.PersonRelationOfInsured = PersonRelationType.Self;
        //        if (personType.ItemId == PersonType.Insured)
        //        {
        //            // 當「人員類別」為被保人此欄位放 1
        //            tempPolicyPersonInfo.InsuredSeq = 1;
        //        }
        //        tempPolicyPersonInfo.CreateBy = custId;
        //        tempPolicyPersonInfo.CreateOn = DateTime.Now;
        //        tempPolicyPersonInfo.UpdateBy = custId;
        //        tempPolicyPersonInfo.UpdateOn = DateTime.Now;
        //        tempTargetPersonInfoList.Add(tempPolicyPersonInfo);
        //    }
        //    #endregion

        //    var deleteMasterList = this.tempPolicyMasterRepo.Queryable().Where(x => x.AcceptNo == acceptNo).ToList();
        //    foreach (var item in deleteMasterList)
        //    {
        //        this.tempPolicyMasterRepo.Delete(item);
        //    }
        //    var deleteDetailList = this.tempPolicyDetailRepo.Queryable().Where(x => x.AcceptNo == acceptNo).ToList();
        //    foreach (var item in deleteDetailList)
        //    {
        //        this.tempPolicyDetailRepo.Delete(item);
        //    }
        //    var deletePersonInfoList = this.tempPolicyPersonInfoRepo.Queryable().Where(x => x.AcceptNo == acceptNo).ToList();
        //    foreach (var item in deletePersonInfoList)
        //    {
        //        this.tempPolicyPersonInfoRepo.Delete(item);
        //    }
        //    await this.insUnitOfWork.SaveChangesAsync();

        //    foreach (var item in tempTargetMasterList)
        //    {
        //        this.tempPolicyMasterRepo.Insert(item);
        //    }

        //    foreach (var item in tempTargetDetailList)
        //    {
        //        this.tempPolicyDetailRepo.Insert(item);
        //    }

        //    foreach (var item in tempTargetPersonInfoList)
        //    {
        //        this.tempPolicyPersonInfoRepo.Insert(item);
        //    }

        //    await this.insUnitOfWork.SaveChangesAsync();
        //    #endregion
        //    messageHeader = this.commonService.GetChannelSuccessMessageHeader();

        //    return this.channelAPICommonService.GetResp<SavePolicyDataResp>(req.Header, messageHeader, StandardResCodeType.NormalProcessing, res);
        //}


        ///// <summary>
        ///// INS CreateOrder
        ///// </summary>
        ///// <param name="req"></param>
        ///// <returns></returns>
        //public async Task<StandardModel<CreateOrderInsResp>> F3101_CreateOrder(StandardModel<CreateOrderInsReq> req)
        //{

        //    CreateOrderInsResp res = new CreateOrderInsResp();
        //    StandardHeader standardHeader = new StandardHeader();
        //    MessageHeader messageHeader = new MessageHeader();
        //    var custId = req?.Header?.CustId;
        //    var acceptNo = req?.Data?.AcceptNo;
        //    var italIfId = req?.Header?.ItalIfId;
        //    // req.Header可能因EAIAdapter.QueryCIF被修改，且未處理response.Header，固先以此方式備份req.Header
        //    var responseHeader = this.mapper.Map<StandardHeader>(req.Header);

        //    var now = DateTime.Now;
        //    try
        //    {
        //        #region check req data
        //        this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(custId), custId, italIfId);
        //        this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(acceptNo), acceptNo, italIfId);
        //        this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(req.Data.OtpId), req.Data.OtpId, italIfId);
        //        this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(req.Data.PayBy), req.Data.PayBy, italIfId);
        //        this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(req.Data.PaymentObject.PaymentNo), req.Data.PaymentObject.PaymentNo, italIfId);

        //        if (this.sysParamRepo.Queryable().Where(x => x.GroupId.Equals(GroupIdType.PaidType) && x.ItemId.Equals(req.Data.PayBy)).FirstOrDefault() == null)
        //        {
        //            // Log
        //            //this.eventLog.Log("LBEI001002", "PolicyService F3101_CreateOrder CheckCreateOrderInsReq Error: LBEI001002, Request PayBy is Error", italIfId, italIfId);
        //            throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001002, "PayBy 錯誤");
        //        }

        //        if (req.Data.PayBy == PaidType.CreditCard)
        //        {
        //            this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(req.Data.PaymentObject.CardExpiredDate), req.Data.PaymentObject.CardExpiredDate, italIfId);
        //            this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(req.Data.PaymentObject.CardCheckCode), req.Data.PaymentObject.CardCheckCode, italIfId);
        //            var pattern = new Regex("^(0[1-9]|1[0-2])([0-9]{2})$");
        //            if (!pattern.IsMatch(req.Data.PaymentObject.CardExpiredDate) || int.TryParse(req.Data.PaymentObject.CardExpiredDate, out _) ? int.Parse(req.Data.PaymentObject.CardExpiredDate.Substring(2, 2)) < now.Year - 2000 : true)
        //            {
        //                // Log
        //                //this.eventLog.Log("LBEI001003", "PolicyService F3101_CreateOrder CheckCreateOrderInsReq Error: LBEI001003, Request CardExpiredDate is Error", italIfId, italIfId);
        //                throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001003, "CardExpiredDate 格式須符合 \"MMYY\"");
        //            }
        //            if (!int.TryParse(req.Data.PaymentObject.CardCheckCode, out int parseCardCheckCode) || parseCardCheckCode <= 0)
        //            {
        //                // Log
        //                //this.eventLog.Log("LBEI001003", "PolicyService F3101_CreateOrder CheckCreateOrderInsReq Error: LBEI001003, Request CardCheckCode is Error", italIfId, italIfId);
        //                throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001003, "CardCheckCode 格式須符合 正整數");
        //            }
        //        }

        //        if (req.Data.PayBy == PaidType.Account)
        //        {
        //            this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(req.Data.PaymentObject.BankCode), req.Data.PaymentObject.BankCode, italIfId);

        //            if (!int.TryParse(req.Data.PaymentObject.BankCode, out int parseBankCode) || parseBankCode <= 0)
        //            {
        //                // Log
        //                //this.eventLog.Log("LBEI001003", "PolicyService F3101_CreateOrder CheckCreateOrderInsReq Error: LBEI001003, Request BankCode is Error", italIfId, italIfId);
        //                throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001003, "BankCode 格式須符合 正整數");
        //            }
        //        }
        //        #endregion

        //        #region get basic data 
        //        var master = this.tempPolicyMasterRepo.Queryable().Where(x => x.AcceptNo == acceptNo && x.CustId == custId).ToList();
        //        var detail = this.tempPolicyDetailRepo.Queryable().Where(x => x.AcceptNo == acceptNo).ToList();
        //        var personInfoList = this.tempPolicyPersonInfoRepo.Queryable().Where(x => x.AcceptNo == acceptNo).ToList();
        //        var member = this.memberProfileRepo.Queryable().Where(x => x.CustId == custId).FirstOrDefault();
        //        var carInfo = this.tempPolicyCarInfoRepo.Queryable().Where(x => x.AcceptNo == acceptNo).FirstOrDefault();
        //        var kycData = this.tempKycrecordV1Repo.Queryable().Where(x => x.AcceptNo == acceptNo).ToList();
        //        var kycSysPararm = this.commonService.SearchSysParamByGroupId(GroupIdType.KycType);
        //        // 取得簽署主檔資料
        //        var policySignMaster = this.policySignMasterRepo.Queryable().FirstOrDefault(p => p.CustId == custId && p.AcceptNo == acceptNo);

        //        Dictionary<string, object> ob = new Dictionary<string, object>();
        //        ob.Add(nameof(TempPolicyMaster), master);
        //        ob.Add(nameof(TempPolicyDetail), detail);
        //        ob.Add(nameof(TempPolicyPersonInfo), personInfoList?.FirstOrDefault());
        //        ob.Add(nameof(MemberProfile), member);
        //        ob.Add(nameof(TempPolicyCarInfo), carInfo);
        //        ob.Add(nameof(TempKycrecordV1), kycData);
        //        ob.Add(nameof(SysParamDTO), kycSysPararm);
        //        ob.Add(nameof(PolicySignMaster), policySignMaster);

        //        foreach (var item in ob)
        //        {
        //            if (item.Value == null)
        //            {
        //                // Log
        //                //this.eventLog.Log("LBEI004001", $"CreateOrderIns Error: LBEI004001, CustId: {custId} and AcceptNo: {acceptNo} check data {item.Key} not exist", italIfId, italIfId);
        //                throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001, $"{item.Key} not exist");
        //            }
        //        }

        //        // 以 custId 發查 CIF，取回 nationId, custNms
        //        var cifRes = await this.EAIAdapter.QueryCIF(req.Header, italIfId);

        //        // 若 CIF 回傳的交易結果不是成功，回傳:ReturnCode != LBNA000001。
        //        if (cifRes == null || (cifRes.Message != null && cifRes.Message.MsgId != ReturnCodes.Success_LBNA000001.ToDisplayName()))
        //        {
        //            var msgJson = string.Empty;
        //            StandardHeader header = req.Header;
        //            if (cifRes != null)
        //            {
        //                msgJson = JsonConvert.SerializeObject(cifRes.Message);
        //                header = cifRes.Header;
        //            }

        //            // Log
        //            //this.eventLog.Log("LBEI005004", $"{nameof(F3101_CreateOrder)} GetCIFRes Error: LBEI005004, Response data missing or MsgId not LBNA000001, Message: {msgJson}", italIfId, italIfId);

        //            throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI005004);
        //        }

        //        // 檢核 CIF 回傳值
        //        this.CheckCIFRes(cifRes.Data, italIfId);
        //        #endregion

        //        #region save req data into db

        //        var customerInfo = this.customerInfoRepo.Queryable().Where(x => x.CustId == custId).FirstOrDefault();
        //        if (customerInfo != null)
        //        {
        //            customerInfo.CustNm = cifRes.Data.CustNm;
        //            customerInfo.CustAlasNm = cifRes.Data.CustAlasNm;
        //            customerInfo.Nick = cifRes.Data.Nick;
        //            customerInfo.EngFmlyNm = cifRes.Data.EngFmlyNm;
        //            customerInfo.EngFrstNm = cifRes.Data.EngFrstNm;
        //            customerInfo.NatlId = cifRes.Data.NatlId;
        //            customerInfo.IndvCustAcctTpCd = cifRes.Data.IndvCustAcctTpCd;
        //            customerInfo.CustChngInfoAprvStsCd = cifRes.Data.CustChngInfoAprvStsCd;
        //            customerInfo.BrthDt = cifRes.Data.BrthDt;
        //            customerInfo.JobClFrstStgeCd = cifRes.Data.JobClFrstStgeCd;
        //            customerInfo.JobClFrstStgeCdNm = cifRes.Data.JobClFrstStgeCdNm;
        //            customerInfo.JobClScndStgeCd = cifRes.Data.JobClScndStgeCd;
        //            customerInfo.JobClScndStgeCdNm = cifRes.Data.JobClScndStgeCdNm;
        //            customerInfo.JobDtlTpCd = cifRes.Data.JobDtlTpCd;
        //            customerInfo.JobDtlTpCdNm = cifRes.Data.JobDtlTpCdNm;
        //            customerInfo.GndrDsCd = cifRes.Data.GndrDsCd;
        //            customerInfo.GndrDsCdNm = cifRes.Data.GndrDsCdNm;
        //            customerInfo.EdctLvlCd = cifRes.Data.EdctLvlCd;
        //            customerInfo.EdctLvlCdNm = cifRes.Data.EdctLvlCdNm;
        //            customerInfo.WkstCmpyNm = cifRes.Data.WkstCmpyNm;
        //            customerInfo.AnalIncmAmt = cifRes.Data.AnalIncmAmt;
        //            customerInfo.IncmSrcCd = cifRes.Data.IncmSrcCd;
        //            customerInfo.IncmSrcCdNm = cifRes.Data.IncmSrcCdNm;
        //            customerInfo.OfcAddrHrcyCd = cifRes.Data.OfcAddrHrcyCd;
        //            customerInfo.RltdAddrHrcyCd = cifRes.Data.RltdAddrHrcyCd;
        //            customerInfo.RltdZipCd = cifRes.Data.RltdZipCd;
        //            customerInfo.RltdAddrId = cifRes.Data.RltdAddrId;
        //            customerInfo.RltdBsicAddrCont = cifRes.Data.RltdBsicAddrCont;
        //            customerInfo.RltdDtlAddrCont = cifRes.Data.RltdDtlAddrCont;
        //            customerInfo.EmalAddr = cifRes.Data.EmalAddr;
        //            customerInfo.MbleIntlNbr = cifRes.Data.MbleIntlNbr;
        //            customerInfo.MbleTelNbr = cifRes.Data.MbleTelNbr;
        //            customerInfo.OfcZipCd = cifRes.Data.OfcZipCd;
        //            customerInfo.OfcAddrId = cifRes.Data.OfcAddrId;
        //            customerInfo.OfcBsicAddrCont = cifRes.Data.OfcBsicAddrCont;
        //            customerInfo.OfcDtlAddrCont = cifRes.Data.OfcDtlAddrCont;
        //            customerInfo.OfcIntlNbr = cifRes.Data.OfcIntlNbr;
        //            customerInfo.OfcTelNbr = cifRes.Data.OfcTelNbr;
        //            customerInfo.BnkPrflImgUrl = cifRes.Data.BnkPrflImgUrl;
        //            customerInfo.CustKycRsltTpCd = cifRes.Data.CustKycRsltTpCd;
        //            customerInfo.CddNxtDt = cifRes.Data.CddNxtDt;
        //            customerInfo.OtpChnlSeqNbr = cifRes.Data.OtpChnlSeqNbr;
        //            customerInfo.EmalOtpCfrmDtm = cifRes.Data.EmalOtpCfrmDtm;
        //            this.customerInfoRepo.Update(customerInfo);
        //        }

        //        // 更新會員主檔
        //        if (member != null)
        //        {
        //            member.Name = cifRes.Data.CustNm;
        //            member.Idno = cifRes.Data.NatlId;
        //            member.UpdateBy = custId;
        //            member.UpdateOn = now;
        //            this.memberProfileRepo.Update(member);
        //        }

        //        var customerProfile = this.customerProfileRepo.Queryable().Where(x => x.CustId == custId).SingleOrDefault();
        //        // 更新客戶主檔
        //        if (customerProfile != null)
        //        {
        //            customerProfile.Name = cifRes.Data.CustNm;
        //            if (!string.IsNullOrEmpty(cifRes.Data.BrthDt))
        //            {
        //                customerProfile.Birthday = DateTime.ParseExact(cifRes.Data.BrthDt, "yyyyMMdd", null, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
        //            }
        //            customerProfile.Mobile = cifRes.Data.MbleTelNbr;
        //            customerProfile.Email = cifRes.Data.EmalAddr;
        //            customerProfile.Idno = cifRes.Data.NatlId;
        //            customerProfile.Zipcode = cifRes.Data.RltdZipCd;
        //            customerProfile.SendCity = cifRes.Data.RltdBsicAddrCont?.Split(" ")?.FirstOrDefault();
        //            customerProfile.SendCountry = cifRes.Data.RltdBsicAddrCont?.Split(" ")?.LastOrDefault();
        //            customerProfile.Addr = cifRes.Data.RltdDtlAddrCont;
        //            customerProfile.UpdateBy = custId;
        //            customerProfile.UpdateOn = now;
        //            this.customerProfileRepo.Update(customerProfile);
        //        }

        //        // 更新投保相關人員檔_暫存檔資料
        //        foreach (var tempPolicyPerson in personInfoList)
        //        {
        //            tempPolicyPerson.Idno = cifRes.Data.NatlId;
        //            tempPolicyPerson.PersonName = cifRes.Data.CustNm;
        //            tempPolicyPerson.Gender = SexyType.MaleCode == cifRes.Data.GndrDsCd
        //                ? SexyType.Male
        //                : SexyType.FemaleCode == cifRes.Data.GndrDsCd
        //                    ? SexyType.Female
        //                    : string.Empty;
        //            if (!string.IsNullOrEmpty(cifRes.Data.BrthDt))
        //            {
        //                tempPolicyPerson.Birthday = DateTime.ParseExact(cifRes.Data.BrthDt, "yyyyMMdd", null, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
        //            }
        //            tempPolicyPerson.Zipcode = cifRes.Data.RltdZipCd;
        //            tempPolicyPerson.SendCity = cifRes.Data.RltdBsicAddrCont?.Split(" ")?.FirstOrDefault();
        //            tempPolicyPerson.SendCountry = cifRes.Data.RltdBsicAddrCont?.Split(" ")?.LastOrDefault();
        //            tempPolicyPerson.Addr = cifRes.Data.RltdDtlAddrCont;
        //            tempPolicyPerson.Nationality = cifRes.Data.RltdAddrHrcyCd;
        //            tempPolicyPerson.Mobile = cifRes.Data.MbleTelNbr;
        //            tempPolicyPerson.Email = cifRes.Data.EmalAddr;
        //            tempPolicyPerson.JobClFrstStgeCd = cifRes.Data.JobClFrstStgeCd;
        //            tempPolicyPerson.JobClScndStgeCd = cifRes.Data.JobClScndStgeCd;
        //            tempPolicyPerson.JobDtlTpCd = cifRes.Data.JobDtlTpCd;
        //            tempPolicyPerson.UpdateBy = custId;
        //            tempPolicyPerson.UpdateOn = now;
        //            this.tempPolicyPersonInfoRepo.Update(tempPolicyPerson);
        //        }

        //        var encryptCode = this.commonService.GetDecryptSysParamValue("Customer_Key");
        //        if (string.IsNullOrWhiteSpace(encryptCode))
        //        {
        //            //this.eventLog.Log("INS0000015", "CreateOrder Error: INS0000015, Customer_Key not exist in SysParam", italIfId, italIfId);

        //            throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);
        //        }

        //        foreach (var m in master)
        //        {
        //            m.PolicySignStatus = PolicySignStatus.Unsigned;
        //            m.PaidType = req.Data.PayBy;
        //            m.AccountNo = req.Data.PaymentObject.PaymentNo;
        //            m.BankNo = req.Data.PaymentObject.BankCode;
        //            if (req.Data.PayBy.Equals(PaidType.CreditCard))
        //            {
        //                m.CardExpiredDate = req.Data.PaymentObject.CardExpiredDate;
        //                m.CardCheckCode = AES256.EncryptStringWithSalt(req.Data.PaymentObject.CardCheckCode, encryptCode, m.AcceptNo);
        //            }

        //            m.HearingSpeechImpaired = YesNoType.Yes.Equals(kycData.Where(x => this.commonService.GetKycIds(KycType.HearingSpeechImpaired).Contains(x.KycId)).OrderByDescending(x => x.SeqNo).FirstOrDefault()?.AnswerValue);
        //            m.UpdateBy = custId;
        //            m.UpdateOn = now;
        //            this.tempPolicyMasterRepo.Update(m);
        //        }

        //        this.otprecordRepo.Insert(new Otprecord()
        //        {
        //            AcceptNo = acceptNo,
        //            CustId = custId,
        //            Action = OTPAction.CreateOrder,
        //            Otpid = req.Data.OtpId,
        //            UpdateTime = now
        //        });

        //        await this.insUnitOfWork.SaveChangesAsync();

        //        #endregion

        //        var signDic = new Dictionary<string, PolicySignRecord> {
        //            { SignType.InsuredDataCheck, new PolicySignRecord() },
        //            { SignType.InsuredTimeCheck, new PolicySignRecord() },
        //            { SignType.TCTimeCheck, new PolicySignRecord() },
        //            { SignType.InformItemLoanCheck, new PolicySignRecord() },
        //            { SignType.InformItemPremiumCheck, new PolicySignRecord() },
        //            { SignType.AMLTimeCheck, new PolicySignRecord() }
        //        };

        //        // 初始化物件
        //        foreach (var item in signDic)
        //        {
        //            item.Value.AcceptNo = acceptNo;
        //            item.Value.CustId = custId;
        //            item.Value.SignType = item.Key;
        //            item.Value.SignDateTime = null;
        //            item.Value.PolicyCheck = true;
        //            item.Value.PremiumFrom = null;
        //            item.Value.CreateBy = custId;
        //            item.Value.CreateOn = now;
        //            item.Value.UpdateBy = custId;
        //            item.Value.UpdateOn = now;
        //        }

        //        #region 投保資料漏勾漏填暨投保規則檢核
        //        #region 檢核 Temp_PolicyMaster 欄位
        //        foreach (var m in master)
        //        {
        //            if (string.IsNullOrEmpty(m.ProductId) || string.IsNullOrEmpty(m.ProjectCode) || string.IsNullOrEmpty(m.CaseSource) || string.IsNullOrEmpty(m.PolicyPeriodE.ToString())
        //                 || string.IsNullOrEmpty(m.PlanCategory) || string.IsNullOrEmpty(m.PlanType)
        //                 || string.IsNullOrEmpty(m.AcceptDate.ToString()) || string.IsNullOrEmpty(m.AccountNo))
        //            {
        //                signDic[SignType.InsuredDataCheck].PolicyCheck = false;
        //                //this.eventLog.Log("LBEI006005", $"CreateOrderIns Check {nameof(TempPolicyMaster)} Error: LBEI006005, AcceptNo: {acceptNo} one of the columns is null or Empty in {nameof(TempPolicyMaster)}", italIfId, italIfId);
        //            }
        //            switch (m.PaidType)
        //            {
        //                case PaidType.CreditCard:
        //                    if (string.IsNullOrEmpty(m.AccountNo) || string.IsNullOrEmpty(m.CardExpiredDate) || string.IsNullOrEmpty(m.CardCheckCode))
        //                    {
        //                        signDic[SignType.InsuredDataCheck].PolicyCheck = false;
        //                        //this.eventLog.Log("LBEI006005", $"CreateOrderIns Check {nameof(TempPolicyMaster)} Error: LBEI006005, AcceptNo: {acceptNo} one of the columns is null or Empty in {nameof(TempPolicyMaster)}", italIfId, italIfId);
        //                    }
        //                    break;
        //                case PaidType.Account:
        //                    if (string.IsNullOrEmpty(m.BankNo))
        //                    {
        //                        signDic[SignType.InsuredDataCheck].PolicyCheck = false;
        //                        //this.eventLog.Log("LBEI006005", $"CreateOrderIns Check {nameof(TempPolicyMaster)} Error: LBEI006005, AcceptNo: {acceptNo} one of the columns is null or Empty in {nameof(TempPolicyMaster)}", italIfId, italIfId);
        //                    }
        //                    break;
        //            }
        //        }
        //        #endregion
        //        #region 檢核 Temp_PolicyDetail 欄位
        //        foreach (var d in detail)
        //        {
        //            if (string.IsNullOrEmpty(d.ProductId) || string.IsNullOrEmpty(d.PlanCode) || string.IsNullOrEmpty(d.ProjectCode)
        //              || string.IsNullOrEmpty(d.PlanVer.ToString()) || string.IsNullOrEmpty(d.PaymentTerm.ToString()) || string.IsNullOrEmpty(d.BenefitTerm.ToString())
        //              || string.IsNullOrEmpty(d.PlanCategory) || string.IsNullOrEmpty(d.OptionKey) || string.IsNullOrEmpty(d.ModalItemPermiumWithDisc.ToString()))

        //            {
        //                signDic[SignType.InsuredDataCheck].PolicyCheck = false;
        //                //this.eventLog.Log("LBEI006005", $"CreateOrderIns Check {nameof(TempPolicyDetail)} Error: LBEI006005, AcceptNo: {acceptNo} one of the columns is null or Empty in {nameof(TempPolicyDetail)}", italIfId, italIfId);
        //            }
        //        }
        //        #endregion
        //        #region 檢核Temp_PolicyCarInfo欄位
        //        if (string.IsNullOrEmpty(carInfo.LicensePlateNo) || string.IsNullOrEmpty(carInfo.VehicleType) || string.IsNullOrEmpty(carInfo.ManufacturedDate)
        //            || string.IsNullOrEmpty(carInfo.Displacement.ToString()) || string.IsNullOrEmpty(carInfo.CarType)
        //                 || string.IsNullOrEmpty(carInfo.Ibrand) || (string.IsNullOrEmpty(carInfo.EngineId) && string.IsNullOrEmpty(carInfo.CarBodyId))
        //                 || string.IsNullOrEmpty(carInfo.TransportUnit.ToString()) || carInfo.VehicleType.Equals(VehicleType.N04) ? string.IsNullOrEmpty(carInfo.Tonnage.ToString()) : false
        //                 || string.IsNullOrEmpty(carInfo.RegistIssueDate.ToString()))
        //        {
        //            signDic[SignType.InsuredDataCheck].PolicyCheck = false;
        //            //this.eventLog.Log("LBEI006005", $"CreateOrderIns Check {nameof(TempPolicyCarInfo)} Error: LBEI006005, AcceptNo: {acceptNo} one of the columns is null or Empty in {nameof(TempPolicyCarInfo)}", italIfId, italIfId);
        //        }
        //        #endregion

        //        #region 檢核Temp_PolicyPersonInfo 欄位
        //        // 取得要保人資訊
        //        var tempPersonInfo = personInfoList.Where(x => x.PersonType == PersonType.Applicant).FirstOrDefault();
        //        if (string.IsNullOrEmpty(tempPersonInfo.Idno) || string.IsNullOrEmpty(tempPersonInfo.PersonName) || tempPersonInfo.Birthday == null
        //            || string.IsNullOrEmpty(tempPersonInfo.Gender) || string.IsNullOrEmpty(tempPersonInfo.Mobile)
        //                 || string.IsNullOrEmpty(tempPersonInfo.Zipcode) || string.IsNullOrEmpty(tempPersonInfo.SendCity)
        //                 || string.IsNullOrEmpty(tempPersonInfo.SendCountry) || string.IsNullOrEmpty(tempPersonInfo.Addr) || string.IsNullOrEmpty(tempPersonInfo.Email)
        //                 || string.IsNullOrEmpty(tempPersonInfo.Nationality))
        //        {
        //            signDic[SignType.InsuredDataCheck].PolicyCheck = false;
        //            //this.eventLog.Log("LBEI006005", $"CreateOrderIns Check {nameof(TempPolicyPersonInfo)} Error: LBEI006005, AcceptNo: {acceptNo} one of the columns is null or Empty in {nameof(CustomerInfo)}", italIfId, italIfId);
        //        }
        //        #endregion

        //        #region 檢核 MemberProfile
        //        if (member == null)
        //        {
        //            signDic[SignType.InsuredDataCheck].PolicyCheck = false;
        //            //this.eventLog.Log("LBEI006005", $"CreateOrderIns Check {nameof(MemberProfile)} Error: LBEI006005, AcceptNo: {acceptNo} {nameof(MemberProfile)} not exists", italIfId, italIfId);
        //        }
        //        else
        //        {
        //            var amlEddStatus = this.commonService.GetAmlEddStatus(member, member.Amleddresult);
        //            if (amlEddStatus.Equals(AmlEDDStatus.Overdue) || amlEddStatus.Equals(AmlEDDStatus.UnderReview))
        //            {
        //                signDic[SignType.InsuredDataCheck].PolicyCheck = false;
        //                //this.eventLog.Log("LBEI006005", $"CreateOrderIns Check {nameof(MemberProfile)} Error: LBEI006005, AcceptNo: {acceptNo} {nameof(MemberProfile)} amlEddStatus is Overdue or UnderReview", italIfId, italIfId);
        //            }
        //        }

        //        #endregion
        //        #region prepare KYC Value
        //        var professionTypeRecord = kycData.Where(x => this.commonService.GetKycIds(KycType.ProfessionType).Contains(x.KycId)).FirstOrDefault();
        //        var terminationWithin3MRecord = kycData.Where(x => this.commonService.GetKycIds(KycType.PremiumFrom).Contains(x.KycId)).FirstOrDefault();
        //        var premiumFromRecord = kycData.Where(x => this.commonService.GetKycIds(KycType.PremiumFrom).Contains(x.KycId)).FirstOrDefault();
        //        #endregion
        //        #region 檢核 Kyc
        //        if (string.IsNullOrEmpty(professionTypeRecord?.AnswerValue) ||
        //            string.IsNullOrEmpty(terminationWithin3MRecord?.AnswerValue) ||
        //            string.IsNullOrEmpty(premiumFromRecord?.AnswerValue))
        //        {
        //            signDic[SignType.InsuredDataCheck].PolicyCheck = false;
        //            //this.eventLog.Log("LBEI006005", $"CreateOrderIns Check {nameof(TempKycrecordV1)} Error: LBEI006005, AcceptNo: {acceptNo} one of the columns is null or Empty in {nameof(TempKycrecordV1)}", italIfId, italIfId);
        //        }
        //        #endregion
        //        signDic[SignType.InsuredDataCheck].SignDateTime = now;
        //        #endregion

        //        #region 驗證保險期間不得早於申請日期檢核
        //        foreach (var m in master)
        //        {
        //            // 現在時間晚於保險起日 = 1
        //            // 現在時間等於保險起日 = 0
        //            // 現在時間早於保險起日 = -1
        //            if (!m.PolicyPeriodS.HasValue || DateTime.Compare(DateTime.Today, m.PolicyPeriodS.Value) == 1)
        //            {
        //                signDic[SignType.InsuredTimeCheck].PolicyCheck = false;
        //                //this.eventLog.Log("LBEI006005", $"CreateOrderIns Check {nameof(TempPolicyMaster)} Error: LBEI006005, AcceptNo: {acceptNo} the period(PolicyPeriodS) should not earlier than apply date", italIfId, italIfId);
        //            }
        //        }
        //        signDic[SignType.InsuredTimeCheck].SignDateTime = now;
        //        #endregion

        //        #region 前端呼叫保單條款「T&C API」的時間(timestamp)
        //        var tcRecord = this.tcRecordRepo.Queryable()
        //            .Where(t => t.CustId == custId && t.AcceptNo == acceptNo && t.Action == ActionType.CreateOrder)
        //            .OrderByDescending(t => t.AgreeTime)
        //            .FirstOrDefault();
        //        if (tcRecord == null)
        //        {
        //            signDic[SignType.TCTimeCheck].PolicyCheck = false;
        //            //this.eventLog.Log("LBEI006005", $"CreateOrderIns Check {nameof(TcRecord)} Error: LBEI006005, AcceptNo: {acceptNo} Action: {ActionType.CreateOrder} data not exists in {nameof(TcRecord)}", italIfId, italIfId);
        //        }
        //        else
        //        {
        //            signDic[SignType.TCTimeCheck].SignDateTime = tcRecord.AgreeTime;
        //        }
        //        #endregion

        //        #region 檢核客戶在前端點選「本行三個月內貸款戶檢核」選項的時間
        //        if (terminationWithin3MRecord == null || !terminationWithin3MRecord.CreateKycTime.HasValue)
        //        {
        //            signDic[SignType.InformItemLoanCheck].PolicyCheck = false;
        //            //this.eventLog.Log("LBEI006005", $"CreateOrderIns Check {nameof(TempKycrecordV1)} Error: LBEI006005, AcceptNo: {acceptNo} CreateKycTime not exists in {nameof(TempKycrecordV1)}", italIfId, italIfId);
        //        }
        //        else
        //        {
        //            signDic[SignType.InformItemLoanCheck].SignDateTime = terminationWithin3MRecord.CreateKycTime;
        //        }
        //        #endregion

        //        #region 檢核客戶在前端點選「保費來源風險警語檢核」選項的時間
        //        if (string.Equals(PremiumFromType.Loan, premiumFromRecord?.AnswerValue) || string.Equals(PremiumFromType.SuspendContract, premiumFromRecord?.AnswerValue))
        //        {
        //            signDic[SignType.InformItemPremiumCheck].PolicyCheck = premiumFromRecord.CreateKycTime.HasValue;
        //            if (!signDic[SignType.InformItemPremiumCheck].PolicyCheck)
        //            {
        //                //this.eventLog.Log("LBEI006005", $"CreateOrderIns Check {nameof(TempKycrecordV1)} Error: LBEI006005, AcceptNo: {acceptNo} kcyId:PremiumFrom CreateKycTime not exists in {nameof(TempKycrecordV1)}", italIfId, italIfId);
        //            }
        //            signDic[SignType.InformItemPremiumCheck].SignDateTime = premiumFromRecord.CreateKycTime;
        //        }
        //        signDic[SignType.InformItemPremiumCheck].PremiumFrom = premiumFromRecord?.AnswerValue;

        //        #endregion

        //        #region 發查 AML 的時間
        //        var memberAML = this.memberAmlRepo.Queryable().OrderByDescending(m => m.UpdateOn).FirstOrDefault(m => m.CustId == custId && m.AcceptNo == acceptNo);

        //        if (memberAML == null || memberAML.UpdateOn == null)
        //        {
        //            signDic[SignType.AMLTimeCheck].PolicyCheck = false;
        //            //this.eventLog.Log("LBEI006005", $"CreateOrderIns Check {nameof(MemberAml)} Error: LBEI006005, AcceptNo: {acceptNo} UpdateOn not exists in {nameof(MemberAml)}", italIfId, italIfId);
        //        }
        //        else
        //        {
        //            signDic[SignType.AMLTimeCheck].SignDateTime = memberAML.UpdateOn;
        //        }
        //        #endregion

        //        #region save date into db

        //        // PolicySignRecord
        //        foreach (var item in signDic)
        //        {
        //            this.policySignRecordRepo.Insert(item.Value);
        //        }

        //        // 案件查詢審核歷程
        //        var policySignLog = new PolicySignLog();
        //        policySignLog.AcceptNo = acceptNo;
        //        policySignLog.CustId = custId;
        //        policySignLog.LogDate = now;
        //        policySignLog.Msg = signDic.Values.Any(p => !p.PolicyCheck)
        //            ? PolicySignLogMsgType.SignRecordFail
        //            : PolicySignLogMsgType.SignRecordSuccess;
        //        policySignLog.ModifyBy = ModifyBy.CreateOrderAPI;
        //        this.policySignLogRepo.Insert(policySignLog);

        //        //更新簽署主檔
        //        policySignMaster.PolicySignStatus = policySignLog.Msg == PolicySignLogMsgType.SignRecordFail
        //            ? PolicySignStatus.CheckFail
        //            : PolicySignStatus.Unsigned;

        //        policySignMaster.UpdateBy = custId;
        //        policySignMaster.UpdateOn = now;
        //        policySignMaster.Pdfstatus = PDFStatus.UnProduced;
        //        policySignMaster.UploadPdfstatus = UploadPDFStatus.UnUpload;
        //        policySignMaster.CreateOrderStatus = CreateOrderStatus.UnSend;
        //        policySignMaster.PolicySignType = PolicySignType.Auto;
        //        policySignMaster.ErrorCount = 0;
        //        this.policySignMasterRepo.Update(policySignMaster);
        //        await this.insUnitOfWork.SaveChangesAsync();

        //        if (signDic.Values.Any(p => !p.PolicyCheck))
        //        {
        //            throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI006005);
        //        }
        //        #endregion

        //        messageHeader = this.commonService.GetChannelSuccessMessageHeader();
        //    }
        //    catch (Exception e)
        //    {
        //        this.eventLog.Error(e.Message, "F3101_CreateOrder");
        //        throw;
        //    }
        //    return this.channelAPICommonService.GetResp<CreateOrderInsResp>(responseHeader, messageHeader, StandardResCodeType.NormalProcessing, res);
        //}

        ///// <summary>
        ///// 設定保險鬧鐘
        ///// </summary>
        ///// <param name="req">查詢保險鬧鐘Request data</param>
        ///// <returns></returns>
        //public async Task<StandardModel<object>> SavePolicyAlarm(StandardModel<SavePolicyAlarmReq> req)
        //{
        //    var custId = req?.Header?.CustId;
        //    var italIfId = req?.Header?.ItalIfId;

        //    #region 資料檢核
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(custId), custId, italIfId);
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(req.Data.TagID), req?.Data?.TagID, italIfId);

        //    if (!req.Data.IsDelete)
        //    {
        //        this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(req.Data.ProductCode), req.Data.ProductCode, italIfId);

        //        if (req.Data.AlarmClockList == null || req.Data.AlarmClockList.Count == 0)
        //        {
        //            // Log
        //            //this.eventLog.Log("LBEI001001", $"{nameof(PolicyService)} {nameof(SavePolicyAlarm)} Error: LBEI001001, Request {nameof(req.Data.AlarmClockList)} is empty", italIfId, italIfId);
        //            throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001001, nameof(req.Data.AlarmClockList));
        //        }

        //        foreach (var item in req.Data.AlarmClockList)
        //        {
        //            this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(item.PlanCategory), item.PlanCategory, italIfId);
        //            this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(item.InsEndDate), item.InsEndDate?.ToString(), italIfId);
        //            this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(item.AvailableRenewalDate), item.AvailableRenewalDate?.ToString(), italIfId);
        //        }
        //    }
        //    #endregion

        //    #region 設定保險鬧鐘

        //    // 判斷IsDelete 
        //    //   true: 依tagId+custid刪除所有保期期間未到期資料
        //    //   false: 新增/更新保險鬧鐘資料

        //    if (req.Data.IsDelete)
        //    {
        //        var deleteAlarmList = this.policyClockRepo.Queryable().Where(x => x.TagId == req.Data.TagID && x.CustId == custId && x.InsEndDate > DateTime.Now).ToList();
        //        foreach (var item in deleteAlarmList)
        //        {
        //            this.policyClockRepo.Delete(item);
        //        }
        //    }
        //    else
        //    {
        //        foreach (var item in req.Data.AlarmClockList)
        //        {
        //            var policyClock = this.policyClockRepo.Queryable().Where(x => x.TagId == req.Data.TagID && x.CustId == custId && x.ProductCode == req.Data.ProductCode && x.PlanCategory == item.PlanCategory && x.InsEndDate > DateTime.Now).FirstOrDefault();

        //            if (policyClock == null)
        //            {
        //                // 新增
        //                policyClock = new PolicyClock()
        //                {
        //                    CustId = custId,
        //                    TagId = req.Data.TagID,
        //                    ProductCode = req.Data.ProductCode,
        //                    PlanCategory = item.PlanCategory,
        //                    InsEndDate = DateTimeOffset.FromUnixTimeSeconds(item.InsEndDate ?? 0).LocalDateTime,
        //                    AvailableRenewalDate = DateTimeOffset.FromUnixTimeSeconds(item.AvailableRenewalDate ?? 0).LocalDateTime,
        //                    IsCallAoa = true,
        //                    CreateBy = custId,
        //                    CreateOn = DateTime.Now
        //                };
        //                this.policyClockRepo.Insert(policyClock);
        //            }
        //            else
        //            {
        //                // 修改
        //                policyClock.InsEndDate = DateTimeOffset.FromUnixTimeSeconds(item.InsEndDate ?? 0).LocalDateTime;
        //                policyClock.AvailableRenewalDate = DateTimeOffset.FromUnixTimeSeconds(item.AvailableRenewalDate ?? 0).LocalDateTime;
        //                policyClock.IsCallAoa = true;
        //                this.policyClockRepo.Update(policyClock);
        //            }
        //        }

        //    }

        //    await this.insUnitOfWork.SaveChangesAsync();

        //    #endregion

        //    var messageHeader = this.commonService.GetChannelSuccessMessageHeader();
        //    return this.channelAPICommonService.GetResp<object>(req.Header, messageHeader, StandardResCodeType.NormalProcessing, null);
        //}

        ///// <summary>
        ///// 查詢保險鬧鐘
        ///// </summary>
        ///// <param name="req">查詢保險鬧鐘Request data</param>
        ///// <returns></returns>
        //public async Task<StandardModel<GetPolicyAlarmResp>> GetPolicyAlarm(StandardModel<GetPolicyAlarmReq> req)
        //{
        //    var custId = req?.Header?.CustId;
        //    var italIfId = req?.Header?.ItalIfId;
        //    var tagID = req?.Data?.TagID;

        //    #region check req data
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(custId), custId, italIfId);
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(tagID), tagID, italIfId);
        //    #endregion

        //    #region get resp data (查詢保險鬧鐘)

        //    // 依tagId+custid查詢保險迄日尚未到期且尚未取消的保險鬧鐘
        //    var alarmClockList = this.policyClockRepo.Queryable()
        //                                          .Where(x => x.TagId == tagID && x.CustId == custId && (x.IsCallAoa ?? true) && x.InsEndDate > DateTime.Now)
        //                                          .Select(x => new AlarmClockItem()
        //                                          {
        //                                              PlanCategory = x.PlanCategory,
        //                                              InsEndDate = this.commonService.ParseDatetimeToTimeSpan(x.InsEndDate),
        //                                              AvailableRenewalDate = this.commonService.ParseDatetimeToTimeSpan(x.AvailableRenewalDate),
        //                                          })
        //                                          .ToList();
        //    var res = new GetPolicyAlarmResp()
        //    {
        //        AlarmClockList = alarmClockList == null || alarmClockList.Count == 0 ? null : alarmClockList
        //    };
        //    #endregion

        //    var messageHeader = this.commonService.GetChannelSuccessMessageHeader();
        //    return this.channelAPICommonService.GetResp<GetPolicyAlarmResp>(req.Header, messageHeader, StandardResCodeType.NormalProcessing, res);
        //}

        /// <summary>
        /// 是否為正式檔資料
        /// </summary>
        /// <param name="acceptNo">進件編號</param>
        /// <param name="planCategory">商品類型</param>
        /// <returns></returns>
        private bool IsOfficialPolicyInfo(string acceptNo, string planCategory)
        {
            return this.policyMasterRepo.Queryable().Any(o => o.AcceptNo == acceptNo && o.PlanCategory == planCategory && string.IsNullOrEmpty(o.PolicyNo));
        }

        ///// <summary>
        ///// 依進件編號取得保單正式檔正式資料
        ///// </summary>
        ///// <param name="acceptNo">進件編號</param>
        ///// <returns></returns>
        //public AcceptData GetOfficialAcceptData(string acceptNo)
        //{
        //    var acceptData = new AcceptData()
        //    {
        //        AcceptNo = acceptNo,
        //        PolicyMasters = this.policyMasterRepo.Queryable().Where(o => o.AcceptNo == acceptNo).ToList(),
        //        PolicyDetails = this.policyDetailRepo.Queryable().Where(o => o.AcceptNo == acceptNo).ToList(),
        //        PolicyPersonInfos = this.policyPersonInfoRepo.Queryable().Where(o => o.AcceptNo == acceptNo).ToList(),
        //        PolicyCarInfos = this.policyCarInfoRepo.Queryable().Where(o => o.AcceptNo == acceptNo).ToList(),
        //        KycRecordV1s = this.kycRecordV1Repo.Queryable().Where(o => o.AcceptNo == acceptNo).ToList()
        //    };
        //    acceptData.ProductId = acceptData.PolicyMasters.FirstOrDefault()?.ProductId;
        //    acceptData.ProjectCode = acceptData.PolicyMasters.FirstOrDefault()?.ProjectCode;
        //    return acceptData;
        //}

        ///// <summary>
        ///// 驗證保單正式檔資料是否正確
        ///// </summary>
        ///// <param name="acceptData">保單正式檔</param>
        ///// <returns>(bool IsValid,string ErrorMsg)</returns>
        //public (bool IsValid, string ErrorMsg) ValidateAcceptData(AcceptData acceptData)
        //{
        //    if (acceptData.PolicyMasters == null || !acceptData.PolicyMasters.Any())
        //    {
        //        return (false, $"PolicyMaster 查無 AcceptNo 為 {acceptData.AcceptNo} 的資料");
        //    }
        //    if (acceptData.PolicyDetails == null || !acceptData.PolicyDetails.Any())
        //    {
        //        return (false, $"PolicyDetail 查無 AcceptNo 為 {acceptData.AcceptNo},ProductId 為 {acceptData.ProductId},ProjectCode 為 {acceptData.ProjectCode} 的資料");
        //    }
        //    if (acceptData.PolicyPersonInfos == null || !acceptData.PolicyPersonInfos.Any(o => o.PersonType == PersonType.Applicant))
        //    {
        //        return (false, $"PolicyPersonInfo 查無 AcceptNo 為 {acceptData.AcceptNo} 的 ApplicantId");
        //    }
        //    if (acceptData.PolicyCarInfos == null || !acceptData.PolicyCarInfos.Any())
        //    {
        //        return (false, $"PolicyCarInfo 查無 AcceptNo 為 {acceptData.AcceptNo} 的資料");
        //    }
        //    return (true, string.Empty);
        //}

        ///// <summary>
        ///// 更新保單續保資訊
        ///// </summary>
        ///// <param name="policyNoList">保單號碼清單</param>
        ///// <param name="batchId"></param>
        //public async Task CheckRenewNotice(IEnumerable<string> policyNoList, string batchId)
        //{
        //    DateTime now = DateTime.Now;
        //    var licensePlateGroupList = (from policy in this.policyMasterRepo.Queryable()
        //                                 join carInfo in this.policyCarInfoRepo.Queryable()
        //                                 on new
        //                                 {
        //                                     policy.PolicyNo,
        //                                     policy.PlanCategory
        //                                 }
        //                                 equals new
        //                                 {
        //                                     carInfo.PolicyNo,
        //                                     carInfo.PlanCategory
        //                                 }
        //                                 where policyNoList.Contains(policy.PolicyNo)
        //                                 && policy.OrderStatus == OrderStatus.Processed
        //                                 && policy.PendingStatus == PendingStatus.UnderwritePass
        //                                 && policy.PaymentStatus == PaymentStatus.Paid
        //                                 && policy.ExtendStatus != ExtendStatus.Already
        //                                 && policy.PolicyPeriodE.HasValue
        //                                 && policy.PolicyPeriodE.Value.Date < now.Date
        //                                 select new
        //                                 {
        //                                     LicensePlateNo = carInfo.LicensePlateNo,
        //                                     PolicyInfo = policy
        //                                 }).ToList()
        //                            .GroupBy(o => o.LicensePlateNo)
        //                            .Select(o => new
        //                            {
        //                                o.Key,
        //                                PolicyList = o.Select(policy => policy.PolicyInfo).OrderByDescending(policy => policy.InsComUwpassDate).Take(2).ToList()
        //                            }).ToList();

        //    foreach (var policyList in licensePlateGroupList.Where(o => o.PolicyList.Count == 2).Select(o => o.PolicyList))
        //    {
        //        policyList.Last().OrgPolicyNo = policyList.First().PolicyNo;
        //        policyList.Last().ExtendStatus = ExtendStatus.Already;
        //        policyList.Last().UpdateBy = batchId;
        //        policyList.Last().UpdateOn = now;
        //        this.policyMasterRepo.Update(policyList.Last());
        //        var dataLogPolicyMaster = this.mapper.Map<DataLogPolicyMaster>(policyList.Last());
        //        this.dataLogPolicyMasterRepo.Insert(dataLogPolicyMaster);
        //    }

        //    this.insUnitOfWork.BeginTransaction();
        //    try
        //    {
        //        await this.insUnitOfWork.SaveChangesAsync();
        //    }
        //    catch (Exception ex)
        //    {
        //        this.insUnitOfWork.Rollback();
        //        eventLog.Log("INS0000004", "更新續保資訊失敗", this.validationService.GetCurrentMethod(), batchId, ex);
        //    }
        //    this.insUnitOfWork.Commit();
        //    this.insUnitOfWork.ResetContextState();
        //}

        ///// <summary>
        ///// 檢查並更新保險鬧鐘
        ///// </summary>
        ///// <param name="policyNoList">保單號碼清單</param>
        ///// <param name="batchId"></param>
        //public async Task CheckPolicyClock(IEnumerable<string> policyNoList, string batchId)
        //{
        //    var carInfoList = (from carInfo in this.policyCarInfoRepo.Queryable()
        //                       join policy in this.policyMasterRepo.Queryable()
        //                       on carInfo.PolicyNo equals policy.PolicyNo
        //                       where policyNoList.Contains(policy.PolicyNo)
        //                       select new
        //                       {
        //                           TagId = carInfo.LicensePlateNo,
        //                           PlanCategory = carInfo.PlanCategory,
        //                       }).ToList()
        //                  .GroupBy(o => new { o.TagId, o.PlanCategory }).Select(o => o.Key).ToList();
        //    var clockList = (from carInfo in carInfoList
        //                     join clock in this.policyClockRepo.Queryable()
        //                     on new
        //                     {
        //                         carInfo.TagId,
        //                         carInfo.PlanCategory
        //                     }
        //                     equals new
        //                     {
        //                         clock.TagId,
        //                         clock.PlanCategory
        //                     }
        //                     where clock.IsCallAoa.HasValue && clock.IsCallAoa.Value
        //                     select clock).ToList();

        //    this.insUnitOfWork.BeginTransaction();
        //    eventLog.Info($"保險鬧鐘需處理的筆數為: {clockList.Count}", this.validationService.GetCurrentMethod(), batchId);
        //    clockList.ForEach(o => o.IsCallAoa = false);
        //    try
        //    {
        //        await this.insUnitOfWork.SaveChangesAsync();
        //    }
        //    catch (Exception ex)
        //    {
        //        this.insUnitOfWork.Rollback();
        //        eventLog.Log("INS0000004", "更新保險鬧鐘", this.validationService.GetCurrentMethod(), batchId, ex);
        //    }
        //    this.insUnitOfWork.Commit();
        //    this.insUnitOfWork.ResetContextState();
        //}


        ///// <summary>
        ///// 查詢保單內容(任意險)
        ///// </summary>
        ///// <param name="req">查詢保單內容 Req</param>
        ///// <returns>查詢保單內容 Resp</returns>
        //public StandardModel<GetPolicyDetailResp> InquirePolicyDetail(StandardModel<GetPolicyDetailReq> req)
        //{
        //    var custId = req.Header?.CustId;
        //    var italIfId = req.Header?.ItalIfId;
        //    var acceptNo = req.Data?.AcceptNo;
        //    var productCode = req.Data?.ProductCode;
        //    var policyNo = req.Data?.PolicyNo;

        //    // 資料檢核
        //    // 傳入的客戶唯一識別碼為空
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(custId), custId, italIfId);

        //    // 判斷是否為正式資料
        //    bool isContractData = false;

        //    PolicyCarInfo policyCarInfo = null;

        //    // 以取得車籍資料為起點，並統一轉換為PolicyCarInfo物件
        //    if (string.IsNullOrWhiteSpace(acceptNo) && !string.IsNullOrWhiteSpace(productCode))
        //    {
        //        // 取得該客戶該專案最新一筆車籍資料
        //        var tempPolicyCarInfo = this.tempPolicyCarInfoRepo.Queryable().AsNoTracking()
        //            .Join(
        //                this.policySignMasterRepo.Queryable().AsNoTracking().Where(x => x.ProductId == productCode && x.CustId == custId && x.PolicySignStatus == null),
        //                c => c.AcceptNo,
        //                s => s.AcceptNo,
        //                (c, s) => c
        //            )
        //            .OrderByDescending(x => x.CreateOn)
        //            .FirstOrDefault();

        //        policyCarInfo = tempPolicyCarInfo != null ? this.mapper.Map<PolicyCarInfo>(tempPolicyCarInfo) : null;

        //        // 因acceptNo為空,設定該筆acceptNo
        //        acceptNo = policyCarInfo?.AcceptNo;
        //    }
        //    else
        //    {
        //        // 取得受理編號狀態檔資料 CheckOrderNo這個欄位判斷 true:在主檔 false:在 temp
        //        isContractData = this.acceptRecordRepo.Queryable().AsNoTracking().Where(x => x.AcceptNo == acceptNo).Select(x => x.CheckOrderNo).FirstOrDefault() ?? false;

        //        if (isContractData)
        //        {
        //            policyCarInfo = this.policyCarInfoRepo.Queryable().AsNoTracking().Where(x => x.AcceptNo == acceptNo).FirstOrDefault();
        //        }
        //        else
        //        {
        //            var tempPolicyCarInfo = this.tempPolicyCarInfoRepo.Queryable().AsNoTracking().Where(x => x.AcceptNo == acceptNo).FirstOrDefault();
        //            policyCarInfo = tempPolicyCarInfo != null ? this.mapper.Map<PolicyCarInfo>(tempPolicyCarInfo) : null;
        //        }

        //        productCode = this.policySignMasterRepo.Queryable()
        //                              .AsNoTracking()
        //                              .Where(x => x.AcceptNo == acceptNo && x.CustId == custId)
        //                              .Select(x => x.ProductId)
        //                              .SingleOrDefault();
        //    }

        //    if (policyCarInfo == null || string.IsNullOrWhiteSpace(acceptNo) || string.IsNullOrWhiteSpace(productCode) || (!isContractData && !string.IsNullOrWhiteSpace(policyNo)))
        //    {
        //        //this.eventLog.Log("LBEI004001", $"{nameof(InquirePolicyDetail)} Error: LBEI004001, AcceptNo: {acceptNo}, PolicyNo: {policyNo}, ProductCode: {productCode}, CustId: {custId} data not exist", italIfId, italIfId);
        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI004001);
        //    }

        //    PolicyMaster policyMaster = null;
        //    List<PolicyDetail> policyDetailList = null;
        //    PolicyPersonInfo policyPersonInfo = null;
        //    TempTrialcalMasterV1 tempTrialcalMaster = null;

        //    if (isContractData)
        //    {
        //        #region 取得PolicyMaster資料
        //        var policyMasterQuery = this.policyMasterRepo.Queryable().AsNoTracking().Where(x => x.AcceptNo == acceptNo);

        //        if (!string.IsNullOrWhiteSpace(policyNo))
        //        {
        //            policyMasterQuery = policyMasterQuery.Where(x => x.PolicyNo == policyNo);
        //        }

        //        policyMaster = policyMasterQuery.FirstOrDefault();
        //        #endregion

        //        #region 取得PolicyDetail資料
        //        policyDetailList = this.policyDetailRepo.Queryable().AsNoTracking().Where(x => x.AcceptNo == acceptNo && x.PolicyNo == policyMaster.PolicyNo).ToList();
        //        #endregion

        //        #region 取得PolicyPersonInfo 要保人資料
        //        policyPersonInfo = this.policyPersonInfoRepo.Queryable().AsNoTracking().Where(x => x.AcceptNo == acceptNo && x.PolicyNo == policyMaster.PolicyNo && x.PersonType == PersonType.Applicant).FirstOrDefault();
        //        #endregion
        //    }
        //    else
        //    {
        //        #region 取得Temp_PolicyMaster資料
        //        var tempPolicyMaster = this.tempPolicyMasterRepo.Queryable().AsNoTracking().Where(x => x.AcceptNo == acceptNo).FirstOrDefault();
        //        policyMaster = tempPolicyMaster != null ? this.mapper.Map<PolicyMaster>(tempPolicyMaster) : null;
        //        #endregion

        //        var planCategory = policyMaster?.PlanCategory ?? string.Empty;
        //        #region 取得Temp_PolicyDetail資料
        //        var tempPolicyDetailList = this.tempPolicyDetailRepo.Queryable().AsNoTracking().Where(x => x.AcceptNo == acceptNo && x.PlanCategory == planCategory).ToList();
        //        policyDetailList = tempPolicyDetailList != null ? this.mapper.Map<List<PolicyDetail>>(tempPolicyDetailList) : null;
        //        #endregion

        //        #region 取得Temp_PolicyPersonInfo 要保人資料
        //        var tempPolicyPersonInfo = this.tempPolicyPersonInfoRepo.Queryable().AsNoTracking().Where(x => x.AcceptNo == acceptNo && x.PersonType == PersonType.Applicant).FirstOrDefault();
        //        policyPersonInfo = tempPolicyPersonInfo != null ? this.mapper.Map<PolicyPersonInfo>(tempPolicyPersonInfo) : null;
        //        #endregion

        //        #region 取得Temp_TrialcalMasterV1資料 (當Temp_PolicyMaster尚未有資料時可使用)
        //        tempTrialcalMaster = this.tempTrialcalMasterV1Repo.Queryable().AsNoTracking().Where(x => x.AcceptNo == acceptNo).OrderByDescending(x => x.SeqNo).FirstOrDefault();
        //        #endregion
        //    }

        //    // 取得專案主檔資料
        //    var productMaster = this.productMasterRepo.Queryable().AsNoTracking().Where(x => x.ProductId == productCode).SingleOrDefault();

        //    // 取得保險公司代碼
        //    var companyCode = productMaster?.CompanyCode ?? string.Empty;
        //    // 取得公司主檔資料
        //    var companyProfile = this.companyProfileRepo.Queryable().AsNoTracking().Where(x => x.CompanyCode == companyCode).SingleOrDefault();

        //    // 取得方案代碼
        //    var projectCode = policyMaster?.ProjectCode ?? tempTrialcalMaster?.ProjectCode;
        //    // 取得方案資料
        //    var campaignMaster = this.campaignMasterRepo.Queryable().AsNoTracking().Where(x => x.ProductId == productCode && x.ProjectCode == projectCode).SingleOrDefault();
        //    // 取得商品顯別
        //    var planType = campaignMaster?.PlanType;

        //    List<PlanMaster> planMasterList = null;
        //    // 取得PlanMaster商品資料
        //    if (policyDetailList != null)
        //    {
        //        planMasterList = policyDetailList
        //                            .Join(
        //                                this.planMasterRepo.Queryable().AsNoTracking(),
        //                                pd => new { CompanyCode = companyCode, pd.PlanCode, pd.PlanVer, pd.PaymentTerm, pd.BenefitTerm, PlanType = planType },
        //                                pm => new { pm.CompanyCode, pm.PlanCode, pm.PlanVer, pm.PaymentTerm, pm.BenefitTerm, pm.PlanType },
        //                                (pd, pm) => pm)
        //                            .ToList();
        //    }


        //    // (A)	傳入acceptNo + policyNo + custId 。(發查我的保單內容)
        //    // 1.	有保單號碼代表保單已成立，故查詢保單主檔（policymaster）
        //    // 2.	取得相關資料:
        //    // 2.1	依acceptNo 到「保單主檔」查詢「專案代碼」，再到「專案主檔」取得對應的 CompanyCode。
        //    // 2.2	依專案代碼至「方案主檔」取得方案代碼、方案名稱。
        //    // 2.3	再到「保險公司主檔」取得保險公司簡稱、保險公司電話號碼。
        //    // 2.4	依acceptNo至「保單明細檔」(PolicyDetail)取得商品代碼。
        //    // 2.5	依acceptNo至「保單車籍資料檔」(PolicyCarInfo)取得displayName、carType、brandID、brandName、transportUnit。
        //    // 2.6	依acceptNo + 人員類別 查詢「投保相關人員檔」(PolicyPersonInfo)，取得要被保險人ID(IDNo)、要被保險人生日(Birthday) 。
        //    // 2.7	是否能續保：請參閱【Inquire保單清單API】。
        //    // 2.8	保單狀態(insStatus)：請參閱【F3103_Inquire保單清單API】規格書內容。
        //    // 2.9	以acceptNo查詢policyDetail取得保障項目(Abbreviation)與保額(Assured)，回傳guaranteeItems:保障項目(content),保額(value)。
        //    // 2.10	其餘回傳欄位，請查詢「保單主檔」同名欄位。

        //    // (B)	僅有acceptNo + custId。(AML/續保通知/Line試算 流程發查既有資料)
        //    // 1.	僅有acceptNo的情境可能為保單成立或保單尚未成立的情境，故保單主檔(policyMaster)與保單暫存檔(temp_policyMaster)均需查找，並取得相關明細。
        //    // 2.	相關明細資料取得同(A)，若該acceptNo存在於保單主檔(policyMaster)則取policyDetail/policyPersonInfo/policyCarinfo；如policyMaster查無資料則取以下暫存資料表temp_policyDetail/temp_policyPersoninfo/temp_policyCarinfo，暫存表中部分資料可能為空，請參考API文件內範例。
        //    // 3.	如方案資訊為空，則由最後一筆Temp_TrialcalMasterV1保費試算結果取得方案資訊。

        //    // (C)	僅有productCode + custId。(保險鬧鐘)
        //    // 1.	以productCode及客戶ID查找temp_policyCarinfo取得最新一筆資料。
        //    // 2.	以productCode及客戶ID查找temp_policyMaster取得方案資訊，若查無資料則由最後一筆Temp_TrialcalMasterV1保費試算結果取得方案資訊，都無資料則回傳空值，請參考API文件內範例。

        //    // 取得專案銷售截止日
        //    var endDate = productMaster.ProductEndOn.AddDays(1).AddSeconds(-1);
        //    DateTime sysDate = DateTime.Today;

        //    var res = new GetPolicyDetailResp()
        //    {
        //        ProductCode = productCode,
        //        ProductName = productMaster?.Name,
        //        ProjectCode = projectCode,
        //        ProjectName = campaignMaster?.Name,
        //        PolicyNo = policyMaster?.PolicyNo,
        //        BeginDate = this.commonService.ParseDatetimeToTimeSpan(policyMaster?.PolicyPeriodS),
        //        EndDate = this.commonService.ParseDatetimeToTimeSpan(policyMaster?.PolicyPeriodE),
        //        Period = policyMaster?.BenefitTerm ?? 1,
        //        TagId = policyCarInfo.LicensePlateNo,
        //        RegistrationIssueDate = policyCarInfo.RegistIssueDate?.ToTaiwanDateTimeStr()?.Replace("/", string.Empty),
        //        VehicleType = policyCarInfo.VehicleType,
        //        ManufacturedDate = policyCarInfo.ManufacturedDate,
        //        Displacement = policyCarInfo.Displacement,
        //        Tonnage = policyCarInfo.Tonnage,
        //        EngineId = policyCarInfo.EngineId,
        //        InsType = this.commonService.GetSysParamValue(GroupIdType.InsTypeMapping, productMaster?.PlanType),
        //        InsStatus = (policyMaster == null || policyMaster.PolicyStatus == null) ? string.Empty : policyMaster.PolicyStatus == PolicyStatusType.Active ? InsStatusType.Active : InsStatusType.Expired,
        //        IsExtend = policyMaster != null && string.Equals(policyMaster.ExtendStatus, PolicyExtendStatus.Renewable),
        //        IsExpired = !(policyMaster == null || policyMaster.PolicyStatus == null || policyMaster.PolicyStatus == PolicyStatusType.Active),
        //        IsProductExist = productMaster != null && (sysDate >= productMaster.ProductStartOn && sysDate <= endDate),
        //        ExpiredLeftDays = (policyMaster == null || policyMaster.PolicyStatus == null) ? 0 : policyMaster.PolicyStatus != PolicyStatusType.Active ? -1 : new TimeSpan(policyMaster.PolicyPeriodE.GetValueOrDefault().Date.Ticks - DateTime.Today.Ticks).Days,
        //        TotalPremium = policyMaster?.ModalPermiumWithDisc ?? tempTrialcalMaster?.TotalPremium ?? 0,
        //        PlanCategory = policyMaster?.PlanCategory,
        //        PlanCategoryName = this.commonService.GetSysParamValue(GroupIdType.PlanCategory, policyMaster?.PlanCategory),
        //        InsuredId = policyPersonInfo?.Idno,
        //        InsuredBirthday = this.commonService.ParseDatetimeToTimeSpan(policyPersonInfo?.Birthday),
        //        PayBy = policyMaster?.PaidType,
        //        CardNo = policyMaster?.AccountNo?.Replace("-", string.Empty),
        //        PolicyDelivery = policyMaster?.PolicySendType,
        //        InsuranceId = companyCode,
        //        InsuranceCompany = companyProfile.CompanyShortName,
        //        InsuranceTel = $"{companyProfile.CompanyPhoneAreaCode}-{companyProfile.CompanyPhoneLastCode}",
        //        CarTypeList = new List<CarTypeInfos>() {
        //            new CarTypeInfos()
        //            {
        //                DisplayName = policyCarInfo.Nbrand,
        //                CarType = policyCarInfo.CarType,
        //                BrandID = policyCarInfo.Ibrand,
        //                BrandName = policyCarInfo.NbrandAbbr,
        //                TransportUnit = policyCarInfo.TransportUnit ?? 0
        //            }
        //        },
        //        Plans = policyDetailList == null || !policyDetailList.Any() ? null :
        //                    policyDetailList.Select(x => new DetailPlanInfo()
        //                    {
        //                        PlanCode = x.PlanCode,
        //                        Item = planMasterList.Where(y => y.CompanyCode == companyCode && y.PlanCode == x.PlanCode && y.PlanVer == x.PlanVer && y.PaymentTerm == x.PaymentTerm && y.BenefitTerm == x.BenefitTerm && y.PlanType == planType).Select(y => y.PlanFullName).SingleOrDefault(),
        //                        ContentPremium = x.ModalItemPermiumWithDisc ?? 0,
        //                        GuaranteeItems = new List<GuaranteeItem>() {
        //                            new GuaranteeItem()
        //                            {
        //                                Content = x.Abbreviation,
        //                                Value = x.Assured
        //                            }
        //                        }
        //                    }).ToList()
        //    };

        //    // Log
        //    //this.eventLog.Trace("InquirePolicyDetail End", italIfId, italIfId);

        //    var messageHeader = this.commonService.GetChannelSuccessMessageHeader();

        //    return this.channelAPICommonService.GetResp<GetPolicyDetailResp>(req.Header, messageHeader, StandardResCodeType.NormalProcessing, res);
        //}

        ///// <summary>
        ///// 查詢保單清單(任意險)
        ///// </summary>
        ///// <param name="req">查詢保單清單 Req</param>
        ///// <returns>保單查詢 Resp</returns>
        //public async Task<StandardModel<InquirePolicyListResp>> InquirePolicyList(StandardModel<InquirePolicyListReq> req)
        //{

        //    var custId = req?.Header?.CustId;
        //    var italIfId = req?.Header?.ItalIfId;
        //    var insType = req?.Data?.InsType;
        //    var insStatus = req?.Data?.InsStatus;

        //    // Log
        //    //this.eventLog.Trace("InquirePolicyList Start", italIfId, italIfId);

        //    #region check req data
        //    // 傳入的客戶唯一識別碼為空
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(custId), custId, italIfId);
        //    // 險種類型為空 
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(insType), insType, italIfId);
        //    // 保單狀態為空
        //    this.validationService.CheckRequestDataNotNullOrEmpty(nameof(PolicyService), this.validationService.GetCurrentMethod(), nameof(insStatus), insStatus, italIfId);

        //    // 險種類型不為 01 汽車或 02 機車或 03 車險
        //    if (insType != InsType.Car && insType != InsType.Moto && insType != InsType.Vehicle)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001002", "InquireList CheckInquireListReq Error: LBEI001002, Request InsType not 01,02,03", italIfId, italIfId);
        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001002, "insType");
        //    }

        //    // 保單狀態值不為 01 生效,02 過期,03 全部任一種
        //    if (insStatus != InsStatusType.Active && insStatus != InsStatusType.Expired && insStatus != InsStatusType.All)
        //    {
        //        // Log
        //        //this.eventLog.Log("LBEI001002", "InquireList CheckInquireListReq Error: LBEI001002, Request InsStatus not 01,02,03", italIfId, italIfId);
        //        throw new ChannelAPIException(ReturnCodes.Failure_InsChannel_LBEI001002, "insStatus");
        //    }
        //    #endregion

        //    #region 檢核會員
        //    // 暫不檢核，但先保留
        //    // this.validationService.CheckChannelMemberStatus(custId, req.Header.ItalIfId);
        //    #endregion

        //    var policyMasters = this.policyMasterRepo.Queryable().AsNoTracking().Where(p => p.CustId == custId);

        //    List<string> expiredList = new List<string>() {
        //        PolicyStatusType.Expired,
        //        PolicyStatusType.Terminate,
        //        PolicyStatusType.Transfer
        //    };

        //    // 依狀態取得對應資料
        //    if (insStatus == InsStatusType.Active)
        //    {
        //        policyMasters = policyMasters.Where(x => x.PolicyStatus == PolicyStatusType.Active);
        //    }
        //    else if (insStatus == InsStatusType.Expired)
        //    {
        //        // 取得系統日-保單到期日<=2年且保單狀態為"退保(解約)"、"過戶"、"到期"的保單
        //        policyMasters = policyMasters.Where(x => expiredList.Contains(x.PolicyStatus) && x.PolicyPeriodE.HasValue && DateTime.Now < x.PolicyPeriodE.Value.AddYears(2));
        //    }
        //    else
        //    {
        //        policyMasters = policyMasters.Where(x => x.PolicyStatus == PolicyStatusType.Active || (expiredList.Contains(x.PolicyStatus) && x.PolicyPeriodE.HasValue && DateTime.Now < x.PolicyPeriodE.Value.AddYears(2)));
        //    }

        //    // 判斷查詢險種 01-汽車險 02-機車險 03-全部
        //    if (insType == InsType.Moto || insType == InsType.Car)
        //    {
        //        var insTypeMappingSysParamList = this.commonService.SearchSysParamByGroupId(GroupIdType.InsTypeMapping);
        //        var planType = insTypeMappingSysParamList.Where(x => x.ItemValue == insType).Select(x => x.ItemId).FirstOrDefault();

        //        policyMasters = policyMasters.Where(x => x.PlanType == planType);
        //    }

        //    var policyMasterList = policyMasters.ToList();

        //    var result = new InquirePolicyListResp();

        //    if (policyMasterList?.Count > 0)
        //    {
        //        var policyList = new List<InquirePolicyListItem>();
        //        Dictionary<string, CompanyProfile> companyProfileDic = new Dictionary<string, CompanyProfile>();
        //        Dictionary<string, CampaignMaster> campaignMasterDic = new Dictionary<string, CampaignMaster>();

        //        foreach (var policyMaster in policyMasterList)
        //        {
        //            // 取得專案主檔
        //            var productMaster = this.GetProductMaster(policyMaster.ProductId);

        //            // 取得保險公司資訊
        //            if (!companyProfileDic.ContainsKey(productMaster.CompanyCode))
        //            {
        //                var companyProfileData = this.companyProfileRepo.Queryable().AsNoTracking().FirstOrDefault(x => x.CompanyCode == productMaster.CompanyCode);
        //                if (companyProfileData != null)
        //                {
        //                    companyProfileDic.Add(productMaster.CompanyCode, companyProfileData);
        //                }
        //            }
        //            CompanyProfile companyProfile = companyProfileDic.ContainsKey(productMaster.CompanyCode) ? companyProfileDic[productMaster.CompanyCode] : null;

        //            // 取得方案資訊
        //            if (!campaignMasterDic.ContainsKey($"{policyMaster.ProductId}{policyMaster.ProjectCode}"))
        //            {

        //                var campaignMasterData = this.campaignMasterRepo.Queryable().AsNoTracking()
        //                      .FirstOrDefault(c => c.ProductId == policyMaster.ProductId && c.ProjectCode == policyMaster.ProjectCode);

        //                if (campaignMasterData != null)
        //                {
        //                    campaignMasterDic.Add($"{policyMaster.ProductId}{policyMaster.ProjectCode}", campaignMasterData);
        //                }
        //            }
        //            CampaignMaster campaignMaster = campaignMasterDic.ContainsKey($"{policyMaster.ProductId}{policyMaster.ProjectCode}") ? campaignMasterDic[$"{policyMaster.ProductId}{policyMaster.ProjectCode}"] : null;

        //            // 保單車籍資料檔
        //            var policyCarInfo = this.policyCarInfoRepo.Queryable().AsNoTracking()
        //                .FirstOrDefault(p => p.AcceptNo == policyMaster.AcceptNo && p.PolicyNo == policyMaster.PolicyNo);
        //            // 取得專案銷售截止日
        //            var endDate = productMaster.ProductEndOn.AddDays(1).AddSeconds(-1);

        //            DateTime systemDate = DateTime.Today;

        //            var item = new InquirePolicyListItem()
        //            {
        //                InsuranceCompanyName = companyProfile?.CompanyName,
        //                PolicyNo = policyMaster.PolicyNo,
        //                AcceptNo = policyMaster.AcceptNo,
        //                ProductCode = policyMaster.ProductId,
        //                ProductName = productMaster?.Name,
        //                ProjectCode = policyMaster.ProjectCode,
        //                ProjectName = campaignMaster?.Name,
        //                BeginDate = this.commonService.ParseDatetimeToTimeSpan(policyMaster.PolicyPeriodS) ?? 0,
        //                EndDate = this.commonService.ParseDatetimeToTimeSpan(policyMaster.PolicyPeriodE) ?? 0,
        //                Period = policyMaster.BenefitTerm ?? 1,
        //                TagId = policyCarInfo?.LicensePlateNo,
        //                InsType = this.commonService.GetSysParamValue(GroupIdType.InsTypeMapping, productMaster.PlanType),
        //                InsStatus = policyMaster.PolicyStatus == PolicyStatusType.Active ? InsStatusType.Active : InsStatusType.Expired,
        //                IsExtend = string.Equals(policyMaster.ExtendStatus, PolicyExtendStatus.Renewable),
        //                IsProductExist = (systemDate >= productMaster.ProductStartOn && systemDate <= endDate),
        //                PlanCategory = policyMaster.PlanCategory,
        //                PlanCategoryName = this.commonService.GetSysParamValue(GroupIdType.PlanCategory, policyMaster.PlanCategory),
        //            };

        //            policyList.Add(item);
        //        }

        //        result.PolicyList = policyList.OrderByDescending(x => x.EndDate).ToList();

        //        #region MemberProfile狀態更新

        //        MemberProfile memberProfile = this.memberProfileRepo.Queryable().AsNoTracking().FirstOrDefault(m => m.CustId == custId);

        //        if (memberProfile != null)
        //        {
        //            string addYear = this.commonService.GetSysParamValue("BatchParam", "MemberPolicy");
        //            int year = 5;

        //            if (string.IsNullOrEmpty(addYear))
        //            {
        //                //this.eventLog.Log("INS0000001", "InquirePolicyList Update ExpirationDate Error: INS0000001, SysParam MemberPolicy is empty", italIfId, italIfId);
        //            }
        //            else if (!int.TryParse(addYear, out year))
        //            {
        //                //this.eventLog.Log("INS0000002", $"InquirePolicyList Update ExpirationDate Error: INS0000002, SysParam MemberPolicy: {addYear}, format is not Integer", italIfId, italIfId);
        //            }

        //            var date = DateTime.Now;
        //            memberProfile.ExpirationDate = date.AddYears(year);
        //            memberProfile.SystemUseDay = date;
        //            memberProfile.UpdateOn = date;

        //            var memberProfileLog = new MemberProfileLog();
        //            memberProfileLog.CustId = custId;
        //            memberProfileLog.LogDate = date;
        //            memberProfileLog.Msg = $"會員過期日: {memberProfile.ExpirationDate.Value.ToString("yyyy-MM-dd")}、保代系統使用日: 今日";
        //            memberProfileLog.ModifyEvent = "更新會員過期日、更新保代系統使用日";
        //            memberProfileLog.ModifyBy = ModifyBy.GetListAPI;

        //            this.memberProfileRepo.Update(memberProfile);
        //            this.memberProfileLogRepo.Insert(memberProfileLog);
        //            await this.insUnitOfWork.SaveChangesAsync();
        //        }
        //        #endregion

        //    }

        //    var messageHeader = this.commonService.GetChannelSuccessMessageHeader();

        //    return this.channelAPICommonService.GetResp<InquirePolicyListResp>(
        //        req.Header, messageHeader, StandardResCodeType.NormalProcessing, result);
        //}
    }
}