﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 車型查詢 Resp
    /// </summary>
    public class GetModelResp
    {
        /// <summary>
        /// 車型查詢結果
        /// </summary>
        [JsonProperty("carTypeList")]
        public List<CarTypeItem> CarTypeList { get; set; }
    }
}
