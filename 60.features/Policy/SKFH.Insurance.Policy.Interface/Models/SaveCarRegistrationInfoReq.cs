﻿using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 儲存車籍資料 Req
    /// </summary>
    public class SaveCarRegistrationInfoReq
    {
        /// <summary>
        /// 受理編號
        /// </summary>
        [JsonProperty("acceptNo")]
        public string AcceptNo { get; set; }

        /// <summary>
        /// 專案代碼
        /// </summary>
        [JsonProperty("productCode")]
        public string ProductCode { get; set; }

        /// <summary>
        /// 牌照號碼
        /// </summary>
        [JsonProperty("tagID")]
        public string TagID { get; set; }

        /// <summary>
        /// 車種代號
        /// </summary>
        [JsonProperty("vehicleType")]
        public string VehicleType { get; set; }

        /// <summary>
        /// 出廠年月(YYYYMM E.g. 198812)
        /// </summary>
        [JsonProperty("manufacturedDate")]
        public string ManufacturedDate { get; set; }

        /// <summary>
        /// 原始發照日(yyyMMdd E.g. 1080901)
        /// </summary>
        [JsonProperty("registrationIssueDate")]
        public string RegistrationIssueDate { get; set; }

        /// <summary>
        /// 排氣量
        /// </summary>
        [JsonProperty("displacement")]
        public string Displacement { get; set; }

        /// <summary>
        /// 乘載人數
        /// </summary>
        [JsonProperty("transportUnit")]
        public string TransportUnit { get; set; }

        /// <summary>
        /// 乘載噸數(當vehicleType=04 自用小貨車才會有值)
        /// </summary>
        [JsonProperty("tonnage")]
        public string Tonnage { get; set; }

        /// <summary>
        /// 廠牌代碼
        /// </summary>
        [JsonProperty("brandCode")]
        public string BrandCode { get; set; }

        /// <summary>
        /// 車型號代碼
        /// </summary>
        [JsonProperty("carType")]
        public string CarType { get; set; }

        /// <summary>
        /// 引擎號碼/車身號碼
        /// </summary>
        [JsonProperty("engineID")]
        public string EngineID { get; set; }
    }
}
