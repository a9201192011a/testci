﻿using System.Collections.Generic;
using TPI.NetCore.WebAPI.Attributes;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 保單資料查詢 Resp
    /// </summary>
    public class PolicyContentInfoResp
    {
        /// <summary>
        /// 車籍資料 區塊 -商品險別非汽車險、機車險回傳 null
        /// </summary>
        public VehicleInfo VehicleInfo { get; set; }

        /// <summary>
        /// 投保明細
        /// </summary>
        public PolicyDetailInfo PolicyDetailInfo { get; set; }
    }

    /// <summary>
    /// 車籍資料 區塊 -商品險別非汽車險、機車險回傳 null
    /// </summary>
    public class VehicleInfo
    {
        /// <summary>
        /// 車牌號碼
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string LicensePlateNo { get; set; }

        /// <summary>
        /// 廠牌車型(廠牌名稱、廠牌車型中文名稱、車型編號)
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string BrandStr { get; set; }

        /// <summary>
        /// 車輛種類
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string VehicleTypeStr { get; set; }

        /// <summary>
        /// 出廠年月(例：2018/05)
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string ManufacturedDate { get; set; }

        /// <summary>
        /// 原始發照日期
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string RegistIssueDate { get; set; }

        /// <summary>
        /// CC/HP(值)
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public decimal? Displacement { get; set; }

        /// <summary>
        /// CC/HP(單位)
        /// </summary>
        public string DisplacementUnit { get; set; }

        /// <summary>
        /// 引擎號碼
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string EngineId { get; set; }

        /// <summary>
        /// 車身號碼
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string CarBodyID { get; set; }
    }

    /// <summary>
    /// 投保明細 區塊
    /// </summary>
    public class PolicyDetailInfo
    {
        /// <summary>
        /// 專案名稱
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string ProductName { get; set; }

        /// <summary>
        /// 方案名稱
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string CampaignName { get; set; }

        /// <summary>
        /// 強制險保險期間 -無強制險回傳 null
        /// </summary>
        public PolicyDatePeriod CompulsoryPeriod { get; set; }

        /// <summary>
        /// 任意險保險期間 -無任意險回傳 null
        /// </summary>
        public PolicyDatePeriod ArbitraryPeriod { get; set; }

        /// <summary>
        /// 投保明細 列表
        /// </summary>
        public List<PolicyPlanData> PolicyDetails { get; set; }

        /// <summary>
        /// 總金額-原始保費
        /// </summary>
        public decimal? TotalModalPlanPremium { get; set; }

        /// <summary>
        /// 總金額-折扣
        /// </summary>
        public decimal? TotalDiscPlanPremium { get; set; }

        /// <summary>
        /// 總金額-實收保費
        /// </summary>
        public decimal? TotalModalItemPermiumWithDisc { get; set; }

        /// <summary>
        /// 是否為強制險
        /// </summary>
        public bool IsCompulsory { get; set; }
    }

    /// <summary>
    /// 保險期間
    /// </summary>
    public class PolicyDatePeriod
    {
        /// <summary>
        /// 強制險保險期間(起日)
        /// </summary>
        public string PolicyPeriodS { get; set; }

        /// <summary>
        /// 強制險保險期間(迄日)
        /// </summary>
        public string PolicyPeriodE { get; set; }
    }

    /// <summary>
    /// 投保產品資料
    /// </summary>
    public class PolicyPlanData
    {
        /// <summary>
        /// 商品
        /// </summary>
        public string PlanName { get; set; }

        /// <summary>
        /// 保障項目
        /// </summary>
        public string CoveredItem { get; set; }

        /// <summary>
        /// 保額
        /// </summary>
        public string ItemAmount { get; set; }

        /// <summary>
        /// 保額單位
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 自負額-無資料來源回傳 null
        /// </summary>
        public string SelfPayAmount { get; set; }

        /// <summary>
        /// 原始保費
        /// </summary>
        public decimal? ModalPlanPremium { get; set; }

        /// <summary>
        /// 折扣
        /// </summary>
        public decimal? DiscPlanPremium { get; set; }

        /// <summary>
        /// 實收保費
        /// </summary>
        public decimal? ModalItemPermiumWithDisc { get; set; }
    }

    /// <summary>
    /// 保障項目內容解析資料
    /// </summary>
    public class CoveredItemData
    {
        /// <summary>
        /// 保障項目
        /// </summary>
        public List<string> CoveredItems { get; set; }

        /// <summary>
        /// 保額
        /// </summary>
        public List<decimal?> ItemAmounts { get; set; }

        /// <summary>
        /// 保額單位
        /// </summary>
        public List<string> Units { get; set; }
    }
}