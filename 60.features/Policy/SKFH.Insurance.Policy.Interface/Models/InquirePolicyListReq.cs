﻿using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 查詢保單清單 Req(任意險)
    /// </summary>
    public class InquirePolicyListReq
    {

        /// <summary>
        /// 險種類型 01:汽車 02:機車 03:All
        /// </summary>
        [JsonProperty("insType")]
        public string InsType { get; set; }

        /// <summary>
        /// 保單狀態 01:active 02:expired 03:all
        /// </summary>
        [JsonProperty("insStatus")]
        public string InsStatus { get; set; }
    }
}
