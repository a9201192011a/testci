using TPI.NetCore.WebAPI.Attributes;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 保單資料共用 Req
    /// </summary>
    public class PolicyReq : MaskReq
    {
        /// <summary>
        /// 保單編號
        /// </summary>
        public string PolicyNo { get; set; }
    }
}