﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 查詢保單內容 Resp
    /// </summary>
    public class GetDetailResp
    {
        /// <summary>
        /// 本行客戶唯一識別碼
        /// </summary>
        [JsonProperty("custId")]
        public string CustId { get; set; }

        /// <summary>
        /// setting by CMS
        /// </summary>
        [JsonProperty("pdCd")]
        public string PdCd { get; set; }

        /// <summary>
        /// 保單號
        /// </summary>
        [JsonProperty("policyNo")]
        public string PolicyNo { get; set; }

        /// <summary>
        /// 強制險方案資料(專案物件)
        /// </summary>
        [JsonProperty("projectList")]
        public ProjectInfo ProjectList { get; set; }

        #region 車籍相關

        /// <summary>
        /// 牌照號碼
        /// </summary>
        [JsonProperty("tagID")]
        public string TagID { get; set; }

        /// <summary>
        /// 車輛種類代碼 ( 01 重型機車 / 02 輕型機車 / 03 自用小客車 / 04 自用小貨車 / 22 自用小客貨車 / 32 大型重型機車)
        /// </summary>
        [JsonProperty("vehicleType")]
        public string VehicleType { get; set; }

        /// <summary>
        /// 原始發照日 yyyMMdd E.g. 1080901
        /// </summary>
        [JsonProperty("issueDate")]
        public string IssueDate { get; set; }

        /// <summary>
        /// 出廠年月 YYYYMM E.g. 198812
        /// </summary>
        [JsonProperty("manufacturedDate")]
        public string ManufacturedDate { get; set; }

        /// <summary>
        /// 排氣量 E.g. 2000
        /// </summary>
        [JsonProperty("displacement")]
        public decimal? Displacement { get; set; }

        /// <summary>
        /// 乘載人數 E.g. 5
        /// </summary>
        [JsonProperty("transportUnit")]
        public int? TransportUnit { get; set; }

        /// <summary>
        /// 險種類型 01 : 汽車，02 : 機車，03：車險 E.g. 01
        /// </summary>
        [JsonProperty("insType")]
        public string InsType { get; set; }

        /// <summary>
        /// 保單狀態 01 : active ， 02 : expired
        /// </summary>
        [JsonProperty("insStatus")]
        public string InsStatus { get; set; }

        /// <summary>
        /// 引擎號碼
        /// </summary>
        [JsonProperty("engineId")]
        public string EngineId { get; set; }

        /// <summary>
        /// 廠牌物件
        /// </summary>
        [JsonProperty("vehicleBrand")]
        public VehicleBrandInfo VehicleBrand { get; set; }

        /// <summary>
        /// 車型物件
        /// </summary>
        [JsonProperty("carTypeList")]
        public List<CarTypeInfo> CarTypeList { get; set; }

        #endregion

        #region 要被保人及KYC相關

        /// <summary>
        /// 郵遞區號
        /// </summary>
        [JsonProperty("sendZipCode")]
        public string SendZipCode { get; set; }

        /// <summary>
        /// 縣市
        /// </summary>
        [JsonProperty("sendCity")]
        public string SendCity { get; set; }

        /// <summary>
        /// 區
        /// </summary>
        [JsonProperty("sendCountry")]
        public string SendCountry { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        [JsonProperty("sendAddr")]
        public string SendAddr { get; set; }

        /// <summary>
        /// 被保險人目前是否受有監護宣告?
        /// </summary>
        [JsonProperty("informItem01")]
        public string InformItem01 { get; set; }

        /// <summary>
        /// 是否境外居住超過半年( Y : 是，N : 否 )
        /// </summary>
        [JsonProperty("informItem02")]
        public string InformItem02 { get; set; }

        /// <summary>
        /// 是否為國際組織重要政治性職務人士
        /// 0：否 ，
        /// 1：是-國內 ，
        /// 2：是-國外 
        /// </summary>
        [JsonProperty("informItem03")]
        public string InformItem03 { get; set; }

        /// <summary>
        /// 是否為特殊職業( Y : 是，N : 否 )
        /// </summary>
        [JsonProperty("informItem04")]
        public string InformItem04 { get; set; }

        /// <summary>
        /// 保費來源問項
        /// 1:薪資收入或公司紅利 ，
        /// 2:投資收入 ，
        /// 3:儲蓄 ，
        /// 4:退休金 ，
        /// 5:財產繼承 ，
        /// 6:貸款或保險單借款 ，
        /// 7:解約或終止契約
        /// </summary>
        [JsonProperty("informItem05")]
        public string InformItem05 { get; set; }

        /// <summary>
        /// 投保前三個月內是否有辦理終止契約、貸款或保險單借款之情形( Y : 是，N : 否 )
        /// </summary>
        [JsonProperty("informItem06")]
        public string InformItem06 { get; set; }

        /// <summary>
        /// 要被保險人 ID
        /// </summary>
        [JsonProperty("insuredId")]
        public string InsuredId { get; set; }

        /// <summary>
        /// 要被保險人姓名
        /// </summary>
        [JsonProperty("insuredName")]
        public string InsuredName { get; set; }

        /// <summary>
        /// 要被保險人生日 YYYYMMDD
        /// </summary>
        [JsonProperty("insuredBirthday")]
        public string InsuredBirthday { get; set; }

        /// <summary>
        /// 要被保人性別 依要被保險人 ID 區分 M:男，F:女
        /// </summary>
        [JsonProperty("insuredSex")]
        public string InsuredSex { get; set; }

        /// <summary>
        /// 要被保險人國籍代碼
        /// </summary>
        [JsonProperty("insuredNationality")]
        public string InsuredNationality { get; set; }

        /// <summary>
        /// 要保人行動電話
        /// </summary>
        [JsonProperty("insuredMobile")]
        public string InsuredMobile { get; set; }

        /// <summary>
        /// 要保人電子郵件信箱
        /// </summary>
        [JsonProperty("insuredEmail")]
        public string InsuredEmail { get; set; }

        /// <summary>
        /// 保單寄送方式 01.紙本 02.電子 依保單主檔 內容回傳
        /// </summary>
        [JsonProperty("policyDelivery")]
        public string PolicyDelivery { get; set; }

        /// <summary>
        /// 繳費方式 01-信用卡，02-活期帳戶扣款，03.Debit-Card 依保單主檔 內容回傳
        /// </summary>
        [JsonProperty("payBy")]
        public string PayBy { get; set; }

        /// <summary>
        /// 要保人職業代碼 1 
        /// Job Classification First Stage Code Name(FromCIF.jobClFrstStgeCd)
        /// </summary>
        [JsonProperty("jobClFrstStgeCd")]
        public string JobClFrstStgeCd { get; set; }

        /// <summary>
        /// 要保人職業代碼 2 
        /// Job Classification Second Stage Code Name(FromCIF.jobClScndStgeCd)
        /// </summary>
        [JsonProperty("jobClScndStgeCd")]
        public string JobClScndStgeCd { get; set; }

        /// <summary>
        /// 要保人職業代碼 3
        /// Job Detail Type Code(FromCIF.jobDtlTpCd)
        /// </summary>
        [JsonProperty("jobDtlTpCd")]
        public string JobDtlTpCd { get; set; }

        /// <summary>
        /// 要保人原風險等級(From 會員主檔)
        /// </summary>
        [JsonProperty("oldRiskLevel")]
        public string OldRiskLevel { get; set; }

        /// <summary>
        /// 要保人新風險等級(From AML)
        /// </summary>
        [JsonProperty("newRiskLevel")]
        public string NewRiskLevel { get; set; }

        /// <summary>
        /// 要保人AML alertID(From AML)
        /// </summary>
        [JsonProperty("alertId")]
        public string AlertId { get; set; }

        /// <summary>
        /// 信用卡號
        /// (格式：1234567890123456，連續數字，沒有短橫線"-")
        /// </summary>
        [JsonProperty("cardNo")]
        public string CardNo { get; set; }

        /// <summary>
        /// 是否可續保 (Y/N)
        /// </summary>
        [JsonProperty("isExtendYn")]
        public string IsExtendYn { get; set; }

        /// <summary>
        /// 屆期狀態Eg. 0 或 null
        /// </summary>
        [JsonProperty("beforeDueStatus")]
        public string BeforeDueStatus { get; set; }

        #endregion
    }

    /// <summary>
    /// 廠牌物件
    /// </summary>
    public class VehicleBrandInfo
    {
        /// <summary>
        /// 汽車廠牌代號 E.g. 31
        /// </summary>
        [JsonProperty("brandId")]
        public string BrandId { get; set; }

        /// <summary>
        /// 汽車廠牌顯示名稱 E.g.Audi
        /// </summary>
        [JsonProperty("brandName")]
        public string BrandName { get; set; }
    }

    /// <summary>
    /// 車型物件
    /// </summary>
    public class CarTypeInfo
    {
        /// <summary>
        /// 車型顯示名稱 E.g. A6 35 TFSI S tronic front 1800cc 4D 5人座
        /// </summary>
        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        /// <summary>
        /// 車型ID E.g. 31037400
        /// </summary>
        [JsonProperty("carType")]
        public string CarType { get; set; }
    }
}
