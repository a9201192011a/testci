using TPI.NetCore.WebAPI.Attributes;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 保險未成功件查詢 Resp
    /// </summary>
    public class TempPolicySearchResp
    {
        /// <summary>
        /// 進件編號
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string AcceptNo { get; set; }

        /// <summary>
        /// 要保人姓名
        /// </summary>
        [Mask(MaskType.ChineseName | MaskType.DashIfEmpty)]
        public string ApplicantName { get; set; }

        /// <summary>
        /// 被保人姓名
        /// </summary>
        [Mask(MaskType.ChineseName | MaskType.DashIfEmpty)]
        public string InsuredName { get; set; }

        /// <summary>
        /// 案件狀態
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string AcceptStatusStr { get; set; }

        /// <summary>
        /// 投保專案/方案
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string ProductCampaignName { get; set; }

        /// <summary>
        /// 案件來源
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string CaseSourceStr { get; set; }

        /// <summary>
        /// 進件日期
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string AcceptDate { get; set; }

        /// <summary>
        /// 簽署/進件資訊
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string SignInfo { get; set; }
    }
}