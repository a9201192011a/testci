using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 取得保險鬧鐘Request
    /// </summary>
    public class GetPolicyAlarmReq
    {
        /// <summary>
        /// 車牌號碼
        /// </summary>
        [JsonProperty("tagID")]
        public string TagID { get; set; }

    }
}