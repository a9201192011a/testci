using TPI.NetCore.WebAPI.Attributes;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 保單資料查詢 Resp
    /// </summary>
    public class PolicySearchResp
    {
        /// <summary>
        /// 保單號碼
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string PolicyNo { get; set; }

        /// <summary>
        /// 商品類型名稱
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string PlanCategoryStr { get; set; }

        /// <summary>
        /// 要保人姓名
        /// </summary>
        [Mask(MaskType.ChineseName | MaskType.DashIfEmpty)]
        public string ApplicantName { get; set; }

        /// <summary>
        /// 被保人姓名
        /// </summary>
        [Mask(MaskType.ChineseName | MaskType.DashIfEmpty)]
        public string InsuredName { get; set; }

        /// <summary>
        /// 保單狀態
        /// </summary>
        [Mask( MaskType.DashIfEmpty)]
        public string PolicyStatusStr { get; set; }

        /// <summary>
        /// 投保專案/方案
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string ProductCampaignName { get; set; }

        /// <summary>
        /// 案件來源
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string CaseSourceStr { get; set; }

        /// <summary>
        /// 生效日期
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string PolicyPeriodDate { get; set; }

        /// <summary>
        /// 到期日期
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string ExpiredDate { get; set; }

        /// <summary>
        /// 投保專案
        /// </summary>
        //public string ProductName { get; set; }

        /// <summary>
        /// 方案
        /// </summary>
        //public string CampaignName { get; set; }
    }
}