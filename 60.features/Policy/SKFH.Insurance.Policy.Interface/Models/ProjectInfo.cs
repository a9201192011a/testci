﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 強制險方案資料(專案物件)
    /// </summary>
    public class ProjectInfo
    {
        /// <summary>
        /// 保險公司代碼
        /// </summary>
        [JsonProperty("insuranceId")]
        public string InsuranceId { get; set; }

        /// <summary>
        /// 保險公司名稱
        /// </summary>
        [JsonProperty("insuranceCompany")]
        public string InsuranceCompany { get; set; }

        /// <summary>
        /// 保險公司電話號碼
        /// </summary>
        [JsonProperty("insuranceTel")]
        public string InsuranceTel { get; set; }

        /// <summary>
        /// 專案代碼
        /// </summary>
        [JsonProperty("projectCode")]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 專案名稱
        /// </summary>
        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        /// <summary>
        /// 強制險保費(元)
        /// </summary>
        [JsonProperty("compulsoryPremium")]
        public decimal? CompulsoryPremium { get; set; }

        /// <summary>
        /// 強制險保險期間(起日) 格式：YYYYMMDD
        /// </summary>
        [JsonProperty("compulsorySDate")]
        public string CompulsorySDate { get; set; }

        /// <summary>
        /// 強制險保險期間(迄日) 格式：YYYYMMDD
        /// </summary>
        [JsonProperty("compulsoryEDate")]
        public string CompulsoryEDate { get; set; }

        /// <summary>
        /// 方案內容(商品物件)
        /// </summary>
        [JsonProperty("projectItems")]
        public List<ProjectItemInfo> ProjectItems { get; set; }
    }
}
