﻿using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 車型查詢結果內容
    /// </summary>
    public class CarTypeItem
    {
        /// <summary>
        /// 型號代碼
        /// </summary>
        [JsonProperty("categoryCode")]
        public string CategoryCode { get; set; }

        /// <summary>
        /// 型號名稱
        /// </summary>
        [JsonProperty("categoryValue")]
        public string CategoryValue { get; set; }
    }
}
