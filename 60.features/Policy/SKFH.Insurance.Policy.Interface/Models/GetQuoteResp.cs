﻿using System.Collections.Generic;
using TPI.NetCore.WebAPI.Attributes;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 保費試算 Response
    /// </summary>
    public class GetQuoteResp
    {
        /// <summary>
        /// 強制險保費(元)
        /// </summary>
        public string CompulsoryPremium { get; set; }
        /// <summary>
        /// 任意險保費(元)
        /// </summary>
        public string VoluntaryPremiums { get; set; }

        /// <summary>
        /// 總保費(元)
        /// </summary>
        public string TotalPremium { get; set; }
        /// <summary>
        /// 折扣前總保費
        /// </summary>
        public string BeforeDiscountTotalPremium { get; set; }
        /// <summary>
        /// 強制險保險期間(起日)
        /// </summary>
        public int? CompulsoryEffectiveDate { get; set; }
        /// <summary>
        /// 強制險保險期間(迄日)
        /// </summary>
        public int? CompulsoryExpirationDate { get; set; }
        /// <summary>
        /// 任意險保險期間(起日)
        /// </summary>
        public int? VoluntaryEffectiveDate { get; set; }
        /// <summary>
        /// 任意險保險期間(迄日)
        /// </summary>
        public int? VoluntaryExpirationDate { get; set; }
        /// <summary>
        /// 選取險種項目與保額
        /// </summary>
        public List<GetQuoteRespSelectedItem> SelectedItems { get; set; }
    }

    /// <summary>
    /// 選取險種項目與保額
    /// </summary>
    public class GetQuoteRespSelectedItem
    {
        /// <summary>
        /// itemKey
        /// </summary>
        public string ItemKey { get; set; }
        /// <summary>
        /// optionKey
        /// </summary>
        public string OptionKey { get; set; }
        /// <summary>
        /// 保額內容
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// 計算後的保費
        /// </summary>
        public int? Premium { get; set; }
    }
}
