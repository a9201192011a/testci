﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 保單查詢 Resp
    /// </summary>
    public class GetListResp
    {
        /// <summary>
        /// 保單清單
        /// </summary>
        [JsonProperty("carList")]
        public List<CarListInfo> CarList { get; set; }
    }

    /// <summary>
    /// 保單清單
    /// </summary>
    public class CarListInfo 
    {
        /// <summary>
        /// 保險公司名稱
        /// </summary>
        [JsonProperty("insuranceCompany")]
        public string InsuranceCompany { get; set; }

        /// <summary>
        /// 保單號
        /// </summary>
        [JsonProperty("policyNo")]
        public string PolicyNo { get; set; }

        /// <summary>
        /// 受理編號
        /// </summary>
        [JsonProperty("acceptNo")]
        public string AcceptNo { get; set; }

        /// <summary>
        /// 方案代碼
        /// </summary>
        [JsonProperty("projectCode")]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 方案名稱
        /// </summary>
        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        /// <summary>
        /// 保險期間(起日) 格式：YYYYMMDD
        /// </summary>
        [JsonProperty("sDate")]
        public string SDate { get; set; }

        /// <summary>
        /// 保險期間(迄日) 格式：YYYYMMDD
        /// </summary>
        [JsonProperty("eDate")]
        public string EDate { get; set; }

        /// <summary>
        /// 牌照號碼
        /// </summary>
        [JsonProperty("tagID")]
        public string TagID { get; set; }

        /// <summary>
        /// 保單狀態 01:active 02:expired
        /// </summary>
        [JsonProperty("insStatus")]
        public string InsStatus { get; set; }

        /// <summary>
        /// 投保類型(強制險或任意險) C:強制險(Compulsory) V:任意險(Voluntary)
        /// </summary>
        [JsonProperty("insuranceType")]
        public string InsuranceType { get; set; }

        /// <summary>
        /// 是否可續保(Y/N)
        /// </summary>
        [JsonProperty("isExtendYn")]
        public string IsExtendYn { get; set; }

        /// <summary>
        /// 險種類型 01 : 汽車 02 : 機車 03：車險
        /// </summary>
        [JsonProperty("insType")]
        public string InsType { get; set; }
    }
}
