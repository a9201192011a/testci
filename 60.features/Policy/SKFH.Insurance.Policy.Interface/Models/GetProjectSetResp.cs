﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 方案組合 Resp
    /// </summary>
    public class GetProjectSetResp
    {
        /// <summary>
        /// 保險公司名稱
        /// </summary>
        [JsonProperty("companyName")]
        public string CompanyName { get; set; }

        /// <summary>
        /// 專案代碼
        /// </summary>
        [JsonProperty("productCode")]
        public string ProductCode { get; set; }

        /// <summary>
        /// 專案名稱
        /// </summary>
        [JsonProperty("productName")]
        public string ProductName { get; set; }

        /// <summary>
        /// 方案組合清單
        /// </summary>
        [JsonProperty("projects")]
        public List<ProjectAvailableItem> Projects { get; set; }
    }

    /// <summary>
    /// 可投保方案組合項目
    /// </summary>
    public class ProjectAvailableItem
    {
        /// <summary>
        /// 參考價格
        /// </summary>
        [JsonProperty("startingPrice")]
        public int StartingPrice { get; set; }

        /// <summary>
        /// 原始參考價格
        /// </summary>
        [JsonProperty("originalPrice")]
        public int OriginalPrice { get; set; }

        /// <summary>
        /// 方案代碼
        /// </summary>
        [JsonProperty("projectCode")]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 方案名稱
        /// </summary>
        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        /// <summary>
        /// 方案標記
        /// </summary>
        [JsonProperty("tag")]
        public string Tag { get; set; }

        /// <summary>
        /// 保障年期
        /// </summary>
        [JsonProperty("period")]
        public int Period { get; set; }

        /// <summary>
        /// 保障範圍陣列
        /// </summary>
        [JsonProperty("coverages")]
        public List<ProjectCoverage> Coverages { get; set; }
    }

    /// <summary>
    /// 方案保障範圍
    /// </summary>
    public class ProjectCoverage
    {
        /// <summary>
        /// 保障範圍
        /// </summary>
        [JsonProperty("coverage")]
        public string Coverage { get; set; }
    }
}
