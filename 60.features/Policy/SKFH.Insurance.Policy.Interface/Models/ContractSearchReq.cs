﻿using TPI.NetCore.WebAPI.Models;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 新契約資料查詢 Req
    /// </summary>
    public class ContractSearchReq : DataTableQueryReq
    {
        /// <summary>
        /// 進件編號
        /// </summary>
        public string AcceptNo { get; set; }

        /// <summary>
        /// 案件來源(參數代碼)
        /// </summary>
        public string CaseSource { get; set; }

        /// <summary>
        /// 案件狀態(參數代碼)
        /// </summary>
        public string AcceptStatus { get; set; }

        /// <summary>
        /// 要保人 ID
        /// </summary>
        public string ApplicantId { get; set; }

        /// <summary>
        /// 要保人姓名
        /// </summary>
        public string ApplicantName { get; set; }

        /// <summary>
        /// 被保人 ID
        /// </summary>
        public string InsuredId { get; set; }

        /// <summary>
        /// 被保人姓名
        /// </summary>
        public string InsuredName { get; set; }

        /// <summary>
        /// 進件日期-起日
        /// </summary>
        public string AcceptStartDate { get; set; }

        /// <summary>
        /// 進件日期-迄日
        /// </summary>
        public string AcceptEndDate { get; set; }

        /// <summary>
        /// 生效日期-起日(查詢【保單主檔】保險期間-起日)
        /// </summary>
        public string PolicyPeriodStartDate { get; set; }

        /// <summary>
        /// 生效日期-迄日(查詢【保單主檔】保險期間-起日)
        /// </summary>
        public string PolicyPeriodEndDate { get; set; }

        /// <summary>
        /// 核保進度
        /// </summary>
        public string PendingStatus { get; set; }

        /// <summary>
        /// 商品類型
        /// </summary>
        public string PlanCategory { get; set; }
    }
}
