﻿using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 付款方式
    /// </summary>
    public class PaymentInfo
    {
        /// <summary>
        /// 信用卡號/帳號
        /// 格式:數字
        /// e.g. 1111222233334444
        /// </summary>
        [JsonProperty("paymentNo")]
        public string PaymentNo { get; set; }

        /// <summary>
        /// 信用卡有效月年
        /// 格式: mmyy
        /// e.g.0727
        /// </summary>
        [JsonProperty("cardExpiredDate")]
        public string CardExpiredDate { get; set; }

        /// <summary>
        /// 卡背末三碼(CVV2)
        /// </summary>
        [JsonProperty("cardCheckCode")]
        public string CardCheckCode { get; set; }

        /// <summary>
        /// 銀行代碼
        /// </summary>
        [JsonProperty("bankCode")]
        public string BankCode { get; set; }
    }
}
