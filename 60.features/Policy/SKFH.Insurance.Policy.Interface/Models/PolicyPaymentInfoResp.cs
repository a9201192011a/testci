using Newtonsoft.Json;
using TPI.NetCore.WebAPI.Attributes;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 繳費資訊 Resp
    /// </summary>
    public class PolicyPaymentInfoResp
    {
        /// <summary>
        /// 保單資訊
        /// </summary>
        public PolicyInfo PolicyInfo { get; set; }

        /// <summary>
        /// 付款資訊
        /// </summary>
        public PayMentInfo PayMentInfo { get; set; }
    }

    /// <summary>
    /// 保單資訊 區塊
    /// </summary>
    public class PolicyInfo
    {
        /// <summary>
        /// 案件來源
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string CaseSourceStr { get; set; }
        /// <summary>
        /// 進件編號
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string AcceptNo { get; set; }
        /// <summary>
        /// 進件日期
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string AcceptDate { get; set; }
        /// <summary>
        /// 案件狀態
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string AcceptStatusStr { get; set; }
        /// <summary>
        /// 保單狀態
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string PolicyStatusStr { get; set; }
        /// <summary>
        /// 付款狀態
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string PaymentStatusStr { get; set; }
        /// <summary>
        /// 保險期間
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string InsurePeriod { get; set; }
        /// <summary>
        /// 生效日期
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string EffectiveDate { get; set; }
        /// <summary>
        /// 保單寄送方式
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string PolicySendTypeStr { get; set; }
        /// <summary>
        /// 密戶件
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string PolicyConfStr { get; set; }
        /// <summary>
        /// 到期日期
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string ExpiredDate { get; set; }
        /// <summary>
        /// 繳別
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string PremiumMethodStr { get; set; }
        /// <summary>
        /// 繳費金額
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public decimal? ModalPermiumWithDisc { get; set; }
        /// <summary>
        /// KYC保費來源
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string PremiumFromStr { get; set; }
        /// <summary>
        /// 強制證號
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string CompulsoryNo { get; set; }
    }

    /// <summary>
    /// 付款資訊 區塊
    /// </summary>
    public class PayMentInfo
    {
        /// <summary>
        /// 付款方式
        /// </summary>
        [JsonIgnore]
        public string PaidType { get; set; }
        /// <summary>
        /// 付款方式
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string PaidTypeStr { get; set; }
        /// <summary>
        /// 付款人姓名
        /// </summary>
        [Mask(MaskType.ChineseName | MaskType.DashIfEmpty)] 
        public string PayerName { get; set; }
        /// <summary>
        /// 付款人ID
        /// </summary>
        [JsonProperty("idNo")]
        [Mask(MaskType.NationalIdentification | MaskType.DashIfEmpty)] 
        public string IdNo { get; set; }
        /// <summary>
        /// 付款人與要保人關係
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string PersonRelationOfApplicantStr { get; set; }
        /// <summary>
        /// 簽帳卡卡號
        /// </summary>
        [Mask(MaskType.DashIfEmpty)] 
        public string AccountNo { get; set; }

        /// <summary>
        /// 檢查碼
        /// </summary>
        [Mask(MaskType.CardSecurityCode | MaskType.DashIfEmpty)]
        public string CardCheckCode { get; set; }
    }
}