﻿using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 保障項目物件
    /// </summary>
    public class GuaranteeItemInfo
    {
        // Content 及 Value 是對應到泰安規格中 projectlist.projectitems.content
        // 其內容需要正規化拆解 （參考版本為泰安V7 20210426） 
        // E.g. 傷害醫療保障_20萬/身故失能保障_200萬
        // SA -> 2021/06/10: 等 LINE Bank 定版分隔符號及規則後再補充。

        /// <summary>
        /// 該方案保險內容 E.g. 傷害醫療保障
        /// </summary>
        [JsonProperty("content")]
        public string Content { get; set; }

        /// <summary>
        /// 保額 E.g. 20萬
        /// </summary>
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
