﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 儲存保單結果 Response
    /// </summary>
    public class SavePolicyDataResp
    {
        /// <summary>
        /// 會員狀態代碼 (未註冊：N-未註冊(查無資料回傳N)  已註冊：0-停用 1-啟用(完成註冊為啟用))
        /// </summary>
        public string MemberStatus { get; set; }

        /// <summary>
        /// 是否需要註冊會員
        /// </summary>
        public bool IsRegister { get; set; }
    }
}
