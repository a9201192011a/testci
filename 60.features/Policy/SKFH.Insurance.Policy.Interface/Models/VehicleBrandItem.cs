﻿using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 廠牌查詢結果內容
    /// </summary>
    public class VehicleBrandItem
    {
        /// <summary>
        /// 廠牌代碼
        /// </summary>
        [JsonProperty("brandCode")]
        public string BrandCode { get; set; }

        /// <summary>
        /// 廠牌名稱
        /// </summary>
        [JsonProperty("brandValue")]
        public string BrandValue { get; set; }
    }
}
