﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 查詢保單內容 Req
    /// </summary>
    [DataContract]
    public class GetPolicyDetailReq
    {
        /// <summary>
        /// 受理編號
        /// </summary>
        [DataMember]
        [JsonProperty("acceptNo")]
        public string AcceptNo { get; set; }

        /// <summary>
        /// 保單號碼
        /// </summary>
        [DataMember]
        [JsonProperty("policyNo")]
        public string PolicyNo { get; set; }

        /// <summary>
        /// 專案代碼
        /// </summary>
        [DataMember]
        [JsonProperty("productCode")]
        public string ProductCode { get; set; }
    }
}
