﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 確認方案組合 Response
    /// </summary>
    public class CheckInquireProjectSetResp
    {
        /// <summary>
        /// 保險公司名稱
        /// </summary>
        public string InsuranceCompanyName { get; set; }
        /// <summary>
        /// 方案代碼
        /// </summary>
        public string ProjectCode { get; set; }
        /// <summary>
        /// 方案名稱
        /// </summary>
        public string ProjectName { get; set; }
        /// <summary>
        /// 保險公司專案代碼
        /// </summary>
        public string CompanyProjectCode { get; set; }
        /// <summary>
        /// 目前選擇保障年期
        /// </summary>
        public int CurrentSelectYear { get; set; }
        /// <summary>
        /// 商品資訊清單
        /// </summary>
        public List<PlanInfoList> Plans { get; set; }
    }

    /// <summary>
    /// 商品資訊清單
    /// </summary>
    public class PlanInfoList
    {
        /// <summary>
        /// 保險項目id
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// 保險項目名稱
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// 是否為必選
        /// </summary>
        public bool IsRequired { get; set; }
        /// <summary>
        /// 預設顯示保額項目
        /// </summary>
        public string DefaultOption { get; set; }
        /// <summary>
        /// 保障範圍
        /// </summary>
        public string CoverTitle { get; set; }
        /// <summary>
        /// 商品類型: CALI：強制險, VALI：任意險
        /// </summary>
        public string PlanCategory { get; set; }
        /// <summary>
        /// 商品類型名稱(強制險/任意險)
        /// </summary>
        public string PlanCategoryName { get; set; }
        /// <summary>
        /// 主附約別: 0: 主約, 1: 附約, 2: 附加條款(目前無案例), 3: 依附主約
        /// </summary>
        public int? ContractType { get; set; }
        /// <summary>
        /// 主約下的附約是否需一併購買
        /// </summary>
        public bool? AccessoryContractSelectAll { get; set; }
        /// <summary>
        /// 保額選項
        /// </summary>
        public List<PremieumOptions> Options { get; set; }
        /// <summary>
        /// 附約清單
        /// </summary>
        public List<SubPlanList> SubPlans { get; set; }
    }

    /// <summary>
    /// 保額選項
    /// </summary>
    public class PremieumOptions
    {
        /// <summary>
        /// 保額id
        /// </summary>
        public string OptionKey { get; set; }
        /// <summary>
        /// 保額
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// 參考價
        /// </summary>
        public decimal? ReferencePrice { get; set; }
    }

    /// <summary>
    /// 附約清單
    /// </summary>
    public class SubPlanList
    {
        /// <summary>
        /// 保險項目id
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// 保險項目名稱
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// 是否為必選
        /// </summary>
        public bool IsRequired { get; set; }
        /// <summary>
        /// 預設顯示保額項目
        /// </summary>
        public string DefaultOption { get; set; }
        /// <summary>
        /// 保障範圍
        /// </summary>
        public string CoverTitle { get; set; }
        /// <summary>
        /// 商品類型: CALI：強制險, VALI：任意險
        /// </summary>
        public string PlanCategory { get; set; }
        /// <summary>
        /// 商品類型名稱(強制險/任意險)
        /// </summary>
        public string PlanCategoryName { get; set; }
        /// <summary>
        /// 主附約別:0: 主約, 1: 附約, 2: 附加條款(目前無案例), 3: 依附主約 (如為3: 依附主約前端可跟主約Bundle在一起選擇ex:3132)
        /// </summary>
        public int? ContractType { get; set; }
        /// <summary>
        /// 保額選項
        /// </summary>
        public List<SubPremieumOptions> Options { get; set; }
    }

    /// <summary>
    /// 保額選項
    /// </summary>
    public class SubPremieumOptions
    {
        /// <summary>
        /// 保額id
        /// </summary>
        public string OptionKey { get; set; }
        /// <summary>
        /// 保額
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// 參考價
        /// </summary>
        public decimal? ReferencePrice { get; set; }
        /// <summary>
        /// 倍率
        /// </summary>
        public decimal? Multipler { get; set; }
    }
}
