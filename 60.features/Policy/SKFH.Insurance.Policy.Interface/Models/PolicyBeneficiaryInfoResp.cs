﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TPI.NetCore.WebAPI.Attributes;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 受益人資訊 Resp
    /// </summary>
    public class PolicyBeneficiaryInfoResp
    {
        /// <summary>
        /// 身故受益人分配方式
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string BeneficiaryPayTypeStr { get; set; }

        /// <summary>
        /// 受益人資訊
        /// </summary>
        public List<Beneficiary> Beneficiary { get; set; }
    }

    /// <summary>
    /// 受益人資訊
    /// </summary>
    public class Beneficiary
    {
        /// <summary>
        /// 給付順位
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string BeneficiarySeq { get; set; }

        /// <summary>
        /// 分配比例
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string BeneficiaryPercent { get; set; }

        /// <summary>
        /// 受益人姓名
        /// </summary>
        [Mask(MaskType.ChineseName | MaskType.DashIfEmpty)]
        public string Name { get; set; }

        /// <summary>
        /// 受益人身分證字號
        /// </summary>
        [JsonProperty("idNo")]
        [Mask(MaskType.NationalIdentification | MaskType.DashIfEmpty)]
        public string IdNo { get; set; }

        /// <summary>
        /// 人員與被保人關係
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string PersonRelationOfApplicantStr { get; set; }

        /// <summary>
        /// 受益人手機
        /// </summary>
        [Mask(MaskType.PhoneNo | MaskType.DashIfEmpty)]
        public string Mobile { get; set; }

        /// <summary>
        /// 受益人通訊地址
        /// </summary>
        [Mask(MaskType.BeneficiaryAddress | MaskType.DashIfEmpty)]
        public string AddrStr { get; set; }
    }
}