﻿using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 回覆核保結果 Req
    /// </summary>
    public class ReplyOrderStatusReq
    {
        /// <summary>
        /// 保險公司訂單編號
        /// 保險公司受理案件的訂單編號
        /// </summary>
        [JsonProperty("orderNo")]
        public string OrderNo { get; set; }

        /// <summary>
        /// 通路的進件編
        /// =LINE Bank 受理編號(進件編號)
        /// </summary>
        [JsonProperty("acceptNo")]
        public string AcceptNo { get; set; }

        /// <summary>
        /// 保單號碼
        /// 保險公司核保保單號碼
        /// </summary>
        [JsonProperty("policyNo")]
        public string PolicyNo { get; set; }

        /// <summary>
        /// 要保人ID
        /// 身分證號
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// 訂單狀態
        /// 0: 成功
        /// 1: 核保失敗
        /// 2. 付款失敗
        /// </summary>
        [JsonProperty("orderStatus")]
        public string OrderStatus { get; set; }

        /// <summary>
        /// 訊息備註
        /// 若 OrderStatus 不等於0，則會寫入失敗原因
        /// </summary>
        [JsonProperty("message ")]
        public string Message { get; set; }
    }
}
