﻿using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 保費試算 Req
    /// </summary>
    public class PremiumTrialReq
    {
        /// <summary>
        /// 客戶唯一識別碼
        /// </summary>
        [JsonProperty("custId")]
        public string CustId { get; set; }

        /// <summary>
        /// 此代碼的編碼規則隱含保險公司＋險種
        /// </summary>
        [JsonProperty("pdCd")]
        public string PdCd { get; set; }

        /// <summary>
        /// 車種代號/車輛種類 (01重型機車/02輕型機車/03自用小客車/04自用小貨車/22自用小客貨車/32 大型重型機車)
        /// </summary>
        [JsonProperty("vehicleTypeCd")]
        public string VehicleTypeCd { get; set; }

        /// <summary>
        /// 牌照號碼-前車牌 E.g. ATE-8797 的 ATE
        /// </summary>
        [JsonProperty("tagIdFirst")]
        public string TagIdFirst { get; set; }

        /// <summary>
        /// 牌照號碼-後車牌 E.g. ATE-8797 的 8797
        /// </summary>
        [JsonProperty("tagIdLast")]
        public string TagIdLast { get; set; }
    }
}
