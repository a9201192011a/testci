﻿using TPI.NetCore.WebAPI.Attributes;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 保單資料共用 Req
    /// </summary>
    public class ContractReq : MaskReq
    {

        /// <summary>
        /// 進件編號
        /// </summary>
        public string AcceptNo { get; set; }

        /// <summary>
        /// 商品類型
        /// </summary>
        public string PlanCategory { get; set; }
    }
}
