﻿using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 車型查詢 Req
    /// </summary>
    public class GetModelReq
    {
        /// <summary>
        /// 廠牌代碼
        /// </summary>
        [JsonProperty("brandCode")]
        public string BrandCode { get; set; }

        /// <summary>
        /// 型號名稱
        /// </summary>
        [JsonProperty("categoryValue")]
        public string CategoryValue { get; set; }

        /// <summary>
        /// 車種代號/車輛種類 (01重型機車/02輕型機車/03自用小客車/04自用小貨車/22自用小客貨車/32 大型重型機車)
        /// </summary>
        [JsonProperty("vehicleTypeCd")]
        public string VehicleTypeCd { get; set; }
    }
}
