﻿using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 查詢保單清單 Req
    /// </summary>
    public class GetListReq
    {
        /// <summary>
        /// 客戶唯一識別碼
        /// </summary>
        [JsonProperty("custId")]
        public string CustId { get; set; }

        /// <summary>
        /// 險種類型 01 : 汽車 02 : 機車 03：車險
        /// </summary>
        [JsonProperty("insType")]
        public string InsType { get; set; }

        /// <summary>
        /// 保單狀態 01:active 02:expired
        /// </summary>
        [JsonProperty("insStatus")]
        public string InsStatus { get; set; }
    }
}
