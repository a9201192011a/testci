﻿using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// CreateOrder Req
    /// </summary>
    public class CreateOrderReq
    {
        /// <summary>
        /// 本行客戶唯一識別碼
        /// </summary>
        [JsonProperty("custId")]
        public string CustId { get; set; }

        /// <summary>
        /// 受理編號(INS產生)
        /// 格式：{#保險公司代碼(產險 1 碼 + 公會 2 碼)} + YYYYMMDD + 6 碼流水碼(每日重置為 1 )
        /// </summary>
        [JsonProperty("acceptNo")]
        public string AcceptNo { get; set; }

        /// <summary>
        /// Channel OTP流水號
        /// </summary>
        [JsonProperty("otpId")]
        public string OtpId { get; set; }

        /// <summary>
        /// 階段
        /// 01:KYC 
        /// 02:CreateOrder(保險公司)
        /// 03:repay
        /// 04:CAR_INFO
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }

        #region 方案及車籍相關

        /// <summary>
        /// 專案代碼
        /// </summary>
        [JsonProperty("projectCode")]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 牌照號碼
        /// </summary>
        [JsonProperty("tagID")]
        public string TagID { get; set; }

        /// <summary>
        /// 車輛種類代碼
        /// 01重型機車/
        /// 02輕型機車/
        /// 03自用小客車/
        /// 04自用小貨車/
        /// 22自用小客貨車/
        /// 32 大型重型機車
        /// </summary>
        [JsonProperty("vehicleType")]
        public string VehicleType { get; set; }

        /// <summary>
        /// 出廠年月 ex：202105
        /// </summary>
        [JsonProperty("manufacturedDate")]
        public string ManufacturedDate { get; set; }

        /// <summary>
        /// 原始發照日 yyyMMdd E.g. 1080901
        /// </summary>
        [JsonProperty("issueDate")]
        public string IssueDate { get; set; }

        /// <summary>
        /// 排氣量
        /// </summary>
        [JsonProperty("displacement")]
        public decimal? Displacement { get; set; }

        /// <summary>
        /// 引擎號碼
        /// </summary>
        [JsonProperty("engineId")]
        public string EngineId { get; set; }

        /// <summary>
        /// 乘載人數
        /// E.g. 5
        /// </summary>
        [JsonProperty("transportUnit")]
        public string TransportUnit { get; set; }

        /// <summary>
        /// 汽車廠牌代號 
        /// E.g. 31
        /// </summary>
        [JsonProperty("brandCode")]
        public string BrandCode { get; set; }

        /// <summary>
        /// 車型代號
        /// </summary>
        [JsonProperty("categoryCode")]
        public string CategoryCode { get; set; }

        #endregion

        #region 要被保人及KYC相關

        /// <summary>
        /// 地址 - 郵遞區號
        /// </summary>
        [JsonProperty("sendZipCode")]
        public string SendZipCode { get; set; }

        /// <summary>
        /// 地址 - 縣市
        /// </summary>
        [JsonProperty("sendCity")]
        public string SendCity { get; set; }

        /// <summary>
        /// 地址 - 區
        /// </summary>
        [JsonProperty("sendCountry")]
        public string SendCountry { get; set; }

        /// <summary>
        /// 地址 - 地址
        /// </summary>
        [JsonProperty("sendAddr")]
        public string SendAddr { get; set; }

        /// <summary>
        /// KYC物件
        /// </summary>
        [JsonProperty("insKyc")]
        public KYCItem InsKyc { get; set; }

        #endregion

        #region 付款相關

        /// <summary>
        /// 繳費方式 01-信用卡/ Debit-Card 02-活期帳戶扣款
        /// </summary>
        [JsonProperty("payBy")]
        public string PayBy { get; set; }

        /// <summary>
        /// 付款方式物件
        /// </summary>
        [JsonProperty("paymentObject")]
        public PaymentInfo PaymentObject { get; set; }

        #endregion
    }
}
