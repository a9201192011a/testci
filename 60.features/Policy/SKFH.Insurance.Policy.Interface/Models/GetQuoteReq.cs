﻿using System.Collections.Generic;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    public class GetQuoteReq
    {
        /// <summary>
        /// 進件編號
        /// </summary>
        public string AcceptNo { get; set; }
        /// <summary>
        /// 保險公司專案代碼
        /// </summary>
        public string CompanyProjectCode { get; set; }

        /// <summary>
        /// 方案代碼
        /// </summary>
        public string ProjectCode { get; set; }
        /// <summary>
        /// 選取險種項目與保額
        /// </summary>
        public List<GetQuoteReqSelectItem> SelectedItems { get; set; }

    }

    public class GetQuoteReqSelectItem
    {
        /// <summary>
        /// itemKey
        /// </summary>
        public string ItemKey { get; set; }
        /// <summary>
        /// optionKey
        /// </summary>
        public string OptionKey { get; set; }
    }
}
