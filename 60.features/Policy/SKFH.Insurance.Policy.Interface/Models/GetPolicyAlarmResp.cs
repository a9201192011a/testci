using System.Collections.Generic;
using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 取得保險鬧鐘Response
    /// </summary>
    public class GetPolicyAlarmResp
    {
         /// <summary>
        /// 保險鬧鐘設定清單
        /// </summary>
        [JsonProperty("alarmClockList")]
        public List<AlarmClockItem> AlarmClockList { get; set; }
    }
}