﻿namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 未簽署的保單資料
    /// </summary>
    public class UnsignPolicyData
    {
        /// <summary>
        /// 未簽署的要被保人資訊
        /// </summary>
        public UnsignedPolicyPersonInfo PolicyApplicantInfo { get; set; }

        /// <summary>
        /// 未簽署的保單/繳費資訊
        /// </summary>
        public UnsignedPolicyPaymentInfo PolicyPaymentInfo { get; set; }

        /// <summary>
        /// 未簽署的受益人資料
        /// </summary>
        public UnsignedPolicyBeneficiaryInfo PolicyBeneficiaryInfo { get; set; }

        /// <summary>
        /// 未簽署的投保內容資料
        /// </summary>
        public UnsignedPolicyContentInfo PolicyContentInfo { get; set; }
    }
}
