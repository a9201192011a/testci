﻿using System.Collections.Generic;
using TPI.NetCore.WebAPI.Attributes;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 未簽署的投保內容
    /// </summary>
    public class UnsignedPolicyContentInfo
    {
        /// <summary>
        /// 受理編號
        /// </summary>
        public string AcceptNo { get; set; }

        /// <summary>
        /// ⾞籍資料
        /// </summary>
        public UnsignedVehicleInfo VehicleInfo { get; set; }

        /// <summary>
        /// 投保明細
        /// </summary>
        public UnsignPolicyDetailInfo PolicyDetailInfo { get; set; }
    }

    /// <summary>
    /// 未簽署 車籍資料
    /// </summary>
    public class UnsignedVehicleInfo
    {
        /// <summary>
        /// 車牌號碼
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string LicensePlateNo { get; set; }

        /// <summary>
        /// 廠牌車型(廠牌名稱、廠牌車型中文名稱、車型編號)
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string BrandStr { get; set; }

        /// <summary>
        /// 車輛種類
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string VehicleTypeStr { get; set; }

        /// <summary>
        /// 出廠年月(例：2018/05)
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string ManufacturedDate { get; set; }

        /// <summary>
        /// 原始發照日期
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string RegistIssueDate { get; set; }

        /// <summary>
        /// CC/HP(值)
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public decimal? Displacement { get; set; }

        /// <summary>
        /// CC/HP(單位)
        /// </summary>
        public string DisplacementUnit { get; set; }

        /// <summary>
        /// 引擎號碼
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string EngineId { get; set; }

        /// <summary>
        /// 車身號碼
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string CarBodyID { get; set; }
    }

    /// <summary>
    /// 未簽署 投保明細
    /// </summary>
    public class UnsignPolicyDetailInfo
    {
        /// <summary>
        /// 專案名稱
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string ProductName { get; set; }

        /// <summary>
        /// 方案名稱
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string CampaignName { get; set; }

        /// <summary>
        /// 投保資料-by商品類型
        /// </summary>
        public List<UnsignedPlanCategoryDetailData> PolicyDetails { get; set; }
    }

    /// <summary>
    /// 投保商品類型明細
    /// </summary>
    public class UnsignedPlanCategoryDetailData
    {
        /// <summary>
        /// 保險期間(起日)
        /// </summary>
        public string PolicyPeriodS { get; set; }

        /// <summary>
        /// 保險期間(迄日)
        /// </summary>
        public string PolicyPeriodE { get; set; }

        /// <summary>
        /// 投保產品列表
        /// </summary>
        public List<PolicyPlanData> PolicyPlanDetails { get; set; }
    }
}
