﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 保費試算 Resp
    /// </summary>
    public class PremiumTrialResp
    {
        /// <summary>
        /// 強制險方案資料(專案物件)
        /// </summary>
        [JsonProperty("projectList")]
        public List<ProjectInfo> ProjectList { get; set; }
    }
}
