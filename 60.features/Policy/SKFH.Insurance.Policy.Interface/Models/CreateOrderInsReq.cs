﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// Create Order Request
    /// </summary>
    public class CreateOrderInsReq
    {
        /// <summary>
        /// 受理編號
        /// </summary>
        public string AcceptNo { get; set; }
        /// <summary>
        /// Channel OTP流水號
        /// </summary>
        public string OtpId { get; set; }
        /// <summary>
        /// 繳費方式 01-信用卡/ Debit-Card  02-活期帳戶扣款
        /// </summary>
        public string PayBy { get; set; }
        /// <summary>
        /// 付款資訊
        /// </summary>
        public PaymentObject PaymentObject { get; set; }
    }

    /// <summary>
    /// 付款資訊
    /// </summary>
    public class PaymentObject
    {
        /// <summary>
        /// 信用卡號/帳號 E.g. 1111222233334444
        /// </summary>
        public string PaymentNo { get; set; }
        /// <summary>
        /// 信用卡有效期限 MMYY
        /// </summary>
        public string CardExpiredDate { get; set; }
        /// <summary>
        /// 卡背末三碼(CVV2)
        /// </summary>
        public string CardCheckCode { get; set; }
        /// <summary>
        /// 銀行代碼
        /// </summary>
        public string BankCode { get; set; }
    }
}
