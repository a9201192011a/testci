﻿using System;
using TPI.NetCore.WebAPI.Attributes;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 核保進度訊息 Resp
    /// </summary>
    public class ContractScheduleInfoResp
    {
        /// <summary>
        /// 核保進度
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string PendingStatusStr { get; set; }

        /// <summary>
        /// 申請日期
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public String ApplyDate { get; set; }

        /// <summary>
        /// 進件日期
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public String AcceptDate { get; set; }

        /// <summary>
        /// 保代簽署日期
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public String PolicySignDate { get; set; }

        /// <summary>
        /// 保代送件日期
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public String SendInsComDate { get; set; }

        /// <summary>
        /// 保險公司收件日期
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public String InsComReceiveDate { get; set; }

        /// <summary>
        /// 保險公司核保日期
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public String InsComUWDate { get; set; }

        /// <summary>
        /// 保險公司核保通過日期
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public String InsComUWPassDate { get; set; }

        /// <summary>
        /// 保單發單日期
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public String TransactionDateStr { get; set; }

        /// <summary>
        /// 保單寄送日期
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public String PolicySendDateStr { get; set; }
    }
}