﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 方案內容(商品物件)
    /// </summary>
    public class ProjectItemInfo
    {
        /// <summary>
        /// 商品代碼
        /// </summary>
        [JsonProperty("planCode")]
        public string PlanCode { get; set; }

        /// <summary>
        /// 該方案保險項目 E.g. 21強制責任保險
        /// </summary>
        [JsonProperty("item")]
        public string Item { get; set; }

        /// <summary>
        /// 扣除折扣之實際保費(資料來源為保險公司提供方案 API 之 projectlost.compulsorypremium)
        /// E.g. 809
        /// </summary>
        [JsonProperty("contentPremium")]
        public decimal? ContentPremium { get; set; }

        /// <summary>
        /// 保障項目物件
        /// </summary>
        [JsonProperty("guaranteeItems")]
        public List<GuaranteeItemInfo> GuaranteeItems { get; set; }
    }
}
