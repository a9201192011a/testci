﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 查詢保單內容 Resp
    /// </summary>
    public class GetPolicyDetailResp
    {

        /// <summary>
        /// 專案代碼
        /// </summary>
        [JsonProperty("productCode")]
        public string ProductCode { get; set; }

        /// <summary>
        /// 專案名稱
        /// </summary>
        [JsonProperty("productName")]
        public string ProductName { get; set; }

        /// <summary>
        /// 方案代碼
        /// </summary>
        [JsonProperty("projectCode")]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 方案名稱
        /// </summary>
        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        /// <summary>
        /// 保單號碼
        /// </summary>
        [JsonProperty("policyNo")]
        public string PolicyNo { get; set; }

        /// <summary>
        /// 保險起日(UNIX timestamp)
        /// </summary>
        [JsonProperty("beginDate")]
        public int? BeginDate { get; set; }

        /// <summary>
        /// 保險迄日(UNIX timestamp)
        /// </summary>
        [JsonProperty("endDate")]
        public int? EndDate { get; set; }

        /// <summary>
        /// 保障年期
        /// </summary>
        [JsonProperty("period")]
        public int Period { get; set; }

        #region 車籍相關

        /// <summary>
        /// 牌照號碼
        /// </summary>
        [JsonProperty("tagId")]
        public string TagId { get; set; }

        /// <summary>
        /// 原始發照日 yyyMMdd E.g. 1080901
        /// </summary>
        [JsonProperty("registrationIssueDate")]
        public string RegistrationIssueDate { get; set; }

        /// <summary>
        /// 車輛種類代碼 (01重型機車/02輕型機車/03自用小客車/04自用小貨車/22自用小客貨車/32大型重型機車/34輕型小型機車)
        /// </summary>
        [JsonProperty("vehicleType")]
        public string VehicleType { get; set; }

        /// <summary>
        /// 出廠年月 YYYYMM E.g. 198812
        /// </summary>
        [JsonProperty("manufacturedDate")]
        public string ManufacturedDate { get; set; }

        /// <summary>
        /// 排氣量 E.g. 2000
        /// </summary>
        [JsonProperty("displacement")]
        public decimal? Displacement { get; set; }

        /// <summary>
        /// 乘載噸數(當vehicleType=04 自用小貨車才會有值)
        /// </summary>
        [JsonProperty("tonnage")]
        public decimal? Tonnage { get; set; }

        /// <summary>
        /// 車型物件
        /// </summary>
        [JsonProperty("carTypeList")]
        public List<CarTypeInfos> CarTypeList { get; set; }

        /// <summary>
        /// 引擎號碼
        /// </summary>
        [JsonProperty("engineId")]
        public string EngineId { get; set; }

        /// <summary>
        /// 險種類型 01:汽車，02:機車
        /// </summary>
        [JsonProperty("insType")]
        public string InsType { get; set; }

        #endregion

        /// <summary>
        /// 保單狀態 01 : active ， 02 : expired
        /// </summary>
        [JsonProperty("insStatus")]
        public string InsStatus { get; set; }

        /// <summary>
        /// 是否能續保
        /// </summary>
        [JsonProperty("isExtend")]
        public bool IsExtend { get; set; }

        /// <summary>
        /// 保單是否過期
        /// </summary>
        [JsonProperty("isExpired")]
        public bool IsExpired { get; set; }

        /// <summary>
        /// 專案是否存在
        /// </summary>
        [JsonProperty("isProductExist")]
        public bool IsProductExist { get; set; }

        /// <summary>
        /// 保單到期剩餘天數
        /// </summary>
        [JsonProperty("expiredLeftDays")]
        public int ExpiredLeftDays { get; set; }

        /// <summary>
        /// 保單總額
        /// </summary>
        [JsonProperty("totalPremium")]
        public decimal TotalPremium { get; set; }

        /// <summary>
        /// 商品類型 ( CALI：強制險  VALI：任意險 )
        /// </summary>
        [JsonProperty("planCategory")]
        public string PlanCategory { get; set; }

        /// <summary>
        /// 商品類型名稱 (強制險/任意險)
        /// </summary>
        [JsonProperty("planCategoryName")]
        public string PlanCategoryName { get; set; }

        /// <summary>
        /// 要被保險人 ID
        /// </summary>
        [JsonProperty("insuredId")]
        public string InsuredId { get; set; }

        /// <summary>
        /// 要被保險人生日 YYYYMMDD
        /// </summary>
        [JsonProperty("insuredBirthday")]
        public int? InsuredBirthday { get; set; }

        /// <summary>
        /// 繳費方式 01-信用卡/Debit-Card，02-活期帳戶扣款
        /// </summary>
        [JsonProperty("payBy")]
        public string PayBy { get; set; }

        /// <summary>
        /// 信用卡號
        /// (格式：1234567890123456，連續數字，沒有短橫線"-")
        /// </summary>
        [JsonProperty("cardNo")]
        public string CardNo { get; set; }

        /// <summary>
        /// 保單寄送方式 01.紙本 02.電子 依保單主檔 內容回傳
        /// </summary>
        [JsonProperty("policyDelivery")]
        public string PolicyDelivery { get; set; }

        /// <summary>
        /// 保險公司代碼
        /// </summary>
        [JsonProperty("insuranceId")]
        public string InsuranceId { get; set; }

        /// <summary>
        /// 保險公司名稱
        /// </summary>
        [JsonProperty("insuranceCompany")]
        public string InsuranceCompany { get; set; }

        /// <summary>
        /// 保險公司電話號碼
        /// </summary>
        [JsonProperty("insuranceTel")]
        public string InsuranceTel { get; set; }

        /// <summary>
        /// 保險項目清單
        /// </summary>
        [JsonProperty("plans")]
        public List<DetailPlanInfo> Plans { get; set; }
    }

    /// <summary>
    /// 保險項目清單
    /// </summary>
    public class DetailPlanInfo
    {
        /// <summary>
        /// 商品代碼
        /// </summary>
        [JsonProperty("planCode")]
        public string PlanCode { get; set; }

        /// <summary>
        /// 保險項目
        /// </summary>
        [JsonProperty("item")]
        public string Item { get; set; }

        /// <summary>
        /// 保險項目保費
        /// </summary>
        [JsonProperty("contentPremium")]
        public decimal ContentPremium { get; set; }

        /// <summary>
        /// 保障項目明細
        /// </summary>
        [JsonProperty("guaranteeItems")]
        public List<GuaranteeItem> GuaranteeItems { get; set; }
    }

    /// <summary>
    /// 保障項目明細
    /// </summary>
    public class GuaranteeItem
    {
        /// <summary>
        /// 保險內容
        /// </summary>
        [JsonProperty("content")]
        public string Content { get; set; }

        /// <summary>
        /// 保額
        /// </summary>
        [JsonProperty("value")]
        public string Value { get; set; }
    }

    /// <summary>
    /// 車型物件
    /// </summary>
    public class CarTypeInfos
    {
        /// <summary>
        /// 車型顯示名稱 E.g. A6 35 TFSI S tronic front 1800cc 4D 5人座
        /// </summary>
        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        /// <summary>
        /// 車型ID E.g. 31037400
        /// </summary>
        [JsonProperty("carType")]
        public string CarType { get; set; }

        /// <summary>
        /// 廠牌代號
        /// </summary>
        [JsonProperty("brandID")]
        public string BrandID { get; set; }

        /// <summary>
        /// 廠牌名稱
        /// </summary>
        [JsonProperty("brandName")]
        public string BrandName { get; set; }

        /// <summary>
        /// 乘載人數 E.g. 5
        /// </summary>
        [JsonProperty("transportUnit")]
        public int TransportUnit { get; set; }
    }
}
