﻿using Newtonsoft.Json;
using TPI.NetCore.WebAPI.Attributes;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 要被保人資料 Resp
    /// </summary>
    public class PolicyApplicantInfoResp
    {
        /// <summary>
        /// 要保人資料
        /// </summary>
        public PolicyApplicantInfo Applicant { get; set; }

        /// <summary>
        /// 被保人資料
        /// </summary>
        public PolicyApplicantInfo Insured { get; set; }

    }

    /// <summary>
    /// 要被保人資料
    /// </summary>
    public class PolicyApplicantInfo
    {

        /// <summary>
        /// applicant(要保人資料)：要保人姓名 / insured(被保人資料)：被保人姓名
        /// </summary>
        [Mask(MaskType.ChineseName | MaskType.DashIfEmpty)]
        public string Name { get; set; }

        /// <summary>
        /// applicant(要保人資料)：要保人身分證字號 / insured(被保人資料)：被保人身分證字號
        /// </summary>
        [JsonProperty("idNo")]
        [Mask(MaskType.NationalIdentification | MaskType.DashIfEmpty)]
        public string IdNo { get; set; }

        /// <summary>
        /// applicant(要保人資料)：要保人身分證字號(查詢用) / insured(被保人資料)：被保人身分證字號(查詢用)
        /// </summary>
        [JsonProperty("hintidNo")]
        public string HintIdNo { get; set; }

        /// <summary>
        /// applicant(要保人資料)：要保人國籍 / insured(被保人資料)：被保人國籍
        /// </summary>
        [Mask( MaskType.DashIfEmpty)]
        public string Nationality { get; set; }

        /// <summary>
        /// applicant(要保人資料)：要保人生日 / insured(被保人資料)：被保人生日
        /// </summary>
        [Mask(MaskType.Birthday | MaskType.DashIfEmpty)]
        public string BirthdayStr { get; set; }

        /// <summary>
        /// applicant(要保人資料)：要保人性別 / insured(被保人資料)：被保人性別
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string GenderStr { get; set; }

        /// <summary>
        /// applicant(要保人資料)：要保人信箱 / insured(被保人資料)：被保人信箱
        /// </summary>
        [Mask(MaskType.Email | MaskType.DashIfEmpty)]
        public string Email { get; set; }

        /// <summary>
        /// applicant(要保人資料)：要保人婚姻狀態 / insured(被保人資料)：被保人婚姻狀態
        /// </summary>
        [Mask(MaskType.Marriage | MaskType.DashIfEmpty)]
        public string MaritalStatusStr { get; set; }

        /// <summary>
        /// applicant(要保人資料)：要保人電話 1 / insured(被保人資料)：被保人電話 1
        /// </summary>
        [Mask(MaskType.PhoneNo | MaskType.DashIfEmpty)]
        public string Phone1 { get; set; }

        /// <summary>
        /// applicant(要保人資料)：要保人電話 2 / insured(被保人資料)：被保人電話 2
        /// </summary>
        [Mask(MaskType.PhoneNo | MaskType.DashIfEmpty)]
        public string Phone2 { get; set; }

        /// <summary>
        /// applicant(要保人資料)：要保人手機 / insured(被保人資料)：被保人手機
        /// </summary>
        [Mask(MaskType.PhoneNo | MaskType.DashIfEmpty)]
        public string Mobile { get; set; }

        /// <summary>
        /// applicant(要保人資料)：要保人通訊地址 / insured(被保人資料)：被保人通訊地址
        /// </summary>
        [Mask(MaskType.ChineseAddress | MaskType.DashIfEmpty)]
        public string AddrStr { get; set; }

        /// <summary>
        /// applicant(要保人資料)：要保人戶籍地址 / insured(被保人資料)：被保人戶籍地址
        /// </summary>
        [Mask(MaskType.ChineseAddress | MaskType.DashIfEmpty)]
        public string HouseholdAddrStr { get; set; }
    }
}