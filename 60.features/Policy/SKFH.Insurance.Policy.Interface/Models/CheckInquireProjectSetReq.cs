﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 確認方案組合 Request
    /// </summary>
    public class CheckInquireProjectSetReq
    {
        /// <summary>
        /// 受理編號
        /// </summary>
        public string AcceptNo { get; set; }
        /// <summary>
        /// 專案代碼
        /// </summary>
        public string ProductCode { get; set; }
        /// <summary>
        /// 方案代碼
        /// </summary>
        public string ProjectCode { get; set; }
        /// <summary>
        /// 選擇投保年期
        /// </summary>
        public int? SelectedYear { get; set; }
    }
}
