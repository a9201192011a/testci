﻿using TPI.NetCore.WebAPI.Models;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 保險未成功件查詢 Req
    /// </summary>
    public class TempPolicySearchReq : DataTableQueryReq
    {
        /// <summary>
        /// 進件編號
        /// </summary>
        public string AcceptNo { get; set; }

        /// <summary>
        /// 案件來源(參數代嗎)
        /// </summary>
        public string CaseSource { get; set; }

        /// <summary>
        /// 要保人 ID
        /// </summary>
        public string ApplicantId { get; set; }

        /// <summary>
        /// 要保人姓名
        /// </summary>
        public string ApplicantName { get; set; }

        /// <summary>
        /// 被保人 ID
        /// </summary>
        public string InsuredId { get; set; }

        /// <summary>
        /// 被保人姓名
        /// </summary>
        public string InsuredName { get; set; }

        /// <summary>
        /// 進件日期 起日
        /// </summary>
        public string AcceptStartDate { get; set; }

        /// <summary>
        /// 進件日期 迄日
        /// </summary>
        public string AcceptEndDate { get; set; }

    }
}
