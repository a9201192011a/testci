﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 強制險與任意險保期 Resp
    /// </summary>
    public class GetInsuranceDateResp
    {
        /// <summary>
        /// 強制險保險期間起日(原有保單)
        /// </summary>
        [JsonProperty("compulsoryInsBegDate")]
        public int? CompulsoryInsBegDate { get; set; }

        /// <summary>
        /// 強制險保險期間迄日(原有保單)
        /// </summary>
        [JsonProperty("compulsoryInsEndDate")]
        public int? CompulsoryInsEndDate { get; set; }

        /// <summary>
        /// 強制險最早續保日
        /// </summary>
        [JsonProperty("compulsoryInsAvailableRenewalDate")]
        public int? CompulsoryInsAvailableRenewalDate { get; set; }

        /// <summary>
        /// 強制險新保單起日
        /// </summary>
        [JsonProperty("compulsoryEffectiveDate")]
        public int? CompulsoryEffectiveDate { get; set; }

        /// <summary>
        /// 強制險目前是否可以續保
        /// </summary>
        [JsonProperty("isCompulsoryInsRenewable")]
        public bool? IsCompulsoryInsRenewable { get; set; }

        /// <summary>
        /// 任意險保險(31)期間起日(原有保單)
        /// </summary>
        [JsonProperty("voluntaryInsBegDate")]
        public int? VoluntaryInsBegDate { get; set; }

        /// <summary>
        /// 任意險保險(31)期間迄日(原有保單)
        /// </summary>
        [JsonProperty("voluntaryInsEndDate")]
        public int? VoluntaryInsEndDate { get; set; }

        /// <summary>
        /// 任意險保險(31)最早續保日
        /// </summary>
        [JsonProperty("voluntaryInsAvailableRenewalDate")]
        public int? VoluntaryInsAvailableRenewalDate { get; set; }

        /// <summary>
        /// 任意險保險(31)新保單起日
        /// </summary>
        [JsonProperty("voluntaryEffectiveDate")]
        public int? VoluntaryEffectiveDate { get; set; }

        /// <summary>
        /// 任意險保險(31)目前是否可以續保
        /// </summary>
        [JsonProperty("isVoluntaryInsRenewable")]
        public bool? IsVoluntaryInsRenewable { get; set; }
    }
}
