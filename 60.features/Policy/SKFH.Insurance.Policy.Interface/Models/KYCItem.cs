﻿using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// KYC 項目物件
    /// </summary>
    public class KYCItem
    {
        /// <summary>
        /// 被保險人目前是否受有監護宣告?
        /// </summary>
        [JsonProperty("informItemGuardianship")]
        public string InformItemGuardianship { get; set; }

        /// <summary>
        /// 是否境外居住超過半年(Y:是 N:否)
        /// </summary>
        [JsonProperty("informItemOversea")]
        public string InformItemOversea { get; set; }

        /// <summary>
        /// 是否為國際組織重要政治性職務人士
        /// 0：否
        /// 1：是，國內
        /// 2：是，國外
        /// </summary>
        [JsonProperty("informItemPolitics")]
        public string InformItemPolitics { get; set; }

        /// <summary>
        /// 是否為特殊職業(Y:是 N:否)
        /// 該欄位前端畫面目前不會傳
        /// </summary>
        [JsonProperty("inforSensitiveOccupation")]
        public string InforSensitiveOccupation { get; set; }

        /// <summary>
        /// 保費來源問項
        /// 1:薪資收入或公司紅利
        /// 2:投資收入
        /// 3:儲蓄
        /// 4:退休金
        /// 5:財產繼承
        /// 6:貸款或保險單借款
        /// 7:解約或終止契約
        /// </summary>
        [JsonProperty("informItemPremium")]
        public string InformItemPremium { get; set; }

        /// <summary>
        /// 投保前三個月內是否有辦理終止契約、貸款或保險單借款之情形(Y:是 N:否)
        /// </summary>
        [JsonProperty("informItemLoan")]
        public string InformItemLoan { get; set; }

        /// <summary>
        /// 本行三個月內貸款戶檢核
        /// 說明：客戶在前端點選選項的時間
        /// yyyy/MM/dd HH:mm:ss(timestamp 格式，例如：2021/07/01 22:18:17.000)
        /// </summary>
        [JsonProperty("informItemPremiumCheck")]
        public string InformItemPremiumCheck { get; set; }

        /// <summary>
        /// 保費來源風險警語檢核
        /// 說明：客戶在前端點選選項的時間
        /// yyyy/MM/dd HH:mm:ss(timestamp格式，例如：2021/07/01 22:18:17.000)
        /// </summary>
        [JsonProperty("informItemLoanCheck")]
        public string InformItemLoanCheck { get; set; }
    }
}
