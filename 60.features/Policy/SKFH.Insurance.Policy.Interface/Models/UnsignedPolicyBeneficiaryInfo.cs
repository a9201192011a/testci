﻿using System.Collections.Generic;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 未簽署的受益人資料
    /// </summary>
    public class UnsignedPolicyBeneficiaryInfo
    {
        /// <summary>
        /// 受理編號
        /// </summary>
        public string AcceptNo { get; set; }

        /// <summary>
        /// 給付方式名稱
        /// </summary>
        public string BeneficiaryPayTypeStr { get; set; }

        /// <summary>
        /// 給付內容
        /// </summary>
        public List<Beneficiary> Beneficiary { get; set; }
    }
}
