using System.Collections.Generic;
using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 設定保險鬧鐘
    /// </summary>
    public class SavePolicyAlarmReq
    {
        /// <summary>
        /// 專案代碼
        /// </summary>
        [JsonProperty("productCode")]
        public string ProductCode { get; set; }

        /// <summary>
        /// 車牌號碼
        /// </summary>
        [JsonProperty("tagID")]
        public string TagID { get; set; }

        /// <summary>
        /// 是否刪除
        /// </summary>
        [JsonProperty("isDelete")]
        public bool IsDelete { get; set; }

        /// <summary>
        /// 保險鬧鐘設定清單
        /// </summary>
        [JsonProperty("alarmClockList")]
        public List<AlarmClockItem> AlarmClockList { get; set; }

    }

    /// <summary>
    /// 保險鬧鐘設定資料
    /// </summary>
    public class AlarmClockItem
    {
        /// <summary>
        /// 商品類型
        /// </summary>
        [JsonProperty("planCategory")]
        public string PlanCategory { get; set; }

        /// <summary>
        /// 保險期間迄日(原有保單)(UNIX timestamp)
        /// </summary>
        [JsonProperty("insEndDate")]
        public long? InsEndDate { get; set; }

        /// <summary>
        /// 最早續保日(UNIX timestamp)
        /// </summary>
        [JsonProperty("availableRenewalDate")]
        public long? AvailableRenewalDate { get; set; }

    }

}