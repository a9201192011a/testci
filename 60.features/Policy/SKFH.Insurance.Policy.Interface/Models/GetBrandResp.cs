﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 廠牌查詢 Resp
    /// </summary>
    public class GetBrandResp
    {
        /// <summary>
        /// 廠牌查詢結果
        /// </summary>
        [JsonProperty("vehicleBrandList")]
        public List<VehicleBrandItem> VehicleBrandList { get; set; }
    }
}
