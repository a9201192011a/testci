﻿using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// CreateOrder Req
    /// </summary>
    public class CreateOrderV2Req
    {
        /// <summary>
        /// 本行客戶唯一識別碼
        /// </summary>
        [JsonProperty("custId")]
        public string CustId { get; set; }

        /// <summary>
        /// 受理編號(INS產生)
        /// 格式：{#保險公司代碼(產險 1 碼 + 公會 2 碼)} + YYYYMMDD + 6 碼流水碼(每日重置為 1 )
        /// </summary>
        [JsonProperty("acceptNo")]
        public string AcceptNo { get; set; }

        /// <summary>
        /// Channel OTP流水號
        /// </summary>
        [JsonProperty("otpId")]
        public string OtpId { get; set; }  
       
        /// <summary>
        /// 繳費方式 01-信用卡/ Debit-Card 02-活期帳戶扣款
        /// </summary>
        [JsonProperty("payBy")]
        public string PayBy { get; set; }

        /// <summary>
        /// 付款方式物件
        /// </summary>
        [JsonProperty("paymentObject")]
        public PayObjects PaymentObject { get; set; }

        /// <summary>
        /// 付款方式物件
        /// </summary>
        public class PayObjects
        {
            /// <summary>
            /// 信用卡號/帳號
            /// 格式:數字
            /// e.g. 1111222233334444
            /// </summary>
            [JsonProperty("paymentNo")]
            public string PaymentNo { get; set; }

            /// <summary>
            /// 信用卡有效月年
            /// 格式: mmyy
            /// e.g.0727
            /// </summary>
            [JsonProperty("cardExpiredDate")]
            public string CardExpiredDate { get; set; }

            /// <summary>
            /// 卡背末三碼(CVV2)
            /// </summary>
            [JsonProperty("cardCheckCode")]
            public string CardCheckCode { get; set; }

            /// <summary>
            /// 銀行代碼
            /// </summary>
            [JsonProperty("bankCode")]
            public string BankCode { get; set; }
        }
    }
}
