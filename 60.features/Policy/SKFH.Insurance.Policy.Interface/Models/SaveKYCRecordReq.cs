﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 儲存KYC結果 Request
    /// </summary>
    public class SaveKYCRecordReq
    {
        /// <summary>
        /// 受理編號
        /// </summary>
        [JsonProperty("acceptNo")]
        public string AcceptNo { get; set; }

        /// <summary>
        /// KYC資訊
        /// </summary>
        [JsonProperty("kycInfo")]
        public List<KYCData> KycInfo { get; set; }

        /// <summary>
        /// KYC資訊
        /// </summary>
        public class KYCData
        {
            /// <summary>
            /// KYC id
            /// </summary>
            [JsonProperty("id")]
            public string Id { get; set; }

            /// <summary>
            /// KYC問項
            /// </summary>
            [JsonProperty("content")]
            public string Content { get; set; }

            /// <summary>
            /// KYC答案描述
            /// </summary>
            [JsonProperty("answerTitle")]
            public string AnswerTitle { get; set; }

            /// <summary>
            /// KYC答案代號
            /// </summary>
            [JsonProperty("answerValue")]
            public string AnswerValue { get; set; }

            /// <summary>
            /// 點擊時間  UNIX timestamp
            /// </summary>
            [JsonProperty("createTime")]
            public int CreateTime { get; set; }
        }
    }

    
}
