﻿using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 取得車籍資料 Req
    /// </summary>
    public class InquireCarRegistrationInfoReq
    {
        /// <summary>
        /// 受理編號
        /// </summary>
        [JsonProperty("acceptNo")]
        public string AcceptNo { get; set; }

        /// <summary>
        /// 專案代碼
        /// </summary>
        [JsonProperty("productCode")]
        public string ProductCode { get; set; }

        /// <summary>
        /// 牌照號碼
        /// </summary>
        [JsonProperty("tagID")]
        public string TagID { get; set; }
    }
}
