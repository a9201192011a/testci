﻿using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 查詢保單內容 Req
    /// </summary>
    public class GetDetailReq
    {
        /// <summary>
        /// 受理編號(INS產生) 
        /// 格式：{#保險公司代碼(公會2碼)} + YYYYMMDD + 6 碼流水碼(每日重置為 1 )
        /// </summary>
        [JsonProperty("acceptNo")]
        public string AcceptNo { get; set; }

        /// <summary>
        /// 保單號
        /// </summary>
        [JsonProperty("policyNo")]
        public string PolicyNo { get; set; }

        /// <summary>
        /// 牌照號碼
        /// </summary>
        [JsonProperty("tagID")]
        public string TagID { get; set; }
    }
}
