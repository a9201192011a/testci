﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 車籍資料 Resp
    /// </summary>
    public class GetCarRegistrationInfoResp
    {
        /// <summary>
        /// 強制險保險期間起日(原有保單)
        /// </summary>
        [JsonProperty("compulsoryInsBegDate")]
        public int? CompulsoryInsBegDate { get; set; }

        /// <summary>
        /// 強制險保險期間迄日(原有保單)
        /// </summary>
        [JsonProperty("compulsoryInsEndDate")]
        public int? CompulsoryInsEndDate { get; set; }

        /// <summary>
        /// 強制險最早續保日
        /// </summary>
        [JsonProperty("compulsoryInsAvailableRenewalDate")]
        public int? CompulsoryInsAvailableRenewalDate { get; set; }

        /// <summary>
        /// 強制險新保單起日
        /// </summary>
        [JsonProperty("compulsoryEffectiveDate")]
        public int? CompulsoryEffectiveDate { get; set; }

        /// <summary>
        /// 強制險目前是否可以續保
        /// </summary>
        [JsonProperty("isCompulsoryInsRenewable")]
        public bool IsCompulsoryInsRenewable { get; set; }

        /// <summary>
        /// 任意險保險(31)期間起日(原有保單)
        /// </summary>
        [JsonProperty("voluntaryInsBegDate")]
        public int? VoluntaryInsBegDate { get; set; }

        /// <summary>
        /// 任意險保險(31)期間迄日(原有保單)
        /// </summary>
        [JsonProperty("voluntaryInsEndDate")]
        public int? VoluntaryInsEndDate { get; set; }

        /// <summary>
        /// 任意險保險(31)最早續保日
        /// </summary>
        [JsonProperty("voluntaryInsAvailableRenewalDate")]
        public int? VoluntaryInsAvailableRenewalDate { get; set; }

        /// <summary>
        /// 任意險保險(31)新保單起日
        /// </summary>
        [JsonProperty("voluntaryEffectiveDate")]
        public int? VoluntaryEffectiveDate { get; set; }

        /// <summary>
        /// 任意險保險(31)目前是否可以續保
        /// </summary>
        [JsonProperty("isVoluntaryInsRenewable")]
        public bool IsVoluntaryInsRenewable { get; set; }

        /// <summary>
        /// 專案名稱
        /// </summary>
        [JsonProperty("productName")]
        public string ProductName { get; set; }

        /// <summary>
        /// 車牌號碼
        /// </summary>
        [JsonProperty("tagID")]
        public string TagID { get; set; }

        /// <summary>
        /// 原始發照日 yyyMMdd E.g. 1080901
        /// </summary>
        [JsonProperty("registrationIssueDate")]
        public string RegistrationIssueDate { get; set; }

        /// <summary>
        /// 車種代號
        /// 01重型機車/02輕型機車/03自用小客車/04自用小貨車/22自用小客貨車/32大型重型機車/34輕型小型機車
        /// </summary>
        [JsonProperty("vehicleType")]
        public string VehicleType { get; set; }

        /// <summary>
        /// 出廠年月 yyyMMdd E.g. 1080901
        /// </summary>
        [JsonProperty("manufacturedDate")]
        public string ManufacturedDate { get; set; }

        /// <summary>
        /// 排氣量
        /// </summary>
        [JsonProperty("displacement")]
        public decimal Displacement { get; set; }

        /// <summary>
        /// 車型物件
        /// </summary>
        [JsonProperty("carTypeList")]
        public List<CarTypesInfo> CarTypeLists { get; set; }

        /// <summary>
        /// 車型物件
        /// </summary>
        public class CarTypesInfo
        {
            /// <summary>
            /// 車型名稱
            /// </summary>
            [JsonProperty("displayName")]
            public string DisplayName { get; set; }

            /// <summary>
            /// 車型ID
            /// </summary>
            [JsonProperty("carType")]
            public string CarType { get; set; }

            /// <summary>
            /// 汽車廠牌代號
            /// </summary>
            [JsonProperty("brandID")]
            public string BrandID { get; set; }

            /// <summary>
            /// 汽車廠牌顯示名稱
            /// </summary>
            [JsonProperty("brandName")]
            public string BrandName { get; set; }

            /// <summary>
            /// 乘載人數
            /// </summary>
            [JsonProperty("transportUnit")]
            public int? TransportUnit { get; set; }
        }
    }
}
