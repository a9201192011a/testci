﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 方案組合 Req
    /// </summary>
    [DataContract]
    public class InquireProjectSetReq
    {
        /// <summary>
        /// 受理編號
        /// </summary>
        [DataMember]
        [JsonProperty("acceptNo")]
        public string AcceptNo { get; set; }

        /// <summary>
        /// 專案代碼
        /// </summary>
        [DataMember]
        [JsonProperty("productCode")]
        public string ProductCode { get; set; }

        /// <summary>
        /// 車牌號碼
        /// </summary>
        [DataMember]
        [JsonProperty("tagID")]
        public string TagID { get; set; }
    }
}
