using System.Collections.Generic;
using Newtonsoft.Json;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 查詢保單清單 Response(任意險)
    /// </summary>
    public class InquirePolicyListResp
    {
        /// <summary>
        /// 保單清單
        /// </summary>
        [JsonProperty("policyList")]
        public List<InquirePolicyListItem> PolicyList { get; set; }
    }

    /// <summary>
    /// 保單清單
    /// </summary>
    public class InquirePolicyListItem
    {
        /// <summary>
        /// 保險公司名稱
        /// </summary>
        [JsonProperty("insuranceCompanyName")]
        public string InsuranceCompanyName { get; set; }

        /// <summary>
        /// 保單號
        /// </summary>
        [JsonProperty("policyNo")]
        public string PolicyNo { get; set; }

        /// <summary>
        /// 受理編號
        /// </summary>
        [JsonProperty("acceptNo")]
        public string AcceptNo { get; set; }

        /// <summary>
        /// 專案代碼
        /// </summary>
        [JsonProperty("productCode")]
        public string ProductCode { get; set; }

        /// <summary>
        /// 專案名稱
        /// </summary>
        [JsonProperty("productName")]
        public string ProductName { get; set; }

        /// <summary>
        /// 方案代碼
        /// </summary>
        [JsonProperty("projectCode")]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 方案名稱
        /// </summary>
        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        /// <summary>
        /// 保險期間(起日) 格式：Timespan
        /// </summary>
        [JsonProperty("beginDate")]
        public int BeginDate { get; set; }

        /// <summary>
        /// 保險期間(迄日) 格式：Timespan
        /// </summary>
        [JsonProperty("endDate")]
        public int EndDate { get; set; }

        /// <summary>
        /// 保障年期
        /// </summary>
        [JsonProperty("period")]
        public int Period { get; set; }

        /// <summary>
        /// 牌照號碼
        /// </summary>
        [JsonProperty("tagId")]
        public string TagId { get; set; }

        /// <summary>
        /// 險種類型 01:汽車 02:機車 03:All
        /// </summary>
        [JsonProperty("insType")]
        public string InsType { get; set; }

        /// <summary>
        /// 保單狀態 01:active 02:expired
        /// </summary>
        [JsonProperty("insStatus")]
        public string InsStatus { get; set; }

        /// <summary>
        /// 是否可續保
        /// </summary>
        [JsonProperty("isExtend")]
        public bool IsExtend { get; set; }

        /// <summary>
        /// 專案是否存在
        /// </summary>
        [JsonProperty("isProductExist")]
        public bool IsProductExist { get; set; }

        /// <summary>
        /// 商品類型 ( CALI：強制險  VALI：任意險 )
        /// </summary>
        [JsonProperty("planCategory")]
        public string PlanCategory { get; set; }

        /// <summary>
        /// 商品類型名稱 (強制險/任意險)
        /// </summary>
        [JsonProperty("planCategoryName")]
        public string PlanCategoryName { get; set; }
    }
}