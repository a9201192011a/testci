﻿using System.Collections.Generic;
using TPI.NetCore.WebAPI.Attributes;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 儲存保單結果 Request
    /// </summary>
    public class SavePolicyDataReq : MaskReq
    {
        /// <summary>
        /// 進件編號
        /// </summary>
        public string AcceptNo { get; set; }
        /// <summary>
        /// 保險公司專案代碼
        /// </summary>
        public string CompanyProjectCode { get; set; }

        /// <summary>
        /// 方案代碼
        /// </summary>
        public string ProjectCode { get; set; }
        /// <summary>
        /// 選取險種項目與保額
        /// </summary>
        public List<SavePolicyDataReqSelectItem> SelectedItems { get; set; }

    }

    /// <summary>
    /// 選取險種項目與保額
    /// </summary>
    public class SavePolicyDataReqSelectItem
    {
        /// <summary>
        /// itemKey
        /// </summary>
        public string ItemKey { get; set; }
        /// <summary>
        /// optionKey
        /// </summary>
        public string OptionKey { get; set; }
    }
}
