﻿using TPI.NetCore.WebAPI.Models;

namespace InsureBrick.Modules.Policy.Interface.Models
{
    /// <summary>
    /// 保單資料查詢 Req
    /// </summary>
    public class PolicySearchReq : DataTableQueryReq
    {
        /// <summary>
        /// 保單號碼
        /// </summary>
        public string PolicyNo { get; set; }

        /// <summary>
        /// 案件來源(參數代碼)
        /// </summary>
        public string CaseSource { get; set; }

        /// <summary>
        /// 保單狀態(參數代碼)
        /// </summary>
        public string PolicyStatus { get; set; }

        /// <summary>
        /// 要保人 ID
        /// </summary>
        public string ApplicantId { get; set; }

        /// <summary>
        /// 要保人姓名
        /// </summary>
        public string ApplicantName { get; set; }

        /// <summary>
        /// 被保人 ID
        /// </summary>
        public string InsuredId { get; set; }

        /// <summary>
        /// 被保人姓名
        /// </summary>
        public string InsuredName { get; set; }

        /// <summary>
        /// 生效日期-起日(查詢【保單主檔】保險期間-起日)
        /// </summary>
        public string PolicyPeriodStartDate { get; set; }

        /// <summary>
        /// 生效日期-迄日(查詢【保單主檔】保險期間-起日)
        /// </summary>
        public string PolicyPeriodEndDate { get; set; }

        /// <summary>
        /// 到期日期-起日(查詢【保單主檔】保險期間-迄日)
        /// </summary>
        public string ExpiredStartDate { get; set; }

        /// <summary>
        /// 到期日期-迄日(查詢【保單主檔】保險期間-迄日)
        /// </summary>
        public string ExpiredEndDate { get; set; }

        /// <summary>
        /// 商品類型
        /// </summary>
        public string PlanCategory { get; set; }
    }
}
