﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using InsureBrick.Modules.Policy.Interface.Models;
using SKFH.Insurance.Policy.Entity.Models;
using TPI.NetCore.WebAPI.Models;

namespace InsureBrick.Modules.Policy.Interface
{
    /// <summary>
    /// UMS Type
    /// </summary>
    public enum UMS
    {
        Renewal,
        AML,
        GetOrderStatus
    }

    /// <summary>
    /// PolicyService interface
    /// </summary>
    public interface IPolicyService
    {
        /// <summary>
        /// 保單資料查詢-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料查詢 Req</param>
        /// <returns>保單資料查詢 Resp</returns>
        DataTableQueryResp<PolicySearchResp> PolicySearch(PolicySearchReq req);

        /// <summary>
        /// 保單資料查詢-明細頁-要/被保人資料-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>要被保人資料 Resp</returns>
        PolicyApplicantInfoResp PolicyApplicantInfo(PolicyReq req);

        /// <summary>
        /// 保單資料查詢-明細頁-保單/繳費資訊-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>繳費資訊 Resp</returns>
        PolicyPaymentInfoResp PolicyPaymentInfo(PolicyReq req);

        /// <summary>
        /// 保單資料查詢-明細頁-投保內容-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>保單資料查詢 Resp</returns>
        PolicyContentInfoResp PolicyContentInfo(PolicyReq req);

        /// <summary>
        /// 保單資料查詢-明細頁-受益人資料-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>受益人資訊 Resp</returns>
        PolicyBeneficiaryInfoResp PolicyBeneficiaryInfo(PolicyReq req);

        /// <summary>
        /// 新契約資料查詢-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">新契約資料查詢 Req</param>
        /// <returns>新契約資料查詢 Resp</returns>
        DataTableQueryResp<ContractSearchResp> ContractSearch(ContractSearchReq req);

        /// <summary>
        /// 新契約資料查詢-明細頁-要/被保人資料-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>要被保人資料 Resp</returns>
        PolicyApplicantInfoResp ContractApplicantInfo(ContractReq req);

        /// <summary>
        /// 新契約資料查詢-明細頁-保單/繳費資訊-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>繳費資訊 Resp</returns>
        PolicyPaymentInfoResp ContractPaymentInfo(ContractReq req);

        /// <summary>
        /// 新契約資料查詢-明細頁-投保內容-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>保單資料查詢 Resp</returns>
        PolicyContentInfoResp ContractContentInfo(ContractReq req);

        /// <summary>
        /// 新契約資料查詢-明細頁-受益人資料-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>受益人資訊 Resp</returns>
        PolicyBeneficiaryInfoResp ContractBeneficiaryInfo(ContractReq req);

        /// <summary>
        /// 新契約資料查詢-明細頁-核保進度-查詢資料 API
        /// </summary>
        /// <param name="req">保單資料共用 Req</param>
        /// <returns>核保進度訊息 Resp</returns>
        ContractScheduleInfoResp ContractScheduleInfo(ContractReq req);

        /// <summary>
        /// 保險未成功件查詢-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">保險未成功件查詢 Req</param>
        /// <returns>保險未成功件查詢 Resp</returns>
        DataTableQueryResp<TempPolicySearchResp> TempPolicySearch(TempPolicySearchReq req);

        /// <summary>
        /// 查詢保單清單
        /// </summary>
        /// <param name="req">查詢保單清單 Req</param>
        /// <returns>保單查詢 Resp</returns>
        Task<StandardModel<GetListResp>> GetList(StandardModel<GetListReq> req);

        ///// <summary>
        ///// 查詢保單清單(任意險)
        ///// </summary>
        ///// <param name="req">查詢保單清單 Req</param>
        ///// <returns>保單查詢 Resp</returns>
        //Task<StandardModel<InquirePolicyListResp>> InquirePolicyList(StandardModel<InquirePolicyListReq> req);

        ///// <summary>
        ///// 查詢保單內容
        ///// </summary>
        ///// <param name="req">查詢保單內容 Req</param>
        ///// <returns>查詢保單內容 Resp</returns>
        //StandardModel<GetDetailResp> GetDetail(StandardModel<GetDetailReq> req);

        ///// <summary>
        ///// 查詢保單內容(任意險)
        ///// </summary>
        ///// <param name="req">查詢保單內容 Req</param>
        ///// <returns>查詢保單內容 Resp</returns>
        //StandardModel<GetPolicyDetailResp> InquirePolicyDetail(StandardModel<GetPolicyDetailReq> req);

        ///// <summary>
        ///// 保費試算
        ///// </summary>
        ///// <param name="req">保費試算 Req</param>
        ///// <returns>保費試算 Resp</returns>
        //Task<StandardModel<PremiumTrialResp>> PremiumTrial(StandardModel<PremiumTrialReq> req);

        ///// <summary>
        ///// 廠牌查詢
        ///// </summary>
        ///// <param name="req">廠牌查詢 Req</param>
        ///// <returns>廠牌查詢 Resp</returns>
        //StandardModel<GetBrandResp> GetBrand(StandardModel<GetBrandReq> req);

        ///// <summary>
        ///// 廠牌查詢(任意險)
        ///// </summary>
        ///// <param name="req">廠牌查詢 Req</param>
        ///// <returns>廠牌查詢 Resp</returns>
        //StandardModel<GetBrandResp> InquireBrandID(StandardModel<InquireBrandReq> req);

        ///// <summary>
        ///// 車型查詢
        ///// </summary>
        ///// <param name="req">車型查詢 Req</param>
        ///// <returns>車型查詢 Resp</returns>
        //StandardModel<GetModelResp> GetModel(StandardModel<GetModelReq> req);

        ///// <summary>
        ///// 車型查詢(任意險)
        ///// </summary>
        ///// <param name="req">車型查詢 Req</param>
        ///// <returns>車型查詢 Resp</returns>
        //StandardModel<GetModelResp> InquireCarType(StandardModel<InquireCarTypeReq> req);

        ///// <summary>
        ///// 取得車籍資料(任意險)
        ///// </summary>
        ///// <param name="req">車籍資料 Req</param>
        ///// <returns>車型查詢 Resp</returns>
        //Task<StandardModel<GetCarRegistrationInfoResp>> InquireCarRegistrationInfo(StandardModel<InquireCarRegistrationInfoReq> req);

        ///// <summary>
        ///// 儲存車籍資料(任意險)
        ///// </summary>
        ///// <param name="req">車籍資料 Req</param>
        ///// <returns>車型查詢 Resp</returns>
        //Task<StandardModel<GetInsuranceDateResp>> SaveCarRegistrationInfo(StandardModel<SaveCarRegistrationInfoReq> req);

        ///// <summary>
        ///// 方案組合(任意險)
        ///// </summary>
        ///// <param name="req">方案組合 Req</param>
        ///// <returns>方案組合 Resp</returns>
        //StandardModel<GetProjectSetResp> F3115_InquireProjectSet(StandardModel<InquireProjectSetReq> req);

        ///// <summary>
        ///// 儲存KYC結果(任意險)
        ///// </summary>
        ///// <param name="req">KYC結果 Req</param>
        ///// <returns></returns>
        //Task<StandardModel<object>> SaveKYCRecord(StandardModel<SaveKYCRecordReq> req);

        ///// <summary>
        ///// 回覆核保結果
        ///// </summary>
        ///// <param name="req">回覆核保結果 Req</param>
        ///// <returns>回覆核保結果 Resp</returns>
        //Task<StandardModel<ReplyOrderStatusResp>> ReplyOrderStatus(StandardModel<ReplyOrderStatusReq> req);

        ///// <summary>
        ///// CreateOrder 使用 GetOrderStatus 進行 DoubleCheck
        ///// </summary>
        ///// <param name="policyMasterCustId">PolicyMaster CustId</param>
        ///// <param name="policyMasterAcceptNo">PolicyMaster AcceptNo</param>
        ///// <param name="policyPersonInfoIdno">PolicyPersonInfo Idno</param>
        ///// <param name="productMasterInsType">ProductMaster InsType</param>
        ///// <param name="productMasterCompanyCode">ProductMaster CompanyCode</param>
        ///// <returns>保險公司 Get Order Status Resp</returns>
        //Task<StandardModel<SendGetOrderStatusResp>> CreateOrderCallGetOrderStatusDoubleCheck(string policyMasterCustId, string policyMasterAcceptNo, string policyPersonInfoIdno, string productMasterInsType, string productMasterCompanyCode);

        ///// <summary>
        ///// 執行 CallGetOrderStatus 並更新資料
        ///// </summary>
        ///// <param name="acceptNo">進件編號</param>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <returns>執行過程是否有錯</returns>
        //Task<bool> CallGetOrderStatus(string acceptNo, string batchId);

        ///// <summary>
        ///// 檢查及添加 GetOrderStatus AOA
        ///// </summary>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <returns>無回傳值</returns>
        //Task ReissueAOA(string batchId);

        ///// <summary>
        ///// 建立指定類型的 GetOrderStatus AOA
        ///// </summary>
        ///// <param name="policyMaster">保單主檔</param>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <returns>無回傳值</returns>
        //Task AddGetOrderStatusRejectAOA(PolicyMaster policyMaster, string batchId);

        ///// <summary>
        ///// 檢查並添加電訪清單
        ///// </summary>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <returns>無回傳值</returns>
        //Task CheckTelInterviewList(string batchId);

        ///// <summary>
        ///// 發送 GetOrderStatus AOA
        ///// </summary>
        ///// <param name="repository">UMS Type</param>
        ///// <param name="dateTime">重送日期</param>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <param name="userSpecifiedDate">是否為使用者觸發</param>
        ///// <returns>無回傳值</returns>
        //Task SendAOA(UMS repository, DateTime dateTime, string batchId, bool userSpecifiedDate = false);

        ///// <summary>
        ///// 檢測逾期電訪
        ///// </summary>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <returns>執行是否成功</returns>
        //Task<bool> CheckOverdueTelInterview(string batchId);

        ///// <summary>
        ///// 判斷是否需要發送核保失敗且拒保的 AOA
        ///// </summary>
        ///// <param name="companyCode">保險公司代碼</param>
        ///// <param name="msgCntList">Additional message ID</param>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <returns>是否需發送拒保AOA</returns>
        //bool CheckSendFailAOA(string companyCode, IEnumerable<AdtlMsgCntModel> msgCntList, string batchId);

        ///// <summary>
        ///// 判斷是否需要發送核保失敗且拒保的 AOA
        ///// </summary>
        ///// <param name="companyCode">保險公司代碼</param>
        ///// <param name="messageHeader">Response Message Header</param>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <returns>是否需發送拒保AOA</returns>
        //bool CheckSendFailAOA(string companyCode, MessageHeader messageHeader, string batchId);

        ///// <summary>
        ///// 判斷是否為可重新送件代碼
        ///// </summary>
        ///// <param name="companyCode">保險公司代碼</param>
        ///// <param name="msgCntList">Additional message ID</param>
        ///// <param name="batchId">執行此項的批次 Id</param>
        ///// <returns>是否為可重新送件</returns>
        //bool CheckResend(string companyCode, IEnumerable<AdtlMsgCntModel> msgCntList, string batchId);

        ///// <summary>
        ///// 判斷是否為保險公司 Retry 代碼
        ///// </summary>
        ///// <param name="companyCode">保險公司代碼</param>
        ///// <param name="msgCntList">Additional message ID</param>
        ///// <returns>是否需 Retry</returns>
        //bool CheckRetry(string companyCode, IEnumerable<AdtlMsgCntModel> msgCntList);

        ///// <summary>
        ///// 取得 GenGuid 中使用的時間
        ///// </summary>
        //DateTime GuidDate { get; }

        ///// <summary>
        ///// 產出 UMS 用的 Header
        ///// </summary>
        ///// <param name="guid"></param>
        ///// <param name="custid"></param>
        ///// <returns>UMS Header</returns>
        //string GenUMSHeader(string guid, string custid);

        ///// <summary>
        ///// 產出 UMS 用的 Guid
        ///// </summary>
        ///// <returns>UMS Guid</returns>
        //string GenGuid();

        ///// <summary>
        ///// 取得完整地址
        ///// </summary>
        ///// <param name="policyPersonInfo">PolicyPersonInfo</param>
        ///// <param name="getHousehold">是否取得 Household</param>
        ///// <returns>完整地址</returns>
        //public string PolicyPersonInfoAddr(PolicyPersonInfo policyPersonInfo, bool getHousehold = false);

        ///// <summary>
        ///// 取得完整地址
        ///// </summary>
        ///// <param name="TempPolicyPersonInfo">TempPolicyPersonInfo</param>
        ///// <param name="getHousehold">是否取得 Household</param>
        ///// <returns>完整地址</returns>
        //public string PolicyPersonInfoAddr(TempPolicyPersonInfo TempPolicyPersonInfo, bool getHousehold = false);

        ///// <summary>
        /////  F3116_確認方案組合
        ///// </summary>
        ///// <param name="req"></param>
        ///// <returns></returns>
        //Task<StandardModel<CheckInquireProjectSetResp>> F3116_CheckInquireProjectSet(StandardModel<CheckInquireProjectSetReq> req);

        ///// <summary>
        ///// 保費試算
        ///// </summary>
        ///// <param name="req"></param>
        ///// <returns></returns>
        //Task<StandardModel<GetQuoteResp>> F3117_GetQuote(StandardModel<GetQuoteReq> req);

        ///// <summary>
        ///// 處存保單結果
        ///// </summary>
        ///// <param name="req"></param>
        ///// <returns></returns>
        //Task<StandardModel<SavePolicyDataResp>> F3118_SavePolicyData(StandardModel<SavePolicyDataReq> req);

        ///// <summary>
        ///// INS CreateOrder
        ///// </summary>
        ///// <param name="req"></param>
        ///// <returns></returns>
        //Task<StandardModel<CreateOrderInsResp>> F3101_CreateOrder(StandardModel<CreateOrderInsReq> req);

        ///// <summary>
        ///// 設定保險鬧鐘
        ///// </summary>
        ///// <param name="req">查詢保險鬧鐘Request data</param>
        ///// <returns></returns>
        //Task<StandardModel<object>> SavePolicyAlarm(StandardModel<SavePolicyAlarmReq> req);

        ///// <summary>
        ///// 查詢保險鬧鐘
        ///// </summary>
        ///// <param name="req">查詢保險鬧鐘Request data</param>
        ///// <returns></returns>
        //Task<StandardModel<GetPolicyAlarmResp>> GetPolicyAlarm(StandardModel<GetPolicyAlarmReq> req);

        ///// <summary>
        ///// 依進件編號取得保單正式檔正式資料
        ///// </summary>
        ///// <param name="acceptNo">進件編號</param>
        ///// <returns></returns>
        //AcceptData GetOfficialAcceptData(string acceptNo);

        ///// <summary>
        ///// 驗證保單正式檔資料是否正確
        ///// </summary>
        ///// <param name="acceptData">保單正式檔</param>
        ///// <returns>(IsValid, ErrorMsg)</returns>
        //(bool IsValid, string ErrorMsg) ValidateAcceptData(AcceptData acceptData);

        ///// <summary>
        ///// 更新保單續保資訊
        ///// </summary>
        ///// <param name="policyNoList">保單號碼清單</param>
        ///// <param name="batchId"></param>
        //Task CheckRenewNotice(IEnumerable<string> policyNoList, string batchId);

        ///// <summary>
        ///// 檢查並更新保險鬧鐘
        ///// </summary>
        ///// <param name="policyNoList">保單號碼清單</param>
        ///// <param name="batchId"></param>
        //Task CheckPolicyClock(IEnumerable<string> policyNoList, string batchId);
    }
}
