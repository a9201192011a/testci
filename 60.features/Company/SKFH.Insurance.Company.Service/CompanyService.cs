﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using AutoMapper;
using InsureBrick.Modules.Company.Interface;
using InsureBrick.Modules.Company.Interface.Models;
using TPI.NetCore.WebAPI.Models;
using TPI.NetCore.Extensions;
using TPI.NetCore;
using TPI.NetCore.Models;
using TPI.NetCore.WebContext;
using coreLibs.DateAccess;
using SKFH.Insurance.Company.Entity.Repository;
using SKFH.Insurance.Log.Entity.Repository;
using Autofac;
using SKFH.Insurance.Log.Entity.Models;
using SKFH.Insurance.domain.Models;
using Microsoft.VisualBasic;
using InsureBrick.Modules.Common.Interface.Models;
using SKFH.Insurance.Company.Entity.Models;

namespace InsureBrick.Modules.Company.Service
{
    /// <summary>
    /// CompanyService
    /// </summary>
    public class CompanyService : ICompanyService
    {
        #region Initialization

        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        private readonly IUnitOfWork logUnitOfWork;
        private readonly ICompanyProfileRepository companyProfileRepo;
        private readonly IApproveCompanyProfileRepository approveCompanyProfileRepo;
        private readonly IApproveRecordRepository approveRecordRepo;

        //private readonly ICommonService commonService;
        //private readonly IProductService productService;
        //private readonly ITrackableRepository<PlanMaster> planMasterRepo;
        //private readonly ITrackableRepository<ProductMaster> productMasterRepo;
        //private readonly ITrackableRepository<CampaignMaster> campaignMasterRepo;
        //private readonly IEventLogService eventLog;

        public CompanyService(
            IMapper mapper,
            IComponentContext componentContext,
            ICompanyProfileRepository companyProfileRepo,
            IApproveCompanyProfileRepository approveCompanyProfileRepo,
            IApproveRecordRepository approveRecordRepo
        //ITrackableRepository<ApproveCompanyProfile> approveCompanyProfileRepo,
        //    ITrackableRepository<PlanMaster> planMasterRepo,
        //    ITrackableRepository<ProductMaster> productMasterRepo,
        //    ITrackableRepository<ApproveRecord> approveRecordRepo,
        //    ITrackableRepository<CampaignMaster> campaignMasterRepo,
        //    IEventLogService eventLog
            )
        {
            this.mapper = mapper;
            this.unitOfWork = componentContext.ResolveNamed<IUnitOfWork>("Company");
            this.logUnitOfWork = componentContext.ResolveNamed<IUnitOfWork>("Log");
            this.companyProfileRepo = companyProfileRepo;
            this.approveCompanyProfileRepo = approveCompanyProfileRepo;
            this.approveRecordRepo = approveRecordRepo;

            //this.commonService = commonService;
            //this.productService = productService;
            //this.planMasterRepo = planMasterRepo;
            //this.productMasterRepo = productMasterRepo;
            //this.campaignMasterRepo = campaignMasterRepo;
            //this.eventLog = eventLog;
        }

        #endregion Initialization

        /// <summary>
        /// 保險公司設定-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">保險公司設定-查詢頁-查詢資料 API</param>
        /// <returns>保險公司設定-查詢頁-查詢資料 API</returns>
        public DataTableQueryResp<CompanySearchResp> Search(CompanySearchReq req)
        {
            try
            {
                var memberList = this.companyProfileRepo.Queryable()
                .QueryItem(o => o.CompanyCode.Contains(req.CompanyCode), req.CompanyCode)
                .QueryItem(o => (o.CompanyShortName.Contains(req.CompanyName) || o.CompanyName.Contains(req.CompanyName))
                , req.CompanyName) //	公司簡稱 	, 公司全名 模糊查詢
                .OrderBySortInfo(req.order, req.columns);

                // 資料庫總筆數
                var totalCount = memberList.Count();


                // 分頁後結果
                var res = memberList.Pagination(req.length, req.start).ToList();

                var m = this.mapper.Map<List<CompanySearchResp>>(res);

                return new DataTableQueryResp<CompanySearchResp>(m, totalCount);
            }
            catch (Exception ex)
            {
                //eventLog.Debug("CompanyService", "Search", "Search", ex);
                throw;
            }
        }

        /// <summary>
        /// 保險公司設定-新增頁-新增 API
        /// </summary>
        /// <param name="req">保險公司設定-新增頁-新增 API</param>
        /// <returns>True</returns>
        public async Task<bool> Insert(InsertEditReq req)
        {
            //檢查保險公司主檔是否有重複資料
            var compony = this.companyProfileRepo.Queryable()
            .Where(o => o.CompanyCode == req.CompanyCode).FirstOrDefault();
            if (compony != null) throw new BusinessException($"{req.CompanyCode} {req.CompanyShortName} 已存在，無法新增。");

            //檢查覆核記錄檔 是否有覆核中的方案資料
            var record = this.approveRecordRepo.Queryable()
            .Where(o =>
            o.FunctionCode == FunctionCodeType.F1301 &&
            o.ApproveKey == req.CompanyCode &&
            o.ApproveStatus == ApproveStatusType.WaitApprove &&
            o.ModifyType == ModifyType.Insert).FirstOrDefault();
            if (record != null) throw new BusinessException($"{req.CompanyCode} {req.CompanyShortName} 已有新增等待覆核中，無法新增。");

            var guid = System.Guid.NewGuid();

            //新增資料至 覆核記錄檔
            var arp = new ApproveRecord()
            {
                FunctionCode = FunctionCodeType.F1301,
                ApproveKey = req.CompanyCode,
                ApproveItem = req.CompanyCode + " " + req.CompanyShortName,
                ModifyType = ModifyType.Insert,//新增
                ModifyBy = WebContext.Account,
                ModifyOn = DateTime.Now,
                ApproveStatus = ApproveStatusType.WaitApprove,//尚未覆核
                ApproveRecordGid = guid
            };

            //新增資料至 保險公司覆核主檔
            var acp = new ApproveCompanyProfile()
            {
                CompanyCode = req.CompanyCode,
                CompanyName = req.CompanyName,
                CompanyShortName = req.CompanyShortName,
                CompanyType = req.CompanyType,
                CompanyTxid = req.CompanyTXID,
                CompanyPhoneAreaCode = req.CompanyPhoneAreaCode,
                CompanyPhoneLastCode = req.CompanyPhoneLastCode,
                CompanyEmail = req.CompanyEmail,
                CreateBy = WebContext.Account,
                CreateOn = DateTime.Now,
                ApproveRecordGid = guid
            };

            this.approveRecordRepo.Insert(arp);
            await this.logUnitOfWork.SaveChangesAsync();
            this.approveCompanyProfileRepo.Insert(acp);
            await this.unitOfWork.SaveChangesAsync();

            return true;
        }


        /// <summary>
        /// 保險公司設定-查詢頁-編輯前檢驗 API
        /// </summary>
        /// <param name="req">Insure 共用 req</param>
        /// <returns>True</returns>
        public bool EditCheck(CompanyReq req)
        {
            //公司資料
            var company = this.companyProfileRepo.Queryable()
           .Where(o => o.CompanyCode == req.CompanyCode).FirstOrDefault();

            //檢查 覆核記錄檔 是否有覆核中的方案資料
            var record = this.approveRecordRepo.Queryable()
            .Where(o => o.FunctionCode == FunctionCodeType.F1301 && o.ApproveKey == req.CompanyCode && o.ApproveStatus == ApproveStatusType.WaitApprove).FirstOrDefault();
            if (record != null) throw new BusinessException($"{req.CompanyCode} {company.CompanyShortName} 已被異動等待覆核中，無法編輯。");

            return true;
        }

        /// <summary>
        /// 保險公司設定-編輯頁-確認修改 API
        /// </summary>
        /// <param name="req">保險公司設定-新增頁-新增 API</param>
        /// <returns>True</returns>
        public async Task<bool> ConfirmEdit(InsertEditReq req)
        {
            //檢查 覆核記錄檔 是否有覆核中的方案資料
            var record = this.approveRecordRepo.Queryable()
            .Where(o => o.FunctionCode == FunctionCodeType.F1301 && o.ApproveKey == req.CompanyCode && o.ApproveStatus == ApproveStatusType.WaitApprove).FirstOrDefault();
            if (record != null) throw new BusinessException($"{req.CompanyCode} {req.CompanyShortName} 已被異動等待覆核中，無法編輯。");

            var guid = System.Guid.NewGuid();

            //新增資料至 覆核記錄檔
            var arp = new ApproveRecord()
            {
                FunctionCode = FunctionCodeType.F1301,
                ApproveKey = req.CompanyCode,
                ApproveItem = req.CompanyCode + " " + req.CompanyShortName,
                ModifyType = ModifyType.Modfiy,//修改
                ModifyBy = WebContext.Account,
                ModifyOn = DateTime.Now,
                ApproveStatus = ApproveStatusType.WaitApprove,//尚未覆核,
                ApproveRecordGid = guid
            };

            //新增資料至 保險公司覆核主檔
            var acp = new ApproveCompanyProfile()
            {
                CompanyCode = req.CompanyCode,
                CompanyName = req.CompanyName,
                CompanyShortName = req.CompanyShortName,
                CompanyType = req.CompanyType,
                CompanyTxid = req.CompanyTXID,
                CompanyPhoneAreaCode = req.CompanyPhoneAreaCode,
                CompanyPhoneLastCode = req.CompanyPhoneLastCode,
                CompanyEmail = req.CompanyEmail,
                CreateBy = WebContext.Account,
                CreateOn = DateTime.Now,
                ApproveRecordGid = guid
            };

            this.approveRecordRepo.Insert(arp);
            await this.logUnitOfWork.SaveChangesAsync();
            this.approveCompanyProfileRepo.Insert(acp);
            await this.unitOfWork.SaveChangesAsync();

            return true;
        }


        /// <summary>
        /// 保險公司設定-查詢頁-刪除 API
        /// </summary>
        /// <param name="req">Insure 共用 req</param>
        /// <returns>True</returns>
        public async Task<bool> Delete(CompanyReq req)
        {
            //公司資料
            var company = this.companyProfileRepo.Queryable()
           .Where(o => o.CompanyCode == req.CompanyCode).FirstOrDefault();

            #region 是否可刪除商品
            //刪除前檢核
            this.CheckValidateForDelete(req.CompanyCode);
            #endregion

            #region 檢核是否有覆核紀錄

            //檢查 覆核記錄檔 是否有覆核中的保險公司資料
            var companyRecord = this.approveRecordRepo.Queryable()
            .Where(o =>
            o.FunctionCode == FunctionCodeType.F1301 &&
            o.ApproveKey == req.CompanyCode &&
            o.ApproveStatus == ApproveStatusType.WaitApprove).ToList();

            if (companyRecord.Any()) throw new BusinessException($"{req.CompanyCode} {company.CompanyShortName} 已被異動等待覆核中，無法刪除。");

            #endregion

            var guid = System.Guid.NewGuid();

            //新增資料至 覆核記錄檔
            var arp = new ApproveRecord()
            {
                FunctionCode = FunctionCodeType.F1301,
                ApproveKey = req.CompanyCode,
                ApproveItem = req.CompanyCode + " " + company.CompanyShortName,
                ModifyType = ModifyType.Delete,//刪除
                ModifyBy = WebContext.Account,
                ModifyOn = DateTime.Now,
                ApproveStatus = ApproveStatusType.WaitApprove,//尚未覆核
                ApproveRecordGid = guid
            };

            //新增資料至 保險公司覆核主檔
            var acp = new ApproveCompanyProfile()
            {
                CompanyCode = req.CompanyCode,
                CompanyName = company.CompanyName,
                CompanyShortName = company.CompanyShortName,
                CompanyType = company.CompanyType,
                CompanyTxid = company.CompanyTxid,
                CompanyPhoneAreaCode = company.CompanyPhoneAreaCode,
                CompanyPhoneLastCode = company.CompanyPhoneLastCode,
                CompanyEmail = company.CompanyEmail,
                CreateBy = WebContext.Account,
                CreateOn = DateTime.Now,
                ApproveRecordGid = guid
            };

            this.approveRecordRepo.Insert(arp);
            this.approveCompanyProfileRepo.Insert(acp);
            await this.logUnitOfWork.SaveChangesAsync();
            await this.unitOfWork.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// 保險公司設定-查詢頁-刪除前檢核
        /// </summary>
        /// <param name="companyCode">保險公司代碼</param>
        public void CheckValidateForDelete(string companyCode)
        {
            //公司資料
            var company = this.companyProfileRepo.Queryable()
           .Where(o => o.CompanyCode == companyCode).FirstOrDefault();

            // 檢查『專案主檔』、『方案主檔』是否有欲刪除的保險公司資料
            //var productList = this.productMasterRepo.Queryable()
            //.Where(o => o.CompanyCode == companyCode).ToList();

            //var projectList = this.campaignMasterRepo.Queryable()
            //.Where(o => o.CompanyCode == companyCode).ToList();

            //if (productList.Any() || projectList.Any()) throw new BusinessException($"{company.CompanyCode} {company.CompanyShortName} 在方案設定有設定資料，無法刪除。");

            //檢查『商品主檔』是否有欲刪除的保險公司資料
            //var palnList = this.planMasterRepo.Queryable()
            //.Where(o => o.CompanyCode == companyCode).ToList();

            //if (palnList.Any()) throw new BusinessException($"{company.CompanyCode} {company.CompanyShortName} 在商品管理設定有設定資料，無法刪除。");

            //檢查 覆核記錄檔 是否有覆核中的商品資料
            var planRecord = this.approveRecordRepo.Queryable()
            .Where(o =>
            o.FunctionCode == FunctionCodeType.F1401 &&
            o.ApproveKey.Contains(companyCode) &&
            o.ApproveStatus == ApproveStatusType.WaitApprove).ToList();

            if (planRecord.Any()) throw new BusinessException($"{companyCode} {company.CompanyShortName} 有商品已被異動等待覆核，無法刪除。");
        }

        /// <summary>
        /// 保險公司設定-複製/編輯頁/明細頁(公司資訊)-查詢資料 API
        /// </summary>
        /// <param name="req">Insure 共用 req</param>
        /// <returns>保險公司設定-複製/編輯頁/明細頁(公司資訊)-查詢資料 API Resp</returns>
        public CompanyDetailResp Detail(CompanyReq req)
        {
            var companyList = this.companyProfileRepo.Queryable()
            .Where(o => o.CompanyCode == req.CompanyCode).FirstOrDefault();

            var res = new CompanyDetailResp();

            res.CompanyCode = companyList.CompanyCode;
            res.CompanyName = companyList.CompanyName;
            res.CompanyShortName = companyList.CompanyShortName;
            res.CompanyType = companyList.CompanyType;
            //res.CompanyTypeStr = this.commonService.GetSysParamValue(GroupIdType.CompanyType, companyList.CompanyType);
            res.CompanyTXID = companyList.CompanyTxid;
            res.CompanyPhoneAreaCode = companyList.CompanyPhoneAreaCode;
            res.CompanyPhoneLastCode = companyList.CompanyPhoneLastCode;
            res.CompanyEmail = companyList.CompanyEmail;

            return res;
        }

        /// <summary>
        /// 保險公司設定-覆核頁(公司資訊)-查詢資料API
        /// </summary>
        /// <param name="approveRecordGid">覆核記錄檔 Gid</param>
        /// <returns>保險公司設定-複製/編輯頁/明細頁(公司資訊)-查詢資料 API Resp</returns>
        public CompanyDetailResp ApproveDetail(Guid approveRecordGid)
        {
            var companyList = this.approveCompanyProfileRepo.Queryable()
            .Where(o => o.ApproveRecordGid == approveRecordGid).FirstOrDefault();

            var res = new CompanyDetailResp();

            res.CompanyCode = companyList.CompanyCode;
            res.CompanyName = companyList.CompanyName;
            res.CompanyShortName = companyList.CompanyShortName;
            res.CompanyType = companyList.CompanyType;
            //res.CompanyTypeStr = this.commonService.GetSysParamValue(GroupIdType.CompanyType, companyList.CompanyType);
            res.CompanyTXID = companyList.CompanyTxid;
            res.CompanyPhoneAreaCode = companyList.CompanyPhoneAreaCode;
            res.CompanyPhoneLastCode = companyList.CompanyPhoneLastCode;
            res.CompanyEmail = companyList.CompanyEmail;

            return res;
        }

        /// <summary>
        /// 覆核-明細頁-覆核 API
        /// </summary>
        /// <returns>覆核結果(true:成功;false:失敗)</returns>
        public async Task<bool> Approve(ApproveReq req)
        {
            try
            {
                var a = this.approveRecordRepo.Queryable();

                var record = this.approveRecordRepo.Queryable()
                    .Where(o => o.FunctionCode == req.FunctionCode &&
                    o.ApproveRecordGid == req.ApproveRecordGid &&
                    o.ApproveStatus == ApproveStatusType.WaitApprove)
                    .FirstOrDefault();

                if (record == null) throw new BusinessException("覆核狀態已不可更動。");  //防呆

                //if (string.IsNullOrEmpty(WebContext.Account))
                //    throw new Exception("Approve WebContext.Account 為空");

                record.ApproveStatus = ApproveStatusType.ApprovePass; //覆核通過
                record.ApproveBy = WebContext.Account;
                record.ApproveOn = DateTime.Now;

                //保險公司覆核主檔
                var ac = this.approveCompanyProfileRepo.Queryable()
                .Where(o => o.ApproveRecordGid == record.ApproveRecordGid).SingleOrDefault();
                //保險公司主檔
                var c = this.companyProfileRepo.Queryable()
                .Where(o => o.CompanyCode == record.ApproveKey).SingleOrDefault();

                var cp = new CompanyProfile()
                {
                    CompanyCode = ac.CompanyCode,
                    CompanyName = ac.CompanyName,
                    CompanyShortName = ac.CompanyShortName,
                    CompanyType = ac.CompanyType,
                    CompanyTxid = ac.CompanyTxid,
                    CompanyPhoneAreaCode = ac.CompanyPhoneAreaCode,
                    CompanyPhoneLastCode = ac.CompanyPhoneLastCode,
                    CompanyEmail = ac.CompanyEmail,
                    CreateBy = record.ApproveBy,
                    CreateOn = record.ApproveOn
                };

                switch (record.ModifyType)
                {
                    case ModifyType.Insert: //保險公司覆核主檔新增進保險公司主檔

                        this.companyProfileRepo.Insert(cp);

                        break;

                    case ModifyType.Modfiy:

                        c.CompanyCode = ac.CompanyCode;
                        c.CompanyName = ac.CompanyName;
                        c.CompanyShortName = ac.CompanyShortName;
                        c.CompanyType = ac.CompanyType;
                        c.CompanyTxid = ac.CompanyTxid;
                        c.CompanyPhoneAreaCode = ac.CompanyPhoneAreaCode;
                        c.CompanyPhoneLastCode = ac.CompanyPhoneLastCode;
                        c.CompanyEmail = ac.CompanyEmail;
                        c.UpdateBy = record.ApproveBy;
                        c.UpdateOn = record.ApproveOn;

                        break;

                    case ModifyType.Delete:

                        this.CheckValidateForDelete(ac.CompanyCode);
                        this.companyProfileRepo.Delete(c);

                        break;
                }
                await this.logUnitOfWork.SaveChangesAsync();
                await this.unitOfWork.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        /// <summary>
        /// 商品管理設定-保險公司下拉選項 API
        /// </summary>
        /// <returns>下拉選項值 API Resp</returns>
        public CommonSelectResp CompanySelectList()
        {
            var result = new CommonSelectResp();
            result.ItemValues = this.companyProfileRepo.Queryable()
                .Select(o => new ItemValue { Key = o.CompanyCode, Value = o.CompanyShortName });

            return result;
        }

        /// <summary>
        /// 保險公司設定-明細頁-查詢上下架專案-方案 API
        /// </summary>
        /// <param name="req">保險公司設定-明細頁-查詢上下架專案-方案 API Req</param>
        /// <returns>保險公司設定-明細頁-查詢上下架專案-方案 API Resp</returns>
        //public DataTableQueryResp<PlanOnlineInfoResp> PlanOnlineInfoSearch(PlanOnlineInfoReq req)
        //{
        //    try
        //    {
        //        // 依 公司代碼 查詢 起專案主檔
        //        IEnumerable<ProductMaster> productMasters = this.productMasterRepo.Queryable()
        //            .Where(o => o.CompanyCode == req.CompanyCode);


        //        //  銷售狀態篩選
        //        if (!string.IsNullOrEmpty(req.SaleState))
        //        {
        //            //1.	未上架：系統日小於「專案銷售起日」
        //            //2.	銷售中：系統日大於等於「專案銷售起日」，且系統日小於等於「專案銷售迄日」
        //            //3.	已停售：系統日大於「商品-銷售起日」
        //            productMasters = productMasters.ToList()
        //                .Where(o => this.commonService.QuerySaleState(req.SaleState, o.ProductStartOn, o.ProductEndOn));
        //        }

        //        var productMasterList = productMasters.ToList();
        //        if (!productMasterList.Any())
        //        {
        //            productMasterList = new List<ProductMaster>();
        //        }

        //        var campaignMasterList = this.campaignMasterRepo.Queryable()
        //            .ToList();

        //        // 方案 與 專案 組合資料
        //        var campaignProductInfoList = campaignMasterList
        //            .Join(productMasterList,
        //            c => c.ProductId,
        //            p => p.ProductId,
        //            (c, p) => new { c, p })
        //            .ToList();

        //        // 組合資料
        //        var planOnlineInfoRes = campaignProductInfoList
        //            .Select(o =>
        //            new PlanOnlineInfoResp
        //            {
        //                ProductCampaignStr = $"{o.p.ProductId} {o.p.Name} / {o.c.Name}",
        //                SaleStateStr = this.commonService
        //                .GetSysParamValue(GroupIdType.SaleState, this.commonService.GetSaleState(o.p.ProductStartOn, o.p.ProductEndOn))
        //            })
        //            .ToList();

        //        // 排序 ,分頁後結果
        //        var res = planOnlineInfoRes.AsQueryable()
        //            .OrderBySortInfo(req.order, req.columns)
        //            .Pagination(req.length, req.start)
        //            .ToList();

        //        // 資料庫總筆數
        //        var totalCount = planOnlineInfoRes.Count();


        //        return new DataTableQueryResp<PlanOnlineInfoResp>(res, totalCount);
        //    }
        //    catch (Exception ex)
        //    {
        //        eventLog.Debug("CompanyService", "PlanOnlineInfoSearch", "PlanOnlineInfoSearch", ex);
        //        throw;
        //    }
        //}
    }
}
