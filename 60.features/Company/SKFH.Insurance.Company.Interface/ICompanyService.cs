﻿using InsureBrick.Modules.Common.Interface.Models;
using InsureBrick.Modules.Company.Interface.Models;
using SKFH.Insurance.domain.Models;
using System;
using System.Threading.Tasks;
using TPI.NetCore.WebAPI.Models;

namespace InsureBrick.Modules.Company.Interface
{
    /// <summary>
    /// CompanyService interface
    /// </summary>
    public interface ICompanyService
    {
        /// <summary>
        /// 保險公司設定-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">保險公司設定-查詢頁-查詢資料 API</param>
        /// <returns>保險公司設定-查詢頁-查詢資料 API</returns>
        DataTableQueryResp<CompanySearchResp> Search(CompanySearchReq req);

        /// <summary>
        /// 保險公司設定-查詢頁-刪除 API
        /// </summary>
        /// <param name="req">Insure 共用 req</param>
        /// <returns>True</returns>
        Task<bool> Delete(CompanyReq req);

        /// <summary>
        /// 保險公司設定-新增頁-新增 API
        /// </summary>
        /// <param name="req">保險公司設定-新增頁-新增 API</param>
        /// <returns>True</returns>
        Task<bool> Insert(InsertEditReq req);

        /// <summary>
        /// 保險公司設定-查詢頁-編輯前檢驗 API
        /// </summary>
        /// <param name="req">Insure 共用 req</param>
        /// <returns>True</returns>
        bool EditCheck(CompanyReq req);

        /// <summary>
        /// 保險公司設定-編輯頁-確認修改 API
        /// </summary>
        /// <param name="req">保險公司設定-新增頁-新增 API</param>
        /// <returns>True</returns>
        Task<bool> ConfirmEdit(InsertEditReq req);

        /// <summary>
        /// 保險公司設定-複製/編輯頁/明細頁(公司資訊)-查詢資料 API
        /// </summary>
        /// <param name="req">Insure 共用 req</param>
        /// <returns>保險公司設定-複製/編輯頁/明細頁(公司資訊)-查詢資料 API Resp</returns>
        CompanyDetailResp Detail(CompanyReq req);

        /// <summary>
        /// 保險公司設定-覆核頁(公司資訊)-查詢資料API
        /// </summary>
        /// <param name="approveRecordGid">覆核記錄檔 Gid</param>
        /// <returns>保險公司設定-複製/編輯頁/明細頁(公司資訊)-查詢資料 API Resp</returns>
        CompanyDetailResp ApproveDetail(Guid approveRecordGid);

        /// <summary>
        /// 覆核-明細頁(公司資訊)-覆核 API
        /// </summary>
        /// <param name="approveRecordGid">審核Request</param>
        /// <returns>覆核-明細頁(公司資訊)-覆核 API Resp</returns>
        Task<bool> Approve(ApproveReq req);


        /// <summary>
        /// 保險公司設定-明細頁-查詢上下架專案-方案 API
        /// </summary>
        /// <param name="req">保險公司設定-明細頁-查詢上下架專案-方案 API Req</param>
        /// <returns>保險公司設定-明細頁-查詢上下架專案-方案 API Resp</returns>
        //DataTableQueryResp<PlanOnlineInfoResp> PlanOnlineInfoSearch(PlanOnlineInfoReq req);

        /// <summary>
        /// 保險公司設定-查詢頁-刪除前檢核
        /// </summary>
        /// <param name="companyCode">保險公司代碼</param>
        void CheckValidateForDelete(string companyCode);


        /// <summary>
        /// 商品管理設定-保險公司下拉選項 API
        /// </summary>
        /// <returns>下拉選項值 API Resp</returns>
        CommonSelectResp CompanySelectList();
    }
}
