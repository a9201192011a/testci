using AutoMapper;
using InsureBrick.Modules.Company.Interface.Models;
using SKFH.Insurance.Company.Entity.Models;

namespace InsureBrick.Modules.Insurer.Interface
{
    /// <summary>
    /// AutoMapper Mapping Profiles
    /// </summary>
    public class MappingProfiles : Profile
    {
        /// <summary>
        /// Create AutoMapper Mapping Profiles
        /// </summary>
        public MappingProfiles()
        {
            CreateMap<CompanyProfile, CompanySearchResp>();
        }
    }
}