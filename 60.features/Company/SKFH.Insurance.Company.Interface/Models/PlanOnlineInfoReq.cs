﻿using TPI.NetCore.WebAPI.Models;

namespace InsureBrick.Modules.Company.Interface.Models
{
    /// <summary>
    /// 保險公司設定-明細頁-查詢上下架專案-方案 API Req
    /// </summary>
    public class PlanOnlineInfoReq : DataTableQueryReq
    {
        /// <summary>
        /// 保險公司代碼
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 銷售狀態
        /// </summary>
        public string SaleState { get; set; }

    }
}
