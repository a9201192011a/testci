namespace InsureBrick.Modules.Company.Interface.Models
{
    /// <summary>
    /// 保險公司設定-新增頁-新增 API
    /// </summary>
    public class InsertEditReq
    {
        /// <summary>
        /// 公司代碼
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 公司名稱
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 公司簡稱
        /// </summary>
        public string CompanyShortName { get; set; }

        /// <summary>
        /// 公司簡稱 "類別(SysParam) P 產險 L 壽險"
        /// </summary>
        public string CompanyType { get; set; }

        /// <summary>
        /// 公司統編
        /// </summary>
        public string CompanyTXID { get; set; }

        /// <summary>
        /// 公司電話區碼
        /// </summary>
        public string CompanyPhoneAreaCode { get; set; }

        /// <summary>
        /// 公司電話
        /// </summary>
        public string CompanyPhoneLastCode { get; set; }

        /// <summary>
        /// 公司信箱
        /// </summary>
        public string CompanyEmail { get; set; }
    }
}