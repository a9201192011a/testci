﻿namespace InsureBrick.Modules.Company.Interface.Models
{
    /// <summary>
    /// 保險公司設定-明細頁-查詢上下架專案-方案 API Resp
    /// </summary>
    public class PlanOnlineInfoResp
    {
        /// <summary>
        /// 投保專案/方案
        /// </summary>
        public string ProductCampaignStr { get; set; }


        /// <summary>
        /// 銷售狀態
        /// </summary>
        public string SaleStateStr { get; set; }
    }
}
