﻿namespace InsureBrick.Modules.Company.Interface.Models
{
    /// <summary>
    /// 保險公司設定-查詢頁-查詢資料 API Resp
    /// </summary>
    public class CompanySearchResp
    {
        /// <summary>
        /// 公司代碼
        /// </summary>
        public string companyCode { get; set; }

        /// <summary>
        /// 公司簡稱
        /// </summary>
        public string companyShortName { get; set; }

        /// <summary>
        /// 公司名稱 / 公司全名
        /// </summary>
        public string companyName { get; set; }
    }
}
