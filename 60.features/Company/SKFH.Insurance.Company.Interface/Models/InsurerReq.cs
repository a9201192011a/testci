namespace InsureBrick.Modules.Company.Interface.Models
{
    /// <summary>
    /// Insure 共用 req
    /// </summary>
    public class CompanyReq
    {
        /// <summary>
        /// 公司代碼
        /// </summary>
        public string CompanyCode { get; set; }
    }
}