﻿using TPI.NetCore.WebAPI.Models;

namespace InsureBrick.Modules.Company.Interface.Models
{
    /// <summary>
    /// 保險公司設定-查詢頁-查詢資料 API Req
    /// </summary>
    public class CompanySearchReq : DataTableQueryReq
    {
        /// <summary>
        /// 公司代碼
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 公司名稱 / 公司全名
        /// </summary>
        public string CompanyName { get; set; }
    }
}
