using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using coreLibs.DateAccess;
using SKFH.Insurance.entity.Models;
using SKFH.Insurance.entity.Repositories;
using SKFH.Insurance.iTemplate;

namespace SKFH.Insurance.Template
{
    public class TemplateService : ITemplateService
    {
        private readonly IUserRepository userRepo;
        private readonly IUserGroupRepository userGroupRepo;

        public TemplateService(IUserRepository userRepo, IUserGroupRepository userGroupRepo)
        {
            this.userRepo = userRepo;
            this.userGroupRepo = userGroupRepo;
        }

        public async Task<IEnumerable<User>> GetUserAsync()
        {
            var user = await this.userRepo.GetAllAsync();
            return user;
        }

        public async Task<bool> AddUserAsync()
        {
            await this.userRepo.AddAsync(new entity.Models.User()
            {
                EmpNo = "0006",
                Name = "my",
                CreateTime = DateTime.Now,
            });

            //await this.userGroupRepo.AddAsync(new entity.Models.UserGroup()
            //{
            //    GroupNo = "G03",
            //    GroupName = "GGG222",
            //    CreateTime = DateTime.Now,
            //});

            return true;
        }

        public async Task<bool> UpdateUserAsync()
        {
            var entities = await this.userRepo.GetAllAsync();
            entities.First().UpdateTime = DateTime.Now;
            entities.First().Name = "Test";
            await this.userRepo.UpdateAsync(entities.FirstOrDefault());

            return  true;
        }

        public async Task<bool> DeleteUserAsync()
        {
            var entities = await this.userRepo.GetAllAsync();
            await this.userRepo.DeleteAsync(entities.FirstOrDefault());

            return true;
        }

    }
}
