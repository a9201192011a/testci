using SKFH.Insurance.entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKFH.Insurance.iTemplate
{
    public interface ITemplateService
    {
        Task<IEnumerable<User>> GetUserAsync();
        Task<bool> AddUserAsync();
        Task<bool> UpdateUserAsync();
        Task<bool> DeleteUserAsync();
    }
}
