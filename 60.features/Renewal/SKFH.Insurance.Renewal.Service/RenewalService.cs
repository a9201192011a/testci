﻿using Autofac;
using coreLibs.DateAccess;
using InsureBrick.Modules.Common.Interface;
using InsureBrick.Modules.Renewal.Interface;
using InsureBrick.Modules.Renewal.Interface.Models;
using SKFH.Insurance.Company.Entity.Models;
using SKFH.Insurance.Company.Entity.Repository;
using SKFH.Insurance.entity.Repositories;
using SKFH.Insurance.Log.Entity.Models;
using SKFH.Insurance.Log.Entity.Repository;
using SKFH.Insurance.Product.Entity.Models;
using SKFH.Insurance.Product.Entity.Repository;
using SKFH.Insurance.Renewal.Entity.Models;
using TPI.NetCore;
using TPI.NetCore.Extensions;
using TPI.NetCore.Models;
using TPI.NetCore.WebAPI.Models;
using TPI.NetCore.WebContext;

namespace InsureBrick.Modules.Renewal.Service
{
    /// <summary>
    /// 續保管理
    /// </summary>
    public class RenewalService : IRenewalService
    {
        #region Initialization

        private readonly IUnitOfWork renewalUnitOfWork;
        private readonly ICommonService commonService;
        private readonly IRenewalSettingRepository renewalSettingRepo;
        private readonly ICompanyProfileRepository companyProfileRepo;
        private readonly ICampaignMasterRepository campaignMasterRepo;
        private readonly IProductMasterRepository productMasterRepo;
        private readonly IPolicyMasterRepository policyMasterRepo;
        private readonly IPolicyPersonInfoRepository policyPersonInfoRepo;
        private readonly IApproveRecordRepository approveRecordRepo;
        private readonly IApproveRenewalSettingRepository approveRenewalSettingRepo;
        //private readonly IeventLogService //eventLog;

        public RenewalService(
            ICommonService commonService, 
            IComponentContext componentContext,
            IRenewalSettingRepository renewalSettingRepo,
            ICompanyProfileRepository companyProfileRepo,
            ICampaignMasterRepository campaignMasterRepo,
            IProductMasterRepository productMasterRepo,
            IPolicyMasterRepository policyMasterRepo,
            IPolicyPersonInfoRepository policyPersonInfoRepo,
            IApproveRecordRepository approveRecordRepo,
            IApproveRenewalSettingRepository approveRenewalSettingRepo
            //IeventLogService eventLog
            )
        {
            this.renewalUnitOfWork = componentContext.ResolveNamed<IUnitOfWork>("Renewal");
            this.commonService = commonService;
            this.renewalSettingRepo = renewalSettingRepo;
            this.companyProfileRepo = companyProfileRepo;
            this.campaignMasterRepo = campaignMasterRepo;
            this.productMasterRepo = productMasterRepo;
            this.policyMasterRepo = policyMasterRepo;
            this.policyPersonInfoRepo = policyPersonInfoRepo;
            this.approveRecordRepo = approveRecordRepo;
            this.approveRenewalSettingRepo = approveRenewalSettingRepo;
            //this.eventLog = eventLog;
        }

        #endregion Initialization

        /// <summary>
        /// 續保通知參數設定-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">續保通知參數查詢 Req</param>
        /// <returns>續保通知參數查詢 Resp</returns>
        public DataTableQueryResp<SearchResp> Search(SearchReq req)
        {
            try
            {
                var renewalSetting = this.renewalSettingRepo.Queryable()
                .QueryItem(o => o.CompanyCode == req.CompanyCode, req.CompanyCode);

                // 去除重複
                var renewalSettingDistinct = (from rs in renewalSetting
                                              group rs by new { rs.CompanyCode, rs.ProductId, rs.ProjectCode } into grp
                                              select new
                                              {
                                                  grp.Key.CompanyCode,
                                                  grp.Key.ProductId,
                                                  grp.Key.ProjectCode
                                              }).Distinct();

                // 資料庫總筆數
                var totalCount = renewalSettingDistinct.Count();

                var joinTable = from rs in renewalSettingDistinct
                                join cp in this.companyProfileRepo.Queryable() on rs.CompanyCode equals cp.CompanyCode into cpl
                                from cp in cpl.DefaultIfEmpty()
                                join pm in this.productMasterRepo.Queryable() on new { rs.CompanyCode, rs.ProductId } equals new { pm.CompanyCode, pm.ProductId } into pml
                                from pm in pml.DefaultIfEmpty()
                                join cm in this.campaignMasterRepo.Queryable() on new { rs.CompanyCode, rs.ProductId, rs.ProjectCode } equals new { cm.CompanyCode, cm.ProductId, cm.ProjectCode } into cml
                                from cm in cml.DefaultIfEmpty()
                                select new SearchResp
                                {
                                    CompanyCodeStr = rs.CompanyCode + " " + cp.CompanyName,
                                    ProductAndProject = (rs.ProductId ?? string.Empty) + " " + (pm.Name ?? string.Empty) + "/" + (cm.Name ?? string.Empty),
                                    CompanyCode = rs.CompanyCode,
                                    ProductId = rs.ProductId,
                                    ProjectCode = rs.ProjectCode,
                                    SaleStateStr = DateTime.Today < pm.ProductStartOn ? "未上架" : //系統日小於專案銷售起日，顯示：未上架
                                       DateTime.Today >= pm.ProductStartOn && DateTime.Today <= pm.ProductEndOn.AddDays(1).AddSeconds(-1) ? "銷售中" : //系統日大於等於專案銷售起日，且系統日小於等於專案銷售迄日 ，顯示：	銷售中
                                       DateTime.Today > pm.ProductEndOn.AddDays(1).AddSeconds(-1) ? "已停售" : //系統日大於專案銷售迄日 ，顯示：已停售
                                       string.Empty
                                };


                var res = joinTable
                .OrderBySortInfo(req.order, req.columns)
                .Pagination(req.length, req.start);

                return new DataTableQueryResp<SearchResp>(res, totalCount);
            }
            catch (Exception ex)
            {
                //eventLog.Debug("RenewalService", "Search", "Search", ex);
                throw;
            }
        }

        /// <summary>
        /// 續保資料查詢-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">續保資料查詢 Req</param>
        /// <returns>續保資料查詢 Resp</returns>
        public DataTableQueryResp<PolicySearchResp> PolicySearch(PolicySearchReq req)
        {
            this.commonService.DateCheck(req.PolicyPeriodStart, req.PolicyPeriodEnd, "到期日期", true);

            var policyMasterList = this.policyMasterRepo.Queryable()
            .Where(o => !string.IsNullOrEmpty(o.PolicyNo))
            .Where(o => o.ExtendStatus == ExtendStatus.Enable) //可續保的資料
            .QueryItem(o => o.PolicyPeriodE >= Convert.ToDateTime(req.PolicyPeriodStart), req.PolicyPeriodStart)
            .QueryItem(o => o.PolicyPeriodE <= (Convert.ToDateTime(req.PolicyPeriodEnd).AddDays(1).AddSeconds(-1)), req.PolicyPeriodEnd);

            //要保人資料
            var applicantInfos = this.policyPersonInfoRepo.Queryable()
                .Where(x => x.PersonType == PersonType.Applicant);

            var joinTable = from plm in policyMasterList
                            join prm in this.productMasterRepo.Queryable() on plm.ProductId equals prm.ProductId into prml //專案資訊
                            from prm in prml.DefaultIfEmpty()
                            join cp in this.companyProfileRepo.Queryable() on prm.CompanyCode equals cp.CompanyCode into cpl //公司名稱
                            from cp in cpl.DefaultIfEmpty()
                            join ap in applicantInfos on plm.PolicyNo equals ap.PolicyNo into apl //要保人資料
                            from ap in apl.DefaultIfEmpty()
                            join cm in this.campaignMasterRepo.Queryable() on new { prm.CompanyCode, plm.ProductId, plm.ProjectCode } equals new { cm.CompanyCode, cm.ProductId, cm.ProjectCode } into cml //方案資訊
                            from cm in cml.DefaultIfEmpty()
                            select new
                            {
                                CompanyCodeStr = prm.CompanyCode + " " + cp.CompanyShortName,
                                PolicyNo = plm.PolicyNo,
                                ApplicantName = ap.PersonName,
                                ProductAndProject = (plm.ProductId ?? string.Empty) + " " + (prm.Name ?? string.Empty) + "/" + (cm.Name ?? string.Empty),
                                PolicyPeriodE = plm.PolicyPeriodE,
                                ModalPermiumWithDisc = plm.ModalPermiumWithDisc,
                                ProjectCode = cm.ProjectCode
                            };

            //如果req有值
            if (!string.IsNullOrEmpty(req.CompanyCode))
            {
                joinTable = joinTable.Where(o => o.CompanyCodeStr.Contains(req.CompanyCode));
            }

            if (!string.IsNullOrEmpty(req.ProductId))
            {
                joinTable = joinTable.Where(o => o.ProductAndProject.Contains(req.ProductId));
            }

            if (!string.IsNullOrEmpty(req.ProjectCode))
            {
                joinTable = joinTable.Where(o => o.ProjectCode.Contains(req.ProjectCode));
            }

            // 資料庫總筆數
            var totalCount = joinTable.Count();

            var res = joinTable
            .OrderBySortInfo(req.order, req.columns)
            .Pagination(req.length, req.start)
            .Select(o =>
            new PolicySearchResp
            {
                CompanyCodeStr = o.CompanyCodeStr,
                PolicyNo = o.PolicyNo,
                ApplicantName = o.ApplicantName,
                ProductAndProject = o.ProductAndProject,
                PolicyPeriodE = o.PolicyPeriodE.HasValue ? o.PolicyPeriodE.Value.ToString("yyyy/MM/dd") : string.Empty,
                ModalPermiumWithDisc = o.ModalPermiumWithDisc.HasValue ? o.ModalPermiumWithDisc.Value.ToString() : string.Empty,
                ProjectCode = o.ProjectCode
            });

            return new DataTableQueryResp<PolicySearchResp>(res.ToList(), totalCount);
        }

        /// <summary>
        /// 續保通知參數設定-明細頁-明細 API
        /// </summary>
        /// <param name="req">續保通知參數明細 Req</param>
        /// <returns>續保通知參數 Resp</returns>
        public async Task<DetailResp> Detail(DetailReq req)
        {
            var res = new DetailResp();

            //公司資訊
            var companyInfo = this.GetCompanyInfo(req.CompanyCode);
            //專案資訊
            var productInfo = this.GetProductInfo(req.CompanyCode, req.ProductId);
            //方案資訊
            var projectInfo = this.GetProjectInfo(req.CompanyCode, req.ProductId, req.ProjectCode);

            res.CompanyCodeStr = $"{req.CompanyCode} {companyInfo?.CompanyName}";
            res.ProductIdStr = $"{req.ProductId} {productInfo?.Name}";
            res.ProjectCodeStr = $"{req.ProjectCode} {projectInfo?.Name}";

            //把一樣的通知頻率/日期拿出來
            var renewals = this.renewalSettingRepo.Queryable()
            .Where(o => o.CompanyCode == req.CompanyCode && o.ProductId == req.ProductId && o.ProjectCode == req.ProjectCode)
            .ToList();

            var renewalGroups = renewals.GroupBy(x => new { x.SettingDays, x.SendKind })
            .Select(x => x.Key)
            .OrderBy(o => o.SettingDays);

            var settingList = new List<Setting>();

            foreach (var r in renewalGroups)
            {
                //加頻率/日期條件
                var rrenewals = renewals
                .Where(o => o.SendKind == r.SendKind && o.SettingDays == r.SettingDays)
                .ToList();

                var methodList = new List<Method>();
                var method = new Method();
                var umsType = new List<string>();

                foreach (var rr in rrenewals)
                {
                    switch (rr.UmsNoticeType)
                    {
                        case UMSNoticeType.Email:
                            var email = new NoticeByEmail()
                            {
                                IsCheck = rr?.UmsNoticeStatus == "1" ? true : false,
                                UmsId = rr?.Umsid,
                                Key = rr.UmsNoticeType
                            };
                            method.NoticeByEmail = email;
                            break;

                        case UMSNoticeType.Msg:
                            var msg = new NoticeByMsg()
                            {
                                IsCheck = rr?.UmsNoticeStatus == "1" ? true : false,
                                UmsId = rr?.Umsid,
                                Key = rr.UmsNoticeType
                            };
                            method.NoticeByMsg = msg;
                            break;

                        case UMSNoticeType.AOA:
                            var aoa = new NoticeByAoa()
                            {
                                IsCheck = rr?.UmsNoticeStatus == "1" ? true : false,
                                UmsId = rr?.Umsid,
                                Key = rr.UmsNoticeType
                            };
                            method.NoticeByAoa = aoa;
                            break;

                        case UMSNoticeType.Push:
                            var push = new NoticeByPush()
                            {
                                IsCheck = rr?.UmsNoticeStatus == "1" ? true : false,
                                UmsId = rr?.Umsid,
                                Key = rr.UmsNoticeType
                            };
                            method.NoticeByPush = push;
                            break;
                    }

                    umsType.Add(rr.UmsNoticeType);
                }

                //如果遇到沒有的 UMSNoticeType method 裡會是 null，但一定要回傳 key
                if (!umsType.Contains(UMSNoticeType.Email))
                {
                    method.NoticeByEmail = new NoticeByEmail()
                    {
                        Key = UMSNoticeType.Email
                    };
                }
                if (!umsType.Contains(UMSNoticeType.Msg))
                {
                    method.NoticeByMsg = new NoticeByMsg()
                    {
                        Key = UMSNoticeType.Msg
                    };
                }
                if (!umsType.Contains(UMSNoticeType.AOA))
                {
                    method.NoticeByAoa = new NoticeByAoa()
                    {
                        Key = UMSNoticeType.AOA
                    };
                }
                if (!umsType.Contains(UMSNoticeType.Push))
                {
                    method.NoticeByPush = new NoticeByPush()
                    {
                        Key = UMSNoticeType.Push
                    };
                }

                methodList.Add(method);
                var setting = new Setting()
                {
                    Freq = $"{this.commonService.GetSysParamValue("SendKind", r.SendKind)} {r.SettingDays} 天",
                    Method = methodList

                };

                settingList.Add(setting);

            }

            res.Setting = settingList;

            return res;
        }


        /// <summary>
        /// 續保通知參數設定-修改頁-查詢修改 API
        /// </summary>
        /// <param name="req">續保通知參數明細 Req</param>
        /// <returns>續保通知參數編輯 Resp</returns>
        public async Task<EditResp> Edit(DetailReq req)
        {
            var res = new EditResp();

            res.CompanyCode = req.CompanyCode;
            res.ProductId = req.ProductId;
            res.ProjectCode = req.ProjectCode;

            //把一樣的通知頻率/日期拿出來
            var renewals = this.renewalSettingRepo.Queryable()
            .Where(o => o.CompanyCode == req.CompanyCode && o.ProductId == req.ProductId && o.ProjectCode == req.ProjectCode)
            .ToList();

            var renewalGroups = renewals.GroupBy(x => new { x.SettingDays, x.SendKind })
            .Select(x => x.Key)
            .OrderBy(o => o.SettingDays);

            var settingList = new List<EditSetting>();

            foreach (var r in renewalGroups)
            {
                //加頻率/日期條件
                var rrenewals = renewals
                .Where(o => o.SendKind == r.SendKind && o.SettingDays == r.SettingDays)
                .ToList();

                var methodList = new List<EditMethod>();
                var method = new EditMethod();
                var umsType = new List<string>();

                foreach (var rr in rrenewals)
                {
                    switch (rr.UmsNoticeType)
                    {
                        case UMSNoticeType.Email:
                            var email = new EditNoticeByEmail()
                            {
                                IsCheck = rr?.UmsNoticeStatus == "1" ? true : false,
                                UmsId = rr?.Umsid,
                                Key = rr.UmsNoticeType
                            };
                            method.NoticeByEmail = email;
                            break;

                        case UMSNoticeType.Msg:
                            var msg = new EditNoticeByMsg()
                            {
                                IsCheck = rr?.UmsNoticeStatus == "1" ? true : false,
                                UmsId = rr?.Umsid,
                                Key = rr.UmsNoticeType
                            };
                            method.NoticeByMsg = msg;
                            break;

                        case UMSNoticeType.AOA:
                            var aoa = new EditNoticeByAoa()
                            {
                                IsCheck = rr?.UmsNoticeStatus == "1" ? true : false,
                                UmsId = rr?.Umsid,
                                Key = rr.UmsNoticeType
                            };
                            method.NoticeByAoa = aoa;
                            break;

                        case UMSNoticeType.Push:
                            var push = new EditNoticeByPush()
                            {
                                IsCheck = rr?.UmsNoticeStatus == "1" ? true : false,
                                UmsId = rr?.Umsid,
                                Key = rr.UmsNoticeType
                            };
                            method.NoticeByPush = push;
                            break;
                    }

                    umsType.Add(rr.UmsNoticeType);
                }

                if (!umsType.Contains(UMSNoticeType.Email))
                {
                    method.NoticeByEmail = new EditNoticeByEmail()
                    {
                        Key = UMSNoticeType.Email
                    };
                }
                if (!umsType.Contains(UMSNoticeType.Msg))
                {
                    method.NoticeByMsg = new EditNoticeByMsg()
                    {
                        Key = UMSNoticeType.Msg
                    };
                }
                if (!umsType.Contains(UMSNoticeType.AOA))
                {
                    method.NoticeByAoa = new EditNoticeByAoa()
                    {
                        Key = UMSNoticeType.AOA
                    };
                }
                if (!umsType.Contains(UMSNoticeType.Push))
                {
                    method.NoticeByPush = new EditNoticeByPush()
                    {
                        Key = UMSNoticeType.Push
                    };
                }

                methodList.Add(method);
                var setting = new EditSetting()
                {
                    SendKind = r.SendKind,
                    SettingDays = r.SettingDays,
                    Method = methodList
                };

                settingList.Add(setting);
            }

            res.Setting = settingList;
            return res;
        }

        /// <summary>
        /// 續保通知參數設定-新增頁-新增 API
        /// </summary>
        /// <param name="req"></param>
        /// <returns>True</returns>
        public async Task<bool> Insert(RenewalInsertReq req)
        {
            //檢查方案是否存在
            var renewalSetting = this.renewalSettingRepo.Queryable()
            .Where(o => o.ProjectCode == req.ProjectCode && o.CompanyCode == req.CompanyCode && o.ProductId == req.ProductId).FirstOrDefault();
            //方案資訊
            var projectInfo = this.campaignMasterRepo.Queryable()
            .Where(x => x.ProjectCode == req.ProjectCode && x.CompanyCode == req.CompanyCode).FirstOrDefault();

            if (renewalSetting != null) throw new BusinessException($"{req.ProjectCode} {projectInfo?.Name}已存在。");

            //新增進覆核
            await this.InsertApproveDataToDb(req, ModifyType.Insert);

            return true;
        }

        /// <summary>
        /// 續保通知參數設定-明細頁-編輯前檢驗 API
        /// </summary>
        /// <param name="req">續保通知參數明細 Req</param>
        /// <returns>True</returns>
        public async Task<bool> EditCheck(DetailReq req)
        {
            //方案資訊
            var projectInfo = this.campaignMasterRepo.Queryable()
            .Where(x => x.ProjectCode == req.ProjectCode && x.CompanyCode == req.CompanyCode).FirstOrDefault();

            //檢查『覆核記錄檔』是否有覆核中的方案資料
            var ap = this.approveRecordRepo.Queryable()
            .Where(o => o.FunctionCode == FunctionCodeType.F1A01 &&
            o.ApproveKey == this.GetApproveKey(req.CompanyCode, req.ProductId, req.ProjectCode) &&
            o.ApproveStatus == ApproveStatusType.WaitApprove).FirstOrDefault();

            if (ap != null) throw new BusinessException($"{req.ProjectCode} {projectInfo?.Name}已被異動等待覆核中，無法編輯。");

            return true;
        }

        /// <summary>
        /// 覆核明細
        /// </summary>
        /// <param name="guid">覆核記錄檔 Gid</param>
        /// <returns>覆核明細</returns>
        public ApproveRenewalDetail ApproveRenewalDetail(Guid guid)
        {
            var res = new ApproveRenewalDetail();

            //單一續保資料
            var singleRenewals = this.approveRenewalSettingRepo.Queryable()
            .Where(o => o.ApproveRecordGid == guid)
            .FirstOrDefault();

            //公司資訊
            var companyInfo = this.GetCompanyInfo(singleRenewals.CompanyCode);
            //專案資訊
            var productInfo = this.GetProductInfo(singleRenewals.CompanyCode, singleRenewals.ProductId);
            //方案資訊
            var projectInfo = this.GetProjectInfo(singleRenewals.CompanyCode, singleRenewals.ProductId, singleRenewals.ProjectCode);

            res.CompanyCodeStr = $"{companyInfo.CompanyCode} {companyInfo?.CompanyName}";
            res.ProductIdStr = $"{productInfo.ProductId} {productInfo?.Name}";
            res.ProjectCodeStr = $"{projectInfo.ProjectCode} {projectInfo?.Name}";

            //續保資料
            var renewals = this.approveRenewalSettingRepo.Queryable()
            .Where(o => o.ApproveRecordGid == guid)
            .ToList();

            var renewalGroups = renewals.GroupBy(x => new { x.SettingDays, x.SendKind })
            .Select(x => x.Key)
            .OrderBy(o => o.SettingDays);

            var settingList = new List<ApproveSetting>();

            foreach (var r in renewalGroups)
            {
                //加頻率/日期條件
                var rrenewals = renewals
                .Where(o => o.SendKind == r.SendKind && o.SettingDays == r.SettingDays)
                .ToList();

                var methodList = new List<ApproveMethod>();
                var method = new ApproveMethod();
                var umsType = new List<string>();

                foreach (var rr in rrenewals)
                {
                    switch (rr.UmsNoticeType)
                    {
                        case UMSNoticeType.Email:
                            var email = new ApproveNoticeByEmail()
                            {
                                IsCheck = rr?.UmsNoticeStatus == "1" ? true : false,
                                UmsId = rr?.Umsid,
                                Key = rr.UmsNoticeType
                            };
                            method.NoticeByEmail = email;
                            break;

                        case UMSNoticeType.Msg:
                            var msg = new ApproveNoticeByMsg()
                            {
                                IsCheck = rr?.UmsNoticeStatus == "1" ? true : false,
                                UmsId = rr?.Umsid,
                                Key = rr.UmsNoticeType
                            };
                            method.NoticeByMsg = msg;
                            break;

                        case UMSNoticeType.AOA:
                            var aoa = new ApproveNoticeByAoa()
                            {
                                IsCheck = rr?.UmsNoticeStatus == "1" ? true : false,
                                UmsId = rr?.Umsid,
                                Key = rr.UmsNoticeType
                            };
                            method.NoticeByAoa = aoa;
                            break;

                        case UMSNoticeType.Push:
                            var push = new ApproveNoticeByPush()
                            {
                                IsCheck = rr?.UmsNoticeStatus == "1" ? true : false,
                                UmsId = rr?.Umsid,
                                Key = rr.UmsNoticeType
                            };
                            method.NoticeByPush = push;
                            break;
                    }

                    umsType.Add(rr.UmsNoticeType);
                }

                if (!umsType.Contains(UMSNoticeType.Email))
                {
                    method.NoticeByEmail = new ApproveNoticeByEmail()
                    {
                        Key = UMSNoticeType.Email
                    };
                }
                if (!umsType.Contains(UMSNoticeType.Msg))
                {
                    method.NoticeByMsg = new ApproveNoticeByMsg()
                    {
                        Key = UMSNoticeType.Msg
                    };
                }
                if (!umsType.Contains(UMSNoticeType.AOA))
                {
                    method.NoticeByAoa = new ApproveNoticeByAoa()
                    {
                        Key = UMSNoticeType.AOA
                    };
                }
                if (!umsType.Contains(UMSNoticeType.Push))
                {
                    method.NoticeByPush = new ApproveNoticeByPush()
                    {
                        Key = UMSNoticeType.Push
                    };
                }

                methodList.Add(method);
                var approveDetail = new ApproveSetting()
                {
                    Freq = $"{this.commonService.GetSysParamValue("SendKind", r.SendKind)} {r.SettingDays} 天",
                    Method = methodList

                };

                settingList.Add(approveDetail);
            }
            res.Setting = settingList;

            return res;
        }


        /// <summary>
        /// 續保通知參數設定-修改頁-確認修改 API
        /// </summary>
        /// <param name="req">續保通知參數新增 Req</param>
        /// <returns>True</returns>
        public async Task<bool> ConfirmEdit(RenewalInsertReq req)
        {
            //新增進覆核
            await this.InsertApproveDataToDb(req, ModifyType.Modfiy);

            return true;
        }

        /// <summary>
        /// 續保通知參數設定-查詢頁-刪除 API
        /// </summary>
        /// <param name="req">續保通知參數明細 Req</param>
        /// <returns>True</returns>
        public async Task<bool> Delete(DetailReq req)
        {
            //刪除前檢核
            this.ValidateDelete(req.CompanyCode, req.ProductId, req.ProjectCode);

            //新增進覆核
            await this.InsertApproveToDbForDelete(req, ModifyType.Delete);

            return true;
        }

        /// <summary>
        /// 取得公司資訊
        /// </summary>
        /// <param name="companyCode">保險公司代碼</param>
        /// <returns></returns>
        private CompanyProfile GetCompanyInfo(string companyCode)
        {
            return this.companyProfileRepo.Queryable()
            .Where(x => x.CompanyCode == companyCode).FirstOrDefault();
        }

        /// <summary>
        /// 取得專案資訊
        /// </summary>
        /// <param name="companyCode">保險公司代碼</param>
        /// <param name="productId">商品 Id</param>
        /// <returns></returns>
        private ProductMaster GetProductInfo(string companyCode, string productId)
        {
            return this.productMasterRepo.Queryable()
            .Where(x => x.ProductId == productId && x.CompanyCode == companyCode).FirstOrDefault();
        }

        /// <summary>
        /// 取得方案資訊
        /// </summary>
        /// <param name="companyCode">保險公司代碼</param>
        /// <param name="productId">商品 Id</param>
        /// <param name="projectCode">專案代碼</param>
        /// <returns></returns>
        private CampaignMaster GetProjectInfo(string companyCode, string productId, string projectCode)
        {
            return this.campaignMasterRepo.Queryable()
            .Where(x => x.ProjectCode == projectCode && x.CompanyCode == companyCode && x.ProductId == productId).FirstOrDefault();
        }

        /// <summary>
        /// 取得覆核ApproveKey 保險公司代碼_專案代碼_方案代碼
        /// </summary>
        /// <param name="companyCode">保險公司代碼</param>
        /// <param name="productId">商品 Id</param>
        /// <param name="projectCode">專案代碼</param>
        /// <returns></returns>
        private string GetApproveKey(string companyCode, string productId, string projectCode)
        {
            return $"{companyCode}_{productId}_{projectCode}";
        }

        /// <summary>
        /// 新增/確認修改 進覆核
        /// </summary>
        /// <param name="req"></param>
        /// <param name="modifyType"></param>
        /// <returns></returns>
        private async Task<bool> InsertApproveDataToDb(RenewalInsertReq req, string modifyType)
        {
            //方案資訊
            var projectInfo = this.campaignMasterRepo.Queryable()
            .Where(x => x.ProjectCode == req.ProjectCode && x.CompanyCode == req.CompanyCode).FirstOrDefault();

            //檢查『覆核記錄檔』是否有覆核中的方案資料
            var ap = this.approveRecordRepo.Queryable()
            .Where(o => o.FunctionCode == FunctionCodeType.F1A01 &&
            o.ApproveKey == this.GetApproveKey(req.CompanyCode, req.ProductId, req.ProjectCode) &&
            o.ApproveStatus == ApproveStatusType.WaitApprove).FirstOrDefault();

            if (ap != null)
            {
                throw new BusinessException($"{req.ProjectCode} {projectInfo?.Name}已有新增等待覆核中，無法新增。");
            }

            //專案資訊
            var productInfo = this.GetProductInfo(req.CompanyCode, req.ProductId);

            var guid = Guid.NewGuid();

            //新增資料至『覆核記錄檔』
            var apRecord = new ApproveRecord()
            {
                ApproveRecordGid = guid,
                FunctionCode = FunctionCodeType.F1A01,
                ApproveKey = this.GetApproveKey(req.CompanyCode, req.ProductId, req.ProjectCode),
                ApproveItem = $"{req.ProductId} {productInfo?.Name}/{projectInfo?.Name}", //專案代碼+空格+專案名稱/方案名稱
                ModifyType = modifyType,
                ModifyBy = WebContext.Account,
                ModifyOn = DateTime.Now,
                ApproveStatus = ApproveStatusType.WaitApprove
            };
            this.approveRecordRepo.Insert(apRecord);

            foreach (var s in req.Setting.ToList())
            {
                foreach (var m in s.Method.ToList())
                {
                    //如果有勾但是沒填 UMSID 回傳錯誤
                    if (m.NoticeByEmail.IsCheck && string.IsNullOrEmpty(m.NoticeByEmail.UmsId))
                    {
                        throw new BusinessException($"Email UMS ID 未填寫。");
                    }
                    if (m.NoticeByMsg.IsCheck && string.IsNullOrEmpty(m.NoticeByMsg.UmsId))
                    {
                        throw new BusinessException($"簡訊 UMS ID 未填寫。");
                    }
                    if (m.NoticeByPush.IsCheck && string.IsNullOrEmpty(m.NoticeByPush.UmsId))
                    {
                        throw new BusinessException($"Push UMS ID 未填寫。");
                    }
                    if (m.NoticeByAoa.IsCheck && string.IsNullOrEmpty(m.NoticeByAoa.UmsId))
                    {
                        throw new BusinessException($"AOA UMS ID 未填寫。");
                    }

                    var email = new ApproveRenewalSetting();
                    email.ApproveRecordGid = guid;
                    email.CompanyCode = req.CompanyCode;
                    email.ProductId = req.ProductId;
                    email.ProjectCode = req.ProjectCode;
                    email.SendKind = s.SendKind;
                    email.SettingDays = s.SettingDays;
                    email.UmsNoticeType = UMSNoticeType.Email;
                    email.Umsid = m.NoticeByEmail.UmsId;
                    email.UmsNoticeStatus = m.NoticeByEmail.IsCheck ? "1" : "0";
                    email.CreateBy = WebContext.Account;
                    email.CreateOn = DateTime.Now;
                    this.approveRenewalSettingRepo.Insert(email);

                    var msg = new ApproveRenewalSetting();
                    msg.ApproveRecordGid = guid;
                    msg.CompanyCode = req.CompanyCode;
                    msg.ProductId = req.ProductId;
                    msg.ProjectCode = req.ProjectCode;
                    msg.SendKind = s.SendKind;
                    msg.SettingDays = s.SettingDays;
                    msg.UmsNoticeType = UMSNoticeType.Msg;
                    msg.Umsid = m.NoticeByMsg.UmsId;
                    msg.UmsNoticeStatus = m.NoticeByMsg.IsCheck ? "1" : "0";
                    msg.CreateBy = WebContext.Account;
                    msg.CreateOn = DateTime.Now;
                    this.approveRenewalSettingRepo.Insert(msg);

                    var aoa = new ApproveRenewalSetting();
                    aoa.ApproveRecordGid = guid;
                    aoa.CompanyCode = req.CompanyCode;
                    aoa.ProductId = req.ProductId;
                    aoa.ProjectCode = req.ProjectCode;
                    aoa.SendKind = s.SendKind;
                    aoa.SettingDays = s.SettingDays;
                    aoa.UmsNoticeType = UMSNoticeType.AOA;
                    aoa.Umsid = m.NoticeByAoa.UmsId;
                    aoa.UmsNoticeStatus = m.NoticeByAoa.IsCheck ? "1" : "0";
                    aoa.CreateBy = WebContext.Account;
                    aoa.CreateOn = DateTime.Now;
                    this.approveRenewalSettingRepo.Insert(aoa);

                    var push = new ApproveRenewalSetting();
                    push.ApproveRecordGid = guid;
                    push.CompanyCode = req.CompanyCode;
                    push.ProductId = req.ProductId;
                    push.ProjectCode = req.ProjectCode;
                    push.SendKind = s.SendKind;
                    push.SettingDays = s.SettingDays;
                    push.UmsNoticeType = UMSNoticeType.Push;
                    push.Umsid = m.NoticeByPush.UmsId;
                    push.UmsNoticeStatus = m.NoticeByPush.IsCheck ? "1" : "0";
                    push.CreateBy = WebContext.Account;
                    push.CreateOn = DateTime.Now;
                    this.approveRenewalSettingRepo.Insert(push);
                }
            };

            await this.renewalUnitOfWork.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// 刪除 進覆核
        /// </summary>
        /// <param name="req"></param>
        /// <param name="modifyType"></param>
        /// <returns></returns>
        private async Task<bool> InsertApproveToDbForDelete(DetailReq req, string modifyType)
        {
            var detail = await this.Edit(req);

            //方案資訊
            var projectInfo = this.campaignMasterRepo.Queryable()
            .Where(x => x.ProjectCode == req.ProjectCode && x.CompanyCode == req.CompanyCode).FirstOrDefault();

            //專案資訊
            var productInfo = this.GetProductInfo(req.CompanyCode, req.ProductId);

            var guid = Guid.NewGuid();

            //新增資料至『覆核記錄檔』
            var apRecord = new ApproveRecord()
            {
                ApproveRecordGid = guid,
                FunctionCode = FunctionCodeType.F1A01,
                ApproveKey = this.GetApproveKey(req.CompanyCode, req.ProductId, req.ProjectCode),
                ApproveItem = $"{req.ProductId} {productInfo?.Name}/{projectInfo?.Name}", //專案代碼+空格+專案名稱/方案名稱
                ModifyType = modifyType,
                ModifyBy = WebContext.Account,
                ModifyOn = DateTime.Now,
                ApproveStatus = ApproveStatusType.WaitApprove
            };
            this.approveRecordRepo.Insert(apRecord);

            foreach (var s in detail.Setting.ToList())
            {
                foreach (var m in s.Method.ToList())
                {
                    //如果有打勾就要新增資料進覆核刪除
                    if (!string.IsNullOrEmpty(m.NoticeByEmail?.UmsId))
                    {
                        var email = new ApproveRenewalSetting();
                        email.ApproveRecordGid = guid;
                        email.CompanyCode = req.CompanyCode;
                        email.ProductId = req.ProductId;
                        email.ProjectCode = req.ProjectCode;
                        email.SendKind = s.SendKind;
                        email.SettingDays = s.SettingDays;
                        email.UmsNoticeType = UMSNoticeType.Email;
                        email.Umsid = m.NoticeByEmail?.UmsId;
                        email.UmsNoticeStatus = m.NoticeByEmail.IsCheck ? "1" : "0";
                        email.CreateBy = WebContext.Account;
                        email.CreateOn = DateTime.Now;
                        this.approveRenewalSettingRepo.Insert(email);
                    }

                    if (!string.IsNullOrEmpty(m.NoticeByMsg?.UmsId))
                    {
                        var msg = new ApproveRenewalSetting();
                        msg.ApproveRecordGid = guid;
                        msg.CompanyCode = req.CompanyCode;
                        msg.ProductId = req.ProductId;
                        msg.ProjectCode = req.ProjectCode;
                        msg.SendKind = s.SendKind;
                        msg.SettingDays = s.SettingDays;
                        msg.UmsNoticeType = UMSNoticeType.Msg;
                        msg.Umsid = m.NoticeByMsg?.UmsId;
                        msg.UmsNoticeStatus = m.NoticeByMsg.IsCheck ? "1" : "0";
                        msg.CreateBy = WebContext.Account;
                        msg.CreateOn = DateTime.Now;
                        this.approveRenewalSettingRepo.Insert(msg);
                    }

                    if (!string.IsNullOrEmpty(m.NoticeByAoa?.UmsId))
                    {
                        var aoa = new ApproveRenewalSetting();
                        aoa.ApproveRecordGid = guid;
                        aoa.CompanyCode = req.CompanyCode;
                        aoa.ProductId = req.ProductId;
                        aoa.ProjectCode = req.ProjectCode;
                        aoa.SendKind = s.SendKind;
                        aoa.SettingDays = s.SettingDays;
                        aoa.UmsNoticeType = UMSNoticeType.AOA;
                        aoa.Umsid = m.NoticeByAoa?.UmsId;
                        aoa.UmsNoticeStatus = m.NoticeByAoa.IsCheck ? "1" : "0";
                        aoa.CreateBy = WebContext.Account;
                        aoa.CreateOn = DateTime.Now;
                        this.approveRenewalSettingRepo.Insert(aoa);
                    }

                    if (!string.IsNullOrEmpty(m.NoticeByPush?.UmsId))
                    {
                        var push = new ApproveRenewalSetting();
                        push.ApproveRecordGid = guid;
                        push.CompanyCode = req.CompanyCode;
                        push.ProductId = req.ProductId;
                        push.ProjectCode = req.ProjectCode;
                        push.SendKind = s.SendKind;
                        push.SettingDays = s.SettingDays;
                        push.UmsNoticeType = UMSNoticeType.Push;
                        push.Umsid = m.NoticeByPush?.UmsId;
                        push.UmsNoticeStatus = m.NoticeByPush.IsCheck ? "1" : "0";
                        push.CreateBy = WebContext.Account;
                        push.CreateOn = DateTime.Now;
                        this.approveRenewalSettingRepo.Insert(push);
                    }
                }
            };

            await this.renewalUnitOfWork.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// 刪除前檢核
        /// </summary>
        /// <param name="companyCode">保險公司代碼</param>
        /// <param name="productId">商品 Id</param>
        /// <param name="productCode">商品代碼</param>
        public void ValidateDelete(string companyCode, string productId, string productCode)
        {
            //方案資訊
            var projectInfo = this.campaignMasterRepo.Queryable()
            .Where(x => x.ProjectCode == productCode && x.CompanyCode == companyCode).FirstOrDefault();

            //檢查『覆核記錄檔』是否有覆核中的方案資料
            var ap = this.approveRecordRepo.Queryable()
            .Where(o => o.FunctionCode == FunctionCodeType.F1A01 &&
            o.ApproveKey == this.GetApproveKey(companyCode, productId, productCode) &&
            o.ApproveStatus == ApproveStatusType.WaitApprove).FirstOrDefault();

            if (ap != null) throw new BusinessException($"{productCode} {projectInfo?.Name}已被異動等待覆核中，無法刪除。");
        }
    }
}
