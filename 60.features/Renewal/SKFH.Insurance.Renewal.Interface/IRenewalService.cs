﻿using System;
using System.Threading.Tasks;
using InsureBrick.Modules.Renewal.Interface.Models;
using TPI.NetCore.WebAPI.Models;

namespace InsureBrick.Modules.Renewal.Interface
{
    /// <summary>
    /// 續保管理 interface
    /// </summary>
    public interface IRenewalService
    {
        /// <summary>
        /// 續保通知參數設定-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">續保通知參數查詢 Req</param>
        /// <returns>續保通知參數查詢 Resp</returns>
        DataTableQueryResp<SearchResp> Search(SearchReq req);

        /// <summary>
        /// 續保資料查詢-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">續保資料查詢 Req</param>
        /// <returns>續保資料查詢 Resp</returns>
        DataTableQueryResp<PolicySearchResp> PolicySearch(PolicySearchReq req);

        /// <summary>
        /// 續保通知參數設定-明細頁-明細 API
        /// </summary>
        /// <param name="req">續保通知參數明細 Req</param>
        /// <returns>續保通知參數 Resp</returns>
        Task<DetailResp> Detail(DetailReq req);

        /// <summary>
        /// 續保通知參數設定-修改頁-查詢修改 API
        /// </summary>
        /// <param name="req">續保通知參數明細 Req</param>
        /// <returns>續保通知參數編輯 Resp</returns>
        Task<EditResp> Edit(DetailReq req);

        /// <summary>
        /// 續保通知參數設定-新增頁-新增 API
        /// </summary>
        /// <param name="req"></param>
        /// <returns>True</returns>
        Task<bool> Insert(RenewalInsertReq req);

        /// <summary>
        /// 續保通知參數設定-修改頁-確認修改 API
        /// </summary>
        /// <param name="req">續保通知參數新增 Req</param>
        /// <returns>True</returns>
        Task<bool> ConfirmEdit(RenewalInsertReq req);

        /// <summary>
        /// 續保通知參數設定-查詢頁-刪除 API
        /// </summary>
        /// <param name="req">續保通知參數明細 Req</param>
        /// <returns>True</returns>
        Task<bool> Delete(DetailReq req);

        /// <summary>
        /// 覆核明細
        /// </summary>
        /// <param name="guid">覆核記錄檔 Gid</param>
        /// <returns>覆核明細</returns>
        ApproveRenewalDetail ApproveRenewalDetail(Guid guid);

        /// <summary>
        /// 續保通知參數設定-明細頁-編輯前檢驗 API
        /// </summary>
        /// <param name="req">續保通知參數明細 Req</param>
        /// <returns>True</returns>
        Task<bool> EditCheck(DetailReq req);

        /// <summary>
        /// 刪除前檢核
        /// </summary>
        /// <param name="companyCode">保險公司代碼</param>
        /// <param name="productId">商品 Id</param>
        /// <param name="productCode">商品代碼</param>
        void ValidateDelete(string companyCode, string productId, string productCode);
    }
}
