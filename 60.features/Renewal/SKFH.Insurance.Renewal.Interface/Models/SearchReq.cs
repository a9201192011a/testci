﻿using TPI.NetCore.WebAPI.Models;

namespace InsureBrick.Modules.Renewal.Interface.Models
{
    /// <summary>
    /// 續保通知參數查詢 Req
    /// </summary>
    public class SearchReq : DataTableQueryReq
    {
        /// <summary>
        /// 公司代碼
        /// </summary>
        public string CompanyCode { get; set; }
    }
}