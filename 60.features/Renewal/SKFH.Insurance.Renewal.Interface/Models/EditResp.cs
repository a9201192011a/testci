﻿using System.Collections.Generic;

namespace InsureBrick.Modules.Renewal.Interface.Models
{
    /// <summary>
    /// 續保通知參數編輯 Resp
    /// </summary>
    public class EditResp
    {
        /// <summary>
        /// 公司代號
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 專案代碼
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// 方案代碼
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 設定
        /// </summary>
        public List<EditSetting> Setting { get; set; }
    }

    /// <summary>
    /// 設定
    /// </summary>
    public class EditSetting
    {
        /// <summary>
        /// 通知頻率
        /// </summary>
        public string SendKind { get; set; }

        /// <summary>
        /// 通知天數
        /// </summary>
        public int? SettingDays { get; set; }

        /// <summary>
        /// 通知方式
        /// </summary>
        public List<EditMethod> Method { get; set; }
    }

    /// <summary>
    /// 通知方式
    /// </summary>
    public class EditMethod
    {
        /// <summary>
        /// Email 提醒
        /// </summary>
        public EditNoticeByEmail NoticeByEmail { get; set; }

        /// <summary>
        /// Message 提醒
        /// </summary>
        public EditNoticeByMsg NoticeByMsg { get; set; }

        /// <summary>
        /// Aoa 提醒
        /// </summary>
        public EditNoticeByAoa NoticeByAoa { get; set; }

        /// <summary>
        /// Push 提醒
        /// </summary>
        public EditNoticeByPush NoticeByPush { get; set; }
    }

    /// <summary>
    /// Email 提醒
    /// </summary>
    public class EditNoticeByEmail
    {
        /// <summary>
        /// 確認
        /// </summary>
        public bool IsCheck { get; set; }

        /// <summary>
        /// UMS 代碼
        /// </summary>
        public string UmsId { get; set; }

        /// <summary>
        /// 參數值
        /// </summary>
        public string Key { get; set; }
    }

    /// <summary>
    /// Message 提醒
    /// </summary>
    public class EditNoticeByMsg
    {
        /// <summary>
        /// 確認
        /// </summary>
        public bool IsCheck { get; set; }

        /// <summary>
        /// UMS 代碼
        /// </summary>
        public string UmsId { get; set; }

        /// <summary>
        /// 參數值
        /// </summary>
        public string Key { get; set; }
    }

    /// <summary>
    /// Aoa 提醒
    /// </summary>
    public class EditNoticeByAoa
    {
        /// <summary>
        /// 確認
        /// </summary>
        public bool IsCheck { get; set; }

        /// <summary>
        /// UMS 代碼
        /// </summary>
        public string UmsId { get; set; }

        /// <summary>
        /// 參數值
        /// </summary>
        public string Key { get; set; }
    }

    /// <summary>
    /// Push 提醒
    /// </summary>
    public class EditNoticeByPush
    {
        /// <summary>
        /// 確認
        /// </summary>
        public bool IsCheck { get; set; }

        /// <summary>
        /// UMS 代碼
        /// </summary>
        public string UmsId { get; set; }

        /// <summary>
        /// 參數值
        /// </summary>
        public string Key { get; set; }
    }
}