﻿using TPI.NetCore.WebAPI.Attributes;

namespace InsureBrick.Modules.Renewal.Interface.Models
{
    /// <summary>
    /// 續保資料查詢 Resp
    /// </summary>
    public class PolicySearchResp
    {
        /// <summary>
        /// 保險公司名稱
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string CompanyCodeStr { get; set; }

        /// <summary>
        /// 保單代碼
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string PolicyNo { get; set; }

        /// <summary>
        /// 要保人姓名
        /// </summary>
        [Mask(MaskType.ChineseName | MaskType.DashIfEmpty)]
        public string ApplicantName { get; set; }

        /// <summary>
        /// 投保專案/方案
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string ProductAndProject { get; set; }

        /// <summary>
        /// 保單到期日
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string PolicyPeriodE { get; set; }

        /// <summary>
        /// 應收保費
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string ModalPermiumWithDisc { get; set; }

        /// <summary>
        /// 方案代碼
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string ProjectCode { get; set; }
    }
}