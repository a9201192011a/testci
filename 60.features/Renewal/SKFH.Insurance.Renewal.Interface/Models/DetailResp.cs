﻿using System.Collections.Generic;

namespace InsureBrick.Modules.Renewal.Interface.Models
{
    /// <summary>
    /// 續保通知參數 Resp
    /// </summary>
    public class DetailResp
    {
        /// <summary>
        /// 公司代號
        /// </summary>
        public string CompanyCodeStr { get; set; }

        /// <summary>
        /// 專案代碼
        /// </summary>
        public string ProductIdStr { get; set; }

        /// <summary>
        /// 方案代碼
        /// </summary>
        public string ProjectCodeStr { get; set; }

        /// <summary>
        /// 設定
        /// </summary>
        public List<Setting> Setting { get; set; }
    }

    /// <summary>
    /// 設定
    /// </summary>
    public class Setting
    {
        /// <summary>
        /// 通知時間
        /// </summary>
        public string Freq { get; set; }

        /// <summary>
        /// 通知方式
        /// </summary>
        public List<Method> Method { get; set; }
    }

    /// <summary>
    /// 通知方式
    /// </summary>
    public class Method
    {
        /// <summary>
        /// Email 提醒
        /// </summary>
        public NoticeByEmail NoticeByEmail { get; set; }

        /// <summary>
        /// Message 提醒
        /// </summary>
        public NoticeByMsg NoticeByMsg { get; set; }
        
        /// <summary>
        /// Aoa 提醒
        /// </summary>
        public NoticeByAoa NoticeByAoa { get; set; }
        
        /// <summary>
        /// Push 提醒
        /// </summary>
        public NoticeByPush NoticeByPush { get; set; }
    }

    /// <summary>
    /// Email 提醒
    /// </summary>
    public class NoticeByEmail
    {
        /// <summary>
        /// 確認
        /// </summary>
        public bool IsCheck { get; set; }

        /// <summary>
        /// UMS 代碼
        /// </summary>
        public string UmsId { get; set; }

        /// <summary>
        /// 參數值
        /// </summary>
        public string Key { get; set; }
    }

    /// <summary>
    /// Message 提醒
    /// </summary>
    public class NoticeByMsg
    {
        /// <summary>
        /// 確認
        /// </summary>
        public bool IsCheck { get; set; }

        /// <summary>
        /// UMS 代碼
        /// </summary>
        public string UmsId { get; set; }

        /// <summary>
        /// 參數值
        /// </summary>
        public string Key { get; set; }

    }

    /// <summary>
    /// Aoa 提醒
    /// </summary>
    public class NoticeByAoa
    {
        /// <summary>
        /// 確認
        /// </summary>
        public bool IsCheck { get; set; }

        /// <summary>
        /// UMS 代碼
        /// </summary>
        public string UmsId { get; set; }

        /// <summary>
        /// 參數值
        /// </summary>
        public string Key { get; set; }

    }

    /// <summary>
    /// Push 提醒
    /// </summary>
    public class NoticeByPush
    {
        /// <summary>
        /// 確認
        /// </summary>
        public bool IsCheck { get; set; }

        /// <summary>
        /// UMS 代碼
        /// </summary>
        public string UmsId { get; set; }

        /// <summary>
        /// 參數值
        /// </summary>
        public string Key { get; set; }

    }
}