﻿using System.Collections.Generic;

namespace InsureBrick.Modules.Renewal.Interface.Models
{
    /// <summary>
    /// 覆核明細
    /// </summary>
    public class ApproveRenewalDetail
    {
        /// <summary>
        /// 公司代號
        /// </summary>
        public string CompanyCodeStr { get; set; }

        /// <summary>
        /// 專案代碼
        /// </summary>
        public string ProductIdStr { get; set; }

        /// <summary>
        /// 方案代碼
        /// </summary>
        public string ProjectCodeStr { get; set; }

        /// <summary>
        /// 覆核設定
        /// </summary>
        public List<ApproveSetting> Setting { get; set; }

    }

    /// <summary>
    /// 覆核設定
    /// </summary>
    public class ApproveSetting
    {
        /// <summary>
        /// 通知頻率
        /// </summary>
        public string Freq { get; set; }

        /// <summary>
        /// 通知方式
        /// </summary>
        public List<ApproveMethod> Method { get; set; }
    }

    /// <summary>
    /// 通知方式
    /// </summary>

    public class ApproveMethod
    {
        /// <summary>
        /// Email 提醒
        /// </summary>
        public ApproveNoticeByEmail NoticeByEmail { get; set; }

        /// <summary>
        /// Message 提醒
        /// </summary>
        public ApproveNoticeByMsg NoticeByMsg { get; set; }

        /// <summary>
        /// Aoa 提醒
        /// </summary>
        public ApproveNoticeByAoa NoticeByAoa { get; set; }

        /// <summary>
        /// Push 提醒
        /// </summary>
        public ApproveNoticeByPush NoticeByPush { get; set; }
    }

    /// <summary>
    /// Email 提醒
    /// </summary>
    public class ApproveNoticeByEmail
    {
        /// <summary>
        /// 確認
        /// </summary>
        public bool IsCheck { get; set; }

        /// <summary>
        /// UMS 代碼
        /// </summary>
        public string UmsId { get; set; }

        /// <summary>
        /// 參數值
        /// </summary>
        public string Key { get; set; }
    }

    /// <summary>
    /// Message 提醒
    /// </summary>
    public class ApproveNoticeByMsg
    {
        /// <summary>
        /// 確認
        /// </summary>
        public bool IsCheck { get; set; }

        /// <summary>
        /// UMS 代碼
        /// </summary>
        public string UmsId { get; set; }

        /// <summary>
        /// 參數值
        /// </summary>
        public string Key { get; set; }
    }

    /// <summary>
    /// Aoa 提醒
    /// </summary>
    public class ApproveNoticeByAoa
    {
        /// <summary>
        /// 確認
        /// </summary>
        public bool IsCheck { get; set; }

        /// <summary>
        /// UMS 代碼
        /// </summary>
        public string UmsId { get; set; }

        /// <summary>
        /// 參數值
        /// </summary>
        public string Key { get; set; }
    }

    /// <summary>
    /// Push 提醒
    /// </summary>
    public class ApproveNoticeByPush
    {
        /// <summary>
        /// 確認
        /// </summary>
        public bool IsCheck { get; set; }

        /// <summary>
        /// UMS 代碼
        /// </summary>
        public string UmsId { get; set; }

        /// <summary>
        /// 參數值
        /// </summary>
        public string Key { get; set; }
    }
}