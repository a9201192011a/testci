﻿using TPI.NetCore.WebAPI.Models;

namespace InsureBrick.Modules.Renewal.Interface.Models
{
    /// <summary>
    /// 續保資料查詢 Req
    /// </summary>
    public class PolicySearchReq : DataTableQueryReq
    {
        /// <summary>
        /// 公司代碼
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 專案代碼
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// 方案代碼
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 起始日
        /// </summary>
        public string PolicyPeriodStart { get; set; }

        /// <summary>
        /// 結束日
        /// </summary>
        public string PolicyPeriodEnd { get; set; }

    }
}