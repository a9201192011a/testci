﻿namespace InsureBrick.Modules.Renewal.Interface.Models
{
    /// <summary>
    /// 續保通知參數查詢 Resp
    /// </summary>
    public class SearchResp
    {
        /// <summary>
        /// 公司代碼
        /// </summary>
        public string CompanyCodeStr { get; set; }

        /// <summary>
        /// 投保專案/方案
        /// </summary>
        public string ProductAndProject { get; set; }

        /// <summary>
        /// 銷售狀態
        /// </summary>
        public string SaleStateStr { get; set; }

        /// <summary>
        /// 公司代碼
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 專案代碼
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// 方案代碼
        /// </summary>
        public string ProjectCode { get; set; }
    }
}