﻿using System.Collections.Generic;

namespace InsureBrick.Modules.Renewal.Interface.Models
{
    /// <summary>
    /// 續保通知參數新增 Req
    /// </summary>
    public class RenewalInsertReq
    {
        /// <summary>
        /// 公司代號
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 專案代碼
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// 方案代碼
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 設定
        /// </summary>
        public List<InsertSetting> Setting { get; set; }
    }

    /// <summary>
    /// 設定
    /// </summary>
    public class InsertSetting
    {
        /// <summary>
        /// 通知頻率
        /// </summary>
        public string SendKind { get; set; }

        /// <summary>
        /// 通知天數
        /// </summary>
        public int? SettingDays { get; set; }

        /// <summary>
        /// 通知方式
        /// </summary>
        public List<InsertMethod> Method { get; set; }
    }

    /// <summary>
    /// 通知方式
    /// </summary>
    public class InsertMethod
    {
        /// <summary>
        /// Email 提醒
        /// </summary>
        public InsertNoticeByEmail NoticeByEmail { get; set; }

        /// <summary>
        /// Message 提醒
        /// </summary>
        public InsertNoticeByMsg NoticeByMsg { get; set; }

        /// <summary>
        /// Aoa 提醒
        /// </summary>
        public InsertNoticeByAoa NoticeByAoa { get; set; }

        /// <summary>
        /// Push 提醒
        /// </summary>
        public InsertNoticeByPush NoticeByPush { get; set; }
    }

    /// <summary>
    /// Email 提醒
    /// </summary>
    public class InsertNoticeByEmail
    {
        /// <summary>
        /// 確認
        /// </summary>
        public bool IsCheck { get; set; }

        /// <summary>
        /// UMS 代碼
        /// </summary>
        public string UmsId { get; set; }

    }

    /// <summary>
    /// Message 提醒
    /// </summary>
    public class InsertNoticeByMsg
    {
        /// <summary>
        /// 確認
        /// </summary>
        public bool IsCheck { get; set; }

        /// <summary>
        /// UMS 代碼
        /// </summary>
        public string UmsId { get; set; }

    }

    /// <summary>
    /// Aoa 提醒
    /// </summary>
    public class InsertNoticeByAoa
    {
        /// <summary>
        /// 確認
        /// </summary>
        public bool IsCheck { get; set; }

        /// <summary>
        /// UMS 代碼
        /// </summary>
        public string UmsId { get; set; }

    }

    /// <summary>
    /// Push 提醒
    /// </summary>
    public class InsertNoticeByPush
    {
        /// <summary>
        /// 確認
        /// </summary>
        public bool IsCheck { get; set; }

        /// <summary>
        /// UMS 代碼
        /// </summary>
        public string UmsId { get; set; }

    }
}