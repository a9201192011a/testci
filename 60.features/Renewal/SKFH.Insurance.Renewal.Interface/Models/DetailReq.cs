﻿namespace InsureBrick.Modules.Renewal.Interface.Models
{
    /// <summary>
    /// 續保通知參數明細 Req
    /// </summary>
    public class DetailReq
    {
        /// <summary>
        /// 公司代碼
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 專案代碼
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// 方案代碼
        /// </summary>
        public string ProjectCode { get; set; }
    }
}