﻿using System;
using AutoMapper;
using TPI.NetCore.WebAPI.Models;
using TPI.NetCore.Extensions;
using InsureBrick.Modules.Customer.Interface;
using InsureBrick.Modules.Customer.Interface.Models;
using TPI.NetCore;
using Microsoft.Data.SqlClient;
using InsureBrick.Domain.Helper;
using Dapper;
using SKFH.Insurance.entity.Repositories;

namespace InsureBrick.Modules.Customer.Service
{
    /// <summary>
    /// CustomerService
    /// </summary>
    public class CustomerService: ICustomerService
    {
        #region Initialization

        private readonly IMapper mapper;
        private readonly ICustomerProfileRepository customerProfileRepo;
        //private readonly IEventLogService eventLog;

        public CustomerService(
            IMapper mapper,
            ICustomerProfileRepository customerProfileRepo
            //IEventLogService eventLog
        )
        {
            this.mapper = mapper;
            this.customerProfileRepo = customerProfileRepo;
            //this.eventLog = eventLog;
        }

        #endregion Initialization

        /// <summary>
        /// 客戶資料查詢-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">客戶資料查詢-查詢頁-查詢資料 API Req</param>
        /// <returns>客戶資料查詢-查詢頁-查詢資料 API Resp</returns>
        public DataTableQueryResp<CustomerSearchResp> Search(CustomerSearchReq req)
        {
            try
            {
                var memberList = this.customerProfileRepo.Queryable()
                .QueryItem(o => o.Idno.Contains(req.IDNo), req.IDNo)
                .QueryItem(o => o.Name.Contains(req.Name), req.Name)
                .OrderBySortInfo(req.order, req.columns);

                // 資料庫總筆數
                var totalCount = memberList.Count();

                // 分頁後結果
                var res = memberList.Pagination(req.length, req.start).ToList();

                var m = this.mapper.Map<List<CustomerSearchResp>>(res);

                return new DataTableQueryResp<CustomerSearchResp>(m, totalCount);
            }
            catch (Exception ex)
            {
                //eventLog.Debug("CustomerService", "Search", "Search", ex);
                throw;
            }
        }

        /// <summary>
        /// 客戶資料查詢-明細-查詢明細 API
        /// </summary>
        /// <param name="req">客戶資料查詢-明細-查詢明細 API Req</param>
        /// <returns>客戶資料查詢-明細-查詢明細 API Resp</returns>
        public CustomerDetailResp Detail(CustomerDetailReq req)
        {
            // 客戶主檔
            var customerProfile = this.customerProfileRepo.Queryable()
                .SingleOrDefault(o=>o.CustId== req.CustId);

            if (customerProfile == null) 
            {
                throw new BusinessException("查無客戶主檔資料");
            }

            // 客戶資料 區塊
            var customerDetail = new CustomerDetailResp()
            {
                iDNo = customerProfile.Idno,
                custId = customerProfile.CustId,
                name = customerProfile.Name,
                Birthday = customerProfile.Birthday.ToString("yyyy/MM/dd"),
                Phone1 = customerProfile.Phone1,
                Mobile = customerProfile.Mobile,
                Email = customerProfile.Email,
                AddrStr = $"{customerProfile.Zipcode} { customerProfile.Addr}"
            };

            string sqlCmd = this.GetDetailCmd();

            using (var conn = new SqlConnection(ConfigHelper.GetINSCon()))
            {
                conn.Open();
                var policies = conn.Query<PolicyData>(sqlCmd, new { CustomerIDNo = customerProfile.Idno });

                if (policies.Any())
                {
                    //  保單資料 區塊
                    customerDetail.policies = policies;
            }
                else
                {
                    customerDetail.policies = new List<PolicyData>();
                }

                return customerDetail;
            }
        }


        private string GetDetailCmd() 
        {
            var sqlCmd = string.Empty;

            sqlCmd = $@"
With 
AcceptNos As --要保人,被保人資料的進件編號 (要保人與被保人為 客戶身分證字號)
(
	Select distinct AcceptNo from PolicyPersonInfo
	Where (PersonType='Applicant' and IDNo = @CustomerIDNo) or  (PersonType='Insured'and IDNo = @CustomerIDNo)
),
Applicants As --要保人
(
	Select * from PolicyPersonInfo
	Where PersonType='Applicant'
),
Insureds As --被保人
(
	Select * from PolicyPersonInfo
	Where PersonType='Insured'
)

Select 
pm.PolicyNo
,ap.PersonName AS ApplicantName
,ins.PersonName AS InsuredName
,prm.ProductId+' '+prm.Name AS ProductName
,cm.Name AS CampaignName
,(Select ItemValue from SysParam Where GroupId='PolicyStatus' and ItemId = pm.PolicyStatus) AS PolicyStatusStr
,(Select ItemValue from SysParam Where GroupId='CaseSource' and ItemId = pm.CaseSource) AS CaseSourceStr
From PolicyMaster as pm
join AcceptNos as acp on acp.AcceptNo=pm.AcceptNo --innerJoin 進件編號
Left join Applicants as ap on ap.AcceptNo = pm.AcceptNo
Left join Insureds as ins on ins.AcceptNo = pm.AcceptNo
Left join ProductMaster as prm on prm.ProductId = pm.ProductId --專案主檔
Left join CampaignMaster as cm on cm.ProductId=pm.ProductId and cm.ProjectCode = pm.ProjectCode --方案主檔
where (pm.PolicyNo!='' and pm.PolicyNo is not null) --有保單號碼
";

            return sqlCmd;
        }

    }
}
