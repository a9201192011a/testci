﻿using InsureBrick.Modules.Customer.Interface.Models;
using TPI.NetCore.WebAPI.Models;

namespace InsureBrick.Modules.Customer.Interface
{
    /// <summary>
    /// CustomerService interface
    /// </summary>
    public interface ICustomerService
    {
        /// <summary>
        /// 客戶資料查詢-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">客戶資料查詢-查詢頁-查詢資料 API Req</param>
        /// <returns>客戶資料查詢-查詢頁-查詢資料 API Resp</returns>
        DataTableQueryResp<CustomerSearchResp> Search(CustomerSearchReq req);

        /// <summary>
        /// 客戶資料查詢-明細-查詢明細 API
        /// </summary>
        /// <param name="req">客戶資料查詢-明細-查詢明細 API Req</param>
        /// <returns>客戶資料查詢-明細-查詢明細 API Resp</returns>
        CustomerDetailResp Detail(CustomerDetailReq req);
    }
}
