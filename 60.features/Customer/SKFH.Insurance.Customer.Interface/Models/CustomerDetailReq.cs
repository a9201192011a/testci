﻿using TPI.NetCore.WebAPI.Attributes;

namespace InsureBrick.Modules.Customer.Interface.Models
{
    /// <summary>
    /// 客戶資料查詢-明細-查詢明細 API Request
    /// </summary>
    public class CustomerDetailReq : MaskReq
    {
        /// <summary>
        /// 會員帳號
        /// </summary>
        public string CustId { get; set; }
    }
}
