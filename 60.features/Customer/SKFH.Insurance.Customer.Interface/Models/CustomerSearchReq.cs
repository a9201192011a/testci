﻿using Newtonsoft.Json;
using TPI.NetCore.WebAPI.Models;

namespace InsureBrick.Modules.Customer.Interface.Models
{
    /// <summary>
    /// 客戶資料查詢-查詢頁-查詢資料 API Request
    /// </summary>
    public class CustomerSearchReq : DataTableQueryReq
    {
        /// <summary>
        /// 會員身分證字號
        /// </summary>
        [JsonProperty("idNo")] 
        public string IDNo { get; set; }

        /// <summary>
        /// 會員姓名
        /// </summary>
        public string Name { get; set; }
    }
}
