﻿using Newtonsoft.Json;
using TPI.NetCore.WebAPI.Attributes;

namespace InsureBrick.Modules.Customer.Interface.Models
{
    /// <summary>
    /// 客戶資料查詢-查詢頁-查詢資料 API Response
    /// </summary>
    public class CustomerSearchResp
    {
        /// <summary>
        /// 會員身分證字號
        /// </summary>
        [JsonProperty("idNo")]
        [Mask(MaskType.NationalIdentification | MaskType.DashIfEmpty)]
        public string IDNo { get; set; }

        /// <summary>
        /// 會員帳號
        /// </summary>
        //[Mask(MaskType.NationalIdentification | MaskType.DashIfEmpty)]
        public string CustId { get; set; }

        /// <summary>
        /// 會員姓名
        /// </summary>
        [Mask(MaskType.ChineseName | MaskType.DashIfEmpty)]
        public string Name { get; set; }
    }
}
