﻿using Newtonsoft.Json;
using System.Collections.Generic;
using TPI.NetCore.WebAPI.Attributes;

namespace InsureBrick.Modules.Customer.Interface.Models
{
    /// <summary>
    /// 客戶資料查詢-明細-查詢明細 API Response
    /// </summary>
    public class CustomerDetailResp
    {
        /// <summary>
        /// 客戶(會員)身分證字號
        /// </summary>
        [JsonProperty("idNo")]
        [Mask(MaskType.NationalIdentification | MaskType.DashIfEmpty)]
        public string iDNo { get; set; }

        /// <summary>
        /// 客戶(會員)帳號
        /// </summary>
        [Mask(MaskType.DashIfEmpty)]
        public string custId { get; set; }

        /// <summary>
        /// 客戶(會員)姓名
        /// </summary>
        [Mask(MaskType.ChineseName | MaskType.DashIfEmpty)]
        public string name { get; set; }

        /// <summary>
        /// 出生日期(yyyy/MM/dd)
        /// </summary>
        [Mask(MaskType.Birthday | MaskType.DashIfEmpty)] 
        public string Birthday { get; set; }

        /// <summary>
        /// 連絡電話
        /// </summary>
        [Mask(MaskType.PhoneNo | MaskType.DashIfEmpty)]
        public string Phone1 { get; set; }

        /// <summary>
        /// 手機號碼
        /// </summary>
        [Mask(MaskType.PhoneNo | MaskType.DashIfEmpty)] 
        public string Mobile { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [Mask(MaskType.Email | MaskType.DashIfEmpty)]
        public string Email { get; set; }

        /// <summary>
        /// 通訊地址
        /// </summary>
        [Mask(MaskType.ChineseAddress | MaskType.DashIfEmpty)]
        public string AddrStr { get; set; }

        /// <summary>
        /// 保單資料 區塊
        /// </summary>
        public IEnumerable<PolicyData> policies { get; set; }
    }

    /// <summary>
    /// 保單資料
    /// </summary>
    public class PolicyData
    {
        /// <summary>
        /// 保單號碼
        /// </summary>
        public string PolicyNo { get; set; }

        /// <summary>
        /// 要保人姓名
        /// </summary>
        [Mask(MaskType.ChineseName | MaskType.DashIfEmpty)] 
        public string ApplicantName { get; set; }

        /// <summary>
        /// 被保人姓名
        /// </summary>
        [Mask(MaskType.ChineseName | MaskType.DashIfEmpty)] 
        public string InsuredName { get; set; }

        /// <summary>
        /// 專案名稱
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 方案名稱
        /// </summary>
        public string CampaignName { get; set; }

        /// <summary>
        /// 保單狀態
        /// </summary>
        public string PolicyStatusStr { get; set; }

        /// <summary>
        /// 案件來源
        /// </summary>
        public string CaseSourceStr { get; set; }


    }
}
