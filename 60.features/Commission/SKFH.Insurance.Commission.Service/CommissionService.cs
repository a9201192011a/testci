﻿using AutoMapper;
using InsureBrick.Modules.Commission.Interface;
using InsureBrick.Modules.Commission.Interface.Models;
using InsureBrick.Modules.Common.Interface;
using TPI.NetCore.Extensions;
using TPI.NetCore.WebAPI.Models;
using TPI.NetCore;
using TPI.NetCore.Models;
using TPI.NetCore.Helper;
using TPI.NetCore.WebContext;
using Microsoft.Data.SqlClient;
using InsureBrick.Domain.Helper;
using Dapper;
using SKFH.Insurance.Log.Entity.Repository;
using SKFH.Insurance.entity.Repositories;
using SKFH.Insurance.Common.Entity.Repository;
using SKFH.Insurance.Company.Entity.Repository;
using SKFH.Insurance.Log.Entity.Models;
using SKFH.Insurance.Commission.Entity.Models;
using SKFH.Insurance.Product.Entity.Models;
using coreLibs.DateAccess;
using Autofac;
using SKFH.Insurance.Product.Entity.Repository;

namespace InsureBrick.Modules.Commission.Service
{
    /// <summary>
    /// CommissionService
    /// </summary>
    public class CommissionService : ICommissionService
    {
        #region Initialization

        private readonly IMapper mapper;
        private readonly IUnitOfWork commissionUnitOfWork;
        private readonly ICommonService commonService;
        private readonly ICommissionSettingRepository commissionSettingRepo;
        private readonly ICommissionSettingDetailRepository commissionSettingDetailRepo;
        private readonly ICompanyProfileRepository companyProfileRepo;
        private readonly IApproveRecordRepository approveRecordRepo;
        private readonly IApproveCommissionSettingRepository approveCommissionSettingRepo;
        private readonly IApproveCommissionSettingDetailRepository approveCommissionSettingDetailRepo;
        private readonly IPolicyDetailRepository policyDetailRepo;
        private readonly IProductMasterRepository productMasterRepo;
        private readonly ICommissionProfileRepository commissionProfileRepo;
        private readonly ICommissionDetailRepository commissionDetailRepo;
        private readonly ICampaignMasterRepository campaignMasterRepo;
        private readonly IPolicyMasterRepository policyMasterRepo;
        private readonly ISysParamRepository sysParamRepo;
        private readonly IPlanMasterRepository planMasterRepo;
        private readonly ICommissionViewRepository commissionViewRepo;
        private readonly ICampaignPlanMappingRepository campaignPlanMappingRepo;
        private readonly IApproveCampaignPlanMappingRepository approveCampaignPlanMappingRepo;
        private string actionName = "CommissionService";


        public CommissionService(
            IMapper mapper, IComponentContext componentContext,
            ICommonService commonService,
            ICommissionSettingRepository commissionSettingRepo,
            ICommissionSettingDetailRepository commissionSettingDetailRepo,
            ICompanyProfileRepository companyProfileRepo,
            IApproveRecordRepository approveRecordRepo,
            IApproveCommissionSettingRepository approveCommissionSettingRepo,
            IApproveCommissionSettingDetailRepository approveCommissionSettingDetailRepo,
            IPolicyDetailRepository policyDetailRepo,
            IProductMasterRepository productMasterRepo,
            ICommissionProfileRepository commissionProfileRepo,
            ICommissionDetailRepository commissionDetailRepo,
            ICampaignMasterRepository campaignMasterRepo,
            IPolicyMasterRepository policyMasterRepo,
            ISysParamRepository sysParamRepo,
            IPlanMasterRepository planMasterRepo,
            ICommissionViewRepository commissionViewRepo,
            ICampaignPlanMappingRepository campaignPlanMappingRepo,
            IApproveCampaignPlanMappingRepository approveCampaignPlanMappingRepo
            )
        {
            this.mapper = mapper;
            this.commissionUnitOfWork = componentContext.ResolveNamed<IUnitOfWork>("Commission");
            this.commonService = commonService;
            this.commissionSettingRepo = commissionSettingRepo;
            this.commissionSettingDetailRepo = commissionSettingDetailRepo;
            this.companyProfileRepo = companyProfileRepo;
            this.approveRecordRepo = approveRecordRepo;
            this.approveCommissionSettingRepo = approveCommissionSettingRepo;
            this.approveCommissionSettingDetailRepo = approveCommissionSettingDetailRepo;
            this.policyDetailRepo = policyDetailRepo;
            this.productMasterRepo = productMasterRepo;
            this.commissionProfileRepo = commissionProfileRepo;
            this.commissionDetailRepo = commissionDetailRepo;
            this.campaignMasterRepo = campaignMasterRepo;
            this.policyMasterRepo = policyMasterRepo;
            this.sysParamRepo = sysParamRepo;
            this.planMasterRepo = planMasterRepo;
            this.commissionViewRepo = commissionViewRepo;
            this.campaignPlanMappingRepo = campaignPlanMappingRepo;
            this.approveCampaignPlanMappingRepo = approveCampaignPlanMappingRepo;
        }

        #endregion Initialization

        /// <summary>
        /// 佣金設定-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-查詢資料 API Req</param>
        /// <returns>查詢結果</returns>
        public DataTableQueryResp<SearchResp> Search(SearchReq req)
        {
            try
            {
                var query = this.commissionViewRepo.Queryable();

                #region req 條件篩選
                if (!string.IsNullOrEmpty(req.CompanyCode))
                {
                    query = query.Where(x => x.CompanyCode == req.CompanyCode);
                }

                if (!string.IsNullOrEmpty(req.PlanType))
                {
                    query = query.Where(x => x.PlanType == req.PlanType);
                }

                if (!string.IsNullOrEmpty(req.PlanCode))
                {
                    query = query.Where(x => x.PlanCode == req.PlanCode);
                }

                if (!string.IsNullOrEmpty(req.PlanShortName))
                {
                    query = query.Where(x => x.PlanShortName == req.PlanShortName);
                }

                if (!string.IsNullOrEmpty(req.SaleState) && int.TryParse(req.SaleState, out int saleState))
                {
                    query = query.Where(x => x.SaleState == saleState);
                }

                if (!string.IsNullOrEmpty(req.BenefitTerm) && int.TryParse(req.BenefitTerm, out int benefitTerm))
                {
                    query = query.Where(x => x.BenefitTerm == benefitTerm);
                }

                if (!string.IsNullOrEmpty(req.PaymentTerm) && int.TryParse(req.BenefitTerm, out int paymentTerm))
                {
                    query = query.Where(x => x.PaymentTerm == paymentTerm);
                }
                #endregion

                // 資料庫總筆數
                var totalCount = query.Count();

                var commissionSearchRsp = query.Select(x => new SearchResp()
                {
                    CompanyCodeStr = x.CompanyCodeStr,
                    PlanCodeStr = x.PlanCodeStr,
                    PlanTypeStr = x.PlanTypeStr,
                    CompanyCode = x.CompanyCode,
                    PlanCode = x.PlanCode,
                    PlanType = x.PlanType,
                    BenefitTerm = x.BenefitTerm,
                    PaymentTerm = x.PaymentTerm,
                    SaleStateStr = x.SaleStateStr

                }).OrderBySortInfo(req.order, req.columns)
                .Pagination(req.length, req.start)
                .Select(x =>
                new SearchResp
                {
                    CompanyCodeStr = x.CompanyCodeStr,
                    PlanCodeStr = x.PlanCodeStr,
                    PlanTypeStr = x.PlanTypeStr,
                    CompanyCode = x.CompanyCode,
                    PlanCode = x.PlanCode,
                    PlanType = x.PlanType,
                    BenefitTerm = x.BenefitTerm,
                    PaymentTerm = x.PaymentTerm,
                    SaleStateStr = x.SaleStateStr
                }).ToList();

                return new DataTableQueryResp<SearchResp>(commissionSearchRsp, totalCount);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 佣金設定-複製/編輯頁/明細頁-查詢資料 API
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-刪除 API Req</param>
        /// <returns>佣金設定-查詢頁-刪除 API Resp</returns>
        public DetailResp Detail(CommissionReq req)
        {

            //佣金設定資料
            var commis = this.commissionSettingRepo.Queryable()
            .Where(o => o.CompanyCode == req.CompanyCode && o.PlanCode == req.PlanCode && o.PaymentTerm == req.PaymentTerm && o.PlanType == req.PlanType && o.BenefitTerm == req.BenefitTerm)
            .FirstOrDefault();
            if (commis == null)
            {
                throw new BusinessException("佣金主檔查無資料");
            }

            var settingDetails = this.commissionSettingDetailRepo.Queryable()
                .Where(o => o.CompanyCode == req.CompanyCode
                && o.PlanCode == req.PlanCode
                && o.PaymentTerm == req.PaymentTerm
                && o.PlanType == req.PlanType
                && o.BenefitTerm == req.BenefitTerm).ToList();


            //公司名稱
            var companyName = this.GetCompanyName(req.CompanyCode);

            //商品主檔
            var planMaster = this.GetPlanMasterSaleState()
            .Where(o => o.CompanyCode == req.CompanyCode && o.PlanCode == req.PlanCode && o.PaymentTerm == req.PaymentTerm && o.PlanType == req.PlanType && o.BenefitTerm == req.BenefitTerm)
            .FirstOrDefault();

            var companyInfo = this.companyProfileRepo.Queryable().FirstOrDefault(o => o.CompanyCode == req.CompanyCode);
            var planTypeStr = this.commonService.GetSysParamValue(GroupIdType.PlanType, req.PlanType);
            var planStartOn = this.GetEarliestSaleDateOfProduct(req);

            var info = new Info()
            {
                CompanyCodeStr = $"{companyInfo.CompanyCode} {companyInfo.CompanyName}",
                CompanyCode = companyInfo.CompanyCode,
                PlanCodeStr = $"{planMaster.PlanCode} {planMaster.PlanShortName}(保障年期{planMaster.BenefitTerm}年)",
                PlanCode = planMaster.PlanCode,
                PlanTypeStr = this.commonService.GetSysParamValue(GroupIdType.PlanType, planMaster.PlanType),
                PlanType = planMaster.PlanType,
                BenefitTerm = planMaster.BenefitTerm,
                PaymentTerm = planMaster.PaymentTerm,
                PlanStartOn = planStartOn.ToString("yyyy/MM/dd")
            };

            var res = new DetailResp()
            {
                Info = info,
                Settings = settingDetails.Select(o => new SettingDetail()
                {
                    CalcTypeStr = this.commonService.GetSysParamValue(GroupIdType.CalcType, o.CalcType),
                    CalcType = o.CalcType,
                    CalcValueStr = o.CalcType == CalcType.Ratio ? $"{o.CalcValue.ToString("0.##")}%" : o.CalcValue.ToString("0.##"),
                    CommissionDate = o.CommissionDate.ToString("yyyy/MM/dd")
                }).ToList()
            };

            return res;
        }

        /// <summary>
        /// 佣金設定-編輯頁-編輯前檢驗 API
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-刪除 API Req</param>
        /// <returns>True</returns>
        public bool EditCheck(CommissionReq req)
        {
            var approveKey = this.GetCommissionApproveKey(req);

            var approveItem = this.GetCommissionApproveItem(req);

            //商品主檔
            var planInfo = this.GetPlanMasterSaleState()
            .Where(o => o.CompanyCode == req.CompanyCode && o.PlanCode == req.PlanCode && o.PaymentTerm == req.PaymentTerm && o.PlanType == req.PlanType && o.BenefitTerm == req.BenefitTerm)
            .FirstOrDefault();

            //檢查 商品是否 已停售
            if (planInfo.PlanEndOn < DateTime.Now) throw new BusinessException($"商品：{approveItem} 已停售，不可編輯。");

            //檢查 覆核記錄檔 是否有覆核中的佣金設定資料
            var record = this.approveRecordRepo.Queryable()
            .Where(o => o.FunctionCode == FunctionCodeType.F1901 && o.ApproveKey == approveKey && o.ApproveStatus == ApproveStatusType.WaitApprove).FirstOrDefault();
            if (record != null) throw new BusinessException($"商品：{approveItem} 異動覆核中，請通知相關人員進行覆核作業。");

            return true;
        }

        /// <summary>
        /// 驗證第一筆佣金生效日是否符合商品最小銷售起日
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        /// <exception cref="BusinessException"></exception>
        private bool CheckEarliestCommissionDate(CommissionInsertReq req)
        {
            var planStartOn = this.GetEarliestSaleDateOfProduct(req);
            if (req.Settings.First().CommissionDate.Date != planStartOn)
            {
                throw new BusinessException("第一筆佣金生效日需為商品最小銷售起日。");
            }
            return true;
        }

        /// <summary>
        /// 佣金設定-查詢頁-新增 API
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-新增 API</param>
        /// <returns>True</returns>
        public async Task<bool> Insert(CommissionInsertReq req)
        {
            var r = this.approveRecordRepo.Queryable()
            .Where(o =>
            o.FunctionCode == FunctionCodeType.F1901 &&
            o.ApproveKey == this.GetCommissionApproveKey(req) &&  //存覆核項目的key = 保險公司代碼_商品代碼_繳費年期_商品險別_保障年期
            o.ApproveStatus == ApproveStatusType.WaitApprove &&
            o.ModifyType == ModifyType.Insert) //新增
            .FirstOrDefault();

            var msg = this.GetCommissionApproveItem(req);

            if (r != null) throw new BusinessException($"商品：{msg} 新增覆核中，請通知相關人員進行覆核作業。");
            this.CheckEarliestCommissionDate(req);

            await this.InsertApproveDataToDb(req, ModifyType.Insert);

            return true;
        }

        /// <summary>
        /// 佣金設定-查詢頁-刪除 API
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-刪除 API Req</param>
        /// <returns>True</returns>
        public async Task<bool> Delete(CommissionReq req)
        {
            this.actionName = $"{this.actionName}/Delete";

            //this.eventLogService.Trace("檢核佣金設定檔是否存在開始", this.actionName, this.actionName);

            var commissionSetting = this.commissionSettingRepo.Queryable()
            .Where(o => o.CompanyCode == req.CompanyCode && o.PlanCode == req.PlanCode && o.PaymentTerm == req.PaymentTerm && o.PlanType == req.PlanType && o.BenefitTerm == req.BenefitTerm)
            .FirstOrDefault();

            //this.eventLogService.Trace($"檢核佣金設定檔是否存在結束", this.actionName, this.actionName);

            if (commissionSetting == null) throw new BusinessException("無此佣金設定。");

            #region 檢核是否可被刪除
            var processSeconds = StopwatchHelper.CreateStopwatch(() =>
            {
                this.CheckValidateForDelete(req);
            });

            //this.eventLogService.Debug($"檢核是否可被刪除，共花費 {processSeconds} 秒", this.actionName, this.actionName);

            #endregion

            #region 檢核是否有覆核紀錄

            //this.eventLogService.Trace("檢核是否有覆核紀錄開始", this.actionName, this.actionName);

            //檢核是否有覆核紀錄
            var approveListExist = this.approveRecordRepo.Queryable()
             .Any(o =>
             o.FunctionCode == FunctionCodeType.F1901 && //功能代碼
             o.ApproveKey == this.GetCommissionApproveKey(req) && //存覆核項目的key = 保險公司代碼_商品代碼_繳費年期_商品險別
             o.ApproveStatus == ApproveStatusType.WaitApprove // 尚未覆核
             );

            var msg = this.GetCommissionApproveItem(req);

            //this.eventLogService.Trace("檢核是否有覆核紀錄結束", this.actionName, this.actionName);

            if (approveListExist) throw new BusinessException($"商品：{msg} 異動覆核中，無法刪除。");
            #endregion

            var commis = this.mapper.Map<CommissionInsertReq>(commissionSetting);
            var settingDeatils = this.commissionSettingDetailRepo.Queryable()
                .Where(o => o.CompanyCode == req.CompanyCode
                && o.PlanCode == req.PlanCode
                && o.PlanType == req.PlanType
                && o.PaymentTerm == req.PaymentTerm
                && o.BenefitTerm == req.BenefitTerm).Select(o => new Setting()
                {
                    CalcType = o.CalcType,
                    CalcValue = o.CalcValue,
                    CommissionDate = o.CommissionDate,
                }).ToList();
            commis.Settings = settingDeatils;

            await this.InsertApproveDataToDb(commis, ModifyType.Delete);

            return true;
        }

        /// <summary>
        /// 檢查佣金設定資料是否可被刪除
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-刪除 API Req</param>
        public void CheckValidateForDelete(CommissionReq req)
        {
            #region 是否可刪除商品
            // 依公司代碼  查詢 「專案主檔」的 專案代碼
            var productMasterProductIds = this.productMasterRepo.Queryable()
                .Where(o => o.CompanyCode == req.CompanyCode)
                .Select(o => o.ProductId)
                .AsList();

            //this.eventLogService.Trace("篩選存在保單主檔的專案代碼開始", this.actionName, this.actionName);

            // 依商品險別、專案代碼查詢保單主檔，篩選存在的專案代碼
            productMasterProductIds = productMasterProductIds.Where(productId =>
                this.policyMasterRepo.Queryable().Any(p => p.PlanType == req.PlanType && p.ProductId == productId && p.BenefitTerm == req.BenefitTerm)
            ).AsList();

            //this.eventLogService.Trace("篩選存在保單主檔的專案代碼結束", this.actionName, this.actionName);

            //this.eventLogService.Trace("檢核刪除資料是否存在保單明細檔開始", this.actionName, this.actionName);

            foreach (var productId in productMasterProductIds)
            {
                // 專案主檔.專案代碼 與 req 資料查詢「保單明細檔」
                var policyDetaiExist = this.policyDetailRepo.Queryable()
                    .Any(o =>
                    o.ProductId == productId //專案代碼
                    && o.PlanCode == req.PlanCode //商品
                    && o.PaymentTerm == req.PaymentTerm //繳費年期
                    );

                if (policyDetaiExist)
                {
                    //this.eventLogService.Trace("檢核刪除資料是否存在保單明細檔結束", this.actionName, this.actionName);

                    throw new BusinessException("已有投保資料，無法刪除。");
                }

            }

            //this.eventLogService.Trace("檢核刪除資料是否存在保單明細檔結束", this.actionName, this.actionName);

            var keyStartWith = $"{req.CompanyCode}_{req.PlanCode}";// 保險公司代碼 + 商品代碼
            var keyEndWith = $"{req.PaymentTerm}_{req.PlanType}_{req.BenefitTerm}";// 繳費年期 + 商品險別 + 保障年期

            //this.eventLogService.Trace("檢核刪除資料是否存在覆核記錄檔開始", this.actionName, this.actionName);

            //檢查覆核檔是否有商品為覆核資訊
            // 商品 ApproveKey: ComponyCode_PlanCode_PlanVer_PaymentTerm_PlanType_BenefitTerm 組合, 比對開頭2欄 與 結尾3欄(排除 版本欄位)
            var planListExist = this.approveRecordRepo.Queryable()
            .Any(o => o.FunctionCode == FunctionCodeType.F1401 &&
            o.ApproveStatus == ApproveStatusType.WaitApprove &&
            o.ApproveKey.StartsWith(keyStartWith) &&
            o.ApproveKey.EndsWith(keyEndWith)
            );

            //this.eventLogService.Trace("檢核刪除資料是否存在覆核記錄檔結束", this.actionName, this.actionName);

            if (planListExist)
            {
                throw new BusinessException($"商品 {req.PlanCode} 已被異動等待覆核中，無法刪除。");
            }

            var isUseByPlan = this.campaignPlanMappingRepo.Queryable()
                .Any(o => o.CompanyCode == req.CompanyCode
                && o.PlanCode == req.PlanCode
                && o.PlanType == req.PlanType
                && o.PaymentTerm == req.PaymentTerm
                && o.BenefitTerm == req.BenefitTerm);
            if (isUseByPlan)
            {
                throw new BusinessException($"商品{req.PlanCode} 已被方案使用，無法刪除。");
            }
            var isUseByApprovePlan = (from ap in this.approveRecordRepo.Queryable().Where(o => o.FunctionCode == FunctionCodeType.F1501)
                                      join acpm in this.approveCampaignPlanMappingRepo.Queryable()
                                      on ap.ApproveRecordGid equals acpm.ApproveRecordGid
                                      where acpm.CompanyCode == req.CompanyCode
                                      && acpm.PlanCode == req.PlanCode
                                      && acpm.PlanType == req.PlanType
                                      && acpm.PaymentTerm == req.PaymentTerm
                                      && acpm.BenefitTerm == req.BenefitTerm
                                      select 1).Any();

            if (isUseByApprovePlan)
            {
                throw new BusinessException($"商品{req.PlanCode} 已被方案覆核中，無法刪除。");
            }


            #endregion

        }

        /// <summary>
        /// 佣金設定-查詢頁-確認修改 API
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-確認修改 API Req</param>
        /// <returns>True</returns>
        public async Task<bool> ConfirmEdit(CommissionInsertReq req)
        {
            // 編輯前檢查
            var commissionReq = this.mapper.Map<CommissionReq>(req);
            this.EditCheck(commissionReq);
            this.CheckEarliestCommissionDate(req);

            var settingDetails = this.commissionSettingDetailRepo.Queryable()
                .Where(o => o.CompanyCode == req.CompanyCode
                && o.PlanCode == req.PlanCode
                && o.PlanType == req.PlanType
                && o.PaymentTerm == req.PaymentTerm
                && o.BenefitTerm == req.BenefitTerm
                && o.CommissionDate <= DateTime.Now.Date).ToList();
            foreach (var settingDetail in settingDetails)
            {
                if (!req.Settings.Any(o => o.CalcType == settingDetail.CalcType && o.CalcValue == settingDetail.CalcValue && o.CommissionDate == settingDetail.CommissionDate))
                {
                    throw new BusinessException("不得修改系統日之前的佣金設定。");
                }
            }

            await this.InsertApproveDataToDb(req, ModifyType.Modfiy);

            return true;
        }

        /// <summary>
        /// 佣金設定-查詢頁-匯出 API
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-匯出 API Req</param>
        /// <returns>匯出檔案</returns>
        public async Task<byte[]> Export(SearchReq req)
        {
            try
            {
                string sqlCmd = this.GetCommissionSettingExportCmd(req);
                List<ExportResp> commisExportDatas = new List<ExportResp>();
                using (var conn = new SqlConnection(ConfigHelper.GetINSCon()))
                {
                    conn.Open();
                    commisExportDatas = conn.Query<ExportResp>(sqlCmd, req).ToList();
                }

                var filePath = Path.Combine(Environment.CurrentDirectory, "ReportTemplates", "佣金設定匯入格式.xlsx");
                var excelHelper = new ExcelHelper(filePath, 1, "佣金設定");

                excelHelper.WriteData(commisExportDatas, 0, 0);

                return await excelHelper.SaveExcel();
            }
            catch (Exception ex)
            {
                //eventLogService.Debug("CommissionService", "Export", "Export", ex);
                throw;
            }

        }

        /// <summary>
        /// 待覆核明細
        /// </summary>
        /// <param name="req">待覆核明細 Req</param>
        /// <returns>佣金待覆核明細 Resp</returns>
        public CommissionDetailResp ApproveDetail(CommissionApproveDetailReq req)
        {
            //佣金設定資料
            var commis = this.approveCommissionSettingRepo.Queryable()
            .Where(o => o.ApproveRecordGid == req.ApproveRecordGid)
            .FirstOrDefault();

            var settingDetails = this.approveCommissionSettingDetailRepo.Queryable()
                .Where(o => o.ApproveRecordGid == req.ApproveRecordGid).ToList();

            //公司名稱
            var companyName = this.GetCompanyName(req.CompanyCode);
            //商品主檔
            var planMaster = this.GetPlanMasterSaleState()
            .Where(o => o.CompanyCode == commis.CompanyCode && o.PlanCode == commis.PlanCode && o.PaymentTerm == commis.PaymentTerm && o.PlanType == commis.PlanType)
            .FirstOrDefault();

            var info = new ApproveInfo()
            {
                CompanyCodeStr = planMaster.CompanyCodeStr,
                PlanCodeStr = $"{planMaster.PlanCode} {planMaster.PlanShortName}(繳費年期{planMaster.PaymentTerm}年)",
                PlanTypeStr = planMaster.PlanTypeStr,
                BenefitTerm = planMaster.BenefitTerm,
                PaymentTerm = planMaster.PaymentTerm
            };

            var res = new CommissionDetailResp()
            {
                Info = info,
                Settings = settingDetails.Select(o => new ApproveSetting()
                {
                    CalcTypeStr = string.IsNullOrEmpty(o.CalcType) ? o.CalcType : this.commonService.GetSysParamValue(GroupIdType.CalcType, o.CalcType),
                    CalcValueStr = o.CalcType == CalcType.Ratio ? $"{o.CalcValue.ToString("0.##")}%" : o.CalcValue.ToString("0.##"),
                    CommissionDate = o.CommissionDate.ToString("yyyy/MM/dd")
                }).ToList(),
            };

            return res;
        }

        /// <summary>
        /// 應收佣金明細表-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">應收佣金明細表-查詢頁-查詢資料 API Req</param>
        /// <returns>應收佣金明細表-查詢頁-查詢資料 API Resp</returns>
        public DataTableQueryResp<ReceivableSearchResp> ReceivableSearch(ReceivableSearchReq req)
        {
            try
            {
                var commisList = this.commissionProfileRepo.Queryable()
            .QueryItem(o => o.CompanyCode == req.CompanyCode, req.CompanyCode)
            .QueryItem(o => o.CalcYear == req.CalcYear, req.CalcYear)
            .QueryItem(o => o.CalcMonth == req.CalcMonth, req.CalcMonth)
            .QueryItem(o => o.CalcItem == req.CalcItem, req.CalcItem);

                // 應收佣金明細表主檔,  保險公司主檔
                var joinTable = from cm in commisList
                                join cp in this.companyProfileRepo.Queryable() on cm.CompanyCode equals cp.CompanyCode into cpl
                                from cp in cpl.DefaultIfEmpty()
                                join ci in this.sysParamRepo.Queryable().Where(o => o.GroupId == GroupIdType.CalcItem) on cm.CalcItem equals ci.ItemId into cil
                                from ci in cil.DefaultIfEmpty()
                                join cs in this.sysParamRepo.Queryable().Where(o => o.GroupId == GroupIdType.CheckStatus) on cm.CheckStatus equals cs.ItemId into csl
                                from cs in csl.DefaultIfEmpty()
                                select new ReceivableSearchResp
                                {
                                    CompanyCodeStr = cm.CompanyCode + " " + (cp != null ? cp.CompanyShortName : string.Empty),
                                    CalcYearAndMonth = cm.CalcYear + cm.CalcMonth,
                                    CalcItemStr = ci.ItemValue,
                                    CalcBefore = cm.CalcBefore,
                                    CalcAfter = cm.CalcAfter,
                                    CalcCount = cm.CalcCount,
                                    CheckStatusStr = cs.ItemValue,
                                    CompanyCode = cm.CompanyCode,
                                    CalcItem = cm.CalcItem
                                };

                // 資料庫總筆數
                var totalCount = commisList.Count();

                // 組合資料
                var searchRes = joinTable
                    .OrderBySortInfo(req.order, req.columns)
                    .Pagination(req.length, req.start);

                return new DataTableQueryResp<ReceivableSearchResp>(searchRes, totalCount);
            }
            catch (Exception ex)
            {
                //eventLogService.Debug("CommissionService", "ReceivableSearch", "ReceivableSearch", ex);
                throw;
            }
        }

        /// <summary>
        /// 應收佣金明細表-明細頁-查詢 API
        /// </summary>
        /// <param name="req">應收佣金明細表-明細頁-查詢 API Req</param>
        /// <param name="isExport">匯出模式</param>
        /// <returns>應收佣金明細表-明細頁-查詢 API Resp</returns>
        public ReceivableDetailResp ReceivableDetail(ReceivableDetailReq req, bool isExport = false)
        {
            //commissionDetail 與 policyMster join
            var details = from cd in this.commissionDetailRepo.Queryable()
                          join pm in this.policyMasterRepo.Queryable() on cd.AcceptNo equals pm.AcceptNo
                          select new { cd, pm };

            details = details
            .QueryItem(o => o.cd.CompanyCode == req.CompanyCode, req.CompanyCode)
            .QueryItem(o => o.cd.CalcYear == req.CalcYear, req.CalcYear)
            .QueryItem(o => o.cd.CalcMonth == req.CalcMonth, req.CalcMonth)
            .QueryItem(o => o.cd.CalcItem == req.CalcItem, req.CalcItem);

            //防個呆
            if (!details.Any()) throw new BusinessException("無此佣金明細。");

            var res = new ReceivableDetailResp();
            var resDetail = new List<Detail>();
            //Stack<int> totalModalItem = new Stack<int>();
            //Stack<int> totalCalcAMT = new Stack<int>();

            foreach (var d in details)
            {
                //專案名稱
                var productName = this.GetProductNameByProductId(d.pm.ProductId, req.CompanyCode);

                //商品名稱
                var planName = this.GetPlanNameByPlanCode(d.cd.PlanCode, req.CompanyCode, d.cd.PaymentTerm, d.cd.PlanType);

                //方案名稱
                var projuctName = this.GetProjuctNameByProjectCode(d.pm.ProjectCode, req.CompanyCode, d.pm.ProductId);

                //var detail = this.mapper.Map<Detail>(d.cd);
                var detail = new Detail();
                detail.PolicyNo = d.pm.PolicyNo;
                detail.AcceptNo = d.cd.AcceptNo;
                detail.ProductIdAndProjectCode = $"{d.pm.ProductId} {productName}/{projuctName}";
                detail.PlanCodeStr = $"{d.cd.PlanCode} {planName}";
                detail.BenefitTerm = d.pm.BenefitTerm;
                detail.PaymentTerm = d.cd.PaymentTerm;
                detail.ModalItemPermiumWithDisc = d.cd.ModalItemPermiumWithDisc;
                detail.PlanTypeStr = this.commonService.GetSysParamValue(GroupIdType.PlanType, d.cd.PlanType);
                detail.PremiumMethodStr = this.commonService.GetSysParamValue(GroupIdType.PremiumMethod, d.pm.PremiumMethod);
                detail.CalcTypeStr = this.commonService.GetSysParamValue(GroupIdType.CalcType, d.cd.CalcType);
                detail.CalcValue = d.cd.CalcValue;
                detail.CalcAMT = d.cd.CalcAmt;

                resDetail.Add(detail);
                //把要累加的push到陣列裡
                //totalModalItem.Push((int)d.cd.ModalItemPermiumWithDisc);
                //totalCalcAMT.Push((int)d.cd.CalcAmt);
            }

            res.TotalModalItemPermiumWithDisc = details.Sum(o => o.cd.ModalItemPermiumWithDisc);
            res.TotalCalcAMT = details.Sum(o => o.cd.CalcAmt);

            if (isExport) // 資料排序
            {
                res.Detail = resDetail.AsQueryable().OrderBySortInfo(req.order, req.columns).ToList();
            }
            else
            {
                res.Detail = resDetail;
            }

            return res;
        }

        /// <summary>
        /// 取得專案名稱
        /// </summary>
        private Dictionary<string, string> productNameDit = new Dictionary<string, string>();

        /// <summary>
        /// 取得專案名稱 
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="companyCode"></param>
        /// <returns></returns>
        private string GetProductNameByProductId(string productId, string companyCode)
        {
            if (productNameDit.ContainsKey(productId)) return productNameDit[productId];
            else
            {
                var productName = this.productMasterRepo.Queryable()
                .Where(o => o.CompanyCode == companyCode && o.ProductId == productId)
                .FirstOrDefault()?.Name;

                return productName;
            }
        }

        /// <summary>
        /// 取得商品名稱
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> planNameDit = new Dictionary<string, string>();

        /// <summary>
        /// 取得商品名稱
        /// </summary>
        /// <param name="planCode"></param>
        /// <param name="companyCode"></param>
        /// <param name="paymentTerm"></param>
        /// <param name="planType"></param>
        /// <returns></returns>
        private string GetPlanNameByPlanCode(string planCode, string companyCode, int? paymentTerm, string planType)
        {
            if (planNameDit.ContainsKey(planCode)) return planNameDit[planCode];
            else
            {
                var planName = this.commonService.GetPlanMaxVersions()
                .Where(x => x.CompanyCode == companyCode &&
                x.PlanCode == planCode &&
                x.PaymentTerm == paymentTerm &&
                x.PlanType == planType
                ).FirstOrDefault()?.PlanShortName;

                return planName;
            }
        }

        /// <summary>
        /// 取得方案名稱
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> projuctNameDit = new Dictionary<string, string>();

        /// <summary>
        /// 取得方案名稱
        /// </summary>
        /// <param name="projectCode"></param>
        /// <param name="companyCode"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        private string GetProjuctNameByProjectCode(string projectCode, string companyCode, string productId)
        {
            if (projuctNameDit.ContainsKey(projectCode)) return projuctNameDit[projectCode];
            else
            {
                var projuctName = this.campaignMasterRepo.Queryable()
                 .Where(o => o.CompanyCode == companyCode &&
                 o.ProductId == productId &&
                 o.ProjectCode == projectCode)
                 .FirstOrDefault()?.Name;

                return projuctName;
            }
        }

        /// <summary>
        /// 應收佣金明細表-查詢頁-對佣完成 API
        /// </summary>
        /// <param name="req">應收佣金明細表-明細頁-查詢 API Req</param>
        /// <returns>True</returns>
        public async Task<bool> ReceivableCheck(ReceivableDetailReq req)
        {
            var commis = this.commissionProfileRepo.Queryable()
            .Where(o => o.CompanyCode == req.CompanyCode &&
            o.CalcYear == req.CalcYear &&
            o.CalcMonth == req.CalcMonth &&
            o.CalcItem == req.CalcItem).FirstOrDefault();

            //防個呆
            if (commis.CheckStatus == "1") throw new BusinessException("此佣金設定已對佣。");

            //更新該筆『應收佣金明細表主檔』資料
            commis.CheckStatus = "1";
            commis.CheckDate = DateTime.Now;
            commis.CheckBy = WebContext.Account;
            commis.UpdateBy = WebContext.Account;
            commis.UpdateOn = DateTime.Now;

            //更新『佣獎明細表』資料
            var details = this.commissionDetailRepo.Queryable()
            .Where(o => o.CompanyCode == req.CompanyCode &&
            o.CalcYear == req.CalcYear &&
            o.CalcMonth == req.CalcMonth &&
            o.CalcItem == req.CalcItem).ToList();

            foreach (var d in details)
            {
                //防個呆
                // if (d.CheckStatus == "1") throw new BusinessException($"保單號碼：{d.PolicyNo}/進件編號：{d.AcceptNo} 佣金設定明細已對佣。");

                d.CheckStatus = "1";
                d.CheckDate = DateTime.Now;
                d.UpdateBy = WebContext.Account;
                d.UpdateOn = DateTime.Now;
            }

            await this.commissionUnitOfWork.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// 應收佣金明細表-明細頁-匯出報表 API
        /// </summary>
        /// <param name="req">應收佣金明細表-明細頁-匯出報表 API Req</param>
        /// <returns>匯出檔案</returns>
        public async Task<byte[]> ReceivableExport(ReceivableDetailReq req)
        {
            var commisData = this.ReceivableDetail(req, true).Detail;
            var commisExportDatas = commisData.ToList();

            var filePath = Path.Combine(Environment.CurrentDirectory, "ReportTemplates", "應收佣金明細表匯入格式.xlsx");
            var excelHelper = new ExcelHelper(filePath, 1, "應收佣金明細表");

            excelHelper.WriteData(commisExportDatas, 0, 0);

            return await excelHelper.SaveExcel();
        }

        /// <summary>
        /// 取得公司名稱
        /// </summary>
        /// <param name="c">保險公司代碼</param>
        /// <returns>保險公司名稱</returns>
        private string GetCompanyName(string c)
        {
            var res = this.companyProfileRepo.Queryable().Where(x => x.CompanyCode == c).FirstOrDefault().CompanyShortName;

            return res;
        }


        /// <summary>
        /// 取得覆核key 保險公司代碼_商品代碼_繳費年期_商品險別_保障年期
        /// </summary>
        /// <param name="p">覆核項目</param>
        /// <returns>覆核key</returns>
        private string GetCommissionApproveKey(dynamic p)
        {
            //欲新增的PK：保險公司代碼_商品代碼_繳費年期_商品險別_保障年期
            if (p is CommissionInsertReq)
            {
                var CommissionInsertReq = p as CommissionInsertReq;

                var res = $"{CommissionInsertReq.CompanyCode}_{CommissionInsertReq.PlanCode}_{CommissionInsertReq.PaymentTerm}_{CommissionInsertReq.PlanType}_{CommissionInsertReq.BenefitTerm}";

                return res;
            }
            else if (p is CommissionReq)
            {
                var commisReq = p as CommissionReq;

                var res = $"{commisReq.CompanyCode}_{commisReq.PlanCode}_{commisReq.PaymentTerm}_{commisReq.PlanType}_{commisReq.BenefitTerm}";

                return res;
            }

            else
            {
                return "ApproveKey錯誤";
            }

        }

        /// <summary>
        /// 取得覆核Item
        /// </summary>
        /// <param name="p">覆核項目</param>
        /// <returns>覆核Item</returns>
        private string GetCommissionApproveItem(dynamic p)
        {
            //：商品代碼+空格 +商品簡稱(繳費年期X年)eg. 21 強制責任保險(繳費年期1年)

            if (p is CommissionInsertReq)
            {
                var CommissionInsertReq = p as CommissionInsertReq;

                var plan = this.commonService.GetPlanMaxVersions()
                .Where(x => x.CompanyCode == CommissionInsertReq.CompanyCode && x.PlanCode == CommissionInsertReq.PlanCode && x.PaymentTerm == CommissionInsertReq.PaymentTerm && x.PlanType == CommissionInsertReq.PlanType && x.BenefitTerm == CommissionInsertReq.BenefitTerm).FirstOrDefault();

                var res = $"{p.PlanCode} {plan?.PlanShortName}(繳費年期{p.PaymentTerm}年、保障年期{p.BenefitTerm}年)";

                return res;

            }
            else if (p is CommissionReq)
            {
                var commisReq = p as CommissionReq;

                var plan = this.commonService.GetPlanMaxVersions()
               .Where(x => x.CompanyCode == commisReq.CompanyCode && x.PlanCode == commisReq.PlanCode && x.PaymentTerm == commisReq.PaymentTerm && x.PlanType == commisReq.PlanType && x.BenefitTerm == commisReq.BenefitTerm).FirstOrDefault();

                var res = $"{p.PlanCode} {plan?.PlanShortName}(繳費年期{p.PaymentTerm}年、保障年期{p.BenefitTerm}年))";

                return res;
            }
            else
            {
                return "ApproveKey錯誤";
            }
        }

        /// <summary>
        /// 新增資料進 db
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-新增 API Req</param>
        /// <param name="modifyType">異動型態</param>
        /// <returns>True</returns>
        private async Task<bool> InsertApproveDataToDb(CommissionInsertReq req, string modifyType)
        {
            if (modifyType == ModifyType.Insert)
            {
                //this.eventLogService.Trace("檢核刪除商品是否重複開始", this.actionName, this.actionName);

                var commis = this.commissionSettingRepo.Queryable()
                .Where(o => o.CompanyCode == req.CompanyCode && o.PlanCode == req.PlanCode && o.PaymentTerm == req.PaymentTerm && o.PlanType == req.PlanType && o.BenefitTerm == req.BenefitTerm).FirstOrDefault();

                //this.eventLogService.Trace("檢核刪除商品是否重複結束", this.actionName, this.actionName);

                if (commis != null) throw new BusinessException($"商品 {req.PlanCode} 重複");
            }

            var guid = Guid.NewGuid();

            //覆核記錄檔
            var record = new ApproveRecord()
            {
                ApproveRecordGid = guid,
                FunctionCode = FunctionCodeType.F1901,
                ApproveItem = this.GetCommissionApproveItem(req),
                ApproveKey = this.GetCommissionApproveKey(req),
                ModifyType = modifyType,
                ModifyBy = WebContext.Account,
                ModifyOn = DateTime.Now,
                ApproveStatus = ApproveStatusType.WaitApprove, //尚未覆核
            };
            this.approveRecordRepo.Insert(record);

            //佣金設定覆核檔
            var approveCommissionSetting = new ApproveCommissionSetting()
            {
                ApproveRecordGid = guid,
                CompanyCode = req.CompanyCode,
                PlanCode = req.PlanCode,
                PlanType = req.PlanType,
                PaymentTerm = req.PaymentTerm,
                BenefitTerm = req.BenefitTerm,
                CreateBy = WebContext.Account,
                CreateOn = DateTime.Now
            };
            foreach (var setting in req.Settings)
            {
                var approveCommissionSettingDetail = new ApproveCommissionSettingDetail()
                {
                    ApproveRecordGid = guid,
                    CompanyCode = req.CompanyCode,
                    PlanCode = req.PlanCode,
                    PlanType = req.PlanType,
                    PaymentTerm = req.PaymentTerm,
                    BenefitTerm = req.BenefitTerm,
                    CalcType = setting.CalcType,
                    CalcValue = setting.CalcValue,
                    CommissionDate = setting.CommissionDate,
                    CreateBy = WebContext.Account,
                    CreateOn = DateTime.Now
                };
                this.approveCommissionSettingDetailRepo.Insert(approveCommissionSettingDetail);
            }

            this.approveCommissionSettingRepo.Insert(approveCommissionSetting);

            //this.eventLogService.Trace("寫入覆核檔開始", this.actionName, this.actionName);

            await this.commissionUnitOfWork.SaveChangesAsync();

            //this.eventLogService.Trace("寫入覆核檔結束", this.actionName, this.actionName);

            return true;
        }

        /// <summary>
        /// 取得
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanMaster> GetPlanMasterWithoutPlanVer()
        {
            var planGroups = this.planMasterRepo.Queryable()
                .GroupBy(o => new { o.CompanyCode, o.PlanCode, o.PaymentTerm, o.PlanType, o.BenefitTerm })
                .ToList();

            var results = new List<PlanMaster>();
            foreach (var planMasters in planGroups)
            {
                var latestProduct = planMasters.OrderByDescending(o => o.PlanVer).First();
                var oldestProduct = planMasters.OrderBy(o => o.PlanVer).First();
                var product = this.mapper.Map<PlanMaster>(latestProduct);
                product.PlanStartOn = oldestProduct.PlanStartOn;
                results.Add(product);
            }


            return results;
        }

        /// <summary>
        /// 商品主檔(取”保障年期”優先順序: 銷售中,未上架,已停售)
        /// </summary>
        /// <returns>商品主檔銷售狀態清單</returns>
        public IEnumerable<PlanMasterSaleStateModel> GetPlanMasterSaleState()
        {
            using (var con = new SqlConnection(ConfigHelper.GetINSCon()))
            {
                var sqlCommand = @"
With
PlanMasterSaleState AS
(
	Select final.* From
	(
		Select p.*
		,ROW_NUMBER() OVER(PARTITION BY p.CompanyCode,p.PlanCode,p.PaymentTerm,p.PlanType ORDER BY p.SaleStateOrder) as RowNumberSaleStateOrder
		 From
		 (
			Select pm.*
			,(Select Top 1 ItemValue From SysParam Where GroupId='SaleState' and ItemId=pm.SaleState) as SaleStateStr
			,case 
				when pm.SaleState=1 then 1 --銷售中 1
				when pm.SaleState=0 then 2 --未上架 0 
				when pm.SaleState=2 then 3 --已停售 2
				else ''
			end as SaleStateOrder --銷售狀態排序:銷售中,未上架,已停售
			, ROW_NUMBER() OVER(PARTITION BY pm.CompanyCode,pm.PlanCode,pm.PaymentTerm,pm.PlanType,pm.SaleState ORDER BY pm.PlanStartOn desc) as RowNumberPlanStartOn
			From
			(
				Select CompanyCode,PlanCode,PlanVer,PaymentTerm,BenefitTerm,PlanType,PlanShortName,PlanStartOn,PlanEndOn,
				case 
					when GETDATE() < PlanStartOn then 0--若系統日<商品銷售-起日，顯示：未上架 0 
					when (GETDATE() >= PlanStartOn and GETDATE() <= DATEADD(second,-1,DATEADD(day,1,PlanEndOn))) then 1 --若系統日在商品銷售-起日、商品銷售-迄日內，顯示：銷售中 1
					when GETDATE() > DATEADD(second,-1,DATEADD(day,1,PlanEndOn)) then 2 --若系統日 > 商品銷售 - 迄日，顯示：已停售 2
					else ''
				end as SaleState
				From PlanMaster
			)As pm
		)As p Where p.RowNumberPlanStartOn=1 --篩選商品銷售-起日降冪排序後第一筆
	)As final where final.RowNumberSaleStateOrder=1  --篩選銷售狀態排序後的第一筆
)

Select 
ps.CompanyCode+' '+cp.CompanyName AS CompanyCodeStr
,ps.PlanCode+' '+ps.PlanShortName AS PlanCodeStr
,(Select Top 1 ItemValue From SysParam Where GroupId='PlanType' and ItemId=ps.PlanType) AS PlanTypeStr
,ps.* 
From PlanMasterSaleState As ps
Left join CompanyProfile As cp On ps.CompanyCode=cp.CompanyCode
";

                var result = con.Query<PlanMasterSaleStateModel>(sqlCommand);

                return result;
            }
        }

        /// <summary>
        /// 佣金查詢sql
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-查詢資料 API Req</param>
        /// <param name="countCmd">string.Empty</param>
        /// <param name="isExport">匯出模式</param>
        /// <returns>SQL 語法</returns>
        private string GetCommissionSettingSearchCmd(SearchReq req, out string countCmd)
        {
            var sqlCmd = string.Empty;
            string where = string.Empty;

            // 排序、分頁條件
            var order = req.GetSortCmd(typeof(SearchResp), isExport: false);

            if (!string.IsNullOrEmpty(req.CompanyCode))
            {
                where += "and pml.CompanyCode = @CompanyCode ";
            }

            if (!string.IsNullOrEmpty(req.PlanType))
            {
                where += "and pml.PlanType = @PlanType ";
            }

            if (!string.IsNullOrEmpty(req.PlanCode))
            {
                where += "and pml.PlanCode = @PlanCode ";
            }

            if (!string.IsNullOrEmpty(req.PlanShortName))
            {
                where += "and pml.PlanShortName = @PlanShortName ";
            }

            if (!string.IsNullOrEmpty(req.SaleState))
            {
                where += "and pml.SaleState = @SaleState ";
            }

            if (!string.IsNullOrEmpty(req.BenefitTerm))
            {
                where += "and pml.BenefitTerm = @BenefitTerm ";
            }

            if (!string.IsNullOrEmpty(req.PaymentTerm))
            {
                where += "and pml.PaymentTerm = @PaymentTerm ";
            }


            sqlCmd = $@"
With
PlanMasterSaleState AS
(
	Select final.* From
	(
		Select p.*
		,ROW_NUMBER() OVER(PARTITION BY p.CompanyCode, p.PlanCode, p.PaymentTerm, p.PlanType, p.BenefitTerm ORDER BY p.SaleStateOrder) as RowNumberSaleStateOrder
		 From
		 (
			Select pm.*
			,(Select Top 1 ItemValue From SysParam Where GroupId='SaleState' and ItemId=pm.SaleState) as SaleStateStr
			,case 
				when pm.SaleState=1 then 1 --銷售中 1
				when pm.SaleState=0 then 2 --未上架 0 
				when pm.SaleState=2 then 3 --已停售 2
				else ''
			end as SaleStateOrder --銷售狀態排序:銷售中,未上架,已停售
			, ROW_NUMBER() OVER(PARTITION BY pm.CompanyCode, pm.PlanCode, pm.PaymentTerm, pm.PlanType, pm.BenefitTerm, pm.SaleState ORDER BY pm.PlanStartOn desc) as RowNumberPlanStartOn
			From
			(
				Select CompanyCode,PlanCode,PlanVer,PaymentTerm,BenefitTerm,PlanType,PlanShortName,PlanStartOn,PlanEndOn,
				case 
					when GETDATE() < PlanStartOn then 0--若系統日<商品銷售-起日，顯示：未上架 0 
					when (GETDATE() >= PlanStartOn and GETDATE() <= DATEADD(second,-1,DATEADD(day,1,PlanEndOn))) then 1 --若系統日在商品銷售-起日、商品銷售-迄日內，顯示：銷售中 1
					when GETDATE() > DATEADD(second,-1,DATEADD(day,1,PlanEndOn)) then 2 --若系統日 > 商品銷售 - 迄日，顯示：已停售 2
					else ''
				end as SaleState
				From PlanMaster
			)As pm
		)As p Where p.RowNumberPlanStartOn=1 --篩選商品銷售-起日降冪排序後第一筆
	)As final where final.RowNumberSaleStateOrder=1  --篩選銷售狀態排序後的第一筆
),
PlanMasterList As
(
	Select 
	ps.CompanyCode+' '+cp.CompanyName AS CompanyCodeStr
	,ps.PlanCode+' '+ps.PlanShortName AS PlanCodeStr
	,(Select Top 1 ItemValue From SysParam Where GroupId='PlanType' and ItemId=ps.PlanType) AS PlanTypeStr
	,ps.* 
	From PlanMasterSaleState As ps
	Left join CompanyProfile As cp On ps.CompanyCode=cp.CompanyCode
)

Select
pml.*
From CommissionSetting as cs
join PlanMasterList as pml on 
cs.CompanyCode=pml.CompanyCode and 
cs.PlanCode=pml.PlanCode and 
cs.PaymentTerm=pml.PaymentTerm and 
cs.PlanType=pml.PlanType and
cs.BenefitTerm = pml.BenefitTerm
Where 1=1
{where} 
{order} ";


            countCmd = $@"
With
PlanMasterSaleState AS
(
	Select final.* From
	(
		Select p.*
		,ROW_NUMBER() OVER(PARTITION BY p.CompanyCode, p.PlanCode, p.PaymentTerm, p.PlanType, p.BenefitTerm ORDER BY p.SaleStateOrder) as RowNumberSaleStateOrder
		 From
		 (
			Select pm.*
			,(Select Top 1 ItemValue From SysParam Where GroupId='SaleState' and ItemId=pm.SaleState) as SaleStateStr
			,case 
				when pm.SaleState=1 then 1 --銷售中 1
				when pm.SaleState=0 then 2 --未上架 0 
				when pm.SaleState=2 then 3 --已停售 2
				else ''
			end as SaleStateOrder --銷售狀態排序:銷售中,未上架,已停售
			, ROW_NUMBER() OVER(PARTITION BY pm.CompanyCode, pm.PlanCode, pm.PaymentTerm, pm.PlanType, pm.BenefitTerm, pm.SaleState ORDER BY pm.PlanStartOn desc) as RowNumberPlanStartOn
			From
			(
				Select CompanyCode,PlanCode,PlanVer,PaymentTerm,BenefitTerm,PlanType,PlanShortName,PlanStartOn,PlanEndOn,
				case 
					when GETDATE() < PlanStartOn then 0--若系統日<商品銷售-起日，顯示：未上架 0 
					when (GETDATE() >= PlanStartOn and GETDATE() <= DATEADD(second,-1,DATEADD(day,1,PlanEndOn))) then 1 --若系統日在商品銷售-起日、商品銷售-迄日內，顯示：銷售中 1
					when GETDATE() > DATEADD(second,-1,DATEADD(day,1,PlanEndOn)) then 2 --若系統日 > 商品銷售 - 迄日，顯示：已停售 2
					else ''
				end as SaleState
				From PlanMaster
			)As pm
		)As p Where p.RowNumberPlanStartOn=1 --篩選商品銷售-起日降冪排序後第一筆
	)As final where final.RowNumberSaleStateOrder=1  --篩選銷售狀態排序後的第一筆
),
PlanMasterList As
(
	Select 
	ps.CompanyCode+' '+cp.CompanyName AS CompanyCodeStr
	,ps.PlanCode+' '+ps.PlanShortName AS PlanCodeStr
	,(Select Top 1 ItemValue From SysParam Where GroupId='PlanType' and ItemId=ps.PlanType) AS PlanTypeStr
	,ps.* 
	From PlanMasterSaleState As ps
	Left join CompanyProfile As cp On ps.CompanyCode=cp.CompanyCode
)

Select COUNT(*)
From CommissionSetting as cs
join PlanMasterList as pml on 
cs.CompanyCode=pml.CompanyCode and 
cs.PlanCode=pml.PlanCode and 
cs.PaymentTerm=pml.PaymentTerm and 
cs.PlanType=pml.PlanType and
cs.BenefitTerm = pml.BenefitTerm
Where 1=1
{where} ";


            return sqlCmd;
        }

        /// <summary>
        /// 佣金查詢sql
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-查詢資料 API Req</param>
        /// <param name="countCmd">string.Empty</param>
        /// <param name="isExport">匯出模式</param>
        /// <returns>SQL 語法</returns>
        private string GetCommissionSettingExportCmd(SearchReq req)
        {
            var sqlCmd = string.Empty;
            string where = string.Empty;
            // 排序、分頁條件
            var order = req.GetSortCmd(typeof(ExportResp), isExport: true);
            if (!string.IsNullOrEmpty(req.CompanyCode))
            {
                where += "and pml.CompanyCode = @CompanyCode ";
            }

            if (!string.IsNullOrEmpty(req.PlanType))
            {
                where += "and pml.PlanType = @PlanType ";
            }

            if (!string.IsNullOrEmpty(req.PlanCode))
            {
                where += "and pml.PlanCode = @PlanCode ";
            }

            if (!string.IsNullOrEmpty(req.PlanShortName))
            {
                where += "and pml.PlanShortName = @PlanShortName ";
            }

            if (!string.IsNullOrEmpty(req.SaleState))
            {
                where += "and pml.SaleState = @SaleState ";
            }

            if (!string.IsNullOrEmpty(req.BenefitTerm))
            {
                where += "and pml.BenefitTerm = @BenefitTerm ";
            }

            if (!string.IsNullOrEmpty(req.PaymentTerm))
            {
                where += "and pml.PaymentTerm = @PaymentTerm ";
            }


            sqlCmd = $@"
With
PlanMasterSaleState AS
(
	Select final.* From
	(
		Select p.*
		,ROW_NUMBER() OVER(PARTITION BY p.CompanyCode, p.PlanCode, p.PaymentTerm, p.PlanType, p.BenefitTerm ORDER BY p.SaleStateOrder) as RowNumberSaleStateOrder
		 From
		 (
			Select pm.*
			,(Select Top 1 ItemValue From SysParam Where GroupId='SaleState' and ItemId=pm.SaleState) as SaleStateStr
			,case 
				when pm.SaleState=1 then 1 --銷售中 1
				when pm.SaleState=0 then 2 --未上架 0 
				when pm.SaleState=2 then 3 --已停售 2
				else ''
			end as SaleStateOrder --銷售狀態排序:銷售中,未上架,已停售
			, ROW_NUMBER() OVER(PARTITION BY pm.CompanyCode, pm.PlanCode, pm.PaymentTerm, pm.PlanType, pm.BenefitTerm, pm.SaleState ORDER BY pm.PlanStartOn desc) as RowNumberPlanStartOn
			From
			(
				Select CompanyCode,PlanCode,PlanVer,PaymentTerm,BenefitTerm,PlanType,PlanShortName,PlanStartOn,PlanEndOn,
				case 
					when GETDATE() < PlanStartOn then 0--若系統日<商品銷售-起日，顯示：未上架 0 
					when (GETDATE() >= PlanStartOn and GETDATE() <= DATEADD(second,-1,DATEADD(day,1,PlanEndOn))) then 1 --若系統日在商品銷售-起日、商品銷售-迄日內，顯示：銷售中 1
					when GETDATE() > DATEADD(second,-1,DATEADD(day,1,PlanEndOn)) then 2 --若系統日 > 商品銷售 - 迄日，顯示：已停售 2
					else ''
				end as SaleState
				From PlanMaster
			)As pm
		)As p Where p.RowNumberPlanStartOn=1 --篩選商品銷售-起日降冪排序後第一筆
	)As final where final.RowNumberSaleStateOrder=1  --篩選銷售狀態排序後的第一筆
),
PlanMasterList As
(
	Select 
	ps.CompanyCode+' '+cp.CompanyName AS CompanyCodeStr
	,ps.PlanCode+' '+ps.PlanShortName AS PlanCodeStr
	,(Select Top 1 ItemValue From SysParam Where GroupId='PlanType' and ItemId=ps.PlanType) AS PlanTypeStr
	,ps.* 
	From PlanMasterSaleState As ps
	Left join CompanyProfile As cp On ps.CompanyCode=cp.CompanyCode
)

Select
pml.*
,(Select Top 1 ItemValue From SysParam Where GroupId='CalcType' and ItemId=csd.CalcType) As CalcTypeStr
,case 
	when csd.CalcType='1' then Cast(csd.CalcValue as nvarchar)+'%'
	else Cast(csd.CalcValue as nvarchar)
end as CalcValueStr
,convert(varchar, csd.CommissionDate, 111) AS CommissionDate
From CommissionSettingDetail as csd
join PlanMasterList as pml on 
csd.CompanyCode=pml.CompanyCode and 
csd.PlanCode=pml.PlanCode and 
csd.PaymentTerm=pml.PaymentTerm and 
csd.PlanType=pml.PlanType and
csd.BenefitTerm = pml.BenefitTerm
Where 1=1
{where}
{order}";
            return sqlCmd;
        }

        /// <summary>
        /// 取得商品最小銷售起日
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private DateTime GetEarliestSaleDateOfProduct(dynamic p)
        {
            var planMasterQuery = this.planMasterRepo.Queryable();
            if (p is CommissionInsertReq)
            {
                var req = p as CommissionInsertReq;
                planMasterQuery = planMasterQuery.Where(o => o.CompanyCode == req.CompanyCode && o.PlanCode == req.PlanCode && o.PlanType == req.PlanType && o.PaymentTerm == req.PaymentTerm && o.BenefitTerm == req.BenefitTerm);
            }
            else if (p is CommissionReq)
            {
                var req = p as CommissionReq;
                planMasterQuery = planMasterQuery.Where(o => o.CompanyCode == req.CompanyCode && o.PlanCode == req.PlanCode && o.PlanType == req.PlanType && o.PaymentTerm == req.PaymentTerm && o.BenefitTerm == req.BenefitTerm);
            }
            else
            {
                throw new BusinessException("無法取得商品最小銷售起日。");
            }
            return planMasterQuery.OrderBy(o => o.PlanStartOn).First().PlanStartOn;
        }
    }
}
