﻿using System.Threading.Tasks;
using InsureBrick.Modules.Commission.Interface.Models;
using TPI.NetCore.WebAPI.Models;

namespace InsureBrick.Modules.Commission.Interface
{
    /// <summary>
    /// 佣金相關 Service Interface
    /// </summary>
    public interface ICommissionService
    {
        /// <summary>
        /// 佣金設定-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-查詢資料 API Req</param>
        /// <returns>查詢結果</returns>
        DataTableQueryResp<SearchResp> Search(SearchReq req);

        /// <summary>
        /// 佣金設定-複製/編輯頁/明細頁-查詢資料 API
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-刪除 API Req</param>
        /// <returns>佣金設定-查詢頁-刪除 API Resp</returns>
        DetailResp Detail(CommissionReq req);

        /// <summary>
        /// 佣金設定-編輯頁-編輯前檢驗 API
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-刪除 API Req</param>
        /// <returns>True</returns>
        bool EditCheck(CommissionReq req);

        /// <summary>
        /// 佣金設定-查詢頁-新增 API
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-新增 API</param>
        /// <returns>True</returns>
        Task<bool> Insert(CommissionInsertReq req);

        /// <summary>
        /// 佣金設定-查詢頁-刪除 API
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-刪除 API Req</param>
        /// <returns>True</returns>
        Task<bool> Delete(CommissionReq req);

        /// <summary>
        /// 佣金設定-查詢頁-確認修改 API
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-確認修改 API Req</param>
        /// <returns>True</returns>
        Task<bool> ConfirmEdit(CommissionInsertReq req);

        /// <summary>
        /// 佣金設定-查詢頁-匯出 API
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-匯出 API Req</param>
        /// <returns>匯出檔案</returns>
        Task<byte[]> Export(SearchReq req);

        /// <summary>
        /// 待覆核明細
        /// </summary>
        /// <param name="req">待覆核明細 Req</param>
        /// <returns>佣金待覆核明細 Resp</returns>
        CommissionDetailResp ApproveDetail(CommissionApproveDetailReq req);

        /// <summary>
        /// 應收佣金明細表-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">應收佣金明細表-查詢頁-查詢資料 API Req</param>
        /// <returns>應收佣金明細表-查詢頁-查詢資料 API Resp</returns>
        DataTableQueryResp<ReceivableSearchResp> ReceivableSearch(ReceivableSearchReq req);

        /// <summary>
        /// 應收佣金明細表-明細頁-查詢 API
        /// </summary>
        /// <param name="req">應收佣金明細表-明細頁-查詢 API Req</param>
        /// <param name="isExport">匯出模式</param>
        /// <returns>應收佣金明細表-明細頁-查詢 API Resp</returns>
        ReceivableDetailResp ReceivableDetail(ReceivableDetailReq req, bool isExport = false);

        /// <summary>
        /// 應收佣金明細表-查詢頁-對佣完成 API
        /// </summary>
        /// <param name="req">應收佣金明細表-明細頁-查詢 API Req</param>
        /// <returns>True</returns>
        Task<bool> ReceivableCheck(ReceivableDetailReq req);

        /// <summary>
        /// 應收佣金明細表-明細頁-匯出報表 API
        /// </summary>
        /// <param name="req">應收佣金明細表-明細頁-匯出報表 API Req</param>
        /// <returns>匯出檔案</returns>
        Task<byte[]> ReceivableExport(ReceivableDetailReq req);

        /// <summary>
        /// 檢查佣金設定資料是否可被刪除
        /// </summary>
        /// <param name="req">佣金設定-查詢頁-刪除 API Req</param>
        void CheckValidateForDelete(CommissionReq req);
    }
}
