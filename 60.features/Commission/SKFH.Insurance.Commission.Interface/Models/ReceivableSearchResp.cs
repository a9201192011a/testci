﻿namespace InsureBrick.Modules.Commission.Interface.Models
{
    /// <summary>
    /// 應收佣金明細表-查詢頁-查詢資料 API Resp
    /// </summary>
    public class ReceivableSearchResp
    {
        /// <summary>
        /// 保險公司
        /// </summary>
        public string CompanyCodeStr { get; set; }

        /// <summary>
        /// 計績年月
        /// </summary>
        public string CalcYearAndMonth { get; set; }

        /// <summary>
        /// 佣獎項目
        /// </summary>
        public string CalcItemStr { get; set; }

        /// <summary>
        /// 保費總額
        /// </summary>
        public decimal? CalcBefore { get; set; }

        /// <summary>
        /// 佣金/獎金總額
        /// </summary>
        public decimal? CalcAfter { get; set; }

        /// <summary>
        /// 總筆數
        /// </summary>
        public int? CalcCount { get; set; }

        /// <summary>
        /// 對佣狀態
        /// </summary>
        public string CheckStatusStr { get; set; }

        /// <summary>
        /// 保險公司代碼
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 佣獎項目(R 獎金/C 佣金)
        /// </summary>
        public string CalcItem { get; set; }
    }
}