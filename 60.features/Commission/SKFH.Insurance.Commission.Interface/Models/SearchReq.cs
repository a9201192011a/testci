﻿using TPI.NetCore.WebAPI.Models;

namespace InsureBrick.Modules.Commission.Interface.Models
{
    /// <summary>
    /// 佣金設定-查詢頁-查詢資料 API Req
    /// 佣金設定-查詢頁-匯出 API Req
    /// </summary>
    public class SearchReq : DataTableQueryReq
    {
        /// <summary>
        /// 公司代號
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 險別
        /// </summary>
        public string PlanType { get; set; }

        /// <summary>
        /// 商品代號
        /// </summary>
        public string PlanCode { get; set; }

        /// <summary>
        /// 商品簡稱
        /// </summary>
        public string PlanShortName { get; set; }

        /// <summary>
        /// 銷售狀態
        /// </summary>
        public string SaleState { get; set; }

        /// <summary>
        /// 保障年期
        /// </summary>
        public string BenefitTerm { get; set; }

        /// <summary>
        /// 繳費年期
        /// </summary>
        public string PaymentTerm { get; set; }

    }
}