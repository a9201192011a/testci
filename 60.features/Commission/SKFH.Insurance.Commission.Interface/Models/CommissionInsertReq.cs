using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace InsureBrick.Modules.Commission.Interface.Models
{
    /// <summary>
    /// 佣金設定-查詢頁-新增 API Req
    /// 佣金設定-查詢頁-確認修改 API Req
    /// </summary>
    public class CommissionInsertReq
    {
        /// <summary>
        /// 公司代號
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 商品代號
        /// </summary>
        public string PlanCode { get; set; }

        /// <summary>
        /// 商品險別
        /// </summary>
        public string PlanType { get; set; }

        /// <summary>
        /// 繳費年期
        /// </summary>
        public int PaymentTerm { get; set; }

        /// <summary>
        /// 保障年期
        /// </summary>
        public int BenefitTerm { get; set; }

        /// <summary>
        /// 設定
        /// </summary>
        public List<Setting> Settings { get; set; } = new List<Setting>();
    }

    /// <summary>
    /// 設定
    /// </summary>
    public class Setting
    {
        /// <summary>
        /// 佣金計算方式
        /// </summary>
        public string CalcType { get; set; } //計算方式

        /// <summary>
        /// 佣金率/佣金金額
        /// </summary>
        public decimal CalcValue { get; set; } //比例

        /// <summary>
        /// 佣金生效日
        /// </summary>
        public DateTime CommissionDate { get; set; }
    }
}