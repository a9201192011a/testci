namespace InsureBrick.Modules.Commission.Interface.Models
{
    /// <summary>
    /// 佣金設定-查詢頁-查詢資料 API Resp
    /// </summary>
    public class SearchResp
    {
        /// <summary>
        /// 公司代號
        /// </summary>
        public string CompanyCodeStr { get; set; }

        /// <summary>
        /// 險別
        /// </summary>
        public string PlanTypeStr { get; set; }

        /// <summary>
        /// 商品代號
        /// </summary>
        public string PlanCodeStr { get; set; }

        /// <summary>
        /// 繳費年期
        /// </summary>
        public int PaymentTerm { get; set; }

        /// <summary>
        /// 保障年期
        /// </summary>
        public int BenefitTerm { get; set; }

        /// <summary>
        /// 銷售狀態
        /// </summary>
        public string SaleStateStr { get; set; }

        /// <summary>
        /// 公司代號
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 商品代號
        /// </summary>
        public string PlanCode { get; set; }

        /// <summary>
        /// 險別
        /// </summary>
        public string PlanType { get; set; }
    }
}