﻿using System.Collections.Generic;

namespace InsureBrick.Modules.Commission.Interface.Models
{
    /// <summary>
    /// 應收佣金明細表-明細頁-查詢 API Resp
    /// </summary>
    public class ReceivableDetailResp
    {
        /// <summary>
        /// 總實收保費
        /// </summary>
        public decimal? TotalModalItemPermiumWithDisc { get; set; }

        /// <summary>
        /// 總佣金
        /// </summary>
        public decimal? TotalCalcAMT { get; set; }

        /// <summary>
        /// 佣金明細
        /// </summary>
        public List<Detail> Detail { get; set; }

    }

    /// <summary>
    /// 佣金明細
    /// </summary>
    public class Detail
    {
        /// <summary>
        /// 保單號碼
        /// </summary>
        public string PolicyNo { get; set; }

        /// <summary>
        /// 進件編號
        /// </summary>
        public string AcceptNo { get; set; }

        /// <summary>
        /// 專案/方案
        /// </summary>
        public string ProductIdAndProjectCode { get; set; }

        /// <summary>
        /// 商品
        /// </summary>
        public string PlanCodeStr { get; set; }

        /// <summary>
        /// 商品險別
        /// </summary>
        public string PlanTypeStr { get; set; }

        /// <summary>
        /// 保障年度
        /// </summary>
        public int? BenefitTerm { get; set; }

        /// <summary>
        /// 繳費年期
        /// </summary>
        public int? PaymentTerm { get; set; }

        /// <summary>
        /// 繳別
        /// </summary>
        public string PremiumMethodStr { get; set; }

        /// <summary>
        /// 實收保費
        /// </summary>
        public decimal? ModalItemPermiumWithDisc { get; set; }

        /// <summary>
        /// 計算方式
        /// </summary>
        public string CalcTypeStr { get; set; }

        /// <summary>
        /// 佣金率/佣金金額
        /// </summary>
        public decimal? CalcValue { get; set; }

        /// <summary>
        /// 佣金
        /// </summary>
        public decimal? CalcAMT { get; set; }
    }
}