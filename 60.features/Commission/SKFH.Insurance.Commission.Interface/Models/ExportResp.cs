namespace InsureBrick.Modules.Commission.Interface.Models
{
    /// <summary>
    /// 匯出 Resp
    /// </summary>
    public class ExportResp
    {
        /// <summary>
        /// 保險公司代碼
        /// </summary>
        public string CompanyCodeStr { get; set; }

        /// <summary>
        /// 商品險別
        /// </summary>
        public string PlanTypeStr { get; set; }

        /// <summary>
        /// 商品代碼
        /// </summary>
        public string PlanCodeStr { get; set; }

        /// <summary>
        /// 繳費年期
        /// </summary>
        public int PaymentTerm { get; set; }

        /// <summary>
        /// 保障年期
        /// </summary>
        public int BenefitTerm { get; set; }

        /// <summary>
        /// 銷售狀態
        /// </summary>
        public string SaleStateStr { get; set; }

        /// <summary>
        /// 計算方式
        /// </summary>
        public string CalcTypeStr { get; set; }

        /// <summary>
        /// 固定比例/金額值
        /// </summary>
        public string CalcValueStr { get; set; }

        /// <summary>
        /// 佣金生效日
        /// </summary>
        public string CommissionDate { get; set; }

    }
}