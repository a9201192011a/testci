﻿namespace InsureBrick.Modules.Commission.Interface.Models
{
    /// <summary>
    /// 佣金設定-查詢頁-刪除 API Req
    /// </summary>
    public class CommissionReq
    {
        /// <summary>
        /// 公司代號
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 商品險別
        /// </summary>
        public string PlanType { get; set; }

        /// <summary>
        /// 商品代號
        /// </summary>
        public string PlanCode { get; set; }

        /// <summary>
        /// 繳費年期
        /// </summary>
        public int PaymentTerm { get; set; }

        /// <summary>
        /// 保障年期
        /// </summary>
        public int BenefitTerm { get; set; }
    }
}