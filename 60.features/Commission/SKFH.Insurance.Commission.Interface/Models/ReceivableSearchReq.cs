﻿using TPI.NetCore.WebAPI.Models;

namespace InsureBrick.Modules.Commission.Interface.Models
{
    /// <summary>
    /// 應收佣金明細表-查詢頁-查詢資料 API Req
    /// </summary>
    public class ReceivableSearchReq : DataTableQueryReq
    {
        /// <summary>
        /// 保險公司代碼
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 計績年
        /// </summary>
        public string CalcYear { get; set; }

        /// <summary>
        /// 計績月
        /// </summary>
        public string CalcMonth { get; set; }

        /// <summary>
        /// 佣獎項目
        /// </summary>
        public string CalcItem { get; set; }
    }
}