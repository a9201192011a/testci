using System;
using System.Collections.Generic;

namespace InsureBrick.Modules.Commission.Interface.Models
{
    /// <summary>
    /// 佣金待覆核明細 Resp
    /// </summary>
    public class CommissionDetailResp
    {
        /// <summary>
        /// 佣金資訊
        /// </summary>
        public ApproveInfo Info { get; set; }

        /// <summary>
        /// 首年佣金設定
        /// </summary>
        public List<ApproveSetting> Settings { get; set; } = new List<ApproveSetting>();
    }

    /// <summary>
    /// 覆核資訊
    /// </summary>
    public class ApproveInfo
    {
        /// <summary>
        /// 公司代號
        /// </summary>
        public string CompanyCodeStr { get; set; }

        /// <summary>
        /// 商品代號
        /// </summary>
        public string PlanCodeStr { get; set; }

        /// <summary>
        /// 商品險別
        /// </summary>
        public string PlanTypeStr { get; set; }

        /// <summary>
        /// 保障年期
        /// </summary>
        public int BenefitTerm { get; set; }

        /// <summary>
        /// 繳費年期
        /// </summary>
        public int PaymentTerm { get; set; }
    }

    /// <summary>
    /// 覆核設定
    /// </summary>
    public class ApproveSetting
    {
        /// <summary>
        /// 佣金計算方式
        /// </summary>
        public string CalcTypeStr { get; set; } //計算方式

        /// <summary>
        /// 佣金率/佣金金額
        /// </summary>
        public string CalcValueStr { get; set; } //比例

        /// <summary>
        /// 佣金生效日
        /// </summary>
        public string CommissionDate { get; set; }
    }
}