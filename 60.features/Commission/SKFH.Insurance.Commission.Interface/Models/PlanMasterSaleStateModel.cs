using System;

namespace InsureBrick.Modules.Commission.Interface.Models
{
    /// <summary>
    /// 商品主檔銷售狀態
    /// </summary>
    public class PlanMasterSaleStateModel
    {
        /// <summary>
        /// 保險公司代碼
        /// </summary>
        public string CompanyCodeStr { get; set; }

        /// <summary>
        /// 商品代碼
        /// </summary>
        public string PlanCodeStr { get; set; }

        /// <summary>
        /// 商品險別
        /// </summary>
        public string PlanTypeStr { get; set; }

        /// <summary>
        /// 保險公司代碼
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 商品代碼
        /// </summary>
        public string PlanCode { get; set; }

        /// <summary>
        /// 商品版本
        /// </summary>
        public string PlanVer { get; set; }

        /// <summary>
        /// 繳費年期
        /// </summary>
        public int PaymentTerm { get; set; }

        /// <summary>
        /// 保障年期
        /// </summary>
        public int BenefitTerm { get; set; }

        /// <summary>
        /// 商品險別
        /// </summary>
        public string PlanType { get; set; }

        /// <summary>
        /// 商品簡稱
        /// </summary>
        public string PlanShortName { get; set; }

        /// <summary>
        /// 商品銷售-起日
        /// </summary>
        public DateTime PlanStartOn { get; set; }

        /// <summary>
        /// 商品銷售-迄日
        /// </summary>
        public DateTime PlanEndOn { get; set; }

        /// <summary>
        /// 銷售狀態
        /// </summary>
        public string SaleState { get; set; }

        /// <summary>
        /// 銷售狀態
        /// </summary>
        public string SaleStateStr { get; set; }

        /// <summary>
        /// 銷售狀態排序
        /// </summary>
        public string SaleStateOrder { get; set; }

        /// <summary>
        /// 商品銷售-起日行號
        /// </summary>
        public string RowNumberPlanStartOn { get; set; }
    }
}