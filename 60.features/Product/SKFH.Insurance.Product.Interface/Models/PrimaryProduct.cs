﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsureBrick.Modules.Product.Interface.Models
{
    /// <summary>
    /// 依附主約
    /// </summary>
    public class PrimaryProduct
    {
        /// <summary>
        /// 保險公司代號
        /// </summary>
        public string CompanyCode { get; set; }
        
        /// <summary>
        /// 商品代碼
        /// </summary>
        public string PlanCode { get; set; }
        
        /// <summary>
        /// 商品版本
        /// </summary>
        public short PlanVer { get; set; }

        /// <summary>
        /// 商品類型
        /// </summary>
        public string PlanType { get; set; }

        /// <summary>
        /// 繳費年期
        /// </summary>
        public int PaymentTerm { get; set; }
  
        /// <summary>
        /// 保障年期
        /// </summary>
        public int BenefitTerm { get; set; }
    }
}
