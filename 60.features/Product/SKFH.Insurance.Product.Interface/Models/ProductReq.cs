﻿using Newtonsoft.Json;
using SKFH.Insurance.Product.Entity.Models;
using System.Collections.Generic;

namespace InsureBrick.Modules.Product.Interface.Models
{
    /// <summary>
    /// 商品管理設定 Req
    /// </summary>
    public class ProductReq : PlanMaster
    {
        /// <summary>
        /// 投保來源
        /// </summary>
        [JsonProperty(Order = 99)]
        public string[] CaseSource { get; set; }

        /// <summary>
        /// 依附主約
        /// </summary>
        public List<PrimaryProduct> PrimaryProducts { get; set; } = new List<PrimaryProduct>();

        /// <summary>
        /// 商品保額項目
        /// </summary>
        public List<PlanAssuredItem> PlanAssuredItems { get; set; } = new List<PlanAssuredItem>();
    }
}