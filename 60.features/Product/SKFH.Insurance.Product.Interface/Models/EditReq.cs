﻿namespace InsureBrick.Modules.Product.Interface.Models
{
    /// <summary>
    /// 商品管理設定-複製/編輯頁-查詢資料 API Req
    /// </summary>
    public class EditReq
    {

        /// <summary>
        /// 公司代碼
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 商品代碼
        /// </summary>
        public string PlanCode { get; set; }

        /// <summary>
        /// 商品版本
        /// </summary>
        public short PlanVer { get; set; }

        /// <summary>
        /// 繳費年期
        /// </summary>
        public int PaymentTerm { get; set; }

        /// <summary>
        /// 商品險別 - 代碼： V 汽車險 M 機車險
        /// </summary>
        public string PlanType { get; set; }

        /// <summary>
        /// 保障年期
        /// </summary>
        public int BenefitTerm { get; set; }

    }
}