﻿using System.Collections.Generic;

namespace InsureBrick.Modules.Product.Interface.Models
{
    /// <summary>
    /// 商品管理設定-複製/編輯頁-查詢資料 API Resp
    /// </summary>
    public class EditResp
    {

        /// <summary>
        /// 保險公司代碼
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 商品代碼
        /// </summary>
        public string PlanCode { get; set; }

        /// <summary>
        /// 商品版本
        /// </summary>
        public short PlanVer { get; set; }

        /// <summary>
        /// 商品全名
        /// </summary>
        public string PlanFullName { get; set; }

        /// <summary>
        /// 商品簡稱
        /// </summary>
        public string PlanShortName { get; set; }

        /// <summary>
        /// 商品英文名稱
        /// </summary>
        public string PlanEngName { get; set; }

        /// <summary>
        /// 商品險別 - 代碼：V 汽車險 M 機車險
        /// </summary>
        public string PlanType { get; set; }

        /// <summary>
        /// 商品銷售-起日
        /// </summary>
        public string PlanStartOn { get; set; }

        /// <summary>
        /// 商品銷售-迄日
        /// </summary>
        public string PlanEndOn { get; set; }

        /// <summary>
        /// 商品類型 - 代碼：CALI 強制險 PDI 車體損失保險 TPLI 第三人責任保險 BI 竊盜險
        /// </summary>
        public string PlanCategory { get; set; }

        /// <summary>
        /// 主附約別 - 代碼：0 主約 1 附約 2 附加條款
        /// </summary>
        public string PrimaryRiderInd { get; set; }

        /// <summary>
        /// 是否被非主約商品依附
        /// 商品非主約一律為 false
        /// </summary>
        public bool IsDependedOn { get; set; }

        /// <summary>
        /// 幣別 - 代碼：NTD 新台幣 USD 美元 JPD 日圓 HKD 港幣 MYR 馬來幣
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// 繳費年期 - 代碼：Term 定期 YearExp 年滿期 AgeExp 歲滿期 LifeTime 終身
        /// </summary>
        public string PaymentTermType { get; set; }

        /// <summary>
        /// 繳費年期
        /// </summary>
        public int PaymentTerm { get; set; }

        /// <summary>
        /// 保障年期 - 代碼：Term 定期 YearExp 年滿期 AgeExp 歲滿期 LifeTime 終身
        /// </summary>
        public string BenefitTermType { get; set; }

        /// <summary>
        /// 保障年期
        /// </summary>
        public int BenefitTerm { get; set; }

        /// <summary>
        /// 繳別 - 代碼：A 年繳 S 半年繳 Q 季繳 M 月繳 D 躉繳/一次繳清 F 彈性繳 複選以'|' 串接 A|M|D
        /// </summary>
        public string PremiumMethod { get; set; }

        /// <summary>
        /// 投保年齡(含)-起
        /// </summary>
        public int? InsuredAgeS { get; set; }

        /// <summary>
        /// 投保年齡(含)-迄
        /// </summary>
        public int? InsuredAgeE { get; set; }

        /// <summary>
        /// 投保來源
        /// </summary>
        public List<string> CaseSource { get; set; } = new List<string>();

        /// <summary>
        /// 銷售期間-起日 是否可編輯
        /// </summary>
        public bool IsPlanStartOnEditable { get; set; }

        /// <summary>
        /// 銷售狀態(畫面不需要,備用)
        /// </summary>
        public string SaleStateStr { get; set; }

        /// <summary>
        /// 依附主約
        /// </summary>
        public List<EditPrimaryProduct> PrimaryProducts { get; set; } = new List<EditPrimaryProduct>();

        /// <summary>
        /// 保障項目
        /// </summary>
        public string Abbreviation { get; set; }

        /// <summary>
        /// 商品保額項目
        /// </summary>
        public List<PlanAssuredItem> PlanAssuredItems { get; set; } = new List<PlanAssuredItem>();
    }

    /// <summary>
    /// 依附主約
    /// </summary>
    public class EditPrimaryProduct
    {
        /// <summary>
        /// 保險公司代號
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 商品代碼
        /// </summary>
        public string PlanCode { get; set; }

        /// <summary>
        /// 商品版本
        /// </summary>
        public short PlanVer { get; set; }

        /// <summary>
        /// 商品類型
        /// </summary>
        public string PlanType { get; set; }

        /// <summary>
        /// 商品主檔簡稱
        /// </summary>
        public string PlanShortName { get; set; }

        /// <summary>
        /// 繳費年期
        /// </summary>
        public int PaymentTerm { get; set; }

        /// <summary>
        /// 保障年期
        /// </summary>
        public int BenefitTerm { get; set; }
    }
}