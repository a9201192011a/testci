﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TPI.NetCore.WebAPI.Attributes;

namespace InsureBrick.Modules.Product.Interface.Models
{
    /// <summary>
    /// 商品保額項目
    /// </summary>
    public class PlanAssuredItem
    {
        /// <summary>
        /// 項目代碼
        /// </summary>
        public string ItemCode { get; set; }

        /// <summary>
        /// 項目簡稱
        /// </summary>
        public string ItemAbbreviation { get; set; }

        /// <summary>
        /// 保額
        /// </summary>
        public string Assured { get; set; }

        /// <summary>
        /// 參考保額
        /// </summary>
        public decimal? ReferAssured { get; set; }

        /// <summary>
        /// 倍數
        /// </summary>
        public decimal? Multiples { get; set; }
    }
}
