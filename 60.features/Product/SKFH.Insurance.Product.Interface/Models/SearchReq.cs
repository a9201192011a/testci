﻿using TPI.NetCore.WebAPI.Models;

namespace InsureBrick.Modules.Product.Interface.Models
{
    /// <summary>
    /// 商品管理設定-查詢頁-查詢資料 API Req
    /// </summary>
    public class SearchReq : DataTableQueryReq
    {
        /// <summary>
        /// 保險公司代碼
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 商品險別 (V 汽車險,M 機車險)
        /// </summary>
        public string PlanType { get; set; }

        /// <summary>
        /// 銷售狀態
        /// </summary>
        public string SaleState { get; set; }

        /// <summary>
        /// 商品代碼
        /// </summary>
        public string PlanCode { get; set; }

        /// <summary>
        /// 商品名稱(商品簡稱)
        /// </summary>
        public string PlanShortName { get; set; }
    }
}