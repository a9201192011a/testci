﻿using System.Collections.Generic;

namespace InsureBrick.Modules.Product.Interface.Models
{
    /// <summary>
    /// 商品管理設定-明細頁-查詢明細 API Resp
    /// </summary>
    public class ProductDetailResp
    {
        /// <summary>
        /// 保險公司
        /// </summary>
        public string CompanyStr { get; set; }

        /// <summary>
        /// 銷售期間
        /// </summary>
        public string PlanSale { get; set; }

        /// <summary>
        /// 商品代碼
        /// </summary>
        public string PlanCode { get; set; }
        /// <summary>
        /// 商品版本
        /// </summary>
        public short PlanVer { get; set; }
        /// <summary>
        /// 商品簡稱
        /// </summary>
        public string PlanShortName { get; set; }
        /// <summary>
        /// 商品全名
        /// </summary>
        public string PlanFullName { get; set; }
        /// <summary>
        /// 商品險別(SysParam)：V 汽車險 M 機車險
        /// </summary>
        public string PlanTypeStr { get; set; }
        /// <summary>
        /// 商品類型(SysParam)：CALI 強制險 PDI 車體損失保險 TPLI 第三人責任保險 BI 竊盜險
        /// </summary>
        public string PlanCategoryStr { get; set; }
        /// <summary>
        /// 主附約別(SysParam)：0 主約 1 附約 2 附加條款
        /// </summary>
        public string PrimaryRiderIndStr { get; set; }
        /// <summary>
        /// 幣別(SysParam)：NTD 新台幣 USD 美元 JPD 日圓 HKD 港幣 MYR 馬來幣
        /// </summary>
        public string CurrencyStr { get; set; }
        /// <summary>
        /// 保障年期
        /// </summary>
        public string BenefitTermStr { get; set; }
        /// <summary>
        /// 繳費年期
        /// </summary>
        public string PaymentTermStr { get; set; }
        /// <summary>
        /// 繳別(SysParam)：A 年繳 S 半年繳 Q 季繳 M 月繳 D 日繳/一次繳清 F 彈性繳 說明：若有一個以上的繳別用|分隔
        /// </summary>
        public string PremiumMethod { get; set; }

        /// <summary>
        /// 投保年齡(含)
        /// </summary>
        public string InsuredAge { get; set; }

        /// <summary>
        /// 案件來源
        /// </summary>
        public List<string> CaseSource { get; set; } = new List<string>(); 

        /// <summary>
        /// 依附主約
        /// </summary>
        public List<string> PrimaryProductsStr { get; set; } = new List<string>();

        /// <summary>
        /// 保障項目
        /// </summary>
        public string Abbreviation { get; set; }

        /// <summary>
        /// 商品保額項目
        /// </summary>
        public List<PlanAssuredItem> PlanAssuredItems { get; set; } = new List<PlanAssuredItem>();
    }
}
