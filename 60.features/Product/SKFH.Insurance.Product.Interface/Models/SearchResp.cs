namespace InsureBrick.Modules.Product.Interface.Models
{
    /// <summary>
    /// 商品管理設定-查詢頁-查詢資料 API Resp
    /// </summary>
    public class SearchResp
    {
        /// <summary>
        /// 保險公司
        /// </summary>
        public string CompanyStr { get; set; }

        /// <summary>
        /// 商品代碼
        /// </summary>
        public string PlanCode { get; set; }

        /// <summary>
        /// 商品版本(畫面Table 欄位隱藏,帶入修改/刪除/明細)
        /// </summary>
        public short PlanVer { get; set; }

        /// <summary>
        /// 商品名稱
        /// </summary>
        public string PlanShortName { get; set; }

        /// <summary>
        /// 繳費年期
        /// </summary>
        public int? PaymentTerm { get; set; }

        /// <summary>
        /// 保障年期
        /// </summary>
        public int? BenefitTerm { get; set; }

        /// <summary>
        /// 銷售狀態
        /// </summary>
        public string SaleStateStr { get; set; }

        /// <summary>
        /// 銷售起日(yyyy/mm/dd)
        /// </summary>
        public string PlanStartOn { get; set; }

        /// <summary>
        /// 銷售迄日(yyyy/mm/dd)
        /// </summary>
        public string PlanEndOn { get; set; }

        /// <summary>
        /// 保險公司代碼
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 商品險別 - 代碼： V 汽車險 M 機車險
        /// </summary>
        public string PlanType { get; set; }

        /// <summary>
        /// 商品險別 (V:汽車險、 M:機車險)
        /// </summary>
        public string PlanTypeStr { get; set; }

    }
}