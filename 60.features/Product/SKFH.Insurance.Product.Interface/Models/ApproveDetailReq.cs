using System;

namespace InsureBrick.Modules.Product.Interface.Models
{
    /// <summary>
    /// 商品管理設定-明細頁-覆核查詢明細API
    /// </summary>
    public class ApproveDetailReq
    {
        /// <summary>
        /// 公司代碼
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 覆核記錄檔 Gid
        /// </summary>
        public Guid ApproveRecordGid { get; set; }
    }
}