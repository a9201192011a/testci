﻿using InsureBrick.Modules.Product.Interface.Models;
using SKFH.Insurance.Product.Entity.Models;
using System;
using System.Threading.Tasks;
using TPI.NetCore.WebAPI.Models;


namespace InsureBrick.Modules.Product.Interface
{
    /// <summary>
    /// ProductService interface
    /// </summary>
    public interface IProductService
    {
        /// <summary>
        /// 商品管理設定-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">商品管理設定-查詢頁-查詢資料 API Req</param>
        /// <returns>商品管理設定-查詢頁-查詢資料 API Resp</returns>
        DataTableQueryResp<SearchResp> Search(SearchReq req);

        /// <summary>
        /// 商品管理設定-新增頁-新增 API
        /// </summary>
        /// <param name="req">商品管理設定 Req</param>
        /// <returns>True</returns>
        Task<bool> Insert(ProductReq req);

        /// <summary>
        /// 商品管理設定-查詢頁-編輯前檢驗 API
        /// </summary>
        /// <param name="req">商品管理設定-複製/編輯頁-查詢資料 API</param>
        /// <returns>True</returns>
        bool EditCheck(EditReq req);

        /// <summary>
        /// 商品管理設定-編輯頁-確認修改 API
        /// </summary>
        /// <param name="req">商品管理設定 Req</param>
        /// <returns>True</returns>
        Task<bool> ConfirmEdit(ProductReq req);

        /// <summary>
        /// 商品管理設定-查詢頁-刪除 API
        /// </summary>
        /// <param name="req">商品管理設定-複製/編輯頁-查詢資料 API</param>
        /// <returns>True</returns>
        Task<bool> Delete(EditReq req);

        /// <summary>
        /// 商品管理設定-複製/編輯頁-查詢資料 API
        /// </summary>
        /// <param name="req">商品管理設定-複製/編輯頁-查詢資料 API</param>
        /// <returns>商品管理設定-複製/編輯頁-查詢資料 API</returns>
        EditResp Edit(EditReq req);

        /// <summary>
        /// 商品管理設定-明細頁-查詢明細 API
        /// </summary>
        /// <param name="req">商品管理設定-複製/編輯頁-查詢資料 API</param>
        /// <returns>商品管理設定-明細頁-查詢明細 API</returns>
        ProductDetailResp Detail(EditReq req);

        /// <summary>
        /// 商品管理設定-明細頁-覆核查詢明細API
        /// </summary>
        /// <param name="req">商品管理設定-明細頁-覆核查詢明細API</param>
        /// <returns>商品管理設定-明細頁-查詢明細 API</returns>
        ProductDetailResp ApproveDetail(ApproveDetailReq req);

        /// <summary>
        /// 商品管理設定-查詢頁-刪除前檢核
        /// </summary>
        /// <param name="req">商品管理設定-複製/編輯頁-查詢資料 API</param>
        /// <returns>商品主檔</returns>
        PlanMaster CheckValidateForDelete(EditReq req);
    }
}