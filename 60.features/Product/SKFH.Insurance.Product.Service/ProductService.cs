﻿using AutoMapper;
using InsureBrick.Modules.Product.Interface;
using InsureBrick.Modules.Product.Interface.Models;
using TPI.NetCore.Extensions;
using TPI.NetCore.WebAPI.Models;
using InsureBrick.Modules.Common.Interface;
using TPI.NetCore;
using TPI.NetCore.Models;
using TPI.NetCore.WebContext;
using SKFH.Insurance.Log.Entity.Repository;
using SKFH.Insurance.Common.Entity.Repository;
using SKFH.Insurance.Company.Entity.Repository;
using SKFH.Insurance.Log.Entity.Models;
using SKFH.Insurance.Product.Entity.Models;
using coreLibs.DateAccess;
using Autofac;
using SKFH.Insurance.Product.Entity.Repository;

namespace InsureBrick.Modules.Product.Service
{
    /// <summary>
    /// ProductService
    /// </summary>
    public class ProductService : IProductService
    {
        #region Initialization

        private readonly IMapper mapper;
        private readonly IUnitOfWork productUnitOfWork; 
        private readonly IUnitOfWork logUnitOfWork;
        private readonly ICommonService commonService;
        //private readonly IValidationService validationService;
        private readonly IPlanMasterRepository planMasterRepo;
        private readonly IPlanCaseSourceRepository planCaseSourceRepo;
        private readonly ICompanyProfileRepository companyProfileRepo;
        private readonly ICampaignMasterRepository campaignMasterRepo;
        private readonly ICampaignPlanMappingRepository campaignPlanMappingRepo;
        //private readonly ICommissionSettingRepository commissionSettingRepo;
        //private readonly IOutBoundCompanySettingRepository outBoundCompanySettingRepo;
        private readonly IApproveRecordRepository approveRecordRepo;
        private readonly IApprovePlanCaseSourceRepository approvePlanCaseSourceRepo;
        private readonly IApproveCampaignPlanMappingRepository approveCampaignPlanMappingRepo;
        private readonly IApprovePlanMasterRepository approvePlanMasterRepo;
        private readonly ISysParamRepository sysParamRepo;
        private readonly IProductMasterRepository productMaster;
        private readonly IApproveProductMasterRepository approveProductMasterRepo;
        private readonly IInsuredMappingRepository insuredMappingRepo;
        private readonly IApproveInsuredMappingRepository approveInsuredMappingRepo;
        private readonly IPlanAssuredRepository planAssuredRepo;
        private readonly IApprovePlanAssuredRepository approvePlanAssuredRepo;



        public ProductService(
            IMapper mapper, IComponentContext componentContext,
            ICommonService commonService,
            //IValidationService validationService,
            IPlanMasterRepository planMasterRepo,
            IPlanCaseSourceRepository planCaseSourceRepo,
            ICompanyProfileRepository companyProfileRepo,
            ICampaignMasterRepository campaignMasterRepo,
            ICampaignPlanMappingRepository campaignPlanMappingRepo,
            //ICommissionSettingRepository commissionSettingRepo,
            //IOutBoundCompanySettingRepository outBoundCompanySettingRepo,
            IApproveRecordRepository approveRecordRepo,
            IApprovePlanCaseSourceRepository approvePlanCaseSourceRepo,
            IApproveCampaignPlanMappingRepository approveCampaignPlanMappingRepo,
            IApprovePlanMasterRepository approvePlanMasterRepo,
            ISysParamRepository sysParamRepo,
            IProductMasterRepository productMaster,
            IApproveProductMasterRepository approveProductMasterRepo,
            IInsuredMappingRepository insuredMappingRepo,
            IApproveInsuredMappingRepository approveInsuredMappingRepo,
            IPlanAssuredRepository planAssuredRepo,
            IApprovePlanAssuredRepository approvePlanAssuredRepo
            )
        {
            this.mapper = mapper;
            this.productUnitOfWork = componentContext.ResolveNamed<IUnitOfWork>("Product");
            this.logUnitOfWork = componentContext.ResolveNamed<IUnitOfWork>("Log");
            this.commonService = commonService;
            //this.validationService = validationService;
            this.planMasterRepo = planMasterRepo;
            this.planCaseSourceRepo = planCaseSourceRepo;
            this.companyProfileRepo = companyProfileRepo;
            this.campaignMasterRepo = campaignMasterRepo;
            this.campaignPlanMappingRepo = campaignPlanMappingRepo;
            //this.commissionSettingRepo = commissionSettingRepo;
            //this.outBoundCompanySettingRepo = outBoundCompanySettingRepo;
            this.approveRecordRepo = approveRecordRepo;
            this.approvePlanCaseSourceRepo = approvePlanCaseSourceRepo;
            this.approveCampaignPlanMappingRepo = approveCampaignPlanMappingRepo;
            this.approvePlanMasterRepo = approvePlanMasterRepo;
            this.sysParamRepo = sysParamRepo;
            this.productMaster = productMaster;
            this.approveProductMasterRepo = approveProductMasterRepo;
            this.insuredMappingRepo = insuredMappingRepo;
            this.approveInsuredMappingRepo = approveInsuredMappingRepo;
            this.planAssuredRepo = planAssuredRepo;
            this.approvePlanAssuredRepo = approvePlanAssuredRepo;
        }

        #endregion Initialization

        /// <summary>
        /// 商品管理設定-查詢頁-查詢資料 API
        /// </summary>
        /// <param name="req">商品管理設定-查詢頁-查詢資料 API Req</param>
        /// <returns>商品管理設定-查詢頁-查詢資料 API Resp</returns>
        public DataTableQueryResp<SearchResp> Search(SearchReq req)
        {
            try
            {
                // 依保險公司代碼, 查詢 商品主檔資料
                var planList = this.planMasterRepo.Queryable()
                    .QueryItem(o => o.CompanyCode == req.CompanyCode, req.CompanyCode)
                    .QueryItem(o => o.PlanType == req.PlanType, req.PlanType)
                    .QueryItem(o => o.PlanCode.Contains(req.PlanCode), req.PlanCode)
                    .QueryItem(o => o.PlanShortName.Contains(req.PlanShortName), req.PlanShortName);

                // 商品主檔 ,  保險公司主檔
                var firstJoinTable = from pl in planList
                                     join cp in this.companyProfileRepo.Queryable() on pl.CompanyCode equals cp.CompanyCode into cpl
                                     from cp in cpl.DefaultIfEmpty()
                                     select new
                                     {
                                         CompanyStr = pl.CompanyCode + " " + (cp != null ? cp.CompanyShortName : ""),
                                         PlanCode = pl.PlanCode,
                                         PlanVer = pl.PlanVer,
                                         PlanShortName = pl.PlanShortName,
                                         PaymentTerm = pl.PaymentTerm,
                                         PlanType = pl.PlanType,
                                         BenefitTerm = pl.BenefitTerm,
                                         PlanStartOn = pl.PlanStartOn,
                                         PlanEndOn = pl.PlanEndOn,
                                         CompanyCode = pl.CompanyCode,
                                         SaleState = DateTime.Today < pl.PlanStartOn ? SaleStateType.PrepareSale : //若系統日<商品銷售-起日，顯示：未上架。 0 
                                                 DateTime.Today >= pl.PlanStartOn && DateTime.Today <= pl.PlanEndOn.AddDays(1).AddSeconds(-1) ? SaleStateType.Sailing : //系統日在商品銷售-起日、商品銷售-迄日內
                                                 DateTime.Today > pl.PlanEndOn.AddDays(1).AddSeconds(-1) ? SaleStateType.StopSale : //系統日 > 商品銷售 - 迄日
                                                 string.Empty
                                     };

                // 系統參數檔 銷售狀態 ,商品險別
                var joinTable = from jt in firstJoinTable
                                join sp in this.sysParamRepo.Queryable().Where(o => o.GroupId == GroupIdType.SaleState) on jt.SaleState equals sp.ItemId into spl
                                from sp in spl.DefaultIfEmpty()
                                join pt in this.sysParamRepo.Queryable().Where(o => o.GroupId == GroupIdType.PlanType) on jt.PlanType equals pt.ItemId into ptl
                                from pt in ptl.DefaultIfEmpty()
                                select new
                                {
                                    jt,
                                    SaleStateStr = sp.ItemValue,
                                    PlanTypeStr = pt.ItemValue
                                };

                //  銷售狀態篩選
                if (!string.IsNullOrEmpty(req.SaleState))
                {
                    joinTable = joinTable.Where(o => o.jt.SaleState == req.SaleState);
                }

                // 資料庫總筆數
                var totalCount = joinTable.Count();

                // 組合資料
                var productSearchRes = joinTable
                                       .Select(o =>
                                        new
                                        {
                                            CompanyStr = o.jt.CompanyStr,
                                            PlanCode = o.jt.PlanCode,
                                            PlanVer = o.jt.PlanVer,
                                            PlanShortName = o.jt.PlanShortName,
                                            PaymentTerm = o.jt.PaymentTerm,
                                            BenefitTerm = o.jt.BenefitTerm,
                                            SaleStateStr = o.SaleStateStr,
                                            PlanStartOn = o.jt.PlanStartOn,
                                            PlanEndOn = o.jt.PlanEndOn,
                                            CompanyCode = o.jt.CompanyCode,
                                            PlanType = o.jt.PlanType,
                                            PlanTypeStr = o.PlanTypeStr
                                        })
                                        .OrderBySortInfo(req.order, req.columns)
                                        .Pagination(req.length, req.start)
                                        .Select(o =>
                                        new SearchResp
                                        {
                                            CompanyStr = o.CompanyStr,
                                            PlanCode = o.PlanCode,
                                            PlanVer = o.PlanVer,
                                            PlanShortName = o.PlanShortName,
                                            PaymentTerm = o.PaymentTerm,
                                            BenefitTerm = o.BenefitTerm,
                                            SaleStateStr = o.SaleStateStr,
                                            PlanStartOn = o.PlanStartOn.ToString("yyyy/MM/dd"),
                                            PlanEndOn = o.PlanEndOn.ToString("yyyy/MM/dd"),
                                            CompanyCode = o.CompanyCode,
                                            PlanType = o.PlanType,
                                            PlanTypeStr = o.PlanTypeStr
                                        });

                return new DataTableQueryResp<SearchResp>(productSearchRes, totalCount);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 商品管理設定-新增頁-新增 API
        /// </summary>
        /// <param name="req">商品管理設定 Req</param>
        /// <returns>True</returns>
        public async Task<bool> Insert(ProductReq req)
        {
            // 商品主檔資料
            var planMasterList = this.planMasterRepo.Queryable()
                .Where(o =>
                o.CompanyCode == req.CompanyCode &&
                o.PlanCode == req.PlanCode &&
                o.PaymentTerm == req.PaymentTerm &&
                o.PlanType == req.PlanType &&
                o.BenefitTerm == req.BenefitTerm
                );

            //檢核是否有前版本
            var ver = planMasterList
                .OrderByDescending(o => o.PlanVer)
                .FirstOrDefault();

            //若有前版本，則需再檢核
            if (ver != null)
            {
                // 檢查1-起日要大於 上一個版本的迄日
                if (req.PlanStartOn <= ver.PlanEndOn) throw new BusinessException($"銷售起日要大於上一個版本的迄日 {ver.PlanEndOn.ToString("yyyy/MM/dd")}。");
                // 檢查2-新增的版本號要檢查必須要大於現行版本號
                if (req.PlanVer <= ver.PlanVer) throw new BusinessException($"商品版本號重複，無法新增。");
            }

            //檢查3-待覆核清單有此商品的資料(ex.重複新增送申請=>不可新增)
            //條件『待覆核主檔』：保險公司代碼 + 商品代碼 + 繳費年期 + 商品險別 + 尚未覆核 =>不看版本，與異動類型
            if (this.IsApproveRecordExist(req))
            {
                //保險公司中文
                var companyName = this.companyProfileRepo.Queryable()
                .Where(o => o.CompanyCode == req.CompanyCode).FirstOrDefault()?.CompanyShortName;

                throw new BusinessException($"商品「{req.CompanyCode} {companyName} {req.PlanCode} {req.PlanShortName}(繳費年期{req.PaymentTerm}年、保障年期{req.BenefitTerm})」已被異動等待覆核中，無法新增。");
            }

            var primaryProducts = this.planMasterRepo.Queryable()
                .Where(o => o.PrimaryRiderInd == "0"
                && o.CompanyCode == req.CompanyCode
                && o.PlanType == req.PlanType)
                .Select(o => new PrimaryProduct()
                {
                    CompanyCode = o.CompanyCode,
                    PlanCode = o.PlanCode,
                    PlanVer = o.PlanVer,
                    PlanType = o.PlanType,
                    PaymentTerm = o.PaymentTerm,
                    BenefitTerm = o.BenefitTerm
                });
            if (req.PrimaryProducts.ExceptList(primaryProducts).Any())
            {
                throw new BusinessException($"所選擇的主約不在商品內。");
            }

            //檢核所有依附的主約都不可以在覆核中
            foreach (var priamryProduct in req.PrimaryProducts)
            {
                ProductReq primary = new ProductReq()
                {
                    CompanyCode = priamryProduct.CompanyCode,
                    PlanCode = priamryProduct.PlanCode,
                    PlanVer = priamryProduct.PlanVer,
                    PlanType = priamryProduct.PlanType,
                    PaymentTerm = priamryProduct.PaymentTerm,
                    BenefitTerm = priamryProduct.BenefitTerm,
                };
                if (this.IsApproveRecordExist(primary))
                {
                    throw new BusinessException($"所選的主約正在覆核中");
                }
            }

            if (req.PlanAssuredItems.Count != req.PlanAssuredItems.Select(o => string.IsNullOrEmpty(o.ItemCode) ? string.Empty : o.ItemCode).Distinct().Count())
            {
                throw new BusinessException("商品保額項目不可重複");
            }

            // 新增資料進 db
            await this.InsertApproveDataToDb(req, ModifyType.Insert);
            return true;
        }

        /// <summary>
        /// 新增資料進 db
        /// </summary>
        /// <returns></returns>
        private async Task<bool> InsertApproveDataToDb(ProductReq req, string modifyType)
        {
            this.commonService
                .DateCheck(req.PlanStartOn.ToString(), req.PlanEndOn.ToString(), "銷售期間");

            if (req.BenefitTerm < 1)
            {
                throw new BusinessException("保障年期值需大於1。");
            }
            if (req.PaymentTerm < 1)
            {
                throw new BusinessException("繳費年期值需大於1。");
            }

            if (req.InsuredAgeS.HasValue && req.InsuredAgeE.HasValue &&
                req.InsuredAgeS.Value > req.InsuredAgeE.Value)
            {
                throw new BusinessException("投保年齡起值不得大於投保年齡迄值。");
            }

            //檢查 保險公司 是否存在
            var company = this.companyProfileRepo.Queryable()
                .Where(o => o.CompanyCode == req.CompanyCode).FirstOrDefault();

            if (company == null) throw new BusinessException("保險公司主檔無資料");

            var guid = Guid.NewGuid();

            //覆核記錄檔
            var record = new ApproveRecord()
            {
                ApproveRecordGid = guid,
                FunctionCode = FunctionCodeType.F1401,
                //公司代碼 +空格+ 公司名稱/商品代碼  +空格+ 商品名稱
                ApproveItem = $"{req.CompanyCode} {company.CompanyShortName}/{req.PlanCode} {req.PlanShortName}",
                ApproveKey = this.GetProductApproveKey(req), //存覆核項目的key = 公司代碼_商品代碼_商品版本_繳費年期_商品險別_保障年期
                ModifyType = modifyType,
                ModifyBy = WebContext.Account,
                ModifyOn = DateTime.Now,
                ApproveStatus = ApproveStatusType.WaitApprove, //尚未覆核
            };

            //商品覆核主檔
            var pm = this.mapper.Map<ApprovePlanMaster>(req);

            pm.ApproveRecordGid = guid;
            pm.CreateBy = WebContext.Account;
            pm.CreateOn = DateTime.Now;

            foreach (var caseSources in req.CaseSource)
            {
                //商品覆核主檔的銷售管道
                var pcs = new ApprovePlanCaseSource()
                {
                    ApproveRecordGid = guid,
                    CompanyCode = req.CompanyCode,
                    PlanCode = req.PlanCode,
                    PlanVer = req.PlanVer,
                    PaymentTerm = req.PaymentTerm,
                    BenefitTerm = req.BenefitTerm,
                    PlanType = req.PlanType,
                    CaseSource = caseSources,
                    CreateBy = WebContext.Account,
                    CreateOn = DateTime.Now
                };

                //新增進 商品覆核主檔的銷售管道
                this.approvePlanCaseSourceRepo.Insert(pcs);
            }

            foreach (var planAssuredItem in req.PlanAssuredItems)
            {
                //商品保額覆核檔
                var apa = new ApprovePlanAssured()
                {
                    ApproveRecordGid = guid,
                    CompanyCode = req.CompanyCode,
                    PlanCode = req.PlanCode,
                    BenefitTerm = req.BenefitTerm,
                    PaymentTerm = req.PaymentTerm,
                    PlanType = req.PlanType,
                    PlanVer = req.PlanVer,
                    ItemCode = planAssuredItem.ItemCode,
                    ItemAbbreviation = planAssuredItem.ItemAbbreviation,
                    Assured = planAssuredItem.Assured,
                    ReferAssured = planAssuredItem.ReferAssured,
                    Multiples = planAssuredItem.Multiples,
                    CreateBy = WebContext.Account,
                    CreateOn = DateTime.Now,
                };
                this.approvePlanAssuredRepo.Insert(apa);
            }

            if (req.PrimaryRiderInd != "0")
            {
                foreach (var primaryProduct in req.PrimaryProducts)
                {
                    //商品覆核主檔的依附主約
                    var apim = new ApproveInsuredMapping()
                    {
                        ApproveRecordGid = guid,
                        CompanyCode = primaryProduct.CompanyCode,
                        PlanType = primaryProduct.PlanType,
                        RidePlanCode = req.PlanCode,
                        RidePlanVer = req.PlanVer,
                        RidePaymentTerm = req.PaymentTerm,
                        RideBenefitTerm = req.BenefitTerm,
                        PrimaryPlanCode = primaryProduct.PlanCode,
                        PrimaryPlanVer = primaryProduct.PlanVer,
                        PrimaryPaymentTerm = primaryProduct.PaymentTerm,
                        PrimaryBenefitTerm = primaryProduct.BenefitTerm,
                        CreateBy = WebContext.Account,
                        CreateOn = DateTime.Now,
                    };
                    //新增進 商品覆核主檔的依附主約
                    this.approveInsuredMappingRepo.Insert(apim);
                }
            }

            //新增進 覆核主檔
            this.approveRecordRepo.Insert(record);
            await this.logUnitOfWork.SaveChangesAsync();
            //新增進 商品覆核主檔
            this.approvePlanMasterRepo.Insert(pm);
            await this.productUnitOfWork.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// 商品管理設定-查詢頁-編輯前檢驗 API
        /// </summary>
        /// <param name="req">商品管理設定-複製/編輯頁-查詢資料 API</param>
        /// <returns>True</returns>
        public bool EditCheck(EditReq req)
        {
            var productReq = this.mapper.Map<ProductReq>(req);
            // 查詢 商品主檔 ,key CompanyCode,PlanCode,PlanVer,PaymentTerm,PlanType,BenefitTerm
            var planMaster = this.GetPlanMaster(productReq);

            if (planMaster == null) throw new BusinessException("商品主檔查無資料");

            //保險公司中文
            var companyName = this.companyProfileRepo.Queryable()
            .Where(o => o.CompanyCode == planMaster.CompanyCode).FirstOrDefault()?.CompanyShortName;

            // 商品資訊
            var productInfoStr = $"{planMaster.CompanyCode} {companyName} {planMaster.PlanCode} {planMaster.PlanShortName}(繳費年期{planMaster.PaymentTerm}年、保障年期{planMaster.BenefitTerm}年)";

            var saleState = this.commonService.GetSaleState(planMaster.PlanStartOn, planMaster.PlanEndOn);

            //如果商品已到期:
            if (saleState == SaleStateType.StopSale)
            {
                throw new BusinessException($"商品「{productInfoStr}」已停售，無法編輯。");
            }

            //如果商品銷售中:
            if (saleState == SaleStateType.Sailing)
            {
                throw new BusinessException($"商品「{productInfoStr}」銷售中，無法編輯。");
            }

            //如果商品被其他商品依附於待覆核清單時：
            if (this.IsApproveInsuredMappingExist(productReq))
            {
                throw new BusinessException($"商品「{productInfoStr}」已被其他附約、依附主約、附加條款選取並等待覆核中，無法編輯。");
            }

            //如果商品未上架:
            //檢查1-佣金有設定此商品 => 不可編輯
            //if (this.IsCommissionExist(productReq))
            //{
            //    throw new BusinessException($"商品「{productInfoStr}」已被佣金使用，無法編輯。");
            //}
            //檢查2-待覆核清單 => 有此佣金商品設定 => 不可編輯
            if (this.IsApproveCommissionExist(productReq))
            {
                throw new BusinessException($"商品「{productInfoStr}」已被佣金異動等待覆核中，無法編輯。");
            }

            //檢查3-方案有使用到此商品，不可編輯
            if (this.IsCampaignExist(productReq))
            {
                throw new BusinessException($"商品「{productInfoStr}」已被方案使用，無法編輯。");
            }
            //檢查4-待覆核清單 => 有此方案商品待覆核 => 不可編輯
            if (this.IsApproveCampaignExist(productReq))
            {
                throw new BusinessException($"商品「{productInfoStr}」已被方案異動等待覆核中，無法編輯。");
            }

            //檢查5-若待覆核清單有此商品的資料>不可編輯)
            //條件『待覆核主檔』：保險公司代碼+商品代碼+繳費年期+商品險別+尚未覆核=>不看版本，與異動類型
            if (this.IsApproveRecordExist(productReq))
            {
                throw new BusinessException($"商品「{productInfoStr}」已被商品異動等待覆核中，無法編輯。");
            }

            return true;
        }

        /// <summary>
        /// 商品管理設定-編輯頁-確認修改 API
        /// </summary>
        /// <param name="req">ProductReq</param>
        /// <returns>bool</returns>
        public async Task<bool> ConfirmEdit(ProductReq req)
        {
            // 編輯前檢查
            var editReq = this.mapper.Map<EditReq>(req);
            this.EditCheck(editReq);

            // 所有商品
            var planMasterAllVerList = this.planMasterRepo.Queryable()
                .Where(o =>
                o.CompanyCode == req.CompanyCode &&
                o.PlanCode == req.PlanCode &&
                o.PaymentTerm == req.PaymentTerm &&
                o.PlanType == req.PlanType &&
                o.BenefitTerm == req.BenefitTerm
                );

            //檢查1-下一個版本
            var nextVerPlan = planMasterAllVerList
                .Where(o => o.PlanVer > req.PlanVer)
                .OrderBy(o => o.PlanVer) //升冪排序 ,挑最小版本號
                .FirstOrDefault();

            //編輯的迄日如果大於等於 下一個版本的起日            
            if (nextVerPlan != null)
            {
                if (req.PlanEndOn >= nextVerPlan.PlanStartOn)
                {
                    throw new BusinessException($"迄日不可與版本{nextVerPlan.PlanVer}(下一個版本)的銷售期間重疊");
                }
            }

            //檢查2-上一個版本
            var previousVerPlan = planMasterAllVerList
                .Where(o => o.PlanVer < req.PlanVer)
                .OrderByDescending(o => o.PlanVer) //降冪排序,挑最大版本號
                .FirstOrDefault();

            //編輯的起日如果小於等於 前一個版本的迄日
            if (previousVerPlan != null)
            {
                if (req.PlanStartOn <= previousVerPlan.PlanEndOn)
                {
                    throw new BusinessException($"起日不可與版本{previousVerPlan.PlanVer}(上一個版本)的銷售期間重疊");
                }
            }

            var primaryProducts = this.planMasterRepo.Queryable().Where(o => o.PrimaryRiderInd == "0").Select(o => new PrimaryProduct()
            {
                CompanyCode = o.CompanyCode,
                PlanCode = o.PlanCode,
                PlanVer = o.PlanVer,
                PlanType = o.PlanType,
                PaymentTerm = o.PaymentTerm,
                BenefitTerm = o.BenefitTerm
            });
            if (req.PrimaryProducts.ExceptList(primaryProducts).Any())
            {
                throw new BusinessException($"所選擇的主約不在商品內。");
            }

            //檢核所有依附的主約都不可以在覆核中
            foreach (var priamryProduct in req.PrimaryProducts)
            {
                ProductReq primary = new ProductReq()
                {
                    CompanyCode = priamryProduct.CompanyCode,
                    PlanCode = priamryProduct.PlanCode,
                    PlanVer = priamryProduct.PlanVer,
                    PlanType = priamryProduct.PlanType,
                    PaymentTerm = priamryProduct.PaymentTerm,
                    BenefitTerm = priamryProduct.BenefitTerm,
                };
                if (this.IsApproveRecordExist(primary))
                {
                    throw new BusinessException($"所選的主約正在覆核中");
                }
            }

            var isDependedOn = IsInsuredMappingExist(req);

            if (req.PlanAssuredItems.Count != req.PlanAssuredItems.Select(o => string.IsNullOrEmpty(o.ItemCode) ? string.Empty : o.ItemCode).Distinct().Count())
            {
                throw new BusinessException("商品保額項目不可重複");
            }

            // 編輯商品
            var planMaster = planMasterAllVerList.Where(o => o.PlanVer == req.PlanVer).FirstOrDefault();

            // 將調整的項目更新
            var product = this.mapper.Map<ProductReq>(planMaster);

            // 銷售期間
            product.PlanStartOn = req.PlanStartOn;
            product.PlanEndOn = req.PlanEndOn;

            product.PlanShortName = req.PlanShortName;
            product.PlanFullName = req.PlanFullName;
            product.PlanCategory = req.PlanCategory;
            product.PrimaryRiderInd = isDependedOn ? product.PrimaryRiderInd : req.PrimaryRiderInd;
            product.Currency = req.Currency;
            product.InsuredAgeS = req.InsuredAgeS;
            product.InsuredAgeE = req.InsuredAgeE;
            product.CaseSource = req.CaseSource;
            product.CreateBy = WebContext.Account;
            product.CreateOn = DateTime.Now;
            product.UpdateBy = null;
            product.UpdateOn = null;
            product.PrimaryProducts = req.PrimaryProducts;
            product.Abbreviation = req.Abbreviation;
            product.PlanAssuredItems = req.PlanAssuredItems;

            //新增資料進 db
            await this.InsertApproveDataToDb(product, ModifyType.Modfiy);

            return true;
        }

        /// <summary>
        /// 商品管理設定-查詢頁-刪除 API
        /// </summary>
        /// <param name="req">EditReq</param>
        /// <returns>bool</returns>
        public async Task<bool> Delete(EditReq req)
        {
            #region 是否可刪除商品
            //刪除前檢核
            var planMaster = this.CheckValidateForDelete(req);
            #endregion

            #region 檢核是否有覆核紀錄
            //檢查8-待覆核清單有此商品的資料(ex.重複新增送申請=>不可刪除)
            //條件『待覆核主檔』：保險公司代碼 + 商品代碼 + 繳費年期 + 尚未覆核 =>不看版本，與異動類型
            var productReq = this.mapper.Map<ProductReq>(req);
            // 商品資訊
            var productInfoStr = $"{planMaster.CompanyCode} {req.CompanyCode} {planMaster.PlanCode} {planMaster.PlanShortName}(繳費年期{planMaster.PaymentTerm}年)";
            if (this.IsApproveRecordExist(productReq))
            {
                throw new BusinessException($"商品「{productInfoStr}」已被商品異動等待覆核中，無法刪除。");
            }
            #endregion

            // 商品主檔的銷售管道
            var planCaseSources = this.planCaseSourceRepo.Queryable()
                .Where(o => o.CompanyCode == req.CompanyCode
                && o.PlanCode == req.PlanCode
                && o.PlanVer == req.PlanVer
                && o.PaymentTerm == req.PaymentTerm
                && o.PlanType == req.PlanType
                && o.BenefitTerm == req.BenefitTerm
                ).ToList();

            var planAssureds = this.planAssuredRepo.Queryable()
                .Where(o => o.CompanyCode == req.CompanyCode
                && o.PlanCode == req.PlanCode
                && o.PlanVer == req.PlanVer
                && o.PaymentTerm == req.PaymentTerm 
                && o.PlanType == req.PlanType
                && o.BenefitTerm == req.BenefitTerm)
                .Select(o => new PlanAssuredItem()
                {
                    ItemCode = o.ItemCode,
                    ItemAbbreviation = o.ItemAbbreviation,
                    Assured = o.Assured,
                    ReferAssured = o.ReferAssured,
                    Multiples = o.Multiples
                }).ToList();

            var primaryProducts = this.insuredMappingRepo.Queryable()
                .Where(o => o.CompanyCode == req.CompanyCode && o.RidePlanCode == req.PlanCode && o.RidePlanVer == req.PlanVer && o.RidePaymentTerm == req.PaymentTerm && o.RideBenefitTerm == req.BenefitTerm)
                .Select(o => new PrimaryProduct()
                {
                    CompanyCode = o.CompanyCode,
                    PlanCode = o.PrimaryPlanCode,
                    PlanVer = o.PrimaryPlanVer,
                    PaymentTerm = o.PrimaryPaymentTerm,
                    BenefitTerm = o.PrimaryPaymentTerm,
                    PlanType = o.PlanType
                }).ToList();
            var product = this.mapper.Map<ProductReq>(planMaster);
            product.CaseSource = planCaseSources.Select(o => o.CaseSource).ToArray();
            product.CreateBy = WebContext.Account;
            product.CreateOn = DateTime.Now;
            product.UpdateBy = null;
            product.UpdateOn = null;
            product.PlanAssuredItems = planAssureds;
            product.PrimaryProducts = primaryProducts;

            //新增資料進 db
            await this.InsertApproveDataToDb(product, ModifyType.Delete);

            return true;
        }

        /// <summary>
        /// 商品管理設定-查詢頁-刪除前檢核
        /// </summary>
        /// <returns></returns>
        public PlanMaster CheckValidateForDelete(EditReq req)
        {
            var productReq = this.mapper.Map<ProductReq>(req);
            // 查詢 商品主檔 ,key CompanyCode,PlanCode,PlanVer,PaymentTerm,BenefitTerm
            var planMaster = this.GetPlanMaster(productReq);

            if (planMaster == null) throw new BusinessException("商品主檔查無資料");

            //保險公司中文
            var companyName = this.companyProfileRepo.Queryable()
            .Where(o => o.CompanyCode == planMaster.CompanyCode).FirstOrDefault()?.CompanyShortName;

            // 商品資訊
            var productInfoStr = $"{planMaster.CompanyCode} {companyName} {planMaster.PlanCode} {planMaster.PlanShortName}(繳費年期{planMaster.PaymentTerm}年、保障年期{planMaster.BenefitTerm}年)";
            //電訪商品資訊
            var outBoundProductInfoStr = $"{planMaster.CompanyCode} {companyName} {planMaster.PlanCode} {planMaster.PlanShortName}";

            //檢查1-電訪有設定此商品 => 不可刪除
            //if (this.IsOutBoundExist(productReq))
            //{
            //    throw new BusinessException($"商品「{outBoundProductInfoStr}」已被電訪參數設定使用，無法刪除。");
            //}

            //檢查2-若待覆核清單 => 有此電訪商品設定 => 不可刪除
            if (this.IsApproveOutBoundExist(productReq))
            {
                throw new BusinessException($"商品「{outBoundProductInfoStr}」已被電訪參數設定異動等待覆核中，無法刪除。");
            }

            //檢查3-佣金有設定此商品 => 不可刪除 
            //if (this.IsCommissionExist(productReq))
            //{
            //    throw new BusinessException($"商品「{productInfoStr}」已被佣金使用，無法刪除。");
            //}

            //檢查4-待覆核清單 => 有此佣金商品設定 => 不可刪除
            if (this.IsApproveCommissionExist(productReq))
            {
                throw new BusinessException($"商品「{productInfoStr}」已被佣金異動等待覆核中，無法刪除。");
            }

            //檢查5-方案有設定此商品 => 不可刪除
            if (this.IsCampaignExist(productReq))
            {
                throw new BusinessException($"商品「{productInfoStr}」已被方案使用，無法刪除。");
            }

            //檢查6-待覆核清單 => 有此方案商品待覆核 => 不可刪除 
            if (this.IsApproveCampaignExist(productReq))
            {
                throw new BusinessException($"商品「{productInfoStr}」已被方案異動等待覆核中，無法刪除。");
            }

            //檢查7-若商品的落在"銷售中"或"已停售"=>不可刪除
            var saleState = this.commonService.GetSaleState(planMaster.PlanStartOn, planMaster.PlanEndOn);
            if (saleState == SaleStateType.Sailing)
            {
                throw new BusinessException($"商品「{productInfoStr}」銷售中，無法刪除。");
            }

            if (saleState == SaleStateType.StopSale)
            {
                throw new BusinessException($"商品「{productInfoStr}」已停售，無法刪除。");
            }

            //檢查8-若商品已被其他非主約依附
            if (this.IsInsuredMappingExist(productReq))
            {
                throw new BusinessException($"商品「{productInfoStr}」已被其他附約、依附主約、附加條款選取無法刪除。");
            }

            //檢查9-主附約關係覆核檔是否有相關商品
            if (this.IsApproveInsuredMappingExist(productReq))
            {
                throw new BusinessException($"商品「{productInfoStr}」依附主約已被商品異動等待覆核中，無法刪除。");
            }

            return planMaster;
        }

        /// <summary>
        /// 商品管理設定-複製/編輯頁-查詢資料 API
        /// </summary>
        /// <param name="req">EditReq</param>
        /// <returns>EditResp</returns>
        public EditResp Edit(EditReq req)
        {
            var productReq = this.mapper.Map<ProductReq>(req);
            // 查詢 商品主檔 ,key CompanyCode,PlanCode,PlanVer,PaymentTerm,PlanType,BenefitTerm
            var p = this.GetPlanMaster(productReq);

            if (p == null) throw new BusinessException("商品主檔查無資料");

            // 依 PK 查詢 商品主檔的銷售管道
            var c = this.planCaseSourceRepo.Queryable().
            Where(o => o.CompanyCode == req.CompanyCode &&
            o.PlanCode == req.PlanCode &&
            o.PlanVer == req.PlanVer &&
            o.PaymentTerm == req.PaymentTerm &&
            o.PlanType == req.PlanType &&
            o.BenefitTerm == req.BenefitTerm)
            .ToList();

            // 銷售狀態對應值
            var saleState = this.commonService.GetSaleState(p.PlanStartOn, p.PlanEndOn);

            // 商品保額
            var planAssuredItems = this.planAssuredRepo.Queryable()
                .Where(o => o.CompanyCode == req.CompanyCode &&
                o.PlanCode == req.PlanCode &&
                o.PlanVer == req.PlanVer &&
                o.PaymentTerm == req.PaymentTerm &&
                o.PlanType == req.PlanType &&
                o.BenefitTerm == req.BenefitTerm).AsEnumerable();

            // 主附約關聯
            var primaryProducts = from im in this.insuredMappingRepo.Queryable()
                                  .Where(o => o.CompanyCode == req.CompanyCode &&
                                  o.RidePlanCode == req.PlanCode &&
                                  o.RidePlanVer == req.PlanVer &&
                                  o.RidePaymentTerm == req.PaymentTerm &&
                                  o.PlanType == req.PlanType &&
                                  o.RideBenefitTerm == req.BenefitTerm)
                                  join pm in this.planMasterRepo.Queryable()
                                  on new
                                  {
                                      CompanyCode = im.CompanyCode,
                                      PlanCode = im.PrimaryPlanCode,
                                      PlanVer = im.PrimaryPlanVer,
                                      PaymentTerm = im.PrimaryPaymentTerm,
                                      PlanType = im.PlanType,
                                      BenefitTerm = im.PrimaryBenefitTerm
                                  }
                                  equals new
                                  {
                                      CompanyCode = pm.CompanyCode,
                                      PlanCode = pm.PlanCode,
                                      PlanVer = pm.PlanVer,
                                      PaymentTerm = pm.PaymentTerm,
                                      PlanType = pm.PlanType,
                                      BenefitTerm = pm.BenefitTerm
                                  }
                                  select new EditPrimaryProduct()
                                  {
                                      CompanyCode = im.CompanyCode,
                                      PlanCode = im.PrimaryPlanCode,
                                      PlanShortName = $"{pm.PlanCode} {pm.PlanShortName}",
                                      PlanVer = im.PrimaryPlanVer,
                                      PaymentTerm = im.PrimaryPaymentTerm,
                                      PlanType = im.PlanType,
                                      BenefitTerm = im.PrimaryBenefitTerm,
                                  };

            var rideProducts = from im in this.insuredMappingRepo.Queryable()
                                  .Where(o => o.CompanyCode == req.CompanyCode &&
                                  o.PrimaryPlanCode == req.PlanCode &&
                                  o.PrimaryPlanVer == req.PlanVer &&
                                  o.PrimaryPaymentTerm == req.PaymentTerm &&
                                  o.PlanType == req.PlanType &&
                                  o.PrimaryBenefitTerm == req.BenefitTerm)
                               join pm in this.planMasterRepo.Queryable()
                               on new
                               {
                                   CompanyCode = im.CompanyCode,
                                   PlanCode = im.RidePlanCode,
                                   PlanVer = im.RidePlanVer,
                                   PaymentTerm = im.RidePaymentTerm,
                                   PlanType = im.PlanType,
                                   BenefitTerm = im.RideBenefitTerm
                               }
                               equals new
                               {
                                   CompanyCode = pm.CompanyCode,
                                   PlanCode = pm.PlanCode,
                                   PlanVer = pm.PlanVer,
                                   PaymentTerm = pm.PaymentTerm,
                                   PlanType = pm.PlanType,
                                   BenefitTerm = pm.BenefitTerm
                               }
                               select $"{pm.PlanCode} {pm.PlanShortName}";

            var result = new EditResp()
            {
                CompanyCode = p.CompanyCode,
                PlanCode = p.PlanCode,
                PlanVer = p.PlanVer,
                PlanFullName = p.PlanFullName,
                PlanShortName = p.PlanShortName,
                PlanEngName = p.PlanEngName,
                PlanType = p.PlanType,
                PlanStartOn = p.PlanStartOn.ToString("yyyy/MM/dd"),
                PlanEndOn = p.PlanEndOn.ToString("yyyy/MM/dd"),
                PlanCategory = p.PlanCategory,
                PrimaryRiderInd = p.PrimaryRiderInd,
                IsDependedOn = rideProducts.Any(),
                Currency = p.Currency,
                PaymentTermType = p.PaymentTermType,
                PaymentTerm = p.PaymentTerm,
                BenefitTermType = p.BenefitTermType,
                BenefitTerm = p.BenefitTerm,
                PremiumMethod = p.PremiumMethod,
                InsuredAgeS = p.InsuredAgeS,
                InsuredAgeE = p.InsuredAgeE,
                CaseSource = c.Select(o => o.CaseSource).ToList(),
                PlanAssuredItems = planAssuredItems.Select(o => new PlanAssuredItem()
                {
                    ItemCode = o.ItemCode,
                    ItemAbbreviation = o.ItemAbbreviation,
                    Assured = o.Assured,
                    ReferAssured = o.ReferAssured.HasValue ? Convert.ToDecimal(o.ReferAssured.Value.ToString("0.##")) : o.ReferAssured,
                    Multiples = o.Multiples.HasValue ? Convert.ToDecimal(o.Multiples.Value.ToString("0.##")) : o.Multiples,
                }).ToList(),
                Abbreviation = p.Abbreviation,
                PrimaryProducts = p.PrimaryRiderInd != "0" ? primaryProducts.ToList() : new List<EditPrimaryProduct>(),
                IsPlanStartOnEditable = (saleState == SaleStateType.PrepareSale),
                SaleStateStr = this.commonService.GetSysParamValue(GroupIdType.SaleState, saleState)
            };

            return result;
        }

        /// <summary>
        /// 商品管理設定-明細頁-查詢明細 API
        /// </summary>
        /// <param name="req">EditReq</param>
        /// <returns>ProductDetailResp</returns>
        public ProductDetailResp Detail(EditReq req)
        {
            //保險公司中文
            var company = this.companyProfileRepo.Queryable()
            .Where(o => o.CompanyCode == req.CompanyCode).FirstOrDefault()?.CompanyShortName;

            var productReq = this.mapper.Map<ProductReq>(req);
            // 查詢 商品主檔 ,key CompanyCode,PlanCode,PlanVer,PaymentTerm
            var p = this.GetPlanMaster(productReq);

            if (p == null) throw new BusinessException("商品主檔查無資料");

            var c = this.planCaseSourceRepo.Queryable().
            Where(o => o.CompanyCode == req.CompanyCode &&
            o.PlanCode == req.PlanCode &&
            o.PlanVer == req.PlanVer &&
            o.PaymentTerm == req.PaymentTerm &&
            o.PlanType == req.PlanType &&
            o.BenefitTerm == req.BenefitTerm).ToList();

            var primarys = from primary in this.insuredMappingRepo.Queryable()
                           join product in this.planMasterRepo.Queryable()
                           on new
                           {
                               CompanyCode = primary.CompanyCode,
                               PlanCode = primary.PrimaryPlanCode,
                               PlanVer = primary.PrimaryPlanVer,
                               PaymentTerm = primary.PrimaryPaymentTerm,
                               PlanType = primary.PlanType,
                               BenefitTerm = primary.PrimaryBenefitTerm
                           } equals new
                           {
                               CompanyCode = product.CompanyCode,
                               PlanCode = product.PlanCode,
                               PlanVer = product.PlanVer,
                               PaymentTerm = product.PaymentTerm,
                               PlanType = product.PlanType,
                               BenefitTerm = product.BenefitTerm
                           }
                           where (primary.CompanyCode == req.CompanyCode
                           && primary.RidePlanCode == req.PlanCode
                           && primary.RidePlanVer == req.PlanVer
                           && primary.RidePaymentTerm == req.PaymentTerm
                           && primary.PlanType == req.PlanType
                           && primary.RideBenefitTerm == req.BenefitTerm)
                           select $"{primary.PrimaryPlanCode} {product.PlanShortName}";

            var planAssuredItems = this.planAssuredRepo.Queryable()
                .Where(o => o.CompanyCode == req.CompanyCode &&
                o.PlanCode == req.PlanCode &&
                o.PlanVer == req.PlanVer &&
                o.PaymentTerm == req.PaymentTerm &&
                o.PlanType == req.PlanType &&
                o.BenefitTerm == req.BenefitTerm)
                .Select(o => new PlanAssuredItem()
                {
                    ItemCode = o.ItemCode,
                    ItemAbbreviation = o.ItemAbbreviation,
                    Assured = o.Assured,
                    ReferAssured = o.ReferAssured.HasValue ? Convert.ToDecimal(o.ReferAssured.Value.ToString("0.##")) : o.ReferAssured,
                    Multiples = o.Multiples.HasValue ? Convert.ToDecimal(o.Multiples.Value.ToString("0.##")) : o.Multiples
                }).ToList();

            var result = new ProductDetailResp()
            {
                CompanyStr = p.CompanyCode + " " + company,
                PlanSale = p.PlanStartOn.ToString("yyyy/MM/dd") + " 至 " + p.PlanEndOn.ToString("yyyy/MM/dd"),
                PlanCode = p.PlanCode,
                PlanVer = p.PlanVer,
                PlanShortName = p.PlanShortName,
                PlanFullName = p.PlanFullName,
                PlanTypeStr = this.commonService.GetSysParamValue("PlanType", p.PlanType),
                PlanCategoryStr = this.commonService.GetSysParamValue("PlanCategory", p.PlanCategory),
                PrimaryRiderIndStr = this.commonService.GetSysParamValue("PrimaryRiderInd", p.PrimaryRiderInd),
                CurrencyStr = p.Currency + " " + this.commonService.GetSysParamValue("Currency", p.Currency),
                BenefitTermStr = this.commonService.GetSysParamValue("BenefitTermType", p.BenefitTermType) + " " + p.BenefitTerm + " " + "年",
                PaymentTermStr = this.commonService.GetSysParamValue("PaymentTermType", p.PaymentTermType) + " " + p.PaymentTerm + " " + "年",
                PremiumMethod = p.PremiumMethod,
                InsuredAge = p.InsuredAgeS + " 歲(含) 至 " + p.InsuredAgeE + " 歲(含)",
                CaseSource = c.Select(o => o.CaseSource).ToList(),
                PrimaryProductsStr = primarys.ToList() ?? new List<string>(),
                Abbreviation = p.Abbreviation,
                PlanAssuredItems = planAssuredItems,
            };
            return result;
        }

        /// <summary>
        /// 商品管理設定-明細頁-覆核查詢明細API
        /// </summary>
        /// <returns></returns>
        public ProductDetailResp ApproveDetail(ApproveDetailReq req)
        {
            //保險公司中文
            var company = this.companyProfileRepo.Queryable()
            .Where(o => o.CompanyCode == req.CompanyCode).FirstOrDefault()?.CompanyShortName;

            var p = this.approvePlanMasterRepo.Queryable()
                .Where(o => o.CompanyCode == req.CompanyCode && o.ApproveRecordGid == req.ApproveRecordGid).FirstOrDefault();

            var c = this.approvePlanCaseSourceRepo.Queryable()
                .Where(o => o.CompanyCode == req.CompanyCode && o.ApproveRecordGid == req.ApproveRecordGid).ToList();

            var insuredMappings = this.approveInsuredMappingRepo.Queryable()
                .Where(o => o.CompanyCode == req.CompanyCode && o.ApproveRecordGid == req.ApproveRecordGid).ToList();
            var primarys = from primary in this.approveInsuredMappingRepo.Queryable()
                           join product in this.planMasterRepo.Queryable()
                           on new
                           {
                               CompanyCode = primary.CompanyCode,
                               PlanCode = primary.PrimaryPlanCode,
                               PlanVer = primary.PrimaryPlanVer,
                               PaymentTerm = primary.PrimaryPaymentTerm,
                               PlanType = primary.PlanType,
                               BenefitTerm = primary.PrimaryBenefitTerm
                           } equals new
                           {
                               CompanyCode = product.CompanyCode,
                               PlanCode = product.PlanCode,
                               PlanVer = product.PlanVer,
                               PaymentTerm = product.PaymentTerm,
                               PlanType = product.PlanType,
                               BenefitTerm = product.BenefitTerm
                           }
                           where (primary.CompanyCode == req.CompanyCode
                           && primary.ApproveRecordGid == req.ApproveRecordGid)
                           select $"{primary.PrimaryPlanCode} {product.PlanShortName}";

            var planAssureds = this.approvePlanAssuredRepo.Queryable()
                .Where(o => o.CompanyCode == req.CompanyCode && o.ApproveRecordGid == req.ApproveRecordGid)
                .Select(o => new PlanAssuredItem()
                {
                    ItemCode = o.ItemCode,
                    ItemAbbreviation = o.ItemAbbreviation,
                    Assured = o.Assured,
                    ReferAssured = o.ReferAssured.HasValue ? Convert.ToDecimal(o.ReferAssured.Value.ToString("0.##")) : o.ReferAssured,
                    Multiples = o.Multiples.HasValue ? Convert.ToDecimal(o.Multiples.Value.ToString("0.##")) : o.Multiples
                }).ToList();

            var result = new ProductDetailResp()
            {
                CompanyStr = p.CompanyCode + " " + company,
                PlanSale = p.PlanStartOn.ToString("yyyy/MM/dd") + " 至 " + p.PlanEndOn.ToString("yyyy/MM/dd"),
                PlanCode = p.PlanCode,
                PlanVer = p.PlanVer,
                PlanShortName = p.PlanShortName,
                PlanFullName = p.PlanFullName,
                PlanTypeStr = this.commonService.GetSysParamValue(GroupIdType.PlanType, p.PlanType),
                PlanCategoryStr = this.commonService.GetSysParamValue(GroupIdType.PlanCategory, p.PlanCategory),
                PrimaryRiderIndStr = this.commonService.GetSysParamValue(GroupIdType.PrimaryRiderInd, p.PrimaryRiderInd),
                CurrencyStr = p.Currency + " " + this.commonService.GetSysParamValue(GroupIdType.Currency, p.Currency),
                BenefitTermStr = this.commonService.GetSysParamValue(GroupIdType.BenefitTermType, p.BenefitTermType) + " " + p.BenefitTerm + " " + "年",
                PaymentTermStr = this.commonService.GetSysParamValue(GroupIdType.PaymentTermType, p.PaymentTermType) + " " + p.PaymentTerm + " " + "年",
                PremiumMethod = p.PremiumMethod,
                InsuredAge = p.InsuredAgeS + " 歲(含) 至 " + p.InsuredAgeE + " 歲(含)",
                CaseSource = c.Select(o => o.CaseSource).ToList(),
                PrimaryProductsStr = primarys.ToList() ?? new List<string>(),
                Abbreviation = p.Abbreviation,
                PlanAssuredItems = planAssureds
            };

            return result;
        }

        /// <summary>
        /// 存覆核項目的key = 公司代碼_商品代碼_商品版本_繳費年期_商品險別_繳費年期
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        private string GetProductApproveKey(ProductReq product)
        {
            return $"{product.CompanyCode}_{product.PlanCode}_{product.PlanVer}_{product.PaymentTerm}_{product.PlanType}_{product.BenefitTerm}";
        }

        /// <summary>
        /// 覆核記錄檔是否有相關商品
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        private bool IsApproveRecordExist(ProductReq product)
        {
            var keyStartWith = $"{product.CompanyCode}_{product.PlanCode}";// 保險公司代碼 + 商品代碼
            var keyEndWith = $"{product.PaymentTerm}_{product.PlanType}_{product.BenefitTerm}";// 繳費年期 + 商品險別 + 保障年期

            //檢查-待覆核清單有此商品的資料
            //條件『待覆核主檔』：保險公司代碼 + 商品代碼 + 繳費年期 + 商品險別 + 保障年期=> 尚未覆核 =>不看版本，與異動類型
            // 商品 ApproveKey: ComponyCode_PlanCode_PlanVer_PaymentTerm_PlanType_BenefitTerm 組合, 比對開頭2欄 與 結尾3欄(排除 版本欄位)
            var records = this.approveRecordRepo.Queryable()
                .Where(o =>
                o.FunctionCode == FunctionCodeType.F1401 &&
                o.ApproveStatus == ApproveStatusType.WaitApprove &&
                o.ApproveKey.StartsWith(keyStartWith) &&
                o.ApproveKey.EndsWith(keyEndWith));


            return records.Any();
        }

        /// <summary>
        /// 商品是否被其他非主約商品依附
        /// </summary>
        /// <param name="product"></param>
        /// <returns>bool</returns>
        private bool IsInsuredMappingExist(ProductReq product)
        {
            return this.insuredMappingRepo.Queryable()
                .Where(o =>
                o.PlanType == product.PlanType &&
                o.CompanyCode == product.CompanyCode &&
                o.PrimaryPlanCode == product.PlanCode &&
                o.PrimaryPlanVer == product.PlanVer &&
                o.PrimaryPaymentTerm == product.PaymentTerm &&
                o.PrimaryBenefitTerm == product.BenefitTerm).Any();
        }

        /// <summary>
        /// 方案是否有使用相關商品
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        private bool IsCampaignExist(ProductReq product)
        {
            // 方案主檔 , 專案主檔
            var campaignMasterList = from cm in this.campaignMasterRepo.Queryable()
                                     join pm in this.productMaster.Queryable() on cm.ProductId equals pm.ProductId into pml
                                     from pm in pml.DefaultIfEmpty()
                                     select new { cm, pm };

            //  依 「商品公司」 與「 險種類型」 查詢方案資料 
            campaignMasterList = campaignMasterList
                .Where(o => o.cm.CompanyCode == product.CompanyCode && o.pm.PlanType == product.PlanType);

            foreach (var c in campaignMasterList)
            {
                var campaignPlanMappingiList = this.campaignPlanMappingRepo.Queryable()
                    .Where(o =>
                    o.CompanyCode == c.cm.CompanyCode//保險公司
                    && o.ProjectCode == c.cm.ProjectCode //方案代碼
                    && o.PlanCode == product.PlanCode //商品
                    && o.PaymentTerm == product.PaymentTerm //繳費年期
                    && o.BenefitTerm == product.BenefitTerm //保障年期
                    ).ToList();

                if (campaignPlanMappingiList.Any())
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 主附約關係覆核檔是否有相關商品
        /// </summary>
        /// <param name="product"></param>
        /// <returns>bool</returns>
        private bool IsApproveInsuredMappingExist(ProductReq product)
        {
            var productList = from approveRecord in this.approveRecordRepo.Queryable()
                              join approveIM in this.approveInsuredMappingRepo.Queryable()
                              on approveRecord.ApproveRecordGid equals approveIM.ApproveRecordGid
                              where approveRecord.FunctionCode == FunctionCodeType.F1401
                              && approveRecord.ApproveStatus == ApproveStatusType.WaitApprove
                              && approveIM.PlanType == product.PlanType
                              && approveIM.CompanyCode == product.CompanyCode
                              && approveIM.PrimaryPlanCode == product.PlanCode
                              && approveIM.PrimaryPlanVer == product.PlanVer
                              && approveIM.PrimaryPaymentTerm == product.PaymentTerm
                              && approveIM.PrimaryBenefitTerm == product.BenefitTerm
                              select approveIM;
            return productList.Any();
        }

        /// <summary>
        /// 方案覆核是否有使用相關商品
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        private bool IsApproveCampaignExist(ProductReq product)
        {
            var approveRecordList = this.approveRecordRepo.Queryable()
                .Where(o => o.FunctionCode == FunctionCodeType.F1501 &&
                o.ApproveStatus == ApproveStatusType.WaitApprove);

            // 交集 險種類型結果
            var approveRecords = from ar in approveRecordList
                                 join apm in this.approveProductMasterRepo.Queryable() on ar.ApproveRecordGid equals apm.ApproveRecordGid into apml
                                 from apm in apml.DefaultIfEmpty()
                                 select new { ar, apm };

            // 依照 險種類型查詢 「覆核主檔」
            approveRecords = approveRecords.Where(o => o.apm.PlanType == product.PlanType);

            foreach (var approveRecord in approveRecords)
            {
                var approveCampaignPlanMappingiList = this.approveCampaignPlanMappingRepo.Queryable()
                    .Where(o =>
                    o.ApproveRecordGid == approveRecord.ar.ApproveRecordGid
                    && o.CompanyCode == product.CompanyCode //保險公司
                    && o.PlanCode == product.PlanCode //商品
                    && o.PaymentTerm == product.PaymentTerm //繳費年期
                    && o.BenefitTerm == product.BenefitTerm //保障年期
                    ).ToList();

                if (approveCampaignPlanMappingiList.Any())
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 佣金是否有使用相關商品
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        //private bool IsCommissionExist(ProductReq product)
        //{
        //    //佣金有設定此商品 => 不可刪除
        //    var commissionSettingList = this.commissionSettingRepo.Queryable()
        //    .Where(o => o.CompanyCode == product.CompanyCode &&
        //    o.PlanCode == product.PlanCode &&
        //    o.PaymentTerm == product.PaymentTerm &&
        //    o.PlanType == product.PlanType &&
        //    o.BenefitTerm == product.BenefitTerm)
        //    .ToList();

        //    if (commissionSettingList.Any())
        //    {
        //        return true;
        //    }

        //    return false;
        //}

        /// <summary>
        /// 佣金覆核是否有使用相關商品
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        private bool IsApproveCommissionExist(ProductReq product)
        {

            // 佣金設定 AprroveKey
            var key = $"{product.CompanyCode}_{product.PlanCode}_{product.PaymentTerm}_{product.PlanType}_{product.BenefitTerm}";

            var approveRecordList = this.approveRecordRepo.Queryable()
            .Where(o => o.FunctionCode == FunctionCodeType.F1901 &&
            o.ApproveStatus == ApproveStatusType.WaitApprove &&
            o.ApproveKey == key)
            .ToList();

            if (approveRecordList.Any())
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 電訪是否有使用相關商品
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        //private bool IsOutBoundExist(ProductReq product)
        //{
        //    var outBoundCompanySettingList = this.outBoundCompanySettingRepo.Queryable()
        //    .Where(o => o.CompanyCode == product.CompanyCode &&
        //    o.PlanCode == product.PlanCode &&
        //    o.PlanType == product.PlanType)
        //    .ToList();

        //    if (outBoundCompanySettingList.Any())
        //    {
        //        return true;
        //    }

        //    return false;
        //}

        /// <summary>
        /// 電訪覆核是否有使用相關商品
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        private bool IsApproveOutBoundExist(ProductReq product)
        {
            // 電訪覆核 AprroveKey
            var key = $"{product.CompanyCode}_{product.PlanCode}_{product.PlanType}";

            var approveRecordList = this.approveRecordRepo.Queryable()
            .Where(o => o.FunctionCode == FunctionCodeType.F1802 &&
            o.ApproveStatus == ApproveStatusType.WaitApprove &&
            o.ApproveKey == key)
            .ToList();

            if (approveRecordList.Any())
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 依PK(CompanyCode,PlanCode,PlanVer,PaymentTerm,PlanType,BenefitTerm) 取得 PlanMaster 資料
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        private PlanMaster GetPlanMaster(ProductReq req)
        {
            // 查詢 商品主檔 ,key CompanyCode,PlanCode,PlanVer,PaymentTerm,PlanType,BenefitTerm
            var planMaster = this.planMasterRepo.Queryable()
               .Where(o =>
               o.CompanyCode == req.CompanyCode &&
               o.PlanCode == req.PlanCode &&
               o.PlanVer == req.PlanVer &&
               o.PaymentTerm == req.PaymentTerm &&
               o.PlanType == req.PlanType &&
               o.BenefitTerm == req.BenefitTerm)
               .FirstOrDefault();

            return planMaster;
        }

        /// <summary>
        /// 商品險別V	汽車險 M 機車險  對應 險種類型 01 : 汽車 02 : 機車
        /// </summary>
        /// <param name="planType"></param>
        /// <returns></returns>
        public string GetInsTypeByPlanType(string planType)
        {
            //商品險別V	汽車險 M 機車險  對應 險種類型 01 : 汽車 02 : 機車
            string insType = string.Empty;

            switch (planType)
            {
                case ProductType.Car_2:
                    insType = InsType.Car;
                    break;
                case ProductType.Moto:
                    insType = InsType.Moto;
                    break;
                default:
                    throw new BusinessException("商品險別查無對應險種類型");
            }

            return insType;
        }
    }
}
